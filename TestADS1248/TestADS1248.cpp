#include <SPI.h>
#include "MyADS1248.h"

int START1 = 9;
int CS1 = 10;
int DRDY1 = 2;


size_t dToStr(char *ch, double val) {
	int i = (int) val;
	val = val - (double) i;
	val = val * 100;
	int f = (int) val;
	return sprintf(ch, "%d.%02d", i, f);
}
class ADSListener: public ADSBoardListener {
public:
	void valueUpdated(ADSBoardEvent val) {

		char buffer[40];
		size_t n = sprintf(buffer, "<1%d ", val.getType());
		n = n + dToStr(buffer + n, val.getVal());
		buffer[n++]=' ';
		n=n+dToStr(buffer+n,val.getMin());
		buffer[n++]=' ';
		n=n+dToStr(buffer+n,val.getMax());
		buffer[n++] = '>';
		buffer[n] = '\0';
		Serial.println(buffer);
#ifdef SERIAL_DEBUG_ENABLE
		char b[20];
		sprintf(b, "Temp Reading Type: [%d] Value :", val.getType());
		Serial.print(b);
		Serial.println(val.getVal(), 2);
#endif


	}
};
MyADS1248 *ads;
ADSListener adsListener;
unsigned long now;
void setup() {
	Serial.begin(115200);
	Serial.println("Setup begins");
	ads =new MyADS1248(-1);
	ads->begin(CS1, START1, DRDY1);

	ads->setListener(&adsListener);
	Serial.println("Setup ends");
	now =millis();
}

void loop() {
	if(millis()-now>16000){
		ads->requestBoardTemp();
		now=millis();
	}
	ads->doReading();
	delay(10);
}
