// Do not remove the include below
#include "PT1000.h"

//----------------------------------------------------------------------------
// RTD Module Example Sketch
// Last revision 30 November 2009
// Licence: GNU GPL
// By Trystan Lea
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Load RTD library and create new instance
//----------------------------------------------------------------------------

RTDModule rtd;

//----------------------------------------------------------------------------
// Temperature variables
//----------------------------------------------------------------------------
double COL[4];
//----------------------------------------------------------------------------
// Setup
//----------------------------------------------------------------------------

OneWire *_ds;
#define DS18S20_Pin 7

// Set these to whichever pins you connected the SHT1x to:
#define dataPin 9
#define clockPin 10

// Set this number larger to slow down communication with the SHT1x, which may
// be necessary if the wires between the Arduino and the SHT1x are long:
#define clockPulseWidth 1

// The next lines are fine if you're using a 5V Arduino. If you're using a 3.3V
// Arduino (such at the Due), comment the next line and uncomment the one after:

#define supplyVoltage sht1xalt::VOLTAGE_5V
//#define supplyVoltage sht1xalt::VOLTAGE_3V5

// If you want to report temperature units in Fahrenheit instead of Celcius,
// comment the next line and uncomment the one after:
#define temperatureUnits sht1xalt::UNITS_CELCIUS
//#define temperatureUnits sht1xalt::UNITS_FAHRENHEIT

sht1xalt::Sensor sensor( dataPin, clockPin, clockPulseWidth,
supplyVoltage, temperatureUnits );

void setup() {

	Serial.begin(9600);
	Serial.println("PT1000 deneme basladi");
	rtd.setPins(4, 5, A2); //RTD PT1000 pins 4,5 are digital pins for multiplexer, 2 is analog input

	//rtd.calibration( input number, scaler, offset )
	rtd.calibration(0, 1,1);   //INPUT 0 pin 12
	rtd.calibration(1, 1,1);
	rtd.calibration(2, 0.065989, -9.280681);
	rtd.calibration(3, 0.076737, -8.718646);
	analogReference(INTERNAL1V1); //Set reference to 1.1V ready for RTD measurements
	delay(3000);
	for (int i = 0; i < 4; i++) {
		COL[i] = rtd.getTemperatureSmooth(i, 0, 0);
	}
	_ds = new OneWire(DS18S20_Pin);

	sensor.configureConnection();
	// Reset the SHT1x, in case the Arduino was reset during communication:
	sensor.softReset();
	rtd.getTemperature(2);

}

//----------------------------------------------------------------------------
// Main loop
//----------------------------------------------------------------------------
double AVG[4];
unsigned long dsWait = 0;
unsigned long shtWait = 0;
float getTemp();
#define CALIBRATE_ITEM 2
void loop() {
#ifdef CALIBRATE_ITEM
//	double val =rtd.getTemperatureSmooth(CALIBRATE_ITEM,0,0);
//	COL[CALIBRATE_ITEM] = digitalLowPass(COL[CALIBRATE_ITEM], val,
//				0.90);
//	Serial.print(val);
//	Serial.print("---DLP:");
//	Serial.print(COL[CALIBRATE_ITEM]);
	Serial.print("----ADC:");
	Serial.println(rtd.getADC());


	delay(100);
	return;
#endif
	//Get temperatures and pass through digital low pass filter
	if (millis() > dsWait + 2000) {
		Serial.print("Temp :");
		Serial.println(getTemp());
		dsWait = millis();
	}
	if (millis() > shtWait + 8000) {
		shtWait=millis();
		float temp;
		float rh;
		sht1xalt::error_t err;

		delay(2000);

		err = sensor.measure(temp, rh);
		if (err) {
			switch (err) {
			case sht1xalt::ERROR_NO_ACK:
				Serial.println("SHT1x failed to acknowledge a command!");
				break;
			case sht1xalt::ERROR_MEASUREMENT_TIMEOUT:
				Serial.println("SHT1x failed to produce a measurement!");
				break;
			}
			sensor.softReset();
		} else {

			Serial.print("The temperature is ");
			Serial.print(temp);
			if (sensor.getUnits() == sht1xalt::UNITS_CELCIUS) {
				Serial.print(" Celcius");
			} else {
				Serial.print(" Fahrenheit");
			}
			Serial.print(", the relative humidity is ");
			Serial.print(rh);
			Serial.println("%");
		}
	}
	for (int j = 0; j < 100; j++)
		for (int i = 0; i < 4; i++) {
			COL[i] = digitalLowPass(COL[i], rtd.getTemperatureSmooth(i, 0, 0),
					0.90);
			AVG[i] += COL[i];
		}

	Serial.print(" ADC:\t");
	for (int i = 0; i < 4; i++) {
		AVG[i] = AVG[i] / 100;
		Serial.print(AVG[i]);
		Serial.print("\t");
		AVG[i] = 0;
	}
	Serial.println("");
	delay(500);
	//Print temperatures out to serial

}
//----------------------------------------------------------------------------

//Digital low pass filter
double digitalLowPass(double last_smoothed, double new_value,
		double filterVal) {
	double smoothed = (new_value * (1 - filterVal))
			+ (last_smoothed * filterVal);
	return smoothed;
}

float getTemp() {
	byte data[12];
	byte addr[8];
	if (!_ds->search(addr)) {
		//no more sensors on chain, reset search
		_ds->reset_search();
		return -100;
	}

	if (OneWire::crc8(addr, 7) != addr[7]) {
		Serial.println("CRC is not valid!");
		return -100;
	}

	if (addr[0] != 0x10 && addr[0] != 0x28) {
		Serial.print("Device is not recognized");
		return -100;
	}

	_ds->reset();
	_ds->select(addr);
	_ds->write(0x44, 1); // start conversion, with parasite power on at the end
	byte present = _ds->reset();
	_ds->select(addr);
	_ds->write(0xBE); // Read Scratchpad

	for (int i = 0; i < 9; i++) { // we need 9 bytes
		data[i] = _ds->read();
	}

	_ds->reset_search();

	byte MSB = data[1];
	byte LSB = data[0];

	float tempRead = ((MSB << 8) | LSB); //using two's compliment
	float TemperatureSum = tempRead / 16;

	return TemperatureSum;
}
