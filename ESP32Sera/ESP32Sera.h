/*
 * ESP32Sera.h
 *
 *  Created on: 25 Jul 2020
 *      Author: ISIK
 */

#ifndef ESP32SERA_H_
#define ESP32SERA_H_



#include <Arduino.h>
#include"MyPt1000.h"
//#include"NokiaMonitor.h"
#include"NextionMonitor.h"
#include"ESPWifi.h"
#include"ActionCode.h"
#include"MyDebug.h"
#include"SoftwareSerial.h"
//add your function definitions for the project SeraProjectWithProMini3_3 here


char* nexToken(char *str, uint8_t *lastIndex, uint8_t *len);
void doActions();
size_t dToStr(char *ch, double val);
//Do not add code below this line



#endif /* ARDUINOPROMINISERA_H_ */



#endif /* ESP32SERA_H_ */
