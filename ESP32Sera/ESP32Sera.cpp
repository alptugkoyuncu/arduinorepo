#include <Arduino.h>

// Do not remove the include below
#include "ESP32Sera.h"
//#define SERIAL_DEBUG_ENABLE

MyPt1000 *pt;
Monitor *monitor;
SeraWifi *wifi;

ConnectionRecord rec;

//#define MEGA
#define PRO_MINI
#ifdef MEGA

uint8_t ADS1248_START_PIN = 3;
uint8_t ADS1248_CS_PIN = 53;
uint8_t ADS1248_DRDY_PIN = 2;
int8_t ADS1248_RESET_PIN = A1;
int8_t ESP8266_RESET_PIN = 4;
#define WIFI_SERIAL Serial2
#endif

#define WIFI_SERIAL Serial
#ifdef PRO_MINI
SoftwareSerial HMISerial(8, 7);
uint8_t ADS1248_START_PIN = A0;
uint8_t ADS1248_CS_PIN = 10;
uint8_t ADS1248_DRDY_PIN = 2;
int8_t ADS1248_RESET_PIN = A1;
int8_t ESP8266_RESET_PIN = 9;
#endif
class ADSListener: public ADSBoardListener {
public:
	void valueUpdated(ADSBoardEvent val) {
		DataPackage sp;
		sp.init();
		char buffer[40];
		size_t n = sprintf(buffer, "<1%d ", val.getType());
		n = n + dToStr(buffer + n, val.getVal());
		buffer[n++] = ' ';
		n = n + dToStr(buffer + n, val.getMin());
		buffer[n++] = ' ';
		n = n + dToStr(buffer + n, val.getMax());
		buffer[n++] = '>';
		buffer[n] = '\0';
		wifi->println(buffer);

		d_print(F("Temp reading Type:"));
		d_print(val.getType());
		d_print(" Value :");
		d_println(val.getVal());
		switch (val.getType()) {
		case ADSBoardSensorType::temp1Updated:
			monitor->setTemp1(val.getVal());
			break;
		case ADSBoardSensorType::temp2Updated:
			monitor->setTemp2(val.getVal());
			break;
		case ADSBoardSensorType::temp3Updated:
			monitor->setTemp3(val.getVal());
			break;
		case ADSBoardSensorType::boardTempUpdated:
			monitor->setBoardTemp(val.getVal());
			break;
		default:
			d_println(F("Unknown Type"))
			;
			break;
		}
	}
};

ADSListener *ptListener;
DataPackage pck;

//The setup function is called once at startup of the sketch
unsigned long now;

void setup() {

#if AK_DEBUG
	Serial.begin(115200);
#endif

	d_println("Started");
	delay(100);
	d_print("Mem :");
	d_println(freeMemory());

	delay(100);
	rec.loadConnectionRecord();

	//	monitor =  new NokiaMonitor(3, 4, 5);
	monitor = new NextionMonitor();
	monitor->begin();
	wifi = new ESPWifi(&WIFI_SERIAL, 38400, ESP8266_RESET_PIN);
	wifi->begin();
	wifi->connect();
	monitor->gotoPage(MonitorPages::MAIN_SCREEN);

	monitor->setConectionSettings(rec);
	ptListener = new ADSListener();
	pt = new MyPt1000(ADS1248_RESET_PIN);
	pt->begin(ADS1248_CS_PIN, ADS1248_START_PIN, ADS1248_DRDY_PIN);

	pt->setListener(ptListener);
// Add your initialization code here
	now = millis();
//	pt->setNumberOfPTReading(1);

	d_print("Mem :");
	d_println(freeMemory());
	delay(100);
	d_println(F("Setup ends"));
	delay(1000);
}

// The loop function is called in an endless loop
#define BOARD_TEMP_REQUEST_TIME 30000
unsigned long index = 0;
unsigned long dateLongPart = 0; //date int parti millis de sakli millis() now dan kucuk olunca millis ba
void loop() {
//	Serial.println(index++);
	unsigned long dt = millis();
	if (millis() - now > BOARD_TEMP_REQUEST_TIME) {
		pt->requestBoardTemp();
		now = millis();
		d_print("Mem :");
		d_println(freeMemory());
	}

	wifi->readAll(&pck);
	if (pck.isFinished) {
//		Serial.print("pck L");
//		Serial.println((char *)(pck.data));
		doActions();
		pck.init();
	}

	monitor->listenKeyboard();

	pt->doReading();
	delay(5);
//Add your repeated code here
}
void doActions() {
#define LEN_SENDBUFFER 50
	char sendBuffer[LEN_SENDBUFFER];
	memset(sendBuffer, 0, LEN_SENDBUFFER);
	uint8_t actCode = 0, len = 0;
	uint8_t nextIndex = 0;
//	Serial.print(F("Buffer :"));
//	int i=0;
//	while(pck.data[i]!=NULL){
//		Serial.print("[");
//		Serial.print((int)(pck.data[i]));
//		Serial.print(":");
//		Serial.print((char)(pck.data[i++]));
//		Serial.print("],");
//	}
//	Serial.println(i);
	char *temp = nexToken((char*)pck.data, &nextIndex, &len);
	if (temp != NULL) {
		actCode = atoi(temp);
		free(temp);
		temp = NULL;
	}
	switch (actCode) {
	case AC_SEND_TEMP0:
		break;
	case AC_SEND_TEMP1:
		break;
	case AC_SEND_TEMP2:
		break;
	case AC_SEND_TEMP3:
		break;
	case AC_SEND_BOARD_LAST_RESET_TIME:
		sprintf(sendBuffer, "<%d %lu>", actCode, millis());
		wifi->println(sendBuffer);
		break;
	case AC_BOARD_FREE_MEMORY:
		sprintf(sendBuffer,"<%d %d>",actCode,freeMemory());
		wifi->println(sendBuffer);
		break;
	case AC_REQUEST_ID:
		break;
	case AC_SEND_WIFI_SSID:
		sprintf(sendBuffer, "<%d %s>", actCode, rec.getSSID());
		wifi->println(sendBuffer);
		break;
	case AC_SEND_PASS:
		sprintf(sendBuffer, "<%d %s>", actCode, rec.getPassword());
		wifi->println(sendBuffer);
		break;
	case AC_SEND_IP:
		sprintf(sendBuffer, "<%d %s>", actCode, rec.getIp());
		wifi->println(sendBuffer);
		break;
	case AC_SEND_PORT:
		sprintf(sendBuffer, "<%d %d>", actCode, rec.getPort());
		wifi->println(sendBuffer);
		break;
	case AC_SEND_ID:
		sprintf(sendBuffer, "<%d %d>", actCode, rec.getId());
		wifi->println(sendBuffer);
		break;
	case AC_ESP_STARTED:
		break;
	case AC_RESET_ESP:
		break;
	case AC_WIFI_CONNECTED:
		break;
	case AC_WIFI_DISCONNECTED:
		break;
	case AC_WIFI_CONNECTION_FAILED:
		break;
	case AC_SERVER_CONNECTED:
		break;
	case AC_SERVER_DISCONNECTED:
		break;
	case AC_SERVER_CONNECTION_FAILED:
		break;
	case AC_ID_ACCEPTED:
		break;
	case AC_ID_REFUSED:
		break;
	default:
		sprintf(sendBuffer, "<%d %d>", AC_INVALID_CODE, actCode);
		wifi->println(sendBuffer);
		break;
	}
	d_print(F("Send Buffer :"));
	d_println(sendBuffer);

}
size_t dToStr(char *ch, double val) {
	int i = (int) val;
	val = val - (double) i;
	val = val * 100;
	int f = (int) val;
	return sprintf(ch, "%d.%02d", i, f);
}
char* nexToken(char *str, uint8_t *lastIndex, uint8_t *len) {
	uint8_t bas = *lastIndex;
	//eliminate empty character and start index
	d_print(F("nex token baslangic :"));
	d_println(str + bas);
	for (; str[bas] != NULL; bas++) {
		if (str[bas] == ' ' || str[bas] == '\t' || str[bas] == '\r'
				|| str[bas] == '\n' || str[bas] == '<' || str[bas] == ':')
			continue;
		else
			break;
	}
	d_print(F("Baslangic index : "));
	d_println(bas);
	//bosluk veya satir sonuna gelinceye kadar tara
	uint8_t sonIndex = bas;
	for (; str[sonIndex] != NULL; sonIndex++) {
		if (str[sonIndex] == ' ' || str[sonIndex] == '\t'
				|| str[sonIndex] == '\r' || str[sonIndex] == '\n'
				|| str[sonIndex] == '>' || str[bas] == ':')
			break;
	}
	d_print(F("Son index : "));
	d_println(sonIndex);
	*lastIndex = sonIndex;
	if (sonIndex == bas) {
		*len = 0;
		return NULL;
	}
	*len = sonIndex - bas;
	char *tmp;
	tmp = (char*) malloc(*len + 1);
	memcpy(tmp, str + bas, *len);
	tmp[*len] = NULL;
	d_print(F("Token :"));
	d_println(tmp);
	d_println(F("nex token bitis"));
	return tmp;
}

