/*
 * SeraAcma.cpp
 *
 *  Created on: 24 Tem 2015
 *      Author: WIN7
 */

#include "SeraAcma.h"
bool h12, PM;
void SeraAcma::init(const uint16_t maximumAciklikMiktari,
		uint16_t aciklikMiktari, uint8_t acmaPini, uint8_t kapamaPini,
		DS3231 *Clock) {
	this->maximumAciklikMiktari = maximumAciklikMiktari;
	this->aciklikMiktari = aciklikMiktari;
	this->lastAciklikMiktari = aciklikMiktari;
	this->acmaPini = acmaPini;
	this->kapamaPini = kapamaPini;
	pinMode(acmaPini, OUTPUT);
	pinMode(kapamaPini, OUTPUT);
	digitalWrite(acmaPini, LOW);
	digitalWrite(kapamaPini, LOW);
	this->calismaDurumu = DURUYOR;
	this->timer = NULL;
	this->Clock = Clock;
}
SeraAcma::SeraAcma(const uint16_t maximumAciklikMiktari, uint8_t acmaPini,
		uint8_t kapamaPini, DS3231 *Clock) {
	// TODO Auto-generated constructor stub
	init(maximumAciklikMiktari, 0, acmaPini, kapamaPini, Clock);
}
SeraAcma::SeraAcma(const uint16_t maximumAciklikMiktari,
		uint16_t aciklikMiktari, uint8_t acmaPini, uint8_t kapamaPini,
		DS3231 *Clock) {
	init(maximumAciklikMiktari, aciklikMiktari, acmaPini, kapamaPini, Clock);
}
SeraAcma::~SeraAcma() {
	// TODO Auto-generated destructor stub
}
uint16_t SeraAcma::getIstenilenMiktar() {
	return this->istenilenMiktar;
}
uint8_t SeraAcma::calistir(uint16_t istenilenMiktar) {

	uint16_t aciklikMiktari = this->getAciklikMiktari(); //ayni zamanda calismaSuresini de gunceller

	if (istenilenMiktar < this->minimumAciklikMiktari)
		this->istenilenMiktar = this->minimumAciklikMiktari;
	else if (istenilenMiktar > this->maximumAciklikMiktari)
		this->istenilenMiktar = this->maximumAciklikMiktari;
	else
		this->istenilenMiktar = istenilenMiktar;
	if (aciklikMiktari == this->istenilenMiktar) {
		if (this->isWorking())
			this->durdur();
		return DURUYOR;
	}
	uint16_t calisilanSure =
			this->timer != NULL && this->timer->baslangic != NULL ?
					this->timer->getCalisilanSure(this->Clock) : 0;

	uint16_t calismaSuresi =
			aciklikMiktari < this->istenilenMiktar ?
					this->istenilenMiktar - aciklikMiktari :
					aciklikMiktari - this->istenilenMiktar;
	if (!this->isWorking()) {
		if (aciklikMiktari < this->istenilenMiktar) {
			this->ac(calismaSuresi);
		} else
			this->kapat(calismaSuresi);
	} else {
		if (((aciklikMiktari < this->istenilenMiktar)
				&& (this->getCalismaDurumu() == ACILIYOR))
				|| ((aciklikMiktari > this->istenilenMiktar)
						&& this->getCalismaDurumu() == KAPANIYOR)) {

			this->timer->beklenenCalismaSuresi = calismaSuresi + calisilanSure;

		} else {

			if (aciklikMiktari < this->istenilenMiktar) {
				this->durdur();//istenilen miktari degistirdigi icin if icin alinir
				delay(ACMA_KAPAMA_GECIS_SURESI);
				this->ac(calismaSuresi);
			} else{
				this->durdur();//istenilen miktari degistirdigi icin if icin alinir
				delay(ACMA_KAPAMA_GECIS_SURESI);
				this->kapat(calismaSuresi);
			}
		}
	}
	/*} else if (aciklikMiktari > istenilenMiktar) {
	 if (this->getCalismaDurumu() == ACILIYOR) {
	 this->durdur(true);
	 }
	 this->calismaSuresi = aciklikMiktari - istenilenMiktar;
	 this->kapat();
	 } else {
	 if (this->getCalismaDurumu() == KAPANIYOR) {
	 this->durdur(true);
	 }
	 this->calismaSuresi = istenilenMiktar - aciklikMiktari;
	 this->ac();
	 }*/
	return this->calismaDurumu;

}
// kapatma islemi suruyorsa calisma suresini guncelle
//duruyorsa kapatmata basla
bool SeraAcma::kapat(uint16_t calismaSuresi) {

	if (this->isWorking())
		return false;
	if (digitalRead(acmaPini) == HIGH) {
		digitalWrite(acmaPini, LOW);
		delay(ACMA_KAPAMA_GECIS_SURESI);
	}
	digitalWrite(kapamaPini, HIGH);

	this->lastAciklikMiktari = this->getAciklikMiktari();
	this->saatiAyarla(calismaSuresi);
	this->calismaDurumu = KAPANIYOR;

	char t[100];
//	int s =
			sprintf(t, "<%d:%d:%d>Kapanmaya basladi(%d. pin=%d)  : Aciklik Miktari : %d\tSure : %d",
			this->timer->baslangic->saat, this->timer->baslangic->dk,
			this->timer->baslangic->sn,kapamaPini,digitalRead(kapamaPini), this->lastAciklikMiktari,
			this->timer->beklenenCalismaSuresi);
//	t[s]=NULL;
	Serial.println(t);

	return true;
}
// acma islemi devam ediyorsa suresi guncelle
//duruyorsa acmaya basla
bool SeraAcma::ac(uint16_t calismaSuresi) {

	if (this->isWorking())
		return false;
	if (digitalRead(kapamaPini) == HIGH) {
		digitalWrite(kapamaPini, LOW);
		delay(ACMA_KAPAMA_GECIS_SURESI);
	}

	this->lastAciklikMiktari = this->getAciklikMiktari();
	this->saatiAyarla(calismaSuresi);

	digitalWrite(acmaPini, HIGH);


	this->calismaDurumu = ACILIYOR;

char t[100];
//	int s =
			sprintf(t, "<%d:%d:%d>Ac�lmaya basladi(%d. pin=%d) : Aciklik Miktari : %d\tSure : %d",
			this->timer->baslangic->saat, this->timer->baslangic->dk,
			this->timer->baslangic->sn,acmaPini,digitalRead(acmaPini), this->lastAciklikMiktari,
			this->timer->beklenenCalismaSuresi);
//	t[s]=NULL;
	Serial.println(t);

	return true;
}
// forced : true ise timer in bitis zamani yenilenir .
//
uint16_t SeraAcma::durdur() {
	if (this->calismaDurumu == DURUYOR)
		return 0;
	digitalWrite(kapamaPini, LOW);
	digitalWrite(acmaPini, LOW);
	bool a, b;
	this->timer->beklenenCalismaSuresi = this->timer->getCalisilanSure(
			this->Clock);
	this->setAciklikMiktari();
	this->calismaDurumu = DURUYOR;
	uint16_t aciklikMiktari = this->getAciklikMiktari();

	char t[100];
//	int s =
			sprintf(t, "<%d:%d:%d>Durdu : Aciklik Miktari : %d\tSure : %d",
			this->timer->baslangic->saat, this->timer->baslangic->dk,
			this->timer->baslangic->sn, aciklikMiktari,
			this->timer->beklenenCalismaSuresi);
//	t[s]=NULL;
			Serial.println(t);

	this->istenilenMiktar = aciklikMiktari;
	return this->timer->beklenenCalismaSuresi;
}
uint8_t SeraAcma::getCalismaDurumu() {
	return this->calismaDurumu;
}

void SeraAcma::setAciklikMiktari() {
	if (this->getCalismaDurumu() == ACILIYOR) {
		this->aciklikMiktari = this->lastAciklikMiktari
				+ this->timer->getCalisilanSure(this->Clock);
	}
	if (this->getCalismaDurumu() == KAPANIYOR) {
		this->aciklikMiktari = this->lastAciklikMiktari
				- this->timer->getCalisilanSure(this->Clock);
	}
	if (this->aciklikMiktari < this->minimumAciklikMiktari) {
		this->aciklikMiktari = this->minimumAciklikMiktari;

	}
	if (this->aciklikMiktari > this->maximumAciklikMiktari) {
		this->aciklikMiktari = this->maximumAciklikMiktari;

	}
}
//cagirildigi zamanki aciklik miktari dondurulur. Eger islem devam ediyorsa belirli bir sure sonra bu miktar degismis olacaktir
uint16_t SeraAcma::getAciklikMiktari() {
	if (this->getCalismaDurumu() != DURUYOR) {
		this->setAciklikMiktari();
	}
	return this->aciklikMiktari;
}
bool SeraAcma::stopIfEnough() {
	if (this->getCalismaDurumu() == DURUYOR)
		return false;
	uint16_t calisilanSure = this->timer->getCalisilanSure(this->Clock);
	this->setAciklikMiktari();
	if (this->aciklikMiktari < this->minimumAciklikMiktari) {
		this->aciklikMiktari = this->minimumAciklikMiktari;
		this->durdur();
		return true;
	}
	if (this->aciklikMiktari > this->maximumAciklikMiktari) {
		this->aciklikMiktari = this->maximumAciklikMiktari;
		this->durdur();
		return true;
	}
	if (calisilanSure >= this->timer->beklenenCalismaSuresi) {
		this->durdur();
		return true;
	}
	return false;
}
void SeraAcma::saatiAyarla(uint16_t sure) {
	bool a, b;
	if (this->timer != NULL) {
		if (this->timer->baslangic != NULL) {
			free(this->timer->baslangic);
			this->timer->baslangic = NULL;
		}

		free(timer);
		timer = NULL;
	}
	this->timer = new DailyTimer(this->Clock, sure);
}
uint16_t SeraAcma::getMaximumAciklikMiktari() {
	return this->maximumAciklikMiktari;
}
uint16_t SeraAcma::getMinimumAciklikMiktari() {
	return this->minimumAciklikMiktari;
}
bool SeraAcma::isWorking() {
	return this->getCalismaDurumu() != DURUYOR;
}
char *SeraAcma::toString(int16_t &size) {
	char buff[256];
	size = sprintf(buff, "%d %d %d", (uint16_t) this->getAciklikMiktari(),
			(uint16_t) this->getIstenilenMiktar(),
			(uint16_t) this->getCalismaDurumu());
	buff[size] = NULL;
	return buff;
}
