################################################################################
# Automatically-generated file. Do not edit!
################################################################################

INO_SRCS := 
ASM_SRCS := 
O_UPPER_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
PDE_SRCS := 
CC_SRCS := 
AR_SRCS := 
C_SRCS := 
C_UPPER_DEPS := 
PDE_DEPS := 
C_DEPS := 
AR := 
CC_DEPS := 
AR_OBJ := 
C++_DEPS := 
LINK_OBJ := 
CXX_DEPS := 
ASM_DEPS := 
HEX := 
INO_DEPS := 
SIZEDUMMY := 
S_UPPER_DEPS := 
ELF := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
. \
core\core \
libraries\AT24C32 \
libraries\AT24C32\Examples\AT24C32_TEST \
libraries\DS3231 \
libraries\DS3231\Examples\DS3231_TEST \
libraries\MemoryFree \
libraries\MemoryFree\examples\FreeMemory \
libraries\Shared \
libraries\Wire\examples\SFRRanger_reader \
libraries\Wire\examples\digital_potentiometer \
libraries\Wire\examples\master_reader \
libraries\Wire\examples\master_writer \
libraries\Wire\examples\slave_receiver \
libraries\Wire\examples\slave_sender \
libraries\Wire\src \
libraries\Wire\src\utility \

