/*
 * File.cpp
 *
 *  Created on: 29 Tem 2015
 *      Author: WIN7
 */

#include "File.h"

File::File(int baslamaAdresi) {
	// TODO Auto-generated constructor stub
#ifdef DEBUG
//	char ch[100];
//	int s =
			sprintf(my_dbg_buf, "Base Class constructor: BAS ADDR: %d", baslamaAdresi);
//	ch[s] = NULL;
	DEBUG_S(my_dbg_buf);
#endif
	this->baslamaAdresi = baslamaAdresi;
	this->deviceAddress = 0x57;
	this->seek = this->baslamaAdresi + 2;
	this->lastWrittenAddress = this->baslamaAdresi + 1;

	delay(100);
	byte c1 = this->i2c_eeprom_read_byte(this->deviceAddress,
			this->baslamaAdresi);
	delay(100);
	byte c2 = this->i2c_eeprom_read_byte(this->deviceAddress,
			this->baslamaAdresi + 1);
	delay(100);

	if ((c1 == 99) && (c2 == 99)) {
#ifdef DEBUG
		DEBUG_S("has data true");
#endif
		has_data = true;
	} else {
#ifdef DEBUG
		DEBUG_S("has data false");
#endif
		has_data = false;
	}
}
bool File::hasData() {
	return has_data;
}
void File::writeHeader() {
	byte c1 = 99;
	byte c2 = 99;
	delay(FILE_WAIT);
	this->i2c_eeprom_write_byte(this->deviceAddress, this->baslamaAdresi, c1);

	delay(FILE_WAIT);
	this->i2c_eeprom_write_byte(this->deviceAddress, this->baslamaAdresi + 1,
			c2);
	delay(FILE_WAIT);
	this->has_data = true;
}
void File::resetAll() {
	delay(FILE_WAIT);
	this->i2c_eeprom_write_byte(this->deviceAddress, this->baslamaAdresi, 0);
	delay(FILE_WAIT);
	this->i2c_eeprom_write_byte(this->deviceAddress, this->baslamaAdresi + 1,
			0);
	delay(FILE_WAIT);
	this->has_data = false;
	this->resetSeek();
	this->resetLastWrittenAddress();
}
void File::resetLastWrittenAddress() {
	this->lastWrittenAddress = this->baslamaAdresi + 1;
}
void File::resetSeek() {
	this->seek = this->baslamaAdresi + 2;
}
byte File::readByte(const unsigned int addr) {
	if (addr < this->baslamaAdresi + 2)
		return NULL;
	delay(FILE_WAIT);
	byte b = this->i2c_eeprom_read_byte(this->deviceAddress, addr);
	delay(FILE_WAIT);
	seek = addr;
	return b;
}
uint16_t File::readShort(unsigned int address) {
	byte hi = this->readByte(address);
	byte lo = this->readByte(address + 1);
	uint16_t value = lo | uint16_t(hi) << 8;
	return value;
}
byte File::readNextByte() {
	delay(FILE_WAIT);
	byte b = this->i2c_eeprom_read_byte(this->deviceAddress, seek);
	delay(FILE_WAIT);
#ifdef DEBUG
//	char ch[100];
//	int size =
			sprintf(my_dbg_buf, "addr: %d vvalue: %d", seek, b);
//	ch[size] = NULL;
	DEBUG_S(my_dbg_buf);
#endif
	seek++;
	return b;
}
uint16_t File::readNextShort() {
	byte hi = readNextByte();
	byte lo = readNextByte();
	uint16_t value = lo | uint16_t(hi) << 8;
	return value;
}
void File::writeByte(unsigned int address, byte data) {
	if (address < baslamaAdresi + 2)
		return;
	if (!this->has_data) {
		this->writeHeader();
	}
	this->lastWrittenAddress = address;
	delay(FILE_WAIT);
	this->i2c_eeprom_write_byte(this->deviceAddress, address, data);
	delay(FILE_WAIT);
}
void File::writeShort(unsigned int address, uint16_t data) {
	byte lo = data & 0xFF;
	byte hi = data >> 8;
	this->writeByte(address, hi);
	this->writeByte(address + 1, lo);
}
void File::writeNextByte(byte data) {
	this->lastWrittenAddress++;
#ifdef DEBUG
//	char ch[100];
//	int s =
			sprintf(my_dbg_buf,"addr : %d value : %d",lastWrittenAddress,data);
//	ch[s]=NULL;
	DEBUG_S(my_dbg_buf);
#endif
	if (!this->has_data)
		this->writeHeader();
	delay(FILE_WAIT);
	this->i2c_eeprom_write_byte(this->deviceAddress, this->lastWrittenAddress,
			data);
	delay(FILE_WAIT);
}
void File::writeNextShort(uint16_t data) {
	byte lo = data & 0xFF;
	byte hi = data >> 8;
	writeNextByte(hi);
	writeNextByte(lo);
}
