/*
 * ArduinoSera.h
 *
 *  Created on: 18 Mar 2020
 *      Author: alptu
 */

#ifndef ARDUINOSERA_H_
#define ARDUINOSERA_H_

#include "Arduino.h"
#include"MyPt1000.h"
#include"NokiaMonitor.h"
#include"ESPWifi.h"
//add your function definitions for the project SeraProjectWithProMini3_3 here



int parseNextInt(char *str,uint8_t *bas);
void doActions(char *buffer);
//Do not add code below this line



#endif /* ARDUINOSERA_H_ */
