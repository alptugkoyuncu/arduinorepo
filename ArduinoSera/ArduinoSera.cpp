//#define SERIAL_DEBUG_ENABLE
#include"ArduinoSera.h"
MyPt1000 *pt;
Monitor *monitor = new NokiaMonitor(3, 4, 5);
SeraWifi *wifi;
size_t dToStr(char *ch, double val);

uint8_t ADS1248_START_PIN = A0;
uint8_t ADS1248_CS_PIN = 10;
uint8_t ADS1248_DRDY_PIN = 2;
int8_t ADS1248_RESET_PIN=A1;
int8_t ESP8266_RESET_PIN=9;
class ADSListener: public ADSBoardListener {
public:
	void valueUpdated(ADSBoardEvent val) {
		DataPackage sp;
		sp.init();
		char buffer[40];
		size_t n = sprintf(buffer, "<1%d ", val.getType());
		n = n + dToStr(buffer + n, val.getVal());
		buffer[n++]=' ';
		n=n+dToStr(buffer+n,val.getMin());
		buffer[n++]=' ';
		n=n+dToStr(buffer+n,val.getMax());
		buffer[n++] = '>';
		buffer[n] = '\0';
		wifi->println(buffer);
#ifdef SERIAL_DEBUG_ENABLE
		char b[20];
		sprintf(b, "Temp Reading Type: [%d] Value :", val.getType());
		Serial.print(b);
		Serial.println(val.getVal(), 2);
#endif
		switch (val.getType()) {
		case ADSBoardSensorType::temp1Updated:
#ifdef SERIAL_DEBUG_ENABLE
			unsigned long son;
			unsigned long bas;
			bas = micros();
#endif
			monitor->setTemp1(val.getVal());
#ifdef SERIAL_DEBUG_ENABLE
			son = micros();
			Serial.print(F("deger : "));
			Serial.println(son - bas);
#endif
			break;
		case ADSBoardSensorType::temp2Updated:
			monitor->setTemp2(val.getVal());
			break;
		case ADSBoardSensorType::temp3Updated:
			monitor->setTemp3(val.getVal());
			break;
		case ADSBoardSensorType::boardTempUpdated:
			monitor->setBoardTemp(val.getVal());
			break;
		default:
#ifdef SERIAL_DEBUG_ENABLE
			Serial.println(F("Unkown Type"));
#endif
			break;
		}
	}
};
ADSListener *ptListener;
DataPackage pck;
//The setup function is called once at startup of the sketch
unsigned long now;
void setup() {

#ifdef SERIAL_DEBUG_ENABLE
	Serial.begin(9600);
	Serial.println(F("Started"));
#endif
	monitor->begin();
	wifi = new ESPWifi(&Serial, 115200,ESP8266_RESET_PIN);
	wifi->begin();
	wifi->connect();
	delay(1000);
	monitor->gotoPage(MonitorPages::openingScreen);

	delay(2000);
	monitor->gotoPage(MonitorPages::MainScreen);

	ptListener = new ADSListener();
	pt = new MyPt1000(ADS1248_RESET_PIN);
	pt->begin(ADS1248_CS_PIN, ADS1248_START_PIN, ADS1248_DRDY_PIN);
#ifdef SERIAL_DEBUG_ENABLE
	Serial.println(F("Setup Ends"));
#endif
	pt->setListener(ptListener);
// Add your initialization code here
	now = millis();
//	pt->setNumberOfPTReading(1);
}

// The loop function is called in an endless loop
#define BOARD_TEMP_REQUEST_TIME 30000
void loop() {
	if (millis() - now > BOARD_TEMP_REQUEST_TIME) {
		pt->requestBoardTemp();
		now = millis();
	}
#ifdef SERIAL_DEBUG_ENABLE
	while (Serial.available()) {
//		BoardSensorType type = (BoardSensorType) Serial.parseInt();
		int a = Serial.parseInt();
		switch (a) {
		case 0:
			Serial.println(pt->temp1, 2);
			break;
		case 1:
			Serial.println(pt->temp2, 2);
			break;
		case 2:
			Serial.println(pt->temp3, 2);
			break;
		case 3:
			Serial.println(pt->boardTemp, 2);
			break;
		}
//		double val = Serial.parseFloat();
//		pt->fireLiistener(type, val);
	}
#endif
	wifi->readAll(&pck);
	if (pck.isFinished) {

	}
	pt->doReading();
	delay(5);
//Add your repeated code here
}
void doActions(char *buffer){
	uint8_t actCode;
	uint8_t nextIndex=0;
	actCode = parseNextInt(buffer,&nextIndex);
	switch(actCode){

	}
}
int parseNextInt(char *str,uint8_t *bas){
	uint8_t tempIndex=0;
	char tmp[10];
	bool baslangicBosluk=true;
	for(; str[*bas]!='\0'; *bas++){
		if(str[*bas]==' ' || str[*bas]=='\t' || str[*bas]=='\r' || str[*bas]=='\n')
			if(baslangicBosluk)
				continue;
			else
				break;
		else{
			if(baslangicBosluk)
				baslangicBosluk=false;
			tmp[tempIndex++] =str[*bas];
		}

	}
	tmp[tempIndex]='\0';
	return atoi(tmp);
}
size_t dToStr(char *ch, double val) {
	int i = (int) val;
	val = val - (double) i;
	val = val * 100;
	int f = (int) val;
	return sprintf(ch, "%d.%02d", i, f);
}
