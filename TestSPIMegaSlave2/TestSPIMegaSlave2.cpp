#include <Arduino.h>
#include<SPI.h>

//SPI DATA TRANSEFER BETWEEN TWO MICROCONTROLER READ AND WRITE
// MEGA AS SLAVE

const uint8_t buffIndex = 50;
char buff[buffIndex];
volatile byte indx, len,sentIndex;
volatile boolean process;
#define INT_PIN 2
byte requestData = 1;
byte sentData = 2;
byte finishData = '\r';
ISR (SPI_STC_vect) // SPI interrupt routine
{
	byte c = SPDR; // read byte from SPI Data Register

	if (c == requestData) {
		if (sentIndex < len) {
			SPDR = buff[sentIndex++];

		} else if (len == sentIndex) {
			SPDR = '\r';
			process = true;
		}
	} else {
		if (indx < sizeof buff) {

			if (c == '\r') { //check for the end of the word
				process = true;
				buff[indx] = 0x0; //set null instead of end of the word
			} else {
				buff[indx++] = c; // save data in the next index in the array buff

			}
		}

	}
}

void setup(void) {
	Serial.begin(57600);
	Serial.println("Setup begins");
	pinMode(MISO, OUTPUT); // have to send on master in so it set as output
	pinMode(INT_PIN, OUTPUT);
	digitalWrite(INT_PIN, HIGH);
	SPCR |= _BV(SPE); // turn on SPI in slave mode
	indx = 0; // buffer empty
	len = 0,sentIndex;
	process = false;
	SPI.attachInterrupt(); // turn on interrupt
	Serial.println("Setup ends");
}
void loop(void) {
	if (process) {
		process = false; //reset the process
		if (sentIndex!=0) {
			Serial.print("Sent ");
			Serial.println(buff);
			sentIndex=0;
		} else {
			Serial.print("Received :");
			Serial.println(buff);
			len=indx;
			indx = 0;

		}
	}
}
