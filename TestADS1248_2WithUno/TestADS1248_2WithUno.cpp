#include <Arduino.h>
// Do not remove the include below

#include <SPI.h>
#include<MyADS1248.h>
#define SERIAL_DEBUG_ENABLE
//#define NEXTION_ENABLE

#define INT_DPIN 2
#define START_PIN 9
#define CS 10
unsigned long A2DVal = 0x0;
volatile int DRDY_STATE = HIGH;

unsigned long sample = 0;

void ads1248_DRDY_STATE() {
	DRDY_STATE = LOW;
}

unsigned long waitForDRDY = 0;
void setup() {

	pinMode(INT_DPIN, INPUT);
	pinMode(START_PIN, OUTPUT);
	pinMode(CS,OUTPUT);

	digitalWrite(START_PIN, HIGH);
	//Enable Serial

	Serial.begin(115200);

	attachInterrupt(digitalPinToInterrupt(INT_DPIN), ads1248_DRDY_STATE,
	FALLING);
	SPI.setClockDivider(SPI_CLOCK_DIV16); //1MHz Bus Speed
	SPI.setDataMode(0x04); //SPI_MODE1

	delay(2000); //Give you time to open up the Serial monitor as it will restart the Arduino but not ADS1247
	SPI.begin(); //Turn on the SPI Bus
	digitalWrite(CS,LOW);
	delay(1);
	SPI.transfer(0x06); //Reset the ADS1247
	delay(6); //Minimum 0.6ms required for Reset to finish.
	SPI.transfer(0x16); //Issue SDATAC
	SPI.transfer(0x40); //Set MUX0 Register (00h) Write 01h
	SPI.transfer(0x00);
//	SPI.transfer(0x2F); //AIN5-AIN7
	SPI.transfer(TEMP2_POSITIVE_INPUT | TEMP2_NEGATIVE_INPUT); //AIN5-AIN7
	SPI.transfer(0x42); //Set MUX1 Register (02h) Write 38h - Select internal reference always on, internal ref connected to REF0 pins. Use 33h if wanting an on chip temp read.
	SPI.transfer(0x00);
//	SPI.transfer(0x28);
	SPI.transfer(REFSELT1_REF0 | VREFCON1_ON);
	SPI.transfer(0x43); //Set SYS0 Register (03h) Write 52h - PGA:32, Sample at 20sps
	SPI.transfer(0x00);
	SPI.transfer(PGA | SPS);
//	SPI.transfer(0x20); //pga 4v sample at 5sps

	SPI.transfer(0x4A); //Set IDAC0 Register (0Ah) Write 07h - Select 1.5mA reference current for RTD
	SPI.transfer(0x00);
		SPI.transfer(IMAG2_100);
//	SPI.transfer(0x03); //0.250ma

	SPI.transfer(0x4B); //Set IDAC1 Register (0Bh) Write 01h - Output reference current on ANIN0,1
	SPI.transfer(0x00);
//	SPI.transfer(0x1C);//ain1 current sink
	SPI.transfer(TEMP2_CURRENT1 | TEMP2_CURRENT2);
//	SPI.transfer(0x01);
	digitalWrite(CS,HIGH);
	delay(1000);
	waitForDRDY = millis();
	Serial.println("Setup ends");

}

#define PGA_GAIN 4.0
#define val 8388607.0
#define VREF 8250.0
#define MAXIMUM_WAIT_FOR_DRDY 5000
const uint8_t sps = 5;
double ohms[sps];
void loop() {

//Reset A2D Storage Value
	if (millis()-waitForDRDY > MAXIMUM_WAIT_FOR_DRDY) {
		Serial.println("WAIT TOO MUCH");

		DRDY_STATE = LOW;
	}
	if (!DRDY_STATE) {
		waitForDRDY = millis();
		digitalWrite(CS,LOW);
		delay(1);
		A2DVal = 0x0;

		SPI.transfer(0x12); //Issue RDATA
		delay(1);
		A2DVal |= SPI.transfer(0xFF); //Write NOP, Read First Byte and Mask to A2DVal
		A2DVal <<= 8; //Left Bit-Shift A2DVal by 8 bits
		A2DVal |= SPI.transfer(0xFF); //Write NOP, Read Second Byte and Mask to A2DVal
		A2DVal <<= 8;
		A2DVal |= SPI.transfer(0xFF); //Write NOP, Read Third Byte and Mask to A2DVal
		digitalWrite(CS,HIGH);
		delay(1);
//SPI.transfer(0x22); //Read Register 0x2
//SPI.transfer(0x00); //N - 1 Bytes To Be Read
//A2DVal |= SPI.transfer(0xFF); //Mask to A2DVal

		double oran = A2DVal / val;
		ohms[sample++ % sps] = oran * (VREF / PGA_GAIN);

		if (sample % sps == 0) {
			double ohm = 0;
			for (int i = 0; i < sps; i++)
				ohm += ohms[i];
			ohm = ohm / sps;

#ifdef SERIAL_DEBUG_ENABLE
			Serial.print("SAMPLE ");
			Serial.print(sample);
			Serial.print(" --> Analog val : ");
			Serial.print(A2DVal);
			Serial.print(" Ohm :");
			Serial.print(ohm);
			Serial.print(" Temp :");
			Serial.println(getTempByMap(ohm));
#endif
		}
//		delay(2000);
		noInterrupts();
		DRDY_STATE = HIGH;
		interrupts();
	}
}
