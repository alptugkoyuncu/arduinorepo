################################################################################
# Automatically-generated file. Do not edit!
################################################################################

INO_SRCS := 
ASM_SRCS := 
O_UPPER_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
PDE_SRCS := 
CC_SRCS := 
AR_SRCS := 
C_SRCS := 
C_UPPER_DEPS := 
PDE_DEPS := 
C_DEPS := 
AR := 
CC_DEPS := 
AR_OBJ := 
C++_DEPS := 
LINK_OBJ := 
CXX_DEPS := 
ASM_DEPS := 
HEX := 
INO_DEPS := 
SIZEDUMMY := 
S_UPPER_DEPS := 
ELF := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
. \
core\core \
libraries\Adafruit_CC3000 \
libraries\Adafruit_CC3000\examples\ChatServer \
libraries\Adafruit_CC3000\examples\EchoServer \
libraries\Adafruit_CC3000\examples\GeoLocation \
libraries\Adafruit_CC3000\examples\HTTPServer \
libraries\Adafruit_CC3000\examples\InternetTime \
libraries\Adafruit_CC3000\examples\SendTweet \
libraries\Adafruit_CC3000\examples\SmartConfigCreate \
libraries\Adafruit_CC3000\examples\SmartConfigReconnect \
libraries\Adafruit_CC3000\examples\WebClient \
libraries\Adafruit_CC3000\examples\buildtest \
libraries\Adafruit_CC3000\examples\driverpatch_1_11 \
libraries\Adafruit_CC3000\examples\driverpatch_1_12 \
libraries\Adafruit_CC3000\examples\driverpatch_1_13 \
libraries\Adafruit_CC3000\examples\driverpatch_1_14 \
libraries\Adafruit_CC3000\examples\ntpTest \
libraries\Adafruit_CC3000\utility \
libraries\SPI\examples\BarometricPressureSensor \
libraries\SPI\examples\DigitalPotControl \
libraries\SPI\src \

