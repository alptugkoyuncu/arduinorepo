// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _ADS1248_PROMINI3_3_H_
#define _ADS1248_PROMINI3_3_H_
#include "Arduino.h"
//add your includes for the project ADS1248_TEST here

#include <SPI.h>
#include "ads12xx.h"
#include"Adafruit_GFX.h"
#include"Adafruit_PCD8544.h"
#include "Pt1000ValueMap.h"
//end of add your includes here

//add your function definitions for the project ADS1248_TEST here

void test_intTemp();
void test_supVoltage();
void test_extrefVoltage();
void test_Voltage();
void test_Thermo();
void printNokia(double val);
//Do not add code below this line
#endif /* _ADS1248_PROMINI3_3_H_ */
