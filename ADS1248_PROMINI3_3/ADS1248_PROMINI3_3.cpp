// Do not remove the include below
#include "ADS1248_PROMINI3_3.h"



int START = 4;
int CS =3;
int DRDY = 2;

//Define which ADC to use in the ads12xx.h file

#define OPT_TEST_VOLTAGE 1
#define OPT_TEST_VDD 2
#define OPT_TEST_INTTEMP 3

//#define SERIAL_ENABLE
//#define NEXTION_ENABLE
#define NOKIA_PNL_ENABLE


#ifdef NOKIA_PNL_ENABLE
Adafruit_PCD8544 display = Adafruit_PCD8544(9, 8, 7, 6, 5);
#endif
uint8_t last_option = 0;
unsigned long lastCheckms = 0;
ads12xx ADS;

uint8_t constrast = 60;
void setup() {

#ifdef SERIAL_ENABLE
#ifndef DEBUG_SERIAL_ENABLE
	Serial.begin(115200);
	while (!Serial) {

	}
	Serial.println("Serial online");
#endif
#endif

#ifdef NOKIA_PNL_ENABLE
	display.begin();
	display.setContrast(constrast);

	display.display(); // show splashscreen
	delay(2000);
	display.clearDisplay();   // clears the screen and buffer
#endif
	ADS.begin(CS, START, DRDY);  //initialize ADS as object of the ads12xx class

	ADS.Reset();

	delay(100);
#ifdef NEXTION_ENABLE
	nexInit();
	btnDTemp.attachPop(updateDTamp,&btnDTemp);
	btnDVolt.attachPop(updateDVoltage,&btnDVolt);
#endif

#ifdef SERIAL_ENABLE
	Serial.println("Commands for testing:");
	Serial.println("'r' to read Register");
	Serial.println("'w' to write Register");
	Serial.println("'R' to get a Conversion Result");
	Serial.println("'x' to SDATAC, 'd' for SDATA");
	Serial.println("'o' to write Pre Predefinde Registers");
	Serial.println("'f' to write a command");
	Serial.println("'c' to select calibration methode");
	Serial.println("'t' to chose test");
	Serial.println("'h' for this help");
	Serial.println("'n for change constrast'");
#endif
	lastCheckms = millis();
}
#define UPDATE_INTERVAL 2000
void loop() {
#ifdef NEXTION_ENABLE
	nexLoop(nex_listen_list);
#endif
	if (millis() - lastCheckms > UPDATE_INTERVAL) {
		test_Voltage();
	}
#ifdef SERIAL_ENABLE
	if (Serial.available()) {
		char cin = Serial.read();
		char check = 'y';
		uint8_t cmd;
		uint8_t cin1;
		switch (cin) {
		case 'r':
			Serial.println("Which Register to read?");
			while (!Serial.available())
				;
			Serial.print("Register Value for: ");
			cin1 = Serial.parseInt();
			Serial.println(cin1);
			Serial.println(ADS.GetRegisterValue(cin1));
			break;
		case 'w':
			Serial.println("Which Register to write?");
			while (!Serial.available())
				;
			cin1 = Serial.parseInt();
			Serial.println("Which Value to write?");
			while (!Serial.available())
				;
			ADS.SetRegisterValue(cin1, Serial.parseInt());
			break;
		case 'R':
			Serial.println("Conversion Result");
			Serial.println(ADS.GetConversion());
			break;
		case 'x':
			Serial.println("Stop SDATAC");
			ADS.Reset();
			break;
		case 'o':
			Serial.println("Writing predefind Registers");
#ifdef ADS1256
			ADS.SetRegisterValue(MUX, P_AIN0 | N_AINCOM);
			ADS.SetRegisterValue(DRATE, DR_1000);
#endif
#ifdef ADS1248
			ADS.SetRegisterValue(SYS0, DOR3_2000 | PGA2_0);

			ADS.SetRegisterValue(IDAC0, DRDY_ONLY | IMAG2_OFF
			//	| IDAC0_ID
					);
			ADS.SetRegisterValue(IDAC1, I1DIR_OFF);
			ADS.SetRegisterValue(MUX1, BCS1_1 | MUX_SP2_AIN0 | MUX_SN2_AIN2);
#endif
			Serial.println("Writing sucessfull");
			break;
		case 'd':
			while (check == 'y') {
				if (Serial.available()) {
					check = Serial.read();

				}
				uint32_t data = ADS.GetConversion();
				int timer1 = micros();
				if (long minus = data >> 23 == 1) {
					long data = data - 16777216;
				}
				Serial.println(data);

#ifdef ADS1256
				//	double voltage = (4.9986 / 8388608)*data;
				//	Serial.println(voltage);
#endif
			}
			break;
		case 'f':
			Serial.println("Which command to write");
			while (!Serial.available())
				;
			cmd = Serial.parseInt();
			Serial.print(cmd, HEX);
			ADS.SendCMD(cmd);
			break;
		case 'c':
			Serial.println("Which Calibration to run?");
#ifdef ADS1256
			Serial.println("'1' for SELFCAL");
#endif
#ifdef ADS1248
			Serial.println("'1' for SELFOCAL");
#endif
			Serial.println("'2' for SYSOGCAL\n'3' for SYSGCAL");
			while (!Serial.available())
				;
			cmd = Serial.parseInt();
			switch (cmd) {
#ifdef ADS1256
			case 1:
			Serial.println("Preforming Self Gain and Offset Callibration");
			ADS.SendCMD(SELFCAL);
			delay(5);
			Serial.print("OFC0: ");
			Serial.println(ADS.GetRegisterValue(OFC0));
			Serial.print("OFC1: ");
			Serial.println(ADS.GetRegisterValue(OFC1));
			Serial.print("OFC2: ");
			Serial.println(ADS.GetRegisterValue(OFC2));
			Serial.print("FSC0: ");
			Serial.println(ADS.GetRegisterValue(FSC0));
			Serial.print("FSC1: ");
			Serial.println(ADS.GetRegisterValue(FSC1));
			Serial.print("FSC2: ");
			Serial.println(ADS.GetRegisterValue(FSC2));
			break;
#endif
#ifdef ADS1248
			case 1:
				Serial.println("Preforming Self Offset Callibration");
				ADS.SendCMD(SELFOCAL);
				delay(5);
				Serial.print("OFC0: ");
				Serial.println(ADS.GetRegisterValue(OFC0));
				Serial.print("OFC1: ");
				Serial.println(ADS.GetRegisterValue(OFC1));
				Serial.print("OFC2: ");
				break;
#endif
			case 2:
				Serial.println("Preforming System Offset Callibration");
				ADS.SendCMD(SYSOCAL);
				delay(5);
				Serial.print("OFC0: ");
				Serial.println(ADS.GetRegisterValue(OFC0));
				Serial.print("OFC1: ");
				Serial.println(ADS.GetRegisterValue(OFC1));
				Serial.print("OFC2: ");
				Serial.println(ADS.GetRegisterValue(OFC2));
				break;
			case 3:
				Serial.println("Preforming System Gain Callibration");
				ADS.SendCMD(SYSGCAL);
				delay(5);
				Serial.print("FSC0: ");
				Serial.println(ADS.GetRegisterValue(FSC0));
				Serial.print("FSC1: ");
				Serial.println(ADS.GetRegisterValue(FSC1));
				Serial.print("FSC2: ");
				Serial.println(ADS.GetRegisterValue(FSC2));
				break;
			default:
				break;
			}
			break;
		case 'h':
			Serial.println("Commands for testing:");
			Serial.println("'r' to read Register");
			Serial.println("'w' to write Register");
			Serial.println("'R' to get a Conversion Result");
			Serial.println("'x' to SDATAC, 'd' for SDATA");
			Serial.println("'o' to write Pre Predefinde Registers");
			Serial.println("'f' to write a command");
			Serial.println("'c' to select calibration methode");
			Serial.println("'t' to chose test");
			Serial.println("'h' for this help");
			break;
#ifdef ADS1248
		case 't':
			Serial.println("Chose which test to run");
			Serial.println(
					"'1' for Internal Temperature\n'2' for Supply Voltage Measurement\n'3' for External Voltage Reference\n'4' for Voltage Measurement\n'5' for Thermocouple Measurement");
			while (!Serial.available())
				;
			cmd = Serial.parseInt();
			switch (cmd) {
			case 1:
				test_intTemp();
				break;
			case 2:
				test_supVoltage();
				break;
			case 3:
				test_extrefVoltage();
				break;
			case 4:
				test_Voltage();
				break;
			case 5:
				test_Thermo();
				break;
			}
			;
			break;
#ifdef NOKIA_PNL_ENABLE
		case 'n': {
			Serial.println("Enter constrasnt");
			uint8_t val = Serial.parseInt();
			if (val > 60)
				val = 60;
			else if (val < 45)
				val = 45;
			constrast = val;
			display.setContrast(constrast);
			break;
		}
#endif

#endif

		default:
			break;
		}
	}
#endif
}

#ifdef ADS1248

/*
 Theses are test functions for the ADS1248 system monitor as well
 as some examples for 4-wire and thermocouple measurment (Warning these are not well tested).

 Run them atleast 3 times to get proper values.

 --------------------------------------------------------------

 This function gets temperature from the internal diode
 */
void test_intTemp() {
	ADS.SetRegisterValue(MUX1, MUXCAL2_TEMP | VREFCON1_ON | REFSELT1_ON);
	last_option = OPT_TEST_INTTEMP;
	unsigned long temp_val = ADS.GetConversion();
#ifdef SERIAL_ENABLE
	Serial.println(temp_val, DEC);
#endif
	double temp_voltage = (2.048 / 8388608) * temp_val;	//only 23bit data here.
#ifdef SERIAL_ENABLE
	Serial.println(temp_voltage, DEC);
#endif
	double temp = ((temp_voltage - 0.118) / (405 * 0.000001)) + 25;	//see Datasheet S.30
#ifdef SERIAL_ENABLE
	Serial.print("Internal Temperature is ");
	Serial.print(temp);
	Serial.println(" Grad Celsius!");
	lastCheckms = millis();
//	updateNextionText(&txtDTemp, temp, 2);
#endif
}

/*
 This function measures the supplied voltages
 */
void test_supVoltage() {
	ADS.SetRegisterValue(MUX1, MUXCAL2_DVDD | VREFCON1_ON | REFSELT1_ON);
	last_option = OPT_TEST_VDD;
	unsigned long volt_val = ADS.GetConversion();
	//Serial.println(volt_val, DEC);
	double voltage = (2.048 / 16777216) * volt_val;
	voltage *= 4 * 2.048;
#ifdef SERIAL_ENABLE
	Serial.print("DVDD Voltage: ");
	Serial.print(voltage);
	Serial.println("V");
#endif
	ADS.SetRegisterValue(MUX1, MUXCAL2_AVDD | VREFCON1_ON | REFSELT1_ON);

	unsigned long volt1_val = ADS.GetConversion();
	//Serial.println(volt1_val, DEC);
	double voltage1 = (2.048 / 16777216) * volt1_val;
	voltage1 *= 4 * 2.048;
#ifdef SERIAL_ENABLE
	Serial.print("AVDD Voltage: ");
	Serial.print(voltage1);
	Serial.println("V");
#endif
	lastCheckms = millis();
//	updateNextionText(&txtDVD, voltage, 2);
//	updateNextionText(&txtAVDD, voltage1, 2);
}

/*
 This function measures the voltage of the external voltage reference 1
 You have to connect the AIN0 to the specific REF0 or REF1 output
 */
void test_extrefVoltage() {
	ADS.SetRegisterValue(MUX1, REFSELT1_ON | VREFCON1_ON | MUXCAL2_REF0);//ADS Reference on Intern, Internal Reference on, System Montitor on REF1
	ADS.SetRegisterValue(IDAC0, IMAG2_1500);		//	IDAC at 1,5mA current
	ADS.SetRegisterValue(IDAC1, I1DIR_AIN0 | I2DIR_OFF);// IDAC1 Currentoutput on AIN0, IDAC2 off
	ADS.SetRegisterValue(SYS0, DOR3_5);

	unsigned long volt_val = ADS.GetConversion();
	//Serial.println(volt_val, DEC);
	double voltage = (2.048 / (16777216)) * volt_val;
	voltage *= 4 * 2.048;
#ifdef SERIAL_ENABLE
	Serial.print("External V_Ref: ");
	Serial.print(voltage, DEC);
	Serial.println("V");
#endif
}

/*
 Example for a 4-wire measurement (ex PT100 probe)
 See ADS1248 application sheet for the setup
 */
void test_Voltage() {
	if (last_option != OPT_TEST_VOLTAGE) {
		ADS.Reset();
		delay(100);
		ADS.SendCMD(SDATAC);
		// AIN4-AIN5
		ADS.SetRegisterValue(MUX0, MUX_SP2_AIN4 | MUX_SN2_AIN5);
		ADS.SetRegisterValue(VBIAS, VBIAS_RESET);
		//ADS Reference on REF0, Internal Reference on
		ADS.SetRegisterValue(MUX1, REFSELT1_REF0 | VREFCON1_ON);

		//PGA:4 SPS:5
		ADS.SetRegisterValue(SYS0, PGA2_4 | DOR3_5);

		//IDAC : 100ma
		ADS.SetRegisterValue(IDAC0, IMAG2_100);		//	IDAC at 0,100mA current

		// IDAC1 : AIN0 IDAC2:YOK
		ADS.SetRegisterValue(IDAC1, I1DIR_AIN0 | I2DIR_OFF);// IDAC Currentsink on AIN1
		ADS.SetRegisterValue(GPIOCFG, 0);
		ADS.SetRegisterValue(GPIODIR, 0);
		ADS.SetRegisterValue(GPIODAT, 0);
	}
	last_option = OPT_TEST_VOLTAGE;
	delay(10);
	unsigned long volt_val = ADS.GetConversion();
#ifdef SERIAL_ENABLE
	Serial.println(volt_val, DEC);
#endif
	const uint8_t PGA = 4;
#define RREF 8250
	double ohm = ((volt_val / 8388608.0) * RREF) / PGA;
#ifdef SERIAL_ENABLE
	Serial.print("Resistance:");
	Serial.print(ohm, DEC);
	Serial.println("Ohm");
#endif
	lastCheckms = millis();
#ifdef NOKIA_PNL_ENABLE
	printNokia(ohm);
#endif
//	updateNextionText(&txtDirenc, ohm, 2);
}

/*
 Untested function for Thermocouple measurment
 */
void test_Thermo() {
	ADS.SetRegisterValue(MUX0, MUX_SP2_AIN0 | MUX_SN2_AIN1);
	ADS.SetRegisterValue(MUX1, REFSELT1_ON | VREFCON1_ON);//ADS Reference on Intern, Internal Reference on
	ADS.SetRegisterValue(VBIAS, VBIAS_0);
	ADS.SetRegisterValue(SYS0, PGA2_128);// 2000 sps vollkommen un�tz rauschen �berwiegt

	long volt_val = ADS.GetConversion();
	if (long minus = volt_val >> 23 == 1) {
		long volt_valneg = volt_val - 16777216;
#ifdef SERIAL_ENABLE
		Serial.println(volt_valneg, DEC);
#endif
	} else {
#ifdef SERIAL_ENABLE
		Serial.println(volt_val, DEC);
#endif
	}

	//Serial.println(minus, BIN);

	double voltage = (2.048 / (16777216 * 2)) * (volt_val / 32);
 #ifdef SERIAL_ENABLE
	Serial.print("Thermocouple: ");
	Serial.println(voltage, DEC);
#endif
}
#endif
#ifdef NOKIA_PNL_ENABLE
void printNokia(double val) {
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(BLACK);
	display.setCursor(0, 0);
	display.write('O');
	display.write('h');
	display.write('m');
	display.write(':');
	display.println(val, 2);
	double temp = getTempByMap(val);
	display.println();
	display.print("Temp : ");
	display.println(temp,2);

	display.display();
}
#endif
