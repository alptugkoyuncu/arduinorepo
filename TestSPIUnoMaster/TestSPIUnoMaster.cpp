#include <Arduino.h>
#include <SPI.h>

//SPI DATA TRANSEFER BETWEEN TWO MICROCONTROLER
// UNO AS MASTER
void setup() {
   digitalWrite(SS, HIGH); // disable Slave Select
   SPI.begin();
   SPI.setClockDivider(SPI_CLOCK_DIV16);//divide the clock by 4
}

void loop() {
   char c;
   digitalWrite(SS, LOW); // enable Slave Select
   for (const char * p = "Hello Mega I am Uno\r" ; c = *p; p++)
   {
      SPI.transfer(c);
   }
   digitalWrite(SS, HIGH); // disable Slave Select
   delay(2000);
}
