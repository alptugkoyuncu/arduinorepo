/**TODO:
 1-PING alma gonderme islemi iptal edildi.Bu islem' dolayli yoldan  arduino uzerinden yaapilacak
 2-board reset sonsuz while dongusu ile gerceklesiyor yapilabilirse daha duzenli bi reset islemi gerekli
 3-server ya da arduino ya gonderilen datalar gidip gitmedigi kontrol edilmiyor ileride daha bi duzenli data alisverisi kontrolu yapilmali
 4- room/client numbar islemi iptal edildi yine bu islem dolayli yoldan arduino uzerinden gerceklesecek.
 1. ve 4. maddelerdeki degisikleriden oturu artik esp guncellemesine gerek kalmayacak yukle esplerin hepsi herbir arduno uzerinden kontrol edilebilecek
 5-room client işlemi tekrar yerleÅŸtirildi.Arduino iÃ§inde loopta sÃ¼rekli kontrol etmek  manasÄ±z
 */

#include"ESP8266-12F.h"

//varÄ±able defÄ±nes

// servera oda numarasÄ±nÄ± gonderÄ±r basarÄ±lÄ± olursa loopa gÄ±rer
// basarÄ±sÄ±z durumda reset atar
#define MAX_FAIL 3
#define NO_TCP_TRY 10
#define NO_WIFI_TRY 30
unsigned long lastReceiveArduino, lastReceiveServer;
const unsigned long MAX_WAIT_MS = 300000; //5 dakika icinde data gelmezse resetler

//#define MAC_SERVER
//#define UNIX_SERVER

Monitor *monitor;
WiFiClient client;
ConnectionRecord rec;
DataPackage pck;
bool wifiConnected = false, serverConnected = false, idAccepted = false;
uint16_t code;
////////////////////////// SETUP //////////////////////////////////////////////////////
double random2(double val) {
	int val2 = (micros() % 100) - 50;
	return val + ((double) val2 / 100.0);
}
void setup() {
#if AK_DEBUG
	Serial.begin(115200);
#endif
//Start setup
	d_println("Started");
	delay(100);
	d_print("Mem :");
	d_println(freeMemory());

// read data from eeprom
	delay(100);
	rec.loadConnectionRecord();

	//connect wifi
	code = connectWifi();
	sentEspPosition(code);
	if (code != AC_WIFI_CONNECTED)
		resetBoard();
	code = connectServer();
	sentEspPosition(code);
	if (code != AC_SERVER_CONNECTED)
		resetBoard();
	code = sentHeader();
	sentEspPosition(code);
	if (code != AC_ID_ACCEPTED)
		resetBoard();

	monitor = new NextionMonitor();
	monitor->begin();
	monitor->gotoPage(MonitorPages::MAIN_SCREEN);
	monitor->setConectionSettings(rec);
	d_print("Mem :");
	d_println(freeMemory());
	delay(100);
	d_println(F("Setup ends"));
	delay(1000);

	lastReceiveServer = millis();
	pck.init();
	d_println("<Setup Done!>");
}

////////////////////////// LOOP //////////////////////////////////////////////////////
int i = 0;
unsigned long now = millis();
double val = 20.0;
void loop() {
	if (!wifiConnected) {
		connectWifi();
	} else {
		if (!serverConnected) {
			connectServer();
		} else {
			if (!idAccepted)
				sentHeader();
		}
	}
	delay(10);
	monitor->listenKeyboard();
	delay(10);
	if (millis() - now > 4000) {
		val = random2(20.0);
		char buffer[50];
		char strDouble[10];
		snprintf(buffer, 50, "<%d %s>", i + 10, dToStr(strDouble, val));
		client.println(buffer);
		switch (i) {
		case 0:
			monitor->setBoardTemp(val);
			break;
		case 1:
			monitor->setTemp1(val);
			break;
		case 2:
			monitor->setTemp2(val);
			break;
			case3: monitor->setTemp3(val);
			break;
		default:
			break;
		}
	}
}
void sentEspPosition(uint16_t actionCode) {
	char buffer[10];
	sprintf(buffer, "<%d>", actionCode);
	Serial.println(buffer);
	Serial.flush();

}

//siradaki tokeni alir fonk geriye dondurur. uzunluk degerini len e verir. tokenin str icindeki bitisini lastIndex'e kaydeder
//NOT: nexToken icin FONKSIYON DA memory de yer ayirttilir ve oraya kaydedilir. Fonksiyon diisndaki degiskenin isi bittiginde memory serbest birakilmalidir. buffer array yerine char*buffer yani char pointer tavsiye edilir
char* nexToken(char *str, uint8_t *lastIndex, uint8_t *len) {
	uint8_t bas = *lastIndex;
//eliminate empty character and start index
	d_print("<nex token baslangic :");
	d_print(str + bas);
	d_println(">")
	for (; str[bas] != NULL; bas++) {
		if (str[bas] == ' ' || str[bas] == '\t' || str[bas] == '\r'
				|| str[bas] == '\n' || str[bas] == '<')
			continue;
		else
			break;
	}
	d_print("<Baslangic index : ");
	d_print(bas);
	d_println(">")
//bosluk veya satir sonuna gelinceye kadar tara
	uint8_t sonIndex = bas;
	for (; str[sonIndex] != NULL; sonIndex++) {
		if (str[sonIndex] == ' ' || str[sonIndex] == '\t'
				|| str[sonIndex] == '\r' || str[sonIndex] == '\n'
				|| str[sonIndex] == '>')
			break;
	}
	d_print("<Son index : ");
	d_print(sonIndex);
	d_println(">")
	*lastIndex = sonIndex;
	if (sonIndex == bas) {
		*len = 0;
		return NULL;
	}
	*len = sonIndex - bas;
	char *tmp;
	tmp = (char*) malloc(*len + 1);
	memcpy(tmp, str + bas, *len);
	tmp[*len] = NULL;
	d_print("<Token :");
	d_print(tmp);
	d_println(">")
	d_println("<nex token bitis>");
	return tmp;
}
uint16_t connectServer() {
	serverConnected = false;
	uint8_t flag;
	int count = 0;
	d_print("<Tcp Connection:");
	client.stopAll();
	while (count < NO_TCP_TRY) {
		d_print(".");
		if (client.connect(rec.getIp(), rec.getPort()) > 0) {
			d_println("Established>");
			serverConnected = true;
			return code = AC_SERVER_CONNECTED;
		}
		yield();
		delay(10);
		count++;
	}
	d_println("Failed>");
	return code = AC_SERVER_CONNECTION_FAILED;
}

// Fucntion to connect WiFi
uint16_t connectWifi() {
	wifiConnected = false;
	int WiFiCounter = 0;
// We start by connecting to a WiFi network
	d_print("<Connecting to ");
	d_print(rec.getSSID());
	WiFi.disconnect();
	WiFi.mode(WIFI_STA);
	WiFi.begin(rec.getSSID(), rec.getPassword());
	while (WiFi.status() != WL_CONNECTED) {
		yield();
		delay(500);
		WiFiCounter++;
		d_print(".");
		if (WiFiCounter >= NO_WIFI_TRY) {
			d_println("Can not connect to the wifi>");
			wifiConnected = true;
			return code = AC_WIFI_CONNECTION_FAILED;
		}
	}
	d_println("WiFi connected>");
	d_print("<IP address: ");
	d_print(WiFi.localIP());
	d_println(">");
	return code = AC_WIFI_CONNECTED;
}
uint16_t sentHeader() {
	idAccepted = false;
	client.flush();
	int8_t errTry = 10;
	char buffer[20];
	sprintf(buffer, "<%d %d>", AC_SEND_ID, rec.getId());
	pck.init();
	unsigned long start = millis();
	const unsigned long MAX_WAIT_MS = 3000;
	while (errTry >= 0) {
		client.println(buffer);
		client.flush();
		delay(10);
		while (client.available() && !pck.isFinished) {
			pck.setData(client.read());
			start = millis();
		}
		if (pck.isFinished) {
			uint8_t lastIndex = 0, len = 0;
			char *actCode = nexToken((char*) pck.data, &lastIndex, &len);
			if (actCode != NULL) {
				char *val = nexToken((char*) pck.data, &lastIndex, &len);
				if (val != NULL) {
					uint16_t ac, ai;
					ac = atoi(actCode);
					free(actCode);
					actCode = NULL;
					ai = atoi(val);
					free(val);
					val = NULL;
					if (AC_SEND_ID == ac && rec.getId() == ai) {
						idAccepted = true;
						return code = AC_ID_ACCEPTED;
					}
				} else {
					free(actCode);
					actCode = NULL;
				}
			}
			pck.init();
		}

		if (millis() - start > MAX_WAIT_MS) {
			errTry--;
			start = millis();
		}
	}
	return code = AC_ID_REFUSED;

}

// Inifinite loop - Causes to reset self
void resetBoard() {
	client.print("<");
	client.print(AC_RESET_ESP);
	client.print(">");
	client.println(SERVER_END_OF_LINE);
	delay(10);
	sentEspPosition(AC_RESET_ESP);
	delay(10);
	yield();
	client.flush();
	Serial.flush();
	client.stopAll();
	WiFi.disconnect();
	d_println("<Reseting>");
//  ESP.restart();
	delay(100);
	do {
	} while (1);
}

char* getParameter(uint16_t actionCode) {

	unsigned long waitms = 500;
	int8_t no_try = 10;
	char buffer[40];
	bool error = false;
	uint8_t i = 0;
	bool started = false, finished = false;
	sprintf(buffer, "<%d>", actionCode);
	Serial.println(buffer);
	unsigned long cur = millis();
	while (no_try >= 0) {
		if ((millis() - cur > waitms) || error) {
			no_try--;
			sprintf(buffer, "<%d>", actionCode);
			started = false;
			finished = false;
			i = 0;
			Serial.println(buffer);
			cur = millis();
			error = false;
		}

		while (Serial.available()) {
			char c = (char) Serial.read();
			if (c == '<') {
				started = true;
				i = 0;
			} else if (c == '>') {
				if (started) {
					finished = true;
					buffer[i] = NULL;
				} else {
					error = true;
					break;
				}
			} else
				buffer[i++] = c;
			if (finished) {
				uint8_t len;
				uint8_t nextIndex;
				nextIndex = 0;
				d_print("<before nexToken : ");
				d_print(buffer);
				d_println(" >")
				char *code = nexToken(buffer, &nextIndex, &len);
				int c = atoi(code);
				free(code);
				code = NULL;
				if (c != actionCode) {
					error = true;
					break;
				}
				d_print("<before 2.nexToken : >");
				char *str = nexToken(buffer, &nextIndex, &len);
				if (str == NULL) {
					error = true;
					break;
				}
				return str;
			}
		}
		delay(5);
	}
	return NULL;
}
size_t dToStr(char *ch, double val) {
	int i = (int) val;
	val = val - (double) i;
	val = val * 100;
	int f = (int) val;
	return sprintf(ch, "%d.%02d", i, f);
}
