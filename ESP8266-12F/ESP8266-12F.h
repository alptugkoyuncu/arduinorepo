/*
 * ESP8266-12F.h
 *
 *  Created on: 10 May 2020
 *      Author: alptu
 */

#ifndef ESP8266_12F_H_
#define ESP8266_12F_H_


/*
 * ESPSera.h
 *
 *  Created on: 6 Mar 2020
 *      Author: alptu
 */


#include<ActionCode.h>
#include<MyDebug.h>
#include<DataPackage.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include<WifiClient.h>
#include"NextionMonitor.h"
#include"FreeMemoryESP8266.h"

size_t dToStr(char *ch, double val);
void sentEspPosition(uint16_t actionCode);
void resetBoard();
uint16_t connectServer();
uint16_t connectWifi();
uint16_t sentHeader();
char* nexToken(char *str, uint8_t *lastIndex, uint8_t *length);
char* getParameter(uint16_t actionCode);



#endif /* ESP8266_12F_H_ */
