/*
 * NextionMonitor.h
 *
 *  Created on: 23 Mar 2020
 *      Author: alptu
 */

#ifndef NEXTIONMONITOR_H_
#define NEXTIONMONITOR_H_

#include "Monitor.h"
#include"Nextion.h"

#include<MyDebug.h>
class NextionMonitor: public Monitor {
public:
	NexButton *kaydetButton;
	static void b0PopCallback(void *ptr);

	NextionMonitor();
	virtual ~NextionMonitor();
	bool setTemp2(double val);
	bool setTemp1(double val);
	bool setBoardTemp(double val);
	bool setTemp3(double val);
	bool setConectionSettings(ConnectionRecord rec);
	void begin();
	void listenKeyboard();


protected:
	NexTouch **nex_listen_list;
	void openOpeningPage();
	void openAdminPage();
	void openMainPage();
	void openSavingPage();
	void openSuccessPage();
	void openFailurePage();
	void openKeypadPage();
};
#endif /* NEXTIONMONITOR_H_ */
