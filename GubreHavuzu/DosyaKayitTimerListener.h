/*
 * DosyaKayitTimerListener.h
 *
 *  Created on: 20 Kas 2016
 *      Author: alptug
 */

#ifndef DOSYAKAYITTIMERLISTENER_H_
#define DOSYAKAYITTIMERLISTENER_H_

#include "DosyaKayit/DosyaKayit.h"
#include "Timer.h"
#include "TimerItemList.h"
class DosyaKayitTimerItemListener:public TimerItemListener{
private:
	DosyaKayit *kayit;
public:
	DosyaKayitTimerItemListener(DosyaKayit *dk){
		kayit=dk;
	}
    void timerItemAdded(TimerItem val, uint8_t index)override{
    	kayit->addTimerItem(val);
    }
    void timerItemRemoved(TimerItem val, uint8_t index)override{
    	kayit->removeTimerItem(val);

    }
	void timerItemUpdated(TimerItem oldVal, TimerItem newVal,
				uint8_t oldIndex, uint8_t newIndex)override{
		kayit->removeTimerItem(oldVal);
		kayit->addTimerItem(newVal);
	}

};
class DosyaKayitTimerListener: public TimerListener {
private:
	DosyaKayit *kayit;
public:
	DosyaKayitTimerListener(DosyaKayit *dk){
		kayit = dk;
	}
	void timerStateChanged(TimerStateEvent evt)override{
		if(kayit!=NULL)
		kayit->setOtomasyonEnabled(evt.isOtomatikEnabled());
	}
	void timerActionPerformed(TimerActionEvent evt) override{

	}
	void timeChanged(uint8_t hh, uint8_t mm, uint8_t ss) override{

	}
	void dateChanged(uint16_t year, uint8_t month, uint8_t date)override {

	}

};

#endif /* DOSYAKAYITTIMERLISTENER_H_ */
