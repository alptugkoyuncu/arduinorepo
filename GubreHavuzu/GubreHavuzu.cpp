// Do not remove the include below
#include "GubreHavuzu.h"

Timer *timer;

DataPackage serialIn;

TimerItemList *list = NULL;
NextionMonitor *nm = NULL;
DosyaKayit *dosyaKayit;
DosyaKayitTimerListener *dktListener;
DosyaKayitTimerItemListener *dktItemListener;

//The setup function is called once at startup of the sketch
class GlobalTimerItemListener: public TimerItemListener {
public:
	void timerItemAdded(TimerItem val, uint8_t index) override {
//		if (dosyaKayit != NULL)
//			dosyaKayit->addTimerItem(&val, index);
	}
	void timerItemRemoved(TimerItem val, uint8_t index) override {
//		if (dosyaKayit != NULL)
//			dosyaKayit->removeTimerItem(&val, index);

	}
	void timerItemUpdated(TimerItem oldVal, TimerItem newVal, uint8_t oldIndex,
			uint8_t newIndex) override {
//		if (dosyaKayit != NULL) {
//			dosyaKayit->removeTimerItem(&oldVal, oldIndex);
//			dosyaKayit->addTimerItem(&newVal, newIndex);
//		}
	}
	~GlobalTimerItemListener() {

	}
};
class GlobalTimerListener: public TimerListener {
public:
	void timerStateChanged(TimerStateEvent evt) override {
		ghPrint("Global timer listener timer state changed :");
		ghPrintln(evt.isOtomatikEnabled());
//		if (dosyaKayit != NULL)
//			dosyaKayit->setOtomasyonEnabled(evt.isOtomatikEnabled());
	}
	void timerActionPerformed(TimerActionEvent evt) override {

		char buffer[100];
		ghPrint("Timer action performed");
		char *temp = timer->getTime();
		ghPrint(temp);
		ghPrint(" : ");
		free(temp);
		RunningTimerItem t = evt.getTimerTime();
		int size = sprintf(buffer, "(%d) %02u:%02u -- %u ", evt.isRunning(),
				t.getSaat(), t.getDakika(), t.getRunningTime());
		buffer[size] = '\0';

		ghPrintln(buffer);
	}

	void timeChanged(uint8_t hh, uint8_t mm, uint8_t ss) override {

	}
	void dateChanged(uint16_t year, uint8_t month, uint8_t date) override {

	}
	~GlobalTimerListener() {

	}

};
GlobalTimerListener gtl;
GlobalTimerItemListener gtil;
void setup() {
	Serial.begin(115200);
	ghPrintln("Serial begin");

	ghPrint("Waiting 3 seconds");
	int del = 3000;
	while (del > 0) {
		delay(100);
		ghPrint(".");
		del = del - 100;
	}
	ghPrintln("");
	Wire.begin();

	dosyaKayit = new EEPRomDosyaKayit();
	list = new TimerItemList();
	dosyaKayit->loadTimerItemList(list);
	timer = new Timer(list);
	nm = new NextionMonitor(timer);

	char *tmp;
	tmp = timer->getTime();
	ghPrint("Saat : ");
	ghPrintln(tmp);
	delay(1);
	free(tmp);

	timer->addTimerListener(&gtl);
	list->addTimerItemListener(&gtil);

	dktListener = new DosyaKayitTimerListener(dosyaKayit);
	dktItemListener = new DosyaKayitTimerItemListener(dosyaKayit);
	timer->addTimerListener(dktListener);
	list->addTimerItemListener(dktItemListener);

	timer->setOtomatikEnabled(dosyaKayit->isOtomasyonEnabled());
// Add your initialization code here
	ghPrintln("Setup ends");
}

// The loop function is called in an endless loop
void loop() {
	nexLoop(NextionMonitor::nex_listen_list);
	timer->checkInterrupted();
	while (Serial.available()) {
		serialIn.setData(Serial.read());
		if (serialIn.isFinished()) {
			uint8_t len;
			int16_t *values = serialIn.toIntArray(&len);
			if (len > 0) {
				doAction(values, len);
				free(values);
				values = '\0';
			}
			serialIn.init();
		}
		delay(1);
	}
//Add your repeated code here
}
void doAction(int16_t *array, uint8_t size) {
	ghPrint("Read Values :(");
	ghPrint(size);
	ghPrint(")->");
	for (int i = 0; i < size; i++) {
		ghPrint((int16_t) array[i]);
		ghPrint(" ");
	}
	ghPrintln("");
	switch (array[0]) {
	case ACTION_GET_MEM_INFO: {
		char buffer[50];
		str_action_get_mem_info(buffer);
		Serial.println(buffer);
		break;
	}
	case ACTION_RESET_EEPROM: {
		dosyaKayit->resetFile();
		break;
	}
	case ACTION_TIMER_START: {
		if (size <= 1) {
			timer->start();
		} else {
			timer->start(array[1]);
		}
		break;
	}
	case ACTION_TIMER_STOP: {
		timer->stop();
		break;
	}
	case ACTION_TIMER_SET_OTOMATIKLIK: {
		timer->setOtomatikEnabled(array[1] != 0);
		break;
	}
	case ACTION_TIMER_INFO: {
		char buffer[100];
		str_action_timer_info(buffer);
		Serial.println(buffer);
		break;
	}
	case ACTION_TIMER_ADD_TIMER_ITEM: {
		if (size < 5)
			break;
		list->addTimerItem(TimerItem(array[1], array[2], array[3], array[4]));
		break;
	}
	case ACTION_TIMER_REMOVE_TIMER_ITEM: {
		if (size < 3) {
			if (size < 2)
				break;
			list->removeTimerItem(array[1]);
		} else
			list->removeTimerItem(TimerItem(array[1], array[2], 0, false));
		break;
	}
	case ACTION_TIMER_UPDATE_TIMER_ITEM: {
		if (size < 7) {
			if (size < 5)
				break;
			list->updateTimerItem(array[1],
					TimerItem(array[1], array[2], array[3], array[4] > 0));
		} else
			list->updateTimerItem(TimerItem(array[1], array[2], 0, false),
					TimerItem(array[3], array[4], array[5], array[6] > 0));
		break;
	}
	case ACTION_TIMER_GET_TIMER_ITEM_AT: {
		if (size < 2)
			break;
		char buffer[100];
		str_action_timer_get_timer_item_at(buffer, array[1]);
		Serial.println(buffer);
		break;
	}
	case ACTION_TIMER_GET_TIMER_ITEM_SIZE: {
		char buffer[20];
		str_action_timer_get_timer_item_size(buffer);
		Serial.println(buffer);
		break;
	}
	case ACTION_TIMER_SET_DATE_TIME: {
		if (size < 8)
			break;
		DateTime dt(array[1], array[2], array[3], array[4], array[5], array[6],
				array[7]);
		break;
	}
	case ACTION_TIMER_GET_DATE_TIME: {
		char buffer[100];
		str_action_timer_get_date_time(buffer);
		Serial.println(buffer);
		break;

	}
	case 150: {
		int val = freeMemory();
		Serial.print("Free mem :");
		Serial.println(val);
		break;
	}
	default: {
		break;

	}
	}
}

int8_t str_action_timer_info(char *buffer) {
	RunningTimerItem *tt = timer->getNextRunningTimerItem();
	uint16_t errFlag = 0;
	if (tt == NULL)
		return sprintf(buffer, "<%u %u %u %u>", ACTION_TIMER_INFO, errFlag,
				timer->isOtomatikEnabled(), timer->isRunning());
	else
		return sprintf(buffer, "<%u %u %u %u %u %u %u %u %u>",
		ACTION_TIMER_INFO, errFlag, timer->isOtomatikEnabled(),
				timer->isRunning(), tt->getRunningTime(), tt->getSaat(),
				tt->getDakika(), tt->getSure(), tt->isAktif());
}
int8_t str_action_timer_get_timer_item_at(char *buffer, uint8_t index) {

	uint16_t errFlag = 0;
	if (index >= list->getSize()) {
		errFlag = 1;
		return sprintf(buffer, "<%u %u>", ACTION_TIMER_GET_TIMER_ITEM_AT,
				errFlag);
	} else {
		TimerItem t = list->getTimerItemAt(index);
		return sprintf(buffer, "<%u %u %u %u %u %u>",
		ACTION_TIMER_GET_TIMER_ITEM_AT, errFlag, t.getSaat(), t.getDakika(),
				t.getSure(), t.isAktif());
	}
}
int8_t str_action_timer_get_timer_item_size(char *buffer) {
	uint16_t errFlag = 0;
	return sprintf(buffer, "<%u %u %u>", ACTION_TIMER_GET_TIMER_ITEM_SIZE,
			errFlag, timer->getTimerItemList()->getSize());
}
uint8_t str_action_timer_get_date_time(char *buffer) {
	uint16_t errFlag = 0;
	DateTime dt = timer->getNow();
	return sprintf(buffer, "<%u %u %u %u %u %u %u %u>",
	ACTION_TIMER_GET_DATE_TIME, errFlag, dt.year(), dt.month(), dt.date(),
			dt.hour(), dt.minute(), dt.second());
}

uint8_t str_action_get_mem_info(char *buffer) {
	uint16_t errFlag = 0;
	return sprintf(buffer, "<%u %u %u>", ACTION_GET_MEM_INFO, errFlag,
			freeMemory());
}
