/*
 * NextionMonitor.cpp
 *
 *  Created on: 15 Kas 2016
 *      Author: alptug
 */

#include "NextionMonitor.h"
#include "MemoryFree.h"
NexTouch *NextionMonitor::nex_listen_list[] = { 0 };
uint8_t NextionMonitor::nex_listen_list_size = 0;
Timer *NextionMonitor::timer = NULL;

AbstractMyNextionPage *NextionMonitor::currentPage = NULL;
NextionMonitor::NextionMonitor(Timer* timer) {
	NextionMonitor::timer = timer;
	NextionMonitor::currentPage = new NextionStartPage();

	delay(50);
	if(!nexInit()){
		currentPage->show();
	}
//	clearNexListenList();
}

NextionMonitor::~NextionMonitor() {
	// TODO Auto-generated destructor stub
}

void NextionMonitor::setCurrentPage(uint32_t pageId) {
	nxPrint("in nextion current page : ");
	nxPrintln(pageId);
	if (currentPage != NULL) {
		uint8_t val = currentPage->getObjPid();
		nxPrint("Destroying object : ");
		currentPage->printObjInfo();
		nxPrint("before destroyed Mem : ");
		nxPrint(freeMemory());
		currentPage->detachPop();
		switch (val) {
		case NexPageId::NextionMainPage:{
			delete (NextionMainPanel *) currentPage;
			break;
		}
		case NexPageId::NextionTimerItemListPage:{
			delete (NextionTimerItemList *) currentPage;
			break;
		}
		case NexPageId::NextionNewTimerItemListPage:{
			delete (NewListItemPage *) currentPage;
			break;
		}
		case NexPageId::NextionSaatUpdate:{
			delete (NextionSaatUpdatePage *) currentPage;
			break;
		}
		default:
			delete currentPage;
		}
		nxPrint(" After Destroyed mem: ");
		nxPrintln(freeMemory());
		currentPage=NULL;
	}
	nxPrint("before creating object Mem :");
	nxPrintln(freeMemory());
	switch (pageId) {
	case NexPageId::NextionMainPage:{
		currentPage = new NextionMainPanel(timer);
		break;
	}
	case NexPageId::NextionTimerItemListPage:
		currentPage = new NextionTimerItemList(timer->getTimerItemList());
		break;

	case NexPageId::NextionNewTimerItemListPage:
		currentPage = new NewListItemPage(timer->getTimerItemList());
		break;
	case NexPageId::NextionSaatUpdate:
		currentPage = new NextionSaatUpdatePage(timer);
		break;
	default:
		currentPage = new NextionStartPage();
		break;
	}
	nxPrint("after page created mem: ");
	nxPrintln(freeMemory());
	currentPage->printObjInfo();
	currentPage->show();
}
