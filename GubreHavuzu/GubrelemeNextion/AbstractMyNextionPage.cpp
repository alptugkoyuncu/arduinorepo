/*
 * AbstractMyNextionPage.cpp
 *
 *  Created on: 16 Kas 2016
 *      Author: alptug
 */

#include "AbstractMyNextionPage.h"
#include"NextionMonitor.h"
#include"MemoryFree.h"
void AbstractMyNextionPage::loadPage(void* ptr) {
	AbstractMyNextionPage *l = (AbstractMyNextionPage *) ptr;
	l->pageLoaded();
	nxPrint("Page loaded ->");
	l->printObjInfo();
}

void AbstractMyNextionPage::destroyPage(void* ptr) {
	uint8_t pid = NextionMonitor::getNextPageId();
	NextionMonitor::setCurrentPage(pid);
}

AbstractMyNextionPage::AbstractMyNextionPage(uint8_t pid, uint8_t cid,
		const char* pageName) :NexPage(pid,cid,pageName){
	this->attachPop(AbstractMyNextionPage::loadPage,this);
}

