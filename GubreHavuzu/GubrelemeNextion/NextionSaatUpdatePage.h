/*
 * NextionSaatPage.h
 *
 *  Created on: 13 Kas 2016
 *      Author: alptug
 */

#ifndef GUBRELEMENEXTION_NEXTIONSAATUPDATEPAGE_H_
#define GUBRELEMENEXTION_NEXTIONSAATUPDATEPAGE_H_

#include"Timer.h"
#include "AbstractMyNextionPage.h"

class NextionSaatUpdatePage :public AbstractMyNextionPage{
private:
	Timer *timer;
	NexButton *btnKaydet,*btnGeri;
public:

	NextionSaatUpdatePage(Timer *t);
    bool pageLoaded() override;
    bool kaydet();
	~NextionSaatUpdatePage();
	static void kaydetClicked(void *ptr);
};
#endif /* GUBRELEMENEXTION_NEXTIONSAATUPDATEPAGE_H_ */
