/*
 * NextionTimer.h
 *
 *  Created on: 31 Eki 2016
 *      Author: alptug
 */

#ifndef LIBRARIES_NEXTIONTIMERLIST_H_
#define LIBRARIES_NEXTIONTIMERLIST_H_

#include"Timer.h"
#include "AbstractMyNextionPage.h"

class NextionTimerItem {
private:
	const static uint8_t pid = 2;
	uint8_t itemIndex;
	NexHotspot *touch;
public:
	NextionTimerItem(uint8_t itemIndex);
	~NextionTimerItem();
	NexHotspot *getTouch();

	bool setItem(TimerItem timerItem, uint8_t currPage);
	bool setNoItem();
};
#define ITEMS_PER_PAGE 5
class NextionTimerItemList:  public AbstractMyNextionPage,public TimerItemListener {
private:

	static void nextPage(void *ptr);
	static void prevPage(void *ptr);
	static void refreshPage(void *ptr);
	NextionTimerItem *items[ITEMS_PER_PAGE];
	uint8_t currPage;
	TimerItemList *timerItemList;
	NexButton *btnPgGeri, *btnPgIleri, *btnRefresh, *btnYeni;
	NexButton *btnGeri;
	uint8_t getPageIndexOfItem(uint8_t timerItemIndex) {
		return timerItemIndex / ITEMS_PER_PAGE + 1;
	}

public:

	NextionTimerItemList(TimerItemList *timerItemList);
	uint8_t getCurrentPage() {
		return currPage;
	}

	NextionTimerItem *getItemAt(uint8_t nextionIndex) {
		return this->items[nextionIndex];
	}
	bool pageLoaded() override;
	void timerItemAdded(TimerItem val, uint8_t timerItemIndex) override;
	void timerItemRemoved(TimerItem val, uint8_t timerItemIndex) override;
	void timerItemUpdated(TimerItem oldVal, TimerItem newVal, uint8_t oldIndex,
			uint8_t newIndex) override;

	bool setCurrentPage(uint8_t pageIndex);
	uint8_t getTotalPage();

	bool setTimerTimeAt(uint8_t timerTimeIndex);
	bool loadTimerPage();
	~NextionTimerItemList();
};

#endif /* LIBRARIES_NEXTIONTIMER_H_ */
