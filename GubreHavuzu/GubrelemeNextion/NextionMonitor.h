/*
 * NextionMonitor.h
 *
 *  Created on: 15 Kas 2016
 *      Author: alptug
 */

#ifndef GUBRELEMENEXTION_NEXTIONMONITOR_H_
#define GUBRELEMENEXTION_NEXTIONMONITOR_H_



#include "NextionMainPanel.h"
#include "NewListItemPage.h"
#include "NextionSaatUpdatePage.h"
#include "Timer.h"
#include "NextionTimerItemList.h"
#include"NextionStartPage.h"

enum NexPageId{
	NextionStartingPage=0,
	NextionMainPage=1,
	NextionTimerItemListPage=2,
	NextionNewTimerItemListPage=3,
	NextionSaatUpdate=4
};
class NextionMonitor {
private:
	static AbstractMyNextionPage *currentPage;
	static uint8_t currentPageId;
public:
	static uint32_t getNextPageId() {
		NexVariable pid(1, 8, "mainPage.nextPageId");
		uint32_t val;
		pid.getValue(&val);
		return val;
	}
	static uint8_t getCurrentPageId(){
		return currentPageId;
	}
	static void setCurrentPage(uint32_t pageId);
	static Timer *timer;
	static NexTouch *nex_listen_list[15];
	static uint8_t nex_listen_list_size;
	static void createNexListenList(uint8_t size) {
		if (nex_listen_list_size != 0)
			clearNexListenList();
		nex_listen_list_size = size;
//		nex_listen_list = new NTouchPtr[nex_listen_list_size];
	}
	static void clearNexListenList() {
//		if (nex_listen_list_size != 0) {
//			delete[] nex_listen_list;
//		}
//		delete nex_listen_list ;
		for(uint8_t i=0; i<15;i++)
			NextionMonitor::nex_listen_list[i]=NULL;
		nex_listen_list_size = 0;
	}
	NextionMonitor(Timer *timer);
	virtual ~NextionMonitor();
};
#endif /* GUBRELEMENEXTION_NEXTIONMONITOR_H_ */
