/*
 * NewListItemPage.h
 *
 *  Created on: 7 Kas 2016
 *      Author: alptug
 */

#ifndef GUBRELEMENEXTION_NEWLISTITEMPAGE_H_
#define GUBRELEMENEXTION_NEWLISTITEMPAGE_H_

#include"Timer.h"
#include "AbstractMyNextionPage.h"

class NewListItemPage :public AbstractMyNextionPage{
private:

	TimerItemList *list;
	NexButton *btnKaydet,*btnSil,*btnGeri;
	bool setSaatNextion(uint8_t val);
	bool getSaat(uint8_t *val);
	bool setDakikaNextion(uint8_t val);
	bool getDakika(uint8_t *val);
	bool setAktiflikNextion(bool isAktif);
	bool isAktif(bool *isAktif);
	bool setSureNextion(uint16_t val);
	bool getSure(uint16_t *val);
	uint8_t itemIndex;
	bool newOne;
	bool loadItemIndex();
	bool loadNewOne();
public:
	bool pageLoaded() override;

	NewListItemPage(TimerItemList *list);
	void deleteItem();
	void kaydetItem();
	 ~NewListItemPage();
};

void kaydetClicked(void *ptr);
void silClicked(void *ptr);
#endif /* GUBRELEMENEXTION_NEWLISTITEMPAGE_H_ */
