/*
 * NextionConfig.h
 *
 *  Created on: 25 Kas 2016
 *      Author: alptug
 */

#ifndef GUBRELEMENEXTION_NEXTIONCONFIG_H_
#define GUBRELEMENEXTION_NEXTIONCONFIG_H_

//#define NEXTION_DEBUG

#ifdef NEXTION_DEBUG
#define nxPrint(a) Serial.print(a)
#define nxPrintln(a) Serial.println(a)
#else
#define nxPrint(a) ;
#define nxPrintln(a) ;
#endif



#endif /* GUBRELEMENEXTION_NEXTIONCONFIG_H_ */
