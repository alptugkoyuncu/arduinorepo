/*
 * NextionMainPanel.h
 *
 *  Created on: 3 Kas 2016
 *      Author: alptug
 */

#ifndef NEXTIONMAINPANEL_H_
#define NEXTIONMAINPANEL_H_

#include"Timer.h"
#include "AbstractMyNextionPage.h"
//#include "NextionTimerList.h"
//#include"NewListItemPage.h"
//#include "NextionSaatUpdatePage.h"

class NextionMainPanel: public AbstractMyNextionPage, public TimerListener {
private:
	NexButton *timerListButton;
	NexText *saat;
	NexDSButton *startStopBtn;
	NexCheckbox *otomatiklikAktif;
	Timer *timer;
public:

	NextionMainPanel(Timer *timer);

	~NextionMainPanel();
	bool setSaat(uint8_t hh, uint8_t mm,
			uint8_t ss);
	bool setNextCalismaZamani(uint8_t hh, uint8_t mm,
			uint8_t ss,uint16_t sure);
	void setTimerStart();
	void setTimerStop();

	bool getStartSopButtonValue(bool *isWorking);
	bool getOtomatikAktiflikCheckBoxValue(bool *isAktif);
	void setOtomatiklikAktif(bool val);
	bool setCalismaZamani(uint16_t sure);
	bool setCalismaBilgileriVisible(bool isVisible);
	void timerStateChanged(TimerStateEvent evt) override;
	void timerActionPerformed(TimerActionEvent evt) override;
	void timeChanged(uint8_t hh, uint8_t mm, uint8_t ss) override;
	void dateChanged(uint16_t year, uint8_t month, uint8_t date) override;
	bool pageLoaded() override;

	static void startStopButtonClicked(void *ptr);
	static void aktiflikCheckBoxClicked(void *ptr);
};
//void loadPage(void *ptr);
#endif /* NEXTIONMAINPANEL_H_ */
