/*
 * PostInitializeListener.h
 *
 *  Created on: 7 Kas 2016
 *      Author: alptug
 */

#ifndef GUBRELEMENEXTION_ABSTRACTMYNEXTIONPAGE_H_
#define GUBRELEMENEXTION_ABSTRACTMYNEXTIONPAGE_H_


#include "Nextion.h"
#include "NextionConfig.h"


class AbstractMyNextionPage :public NexPage {
public:
	AbstractMyNextionPage(uint8_t pid,uint8_t cid,const char *pageName);
	virtual bool pageLoaded();//page load da sorun varsa false yoksa true dondurur
	virtual ~AbstractMyNextionPage(){

	}
	 static void loadPage(void *ptr);
//	{
//
//	 }
	  static void destroyPage(void *ptr);
//	{
//
//	}
};


#endif /* GUBRELEMENEXTION_ABSTRACTMYNEXTIONPAGE_H_ */

