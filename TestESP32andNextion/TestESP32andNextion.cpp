#include <Arduino.h>
#include"Nextion.h"

#define RXD2 16
#define TXD2 17

NexButton btn(0,1,"b0");
NexNumber num(0,2,"n0");
NexTouch *list[2]{
		&btn,NULL
};

void buttonClicked(void *ptr){
	Serial.println("Button released");
	NexButton *btn=(NexButton *)ptr;
	uint32_t number;
	bool flag = num.getValue(&number);
	number++;
	Serial.print(flag);;
	Serial.print(" value :");
	Serial.println(number);
	num.setValue(number);
}
void setup() {
	Serial.begin(115200);
	Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
	Serial.println("Setup begins");
	nexInit();
	btn.attachPop(buttonClicked, &btn);
	Serial.println("Setup ends");
}

void loop() {
nexLoop(list);
}
