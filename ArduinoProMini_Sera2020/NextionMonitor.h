/*
 * NextionMonitor.h
 *
 *  Created on: 23 Mar 2020
 *      Author: alptu
 */

#ifndef NEXTIONMONITOR_H_
#define NEXTIONMONITOR_H_
#include"Nextion.h"
#include"RecordData.h"
#include "Monitor.h"


#include<MyDebug.h>
#define NEXT_PIC_ID_WIFI_NOT_CONNECTED 0
#define NEXT_PIC_ID_WIFI_CONNECTED 1
#define NEXT_PIC_ID_SERVER_NOT_CONNECTED 2
#define NEXT_PIC_ID_SERVER_CONNECTED 3
#define NEXT_PIC_ID_CLIENT_ID_ACCEPTED 4
#define NEXT_PIC_ID_CLIENT_ID_REFUSED 5
class NextionMonitor: public Monitor {
private:
	ConnectionParams *params;

public:
	NexButton *kaydetButton;

	static void b0PopCallback(void *ptr);

	NextionMonitor(ConnectionParams *p);
	virtual ~NextionMonitor();
	bool setTemp2(double val);
	bool setTemp1(double val);
	bool setBoardTemp(double val);
	bool setTemp3(double val);
	ConnectionParams *getParams();
	bool setConectionSettings();
	void begin();
	void listenKeyboard();
	 bool setWifiConnected(bool flag);
	 bool setServerConnected(bool flag);
	  bool setClientIdAccepted(bool flag);
protected:
	NexTouch **nex_listen_list;
	void openOpeningPage();
	void openAdminPage();
	void openMainPage();
	void openSavingPage();
	void openSuccessPage();
	void openFailurePage();
	void openKeypadPage();
};
#endif /* NEXTIONMONITOR_H_ */
