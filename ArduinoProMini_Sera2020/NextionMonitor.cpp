/*
 * NextionMonitor.cpp
 *
 *  Created on: 23 Mar 2020
 *      Author: alptu
 */

#include "NextionMonitor.h"
bool setTemp(uint8_t pageId, uint8_t cmpId, const char *cmpName, double val);
void parseDouble(const double val, int precision, int *intValue,
		int *floatValue);
NextionMonitor::NextionMonitor(ConnectionParams *p) {
	// TODO Auto-generated constructor stub
	nexInit();
	sendCommand("rest");
	kaydetButton = new NexButton(2, 16, "b0");
	nex_listen_list = new NexTouch*[2];
	nex_listen_list[0] = kaydetButton;
	nex_listen_list[1] = NULL;
	this->params=p;
}

NextionMonitor::~NextionMonitor() {
	// TODO Auto-generated destructor stub
}

bool NextionMonitor::setTemp2(double val) {
	return setTemp(1, 9, "pageMain.t7", val);
}

bool NextionMonitor::setTemp1(double val) {
	return setTemp(1, 8, "pageMain.t6", val);
}

bool NextionMonitor::setBoardTemp(double val) {
	return false;
}

bool NextionMonitor::setTemp3(double val) {
	return setTemp(1, 10, "pageMain.t8", val);
}

void NextionMonitor::begin() {
	/* Set the baudrate which is for debug and communicate with Nextion screen. */

//	   b0PopCallback();
	/* Register the pop event callback function of the current button component. */

	kaydetButton->attachPush(b0PopCallback, this);

}

bool setTemp(uint8_t pageId, uint8_t cmpId, const char *cmpName, double val) {
	NexText txt(pageId, cmpId, cmpName);
	char buffer[10];
	int iVal;
	int fVal;

	parseDouble(val, 2, &iVal, &fVal);

	sprintf(buffer, "%d.%02d", iVal, fVal);
	return txt.setText(buffer);
}
void parseDouble(const double val, int precision, int *intValue,
		int *floatValue) {

	int pre = 1;
	for (int i = 0; i < precision; i++) {
		pre = pre * 10;
	}

	*intValue = (int) val;
	*floatValue = (int) ((val - *intValue) * pre);
}
void NextionMonitor::b0PopCallback(void *ptr) {
	NextionMonitor *nm = (NextionMonitor *)ptr;
	d_println(F("BUTTON CLICKED"));
	ConnectionParams *params = nm->getParams();
	uint8_t pid = 2;
	char  buffer[33];

	NexText t(pid, 11, "pageAyarlar0.tSsid");
	int size = t.getText(buffer, LEN_SSID-1);
	buffer[size] = NULL;
	d_2println(F("pageAyarlar0.tSsid : "),buffer);
	strcpy(params->SSID,buffer);


	t = NexText(pid, 12, "pageAyarlar0.tPass");
	size = t.getText(buffer, LEN_PASS-1);
	buffer[size] = NULL;
	d_2println(F("pageAyarlar0.tPass : "),buffer);
	strcpy(params->password,buffer);

	t = NexText(pid, 13, "pageAyarlar0.tIp");
	size = t.getText(buffer, LEN_IP-1);
	buffer[size] = NULL;
	d_2println(F("pageAyarlar0.tIp : "),buffer);
	strcpy(params->ip,buffer);

	t = NexText(pid, 14, "pageAyarlar0.tPort");
	size = t.getText(buffer, LEN_PORT-1);
	buffer[size] = NULL;
	d_2println(F("pageAyarlar0.tPort : "),buffer);
	strcpy(params->port,buffer);

	t = NexText(pid, 15, "pageAyarlar0.tId");
	size = t.getText(buffer, LEN_ID-1);
	buffer[size] = NULL;
	d_2println(F("pageAyarlar0.tId : "),buffer);
	strcpy(params->id,buffer);
//	delay(1000);

	nm->gotoPage(MonitorPages::SAVING_PAGE);
//	NexPage p(4,0,"pageSaving");
//	p.show();
	EEPROMRecord rec2(params);
	if(rec2.saveConnectionRecord()){
	nm->gotoPage(MonitorPages::SUCCESS_PAGE);
	}else{
		nm->gotoPage(MonitorPages::FAILURE_PAGE);
	}
//	rec.saveConnectionRecord();
//	ConnectionRecord rec2;
//	rec2.loadConnectionRecord();
//	d_2println(F("Mem : "),freeMemory());
//	if(rec.equals(rec2)){
//		NexPage p1(6,0,"pageSuccess");
//		p1.show();
//		d_println(F("Kayit basarili"));
//	}else{
//		NexPage p2(5,0,"pageError");
//		p2.show();
//		d_println(F("Kayit hatasi"));
//	}
//	m->gotoPage(rec.equals(rec2)?MonitorPages::SUCCESS_PAGE:MonitorPages::FAILURE_PAGE);
	d_println(F("button clicked end"));
}

void NextionMonitor::listenKeyboard() {
	nexLoop(nex_listen_list);
}

bool NextionMonitor::setConectionSettings() {
//	rec->loadConnectionRecord();
//	delay(1000);
	uint8_t pid = 2;
	NexText t(pid, 11, "pageAyarlar0.tSsid");
	t.setText(params->SSID);

	t = NexText(pid, 12, "pageAyarlar0.tPass");
	t.setText(params->password);

	t = NexText(pid, 13, "pageAyarlar0.tIp");
	t.setText(params->ip);

	t = NexText(pid, 14, "pageAyarlar0.tPort");
	t.setText(params->port);

	t = NexText(pid, 15, "pageAyarlar0.tId");
	t.setText(params->id);

//	d_2println(F("HAS RECORD :"), rec->hasAvailableRecord());
	d_2println(F("SSID :"), params->SSID);
	d_2println(F("Pass :"), params->password);
	d_2println(F("Ip:"), params->ip);
	d_2println(F("Port :"), params->port);
	d_2println(F("Id :"), params->id);

	return true;
}
void NextionMonitor::openOpeningPage() {
	NexPage page(0, 0, "pageOpening");
	page.show();
}

void NextionMonitor::openAdminPage() {
	NexPage page(2, 0, "pageAyarlar0");
	page.show();
}

void NextionMonitor::openMainPage() {
	NexPage page(1, 0, "pageMain");
	page.show();
}
void NextionMonitor::openSavingPage() {
	NexPage page(4, 0, "pageSaving");
	page.show();
}

void NextionMonitor::openSuccessPage() {
	NexPage page(6, 0, "pageSuccess");
	page.show();
}

void NextionMonitor::openFailurePage() {
	NexPage page(5, 0, "pageError");
	page.show();
}

void NextionMonitor::openKeypadPage() {
	NexPage page(3, 0, "pageKeypad");
	page.show();
}

bool NextionMonitor::setWifiConnected(bool flag) {
	NexPicture pic(1, 16, "p0");
	return pic.setPic(
			flag ? NEXT_PIC_ID_WIFI_CONNECTED : NEXT_PIC_ID_WIFI_NOT_CONNECTED);

}

bool NextionMonitor::setServerConnected(bool flag) {
	NexPicture pic(1, 16, "p0");
	return pic.setPic(
			flag ? NEXT_PIC_ID_SERVER_CONNECTED : NEXT_PIC_ID_SERVER_NOT_CONNECTED);
}
bool NextionMonitor::setClientIdAccepted(bool flag) {
	NexPicture pic(1, 16, "p0");
	return pic.setPic(
			flag ? NEXT_PIC_ID_CLIENT_ID_ACCEPTED : NEXT_PIC_ID_CLIENT_ID_REFUSED);
}

ConnectionParams* NextionMonitor::getParams() {
	return this->params;
}
