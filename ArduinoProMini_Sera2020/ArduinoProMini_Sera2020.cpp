// Do not remove the include below
#include "ArduinoProMini_Sera2020.h"
//#define SERIAL_DEBUG_ENABLE

MyADS1248 *pt;
Monitor *monitor;
SeraWifi *wifi;


ConnectionParams params;

double temp0, temp1, temp2, temp3;
NetworkPackage pck;
// The loop function is called in an endless loop
#define BOARD_TEMP_REQUEST_TIME 60000L
#define DATA_SENT_INTERVAL 30000L
unsigned long  lastReceiveDataTime, lastSentDataTime;
unsigned long lastBoardTempRequest;
class ADSListener: public ADSBoardListener {
public:
	void valueUpdated(ADSBoardEvent val) {
//		DataPackage sp;
//		sp.init();
//		char buffer[40];
//		size_t n = sprintf(buffer, "<1%d ", val.getType());
//		n = n + dToStr(buffer + n, val.getVal());
//		buffer[n++] = ' ';
//		n = n + dToStr(buffer + n, val.getMin());
//		buffer[n++] = ' ';
//		n = n + dToStr(buffer + n, val.getMax());
//		buffer[n++] = '>';
//		buffer[n] = '\0';
//		wifi->println(buffer);

		d_print(F("Temp reading Type:"));
		d_print(val.getType());
		d_print(F(" Value :"));
		d_println(val.getVal());
		uint16_t actCode;
		switch (val.getType()) {
		case ADSBoardSensorType::temp1Updated:
			actCode = AC_SEND_TEMP1;
//			monitor->setTemp1(val.getVal());
			temp1 = val.getVal();
			break;
		case ADSBoardSensorType::temp2Updated:
			actCode = AC_SEND_TEMP2;
//			monitor->setTemp2(val.getVal());
			temp2 = val.getVal();
			break;
		case ADSBoardSensorType::temp3Updated:
			actCode = AC_SEND_TEMP3;
//			monitor->setTemp3(val.getVal());
			temp3 = val.getVal();
			break;
		case ADSBoardSensorType::boardTempUpdated:
			actCode = AC_SEND_TEMP0;
//			monitor->setBoardTemp(val.getVal());
			temp0 = val.getVal();
			break;
		default:
			actCode = AC_INVALID_CODE;
			d_println(F("Unknown Type"))
			;
			break;
		}
		/*		if (!wifi->isServerBindEstablished())
		 return;
		 delay(20);
		 char ort[15];
		 dToStr(ort, val.getVal());
		 char *datas[] = {  ort };
		 pck.changeMessage(SERVER_ID, actCode, datas, 1);
		 sendPackage();
		 //		wifi->sendServerPackage(pck);
		 pck.reset();
		 */
	}
};

ADSListener *ptListener;

//The setup function is called once at startup of the sketch
void printParams() {
	Serial.println(params.SSID);
}
void setup() {

#if AK_DEBUG
	Serial.begin(115200);
#endif
	temp0 = 120.0;
	temp1 = 120.0;
	temp2 = 120.0;
	temp3 = 120.0;
	d_println(F("Started"));
	delay(100);
	d_2println(F("Mem :"), freeMemory());

	delay(100);
	EEPROMRecord *rec = new EEPROMRecord(&params);
//	rec->resetRecord();
	rec->loadConnectionRecord();
	delete rec;
	printParams();

//	rec.resetRecord();
	//	monitor =  new NokiaMonitor(3, 4, 5);
	monitor = new NextionMonitor(&params);

	monitor->begin();

	delay(1000);
	monitor->setConectionSettings();

	ptListener = new ADSListener();
	pt = new MyADS1248(ADS1248_RESET_PIN);
	pt->begin(ADS1248_CS_PIN, ADS1248_START_PIN, ADS1248_DRDY_PIN);

	pt->setListener(ptListener);

	wifi = new ESPWifi(&WIFI_SERIAL, 38400, ESP8266_RESET_PIN);
	wifi->connect();
// Add your initialization code here

//	pt->setNumberOfPTReading(1);

	d_2println(F("Mem :"), freeMemory());
	delay(100);

//	delay(1000);
	lastBoardTempRequest = millis();
	lastReceiveDataTime = millis();
//	lastSentData = millis();
	d_println(F("Setup ends"));
}
void loop() {
//	Serial.println(index++);

	wifi->readAll(&pck);

	if (pck.isDataCompleted()) {
		lastReceiveDataTime = millis();
//		Serial.print("pck L");
//		Serial.println((char *)(pck.data));
		d_2println(F("Package Completed Pck:"), pck.getMessage());
		doActions();
		pck.reset();
	}


	if (millis() - lastReceiveDataTime > RESET_WAIT_MS) {
		wifi->reset();
		lastReceiveDataTime = millis();
	}

	//eskiden listener icinde her data geldiginde o datayi gonderirlirdi o kod su anda yorumda
	//artik listener da datalar temp degiskenlerine kayit altina aliniyor ve belli bir zaman araliklarinda gonderiliyor o kod da bu if blogunda

	if(millis() - lastSentDataTime>=DATA_SENT_INTERVAL){
		if(wifi->isServerBindEstablished()){
			char tmp0[10],tmp1[10],tmp2[10],tmp3[10];
			dToStr(tmp0, temp0);
			dToStr(tmp1, temp1);
			dToStr(tmp2, temp2);
			dToStr(tmp3, temp3);
			char *datas[]={tmp0,tmp1,tmp2,tmp3};
			unsigned char dataMiktari = 4;
			pck.changeMessage(SERVER_ID, AC_SEND_TEMP_ALL, datas, dataMiktari);
			sendPackage();
			pck.reset();
		}
		monitor->setTemp1(temp1);
		monitor->setTemp2(temp2);
		monitor->setTemp3(temp3);
		monitor->setBoardTemp(temp0);
	}
	if (wifi->isServerBindEstablished()) {
		if (millis() - lastSentDataTime > PING_WAIT_MS) {
			wifi->sendPig();
			lastSentDataTime = millis();

		}

	}

	if (millis() - lastBoardTempRequest > BOARD_TEMP_REQUEST_TIME) {
		pt->requestBoardTemp();
		lastBoardTempRequest = millis();
		d_2println(F("Mem :"), freeMemory());

	}

	monitor->listenKeyboard();

	pt->doReading();

	delay(5);
//Add your repeated code here
}

void doActions() {
//#define LEN_SENDBUFFER 50
//	char sendBuffer[LEN_SENDBUFFER];
//	memset(sendBuffer, 0, LEN_SENDBUFFER);

//	uint8_t nextIndex = 0;
//	Serial.print(F("Buffer :"));
//	int i=0;
//	while(pck.data[i]!=NULL){
//		Serial.print("[");
//		Serial.print((int)(pck.data[i]));
//		Serial.print(":");
//		Serial.print((char)(pck.data[i++]));
//		Serial.print("],");
//	}
//	Serial.println(i);
	uint16_t actCode = 0, targetId = 0;
	uint8_t dataMiktari;
	uint16_t ac = pck.getActionCode();
	switch (ac) {

	case AC_CLIENT_CONNECTED: {
//		wifi->setConnected(true);
//		monitor->setServerConnected(true);
		return;
	}
	case AC_CLIENT_DISCONNECTED: {
		monitor->setServerConnected(false);
		wifi->reset();
		return;
	}

	case AC_FRIEND_CONNECTED: {
//		wifi->setConnected(true);
		return;
	}
	case AC_FRIEND_DISCONNECTED: {
//		wifi->setConnected(true);
		return;
	}
	case AC_SEND_TEMP_ALL:{
		actCode = ac;
		targetId = pck.getId();
		char tmp0[10], tmp1[10], tmp2[10], tmp3[10];
		dToStr(tmp0, temp0);
		dToStr(tmp1, temp1);
		dToStr(tmp2, temp2);
		dToStr(tmp3, temp3);
		char *val[] = { tmp0, tmp1, tmp2, tmp3 };
		dataMiktari = 4;
		pck.changeMessage(targetId, ac, val, dataMiktari);
		break;
	}
	case AC_SEND_TEMP0:{
		actCode = ac;
		targetId = pck.getId();
		char tmp[10];
		dToStr(tmp, temp0);
		char *val[] = { tmp };
		dataMiktari = 1;
		pck.changeMessage(targetId, ac, val, dataMiktari);
		break;
	}
	case AC_SEND_TEMP1:{
		actCode = ac;
		targetId = pck.getId();
		char tmp[10];
		dToStr(tmp, temp1);
		char *val[] = { tmp };
		dataMiktari = 1;
		pck.changeMessage(targetId, ac, val, dataMiktari);
		break;
	}
	case AC_SEND_TEMP2:{
		actCode = ac;
		targetId = pck.getId();
		char tmp[10];
		dToStr(tmp, temp2);
		char *val[] = { tmp };
		dataMiktari = 1;
		pck.changeMessage(targetId, ac, val, dataMiktari);
		break;
	}
	case AC_SEND_TEMP3:{
		actCode = ac;
		targetId = pck.getId();
		char tmp[10];
		dToStr(tmp, temp3);
		char *val[] = { tmp };
		dataMiktari = 1;
		pck.changeMessage(targetId, ac, val, dataMiktari);
		break;
	}
	case AC_REQUEST_FOCUSED_CLIENTS: {
//		wifi->setConnected(true);
		return;
	}
	case AC_SEND_BOARD_LAST_RESET_TIME: {
//		serverData = true;
		actCode = ac;
		targetId = SERVER_ID;
		char tmp[10];
		sprintf(tmp, "%lu", millis());
		char *val[] = { tmp };
		dataMiktari = 1;
		pck.changeMessage(targetId, actCode, val, dataMiktari);
		break;
	}
	case AC_BOARD_FREE_MEMORY: {
//		serverData = true;
		actCode = ac;
		targetId = SERVER_ID;
		char tmp[10];
		uint8_t s = sprintf(tmp, "%d", freeMemory());
		char *val[] = { tmp };
		dataMiktari = 1;
		pck.changeMessage(targetId, actCode, val, dataMiktari);
		break;
	}
	case AC_BOARD_SENSOR_ACTION_CODES: {
		targetId = pck.getId();
		actCode = AC_BOARD_SENSOR_ACTION_CODES;
		char tmp1[5];
		sprintf(tmp1, "%d", AC_SEND_TEMP0);
		char tmp2[5];
		sprintf(tmp2, "%d", AC_SEND_TEMP1);
		char tmp3[5];
		sprintf(tmp3, "%d", AC_SEND_TEMP2);
		char tmp4[5];
		sprintf(tmp4, "%d", AC_SEND_TEMP3);
		char *val[] = { tmp1, tmp2, tmp3, tmp4 };
		pck.changeMessage(targetId, actCode, val, 4);
		break;
	}
	case AC_SEND_PING: {
//		wifi->setConnected(true);
		return;
	}

	case AC_SEND_WIFI_SSID: {
		actCode = AC_SEND_WIFI_SSID;
		targetId = SERVER_ID;
		char *val[] = { params.SSID };
		dataMiktari = 1;
		pck.changeMessage(targetId, actCode, val, dataMiktari);
		break;
	}
	case AC_SEND_WIFI_PASS: {
		actCode = AC_SEND_WIFI_PASS;
		targetId = SERVER_ID;
		char *val[] = { params.password };
		dataMiktari = 1;
		pck.changeMessage(targetId, actCode, val, dataMiktari);
		break;
	}
	case AC_SEND_SERVER_IP: {
		actCode = AC_SEND_SERVER_IP;
		targetId = SERVER_ID;
		char *val[] = { params.ip };
		dataMiktari = 1;
		pck.changeMessage(targetId, actCode, val, dataMiktari);
		break;
	}
	case AC_SEND_SERVER_PORT: {
		actCode = AC_SEND_SERVER_PORT;
		targetId = SERVER_ID;
		char *val[] = { params.port };
		dataMiktari = 1;
		pck.changeMessage(targetId, actCode, val, dataMiktari);
		break;
	}
	case AC_SEND_CLIENT_ID:
//		wifi->setConnected(true);
		return;
	case AC_ESP_STARTED:
		wifi->setWifiConnectionEstablished(false);
		return;
	case AC_ESP_RESET:
		return;

	case AC_WIFI_CONNECTED:
		wifi->setWifiConnectionEstablished(true);
		monitor->setWifiConnected(true);
		return;
	case AC_WIFI_DISCONNECTED:
		wifi->setWifiConnectionEstablished(false);
		monitor->setWifiConnected(false);
		return;
	case AC_WIFI_CONNECTION_FAILED:
		wifi->setWifiConnectionEstablished(false);
		monitor->setWifiConnected(false);
		return;
	case AC_SERVER_CONNECTED:
//		wifi->setServerBindEstablished(true); id request de yapilacak
//		monitor->setServerConnected(true);		id request de yapilacak
		return;
	case AC_SERVER_DISCONNECTED:
		wifi->setServerBindEstablished(false);
		monitor->setServerConnected(false);
		return;
	case AC_SERVER_CONNECTION_FAILED:
		wifi->setServerBindEstablished(false);
		monitor->setServerConnected(false);
		return;
	case AC_ID_REQUESTED: {
//		wifi->setConnected(true);
//		serverData = true;
		monitor->setServerConnected(true);
		wifi->setServerBindEstablished(true);
		actCode = AC_SEND_CLIENT_ID;
		targetId = SERVER_ID;
		char *val[] = { params.id };
		dataMiktari = 1;
		pck.changeMessage(targetId, actCode, val, dataMiktari);
		break;
	}

	case AC_ID_ACCEPTED: {
		wifi->setServerBindEstablished(true);
		monitor->setClientIdAccepted(true);
		return;
	}
	case AC_ID_REFUSED:
		wifi->reset();
		monitor->setClientIdAccepted(false);
		return;
	case AC_ESP_DEBUG: {
		return;
	}
	case AC_SET_WIFI_SSID: {
		pck.getDataLine(params.SSID);
		return;
	}
	case AC_SET_WIFI_PASS: {
		pck.getDataLine(params.password);
		return;
	}
	case AC_SET_SERVER_IP: {
		pck.getDataLine(params.ip);
		return;
	}
	case AC_SET_SERVER_PORT: {
		pck.getDataLine(params.port);
		return;
	}
	case AC_SET_CLIENT_ID: {
		pck.getDataLine(params.id);
		return;
	}

	case AC_RESET_NETWORK_PARAMS_RECORD: {
		EEPROMRecord *rec = new EEPROMRecord(&params);
		rec->resetRecord();
		delete rec;
		return;
	}
	case AC_SAVE_NETWORK_PARAMS_RECORD: {
		EEPROMRecord *rec = new EEPROMRecord(&params);
		rec->saveConnectionRecord();
		delete rec;
		return;
	}
	case AC_LOAD_NETWORK_PARAMS_RECORD: {
		EEPROMRecord *rec = new EEPROMRecord(&params);
		rec->loadConnectionRecord();
		delete rec;
		return;
	}
	case AC_INVALID_CODE:
	default: {
		actCode = AC_INVALID_CODE;
		targetId = SERVER_ID;
		char tmp[10];
		sprintf(tmp, "%d", actCode);
		char *val[] = { tmp };
		dataMiktari = 1;
		pck.changeMessage(targetId, actCode, val, dataMiktari);
		break;
	}

	}
	sendPackage();
	delay(5);
	d_2println(F("Send Buffer :"), pck.getMessage());

}
void sendPackage() {
	wifi->sendServerPackage(pck);
	lastSentDataTime = millis();
}
size_t dToStr(char *ch, double val) {
	bool eksi = false;
	if (val < 0) {
		eksi = true;
		val = val * -1;
	}
	int i = (int) val;
	val = val - (double) i;
	val = val * 100;
	int f = (int) val;
	if (eksi) {
		return sprintf(ch, "-%d.%02d", i, f);
	} else {
		return sprintf(ch, "%d.%02d", i, f);
	}
}

