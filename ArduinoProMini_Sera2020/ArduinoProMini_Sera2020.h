/*
 * ArduinoProMiniSera.h
 *
 *  Created on: 1 May 2020
 *      Author: alptu
 */

#ifndef ARDUINOPROMINI_SERA2020_H_
#define ARDUINOPROMINI_SERA2020_H_


#include <Arduino.h>
#include"MyADS1248.h"
//#include"NokiaMonitor.h"
#include"SoftwareSerial.h"
#include"NextionMonitor.h"
#include"MyDebug.h"

#include"ActionCode.h"
#include"RecordData.h"
#include"GeneralSystemConstants.h"
#include"NetworkPackage/NetworkPackage.h"
#include"ESPWifi.h"
//add your function definitions for the project SeraProjectWithProMini3_3 here

//#define MEGA
#define PRO_MINI_BOARD_1
//#define PRO_MINI_BOARD_2

#ifdef MEGA

uint8_t ADS1248_START_PIN = 3;
uint8_t ADS1248_CS_PIN = 53;
uint8_t ADS1248_DRDY_PIN = 2;
int8_t ADS1248_RESET_PIN = A1;
int8_t ESP8266_RESET_PIN = 4;
#define WIFI_SERIAL Serial2
#endif

#define WIFI_SERIAL Serial
#ifdef PRO_MINI_BOARD_1
SoftwareSerial HMISerial(7, 8);
uint8_t ADS1248_START_PIN = A0;
uint8_t ADS1248_CS_PIN = 10;
uint8_t ADS1248_DRDY_PIN = 2;
int8_t ADS1248_RESET_PIN = A1;
int8_t ESP8266_RESET_PIN = 9;
#endif

#ifdef PRO_MINI_BOARD_2
SoftwareSerial HMISerial(5, 6);
uint8_t ADS1248_START_PIN = A0;
uint8_t ADS1248_CS_PIN = 10;
uint8_t ADS1248_DRDY_PIN = 2;
int8_t ADS1248_RESET_PIN = A1;
int8_t ESP8266_RESET_PIN = 4;
#endif

void doActions();
size_t dToStr(char *ch, double val);
void sendPackage();
//Do not add code below this line



#endif /* ARDUINOPROMINI_SERA2020_H_ */
