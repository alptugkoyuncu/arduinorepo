// Do not remove the include below
#include "GubreHazirlama.h"

const byte ROWS = 5; //four rows
const byte COLS = 4; //three columns
uint8_t index = 0;
#define RTD_CS_PIN 10

char keys[ROWS][COLS] = { { 'A', 'B', '#', '*' }, { '1', '2', '3', ust }, { '4',
		'5', '6', alt }, { '7', '8', '9', esc }, { sol, '0', sag, ent } };
byte rowPins[ROWS] = { 3, 4, 5, 6, 7 }; //connect to the row pinouts of the keypad
byte colPins[COLS] = { A0, A1, A2, A3 }; //connect to the column pinouts of the keypad

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);
/*
 char keys[]{
 'A','B','#','*',
 '1','2','3',ust,
 '5','6','7',alt,
 '7','8','9',esc,
 sag,'0',sol,ent
 };
 OnewireKeypad <Print, 16 > keypad(Serial, keys, 5, 4, A1, 4700, 1000 );
 */
//The setup function is called once at startup of the sketch
Ekran *e;
PT1000 *rtd;
void printMemory() {

	Serial.print(F("Memory :"));
	Serial.println(freeMemory());
}
void setup() {
	Serial.begin(115200);

	temp = (double *) malloc(sizeof(double));
	*temp = 10;
	e = new Ekran();
	Serial.println(F("Setup begins"));
	Task *t;
	rtd = new PT1000(RTD_CS_PIN,RTD_4_WIRE);
	printMemory();
	t = new IsitmaTask();
	((IsitmaTask *) t)->setBitisSicaklik(90);
	printMemory();
	to->addNewTask(t);
	printMemory();
	t = new ZamanTask();
	((ZamanTask *) t)->setSure(10000);
	to->addNewTask(t);
	t = new SogutmaTask();
	((SogutmaTask *) t)->setBitisSicaklik(30);
	to->addNewTask(t);
	t = new ZamanTask();
	((ZamanTask *) t)->setSure(3000);
	to->addNewTask(t);

	t = new IsitmaTask();
	((IsitmaTask *) t)->setBitisSicaklik(70);
	to->addNewTask(t);
	t = new IsiSabitleme();
	((IsiSabitleme *) t)->setBitisSicaklik(120);
	((IsiSabitleme*) t)->setSure(10000);
	to->addNewTask(t);
	t = NULL;
	e->showMainScreen();
	delay(1000);
	// Add your initialization code here
}

// The loop function is called in an endless loop
void loop() {
	if (to->check()) {
		e->showMainScreen();
	}
	char key = keypad.getKey();
//	char key = keypad.Getkey();
	if (key != NO_KEY) {
		Serial.print(F("Pressed "));
		Serial.println(key);
		e->readKey(key);
		printMemory();
	}

	delay(10);
	if(!rtd->checkForNewReading())
		return;
	if(rtd->hasFault()){
		Serial.println(F("pt okuma hatasi"));
	}else{
		if(rtd->getTemp()!=*temp){
			*temp = rtd->getTemp();
			e->tempUpdated();
		}
	}
//Add your repeated code here
}

