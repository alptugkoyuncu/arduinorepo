/*
 * Ekran.h
 *
 *  Created on: 17 Haz 2018
 *      Author: alptug
 */

#ifndef EKRAN_H_
#define EKRAN_H_
#include"LiquidCrystal_I2C.h"
#include"TaskOrganizer.h"

extern const unsigned char sol;// = 127;
extern const unsigned char sag;// = 126;
extern const unsigned char ust;// = 1;
extern const unsigned char alt;// = 2;
extern const unsigned char ent;// = 3;
extern const unsigned char esc;// = 4;
extern const unsigned char x;// = 5;
extern const unsigned char tick;// = 6;
extern const unsigned char ustc[8];// = { 0x04, 0x0E, 0x15, 0x04, 0x04, 0x04, 0x04, 0x00 };
extern const unsigned char altc[8];// = { 0x04, 0x04, 0x04, 0x04, 0x15, 0x0E, 0x04, 0x00 };
extern const unsigned char entc[8];// = { 0x01, 0x01, 0x05, 0x09, 0x1F, 0x08, 0x04, 0x00 };
extern const unsigned char escc[8];// = { 0x0F, 0x01, 0x01, 0x0F, 0x01, 0x01, 0x0F, 0x00 };
 extern uint8_t xc[8];// = { 0x00, 0x11, 0x0A, 0x04, 0x0A, 0x11, 0x00, 0x00 };
extern uint8_t tickc[8];// = { 0x1F, 0x00, 0x01, 0x02, 0x14, 0x08, 0x00, 0x1F };

extern LiquidCrystal_I2C *lcd;
extern double *temp;
extern void clearLine(uint8_t lineNumber);
extern TaskOrganizer *to;

class Ekran {
private:
	enum EkranDurumu {
		null,
		ANA_EKRAN,
		PROGRAM_LISTE_EKRANI,
		PROGRAM_ISLEM_EKRANI,
		GOREV_SECME_EKRANI,
		INDEX_AYARLAMA_EKRANI,
		SICAKLIK_AYARLAMA_EKRANI,
		SURE_SECME_EKRANIM,
		KARISTIRMA_SECME_EKRANI,
		SONLANDIRMA_EKRANI,
		BILGI_EKRANI
	}
	;
	static EkranDurumu ekranDurumu, targetEkranDurumu;
public:
	class AnaEkran {
	public:
		void updateTempandToInfo(bool clear = true);
		void updateTaskName(bool clear = true);
		void updateTaskInfo(bool clear = true);
	};
	class MenuEkrani {
#define ITEMS_PER_PAGE 4
	private:
		uint8_t selecetedIndex;
		uint8_t minShowingIndex;
		uint8_t maximumShowingIndex;
	public:
		MenuEkrani();
		void showProgram();
		void nextItem();
		void prevItem();
		void drowItemIcon();
		void removeItemIcon();
		uint8_t getSelectedIndex() {
			return this->selecetedIndex;
		}
	};
	class PrograIslem {
	private:
		uint8_t listIndex;

		Task *newTask;
	public:
		PrograIslem();
		void programIslemEkrani(uint8_t index);
		void gorevSecmeEkrani();
		void indexAyarlamaSayfasi();
		void sicaklikSecimi();
		void sureSecimi();
		void karistimaSecimi();
		void sonlandirmaEkrani();
		void readKey(uint8_t keypadKey);
		void bilgiMesaji();
	};

	AnaEkran a;
	MenuEkrani m;
	PrograIslem p;
	Ekran() {
		lcd->begin();
		lcd->backlight();
		lcd->clear();
		lcd->createChar(x, xc);
		lcd->createChar(tick,tickc);
		lcd->home();
	}
	void readKey(uint8_t keypadKey);
	//true next item,
	//	false prev item
	void selectNextItem(bool flag = true);
	void showMainScreen();
	void tempUpdated() {
		if (ekranDurumu == EkranDurumu::ANA_EKRAN)
			a.updateTempandToInfo(true);
	}

};
#endif /* EKRAN_H_ */
