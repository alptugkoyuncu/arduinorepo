/*
 * TaskList.h
 *
 *  Created on: 15 Haz 2018
 *      Author: alptug
 */

#ifndef TASKORGANIZER_H_
#define TASKORGANIZER_H_
#include"Arduino.h"

#define TASK_NO_PIN 255

#define ISITICI_PIN 2
#define SOGUTUCU_PIN 3

extern void declarePins();
extern void startIsitici();
extern void stopIsitici();
extern void startsogutucu();
extern void stopSogutucu();
extern void startKaristirici();
extern void stopKaristirici();

enum Class {
	NOT_DECLARED, ISITMA, SOGUTMA, SICAKLIK_TUTMA, ZAMAN
};
class Task {
private:
	unsigned long startTime;bool karistiriciAktif;
	char name[15];

protected:
	bool working;
public:
	Task(const char *n) {
		startTime = 0;
		this->karistiriciAktif = false;
		working = false;
		strcpy(name, n);
	}
	void startTimer() {
		this->startTime = millis();
	}
	unsigned long getBaslamaZamani() {
		return this->startTime;
	}

	bool isKaristiriciAktif() const {
		return karistiriciAktif;
	}

	void setKaristiriciAktif(bool karistiriciAktif) {
		this->karistiriciAktif = karistiriciAktif;
	}
	const char *getName() const {
		return name;
	}
	virtual Class getClass()=0;

	virtual void start()=0;
	virtual void stop()=0;
	virtual bool check(void *value = NULL)=0;
	~Task() {

	}

	bool isWorking() const {
		return working;
	}
};
class IsiSabitleme: public Task {
private:
	int bitisSicaklik;
	uint16_t sure;
	unsigned long sicaklikSabitlendi;
public:
	IsiSabitleme() :
			Task("Sicaklik Tutma") {
		bitisSicaklik = 200;
		sure = 0;
		sicaklikSabitlendi = 0;

	}
	Class getClass() {
		return Class::SICAKLIK_TUTMA;
	}
	void start() {
		this->startTimer();
		sicaklikSabitlendi = 0;
		working = true;
		if (this->isKaristiriciAktif())
			startKaristirici();
	}
	void stop() {
		working = false;
		if (this->isKaristiriciAktif())
			stopKaristirici();
	}
	bool check(void *sicaklik) {
		int8_t *t = (int8_t *) sicaklik;

		if (this->getBitisSicaklik() > *t) {
			stopSogutucu();
			startIsitici();
		} else if (this->getBitisSicaklik() < *t) {
			stopIsitici();
			startsogutucu();
		} else {
			stopIsitici();
			stopSogutucu();
			if (sicaklikSabitlendi != 0) {
				unsigned long t = millis() - sicaklikSabitlendi;
				if (t >= sure) {
					this->stop();
					return true;
				}
			} else {
				sicaklikSabitlendi = millis();
			}
		}
		return false;
	}

	int getBitisSicaklik() const {
		return bitisSicaklik;
	}

	void setBitisSicaklik(int bitisSicaklik) {
		this->bitisSicaklik = bitisSicaklik;
	}

	uint16_t getSure() const {
		return sure;
	}

	void setSure(uint16_t sure) {
		this->sure = sure;
	}
};
class SogutmaTask: public Task {
private:
	int bitisSicaklik;
public:
	SogutmaTask() :
			Task("Sogutma") {
		bitisSicaklik = 0;
	}
	void setBitisSicaklik(int bitisSicaklik) {
		this->bitisSicaklik = bitisSicaklik;
	}
	Class getClass() {
		return Class::SOGUTMA;
	}
	void start() {
		stopIsitici();
		startsogutucu();
		startTimer();
		if (this->isKaristiriciAktif())
			startKaristirici();
		working = true;
	}
	void stop() {
		stopSogutucu();
		if (this->isKaristiriciAktif())
			stopKaristirici();
		working = false;
	}
	bool check(void *sicaklik) {
		int8_t *t = (int8_t *) sicaklik;
		if (*t <= bitisSicaklik) {
			this->stop();
			return true;
		}
		return false;
	}

	int getBitisSicaklik() const {
		return bitisSicaklik;
	}
};

class IsitmaTask: public Task {
private:
	int bitisSicaklik;
public:
	IsitmaTask() :
			Task("Isitma") {
		bitisSicaklik = 127;
	}
	void setBitisSicaklik(int bitisSicaklik) {
		this->bitisSicaklik = bitisSicaklik;
	}
	Class getClass() {
		return Class::ISITMA;
	}
	void start() {
		stopSogutucu();
		startIsitici();
		startTimer();
		if (this->isKaristiriciAktif())
			startKaristirici();
		working = true;
	}
	void stop() {
		stopIsitici();
		if (this->isKaristiriciAktif())
			stopKaristirici();
		working = false;
	}

	bool check(void *sicaklik) {
		int8_t *t = (int8_t *) sicaklik;
		if (*t >= bitisSicaklik) {
			this->stop();
			return true;
		}
		return false;
	}

	int getBitisSicaklik() const {
		return bitisSicaklik;
	}
};

class ZamanTask: public Task {
private:
	uint16_t sure;
public:
	ZamanTask() :
			Task("BEKLETME") {
		sure = 0;
	}

	Class getClass() {
		return Class::ZAMAN;
	}
	void start() {
		startTimer();
		if (this->isKaristiriciAktif())
			startKaristirici();
		working = true;
	}
	void stop() {
		if (this->isKaristiriciAktif()) {
			stopKaristirici();
		}
		working = false;
	}
	bool check(void *value) {
		unsigned long t = millis() - this->getBaslamaZamani();
		if (t >= sure) {
			this->stop();
			return true;
		}
		return false;
	}

	void setSure(uint16_t sure) {
		this->sure = sure;
	}

	uint16_t getSure() const {
		return sure;
	}
};

#define WAIT_SECOND_BETWEEN_TASKS 3000
#define TASK_LIST_MAXIMUM_SIZE 15
enum TaskOrganizerEvent {
	NO_EVENT,
	TASK_STARTED,
	TASK_COMPLETED,
	TASK_CANCELED,
	NEW_ITEM_ADDED,
	NEW_ITEM_REMOVED
};

class TaskOrganizer {
private:
	uint8_t size;
	unsigned long waitForStart;
	uint8_t currIndex;
	int8_t temp2;
	Task *list[TASK_LIST_MAXIMUM_SIZE];
	TaskOrganizerEvent to_event;bool programStarted;
public:
	TaskOrganizer() {
		programStarted = false;
		currIndex = 0;
		size = 0;
		temp2 = 0;
		waitForStart = 0;
		to_event = TaskOrganizerEvent::NO_EVENT;
	}
	void start();
	void stop();
	void addNewTask(Task *t, int8_t targetIndex = -1);
	Task *removeTask(uint8_t i);bool check();
	Task *getCurrentWorkingTask();
	void removeAll();
	uint8_t getCurrIndex() const {
		return currIndex;
	}

	Task* getTaskFromList(uint8_t index) {

		return index < size ? list[index] : NULL;
	}

	uint8_t getSize() const {
		return size;
	}

	int8_t getTemp() const {
		return temp2;
	}

	void setTemp(int8_t temp2) {
		this->temp2 = temp2;
	}

	TaskOrganizerEvent getToEvent() const {
		return to_event;
	}
	void resetEvent() {
		this->to_event = TaskOrganizerEvent::NO_EVENT;
	}

	bool isProgramStarted() const {
		return programStarted;
	}
};

#endif /* TASKORGANIZER_H_ */
