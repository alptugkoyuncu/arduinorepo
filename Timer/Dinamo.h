/*
 * Dinamo.h
 *
 *  Created on: 24 Haz 2015
 *      Author: WIN7
 */

#ifndef DINAMO_H_
#define DINAMO_H_

#include"Timer.h"

class Dinamo{
private:
	unsigned char LOG;
	bool calisiyor;
	unsigned long baslamaSuresi;
	unsigned long umulanBitisSuresi;
	unsigned long calismaSuresi;
	long toplamHataPayi;//+ deger acmada kayan milisaniye -  deger kapatmada kayan milisaniyedir
	void log(String c,unsigned long val);
	void log(String c);
	unsigned long getFark(unsigned long b,unsigned long k);
public:
	Dinamo();
    void reset();
    bool isCalisiyor();
	void calistir(unsigned long calismaSuresi);
	bool isWorkEnough();
	unsigned int durdur(); //geriye calisma suresini saniye olarak dondurur
	unsigned long getCalismaSuresi(); //milisaniye olarak dondurur
//    long getToplamHataPayi();
};



#endif /* DINAMO_H_ */
