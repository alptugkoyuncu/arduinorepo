/*
 * BasicClock.h
 *
 *  Created on: 7 Tem 2015
 *      Author: WIN7
 */

#ifndef BASICCLOCK_H_
#define BASICCLOCK_H_



class BasicClock{
private:
	unsigned char min;
	unsigned char sec;
	unsigned char hour;
	unsigned char day;
    void increaseSec();
public:
	BasicClock();
	void checkValue();
	unsigned char getMinute();
	unsigned char getSecond();
	unsigned char gethour();
	unsigned char getDay();
};

#endif /* BASICCLOCK_H_ */
