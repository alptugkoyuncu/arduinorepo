/*
 * Dinamo.cpp
 *
 *  Created on: 24 Haz 2015
 *      Author: WIN7
 */

#include"Dinamo.h"
#include<assert.h>

Dinamo::Dinamo() {
	this->LOG = ARDUINO_PRINT_DATA;
	this->baslamaSuresi = 0;
	this->calismaSuresi = 0;
	this->toplamHataPayi = 0;
	this->calisiyor = false;
	this->umulanBitisSuresi = 0;
}
void Dinamo::calistir(unsigned long sure) {
	this->log("Calismaya basladi : ", sure);
	this->baslamaSuresi = millis();
	this->umulanBitisSuresi = this->baslamaSuresi + sure;
	digitalWrite(ACMA_PINI, HIGH);
	digitalWrite(LED, HIGH);
	this->calisiyor = true;
}

unsigned int Dinamo::durdur() {
	if (!this->isCalisiyor())
		return this->calismaSuresi;
	unsigned long bitis = millis();
	this->calisiyor = false;
	unsigned long sure;

	digitalWrite(ACMA_PINI, LOW);
	digitalWrite(LED, LOW);
	if (((long) this->baslamaSuresi < 0) && ((long) bitis >= 0))
		sure = (long) bitis - (long) this->baslamaSuresi;
	else
		sure = bitis - this->baslamaSuresi;
	this->calismaSuresi = sure;
	this->log("Calisma bitti : ", this->getCalismaSuresi());
	return int(sure / 1000);
}

unsigned long Dinamo::getCalismaSuresi() {
	if (this->isCalisiyor()) {
		unsigned long bitis = millis();
		return getFark(bitis, this->baslamaSuresi);
	} else {
		return this->calismaSuresi;
	}
}
unsigned long Dinamo::getFark(unsigned long b, unsigned long k) {
	if ((long) b >= 0 && (long) k < 0)
		return b - (long) k;
	else
		return b - k;

}
bool Dinamo::isCalisiyor() {
	return this->calisiyor;
}
bool Dinamo::isWorkEnough() {
	return this->getCalismaSuresi()
			>= this->umulanBitisSuresi - this->baslamaSuresi;
}
void Dinamo::log(String c, unsigned long val) {
	if (LOGGING_AVAIBLE) {
		Serial.print(this->LOG);
		Serial.print(c);
		const int n = snprintf(NULL, 0, "%lu", val);
		assert(n > 0);
		char buf[n + 1];
		int c = snprintf(buf, n + 1, "%lu", val);
		assert(buf[n] == '\0');
		assert(c == n);
		Serial.println(buf);
		Serial.flush();
	}
}
void Dinamo::log(String c) {
	if (LOGGING_AVAIBLE) {
		Serial.print(this->LOG);
		Serial.println(c);
		Serial.flush();
	}
}
void Dinamo::reset() {
	this->LOG = ARDUINO_PRINT_DATA;
	this->baslamaSuresi = 0;
	this->calismaSuresi = 0;
	this->toplamHataPayi = 0;
	this->calisiyor = false;
	this->umulanBitisSuresi = 0;

}
