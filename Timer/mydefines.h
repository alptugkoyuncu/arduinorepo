/*
 * mydefines.h
 *
 *  Created on: 24 Haz 2015
 *      Author: WIN7
 */

#ifndef MYDEFINES_H_
#define MYDEFINES_H_


//ARDUINO DAN GIDEN DATALAR ICIN HEADERLAR
#define ARDUINO_PRINT_DATA 1
#define ARDUINO_INFO_DATA 2
#define ARDUINO_RESPOND_DATA 3
#define ARDUINO_STOP 4
//ANDROIDDEN GELEN DATALAR ICIN HEADERLAR
#define ANDROID_START 5
#define ANDROID_STOP 6
#define ANDROID_TIMELESS_START 7


//PINLER
#define ACMA_PINI 3
#define LED 13

#define DEBUG false
#define LOGGING_AVAIBLE false
#endif /* MYDEFINES_H_ */
