// Do not remove the include below

#include "Dinamo.h"
Dinamo p;
unsigned int byteToInt(unsigned char b[], int start);
unsigned char *toByte(int n);
int size;
unsigned char data[5];
int flag;
void setup() {
	pinMode(LED, OUTPUT);
	pinMode(ACMA_PINI, OUTPUT);
	p.reset();
	Serial.begin(115200);
	size = 0;

	// Add your initialization code here
}

// The loop function is called in an endless loop
void loop() {
//Add your repeated code here
	if (Serial.available()) {
		unsigned char b = Serial.read();
		if (DEBUG)
			b -= 48;
		if (size == 0) {
			flag = b;
		}

		switch (flag) {
		case ANDROID_START:
			if (size != 0) {
				data[size - 1] = b;
			}
			if (size == 4) {
				unsigned int a = byteToInt(data, 0);
				unsigned long t = a * (unsigned long) 1000;
				p.calistir(t);
				size = 0;
			} else {
				size++;
			}
			break;
		case ANDROID_STOP:
			long time = millis();
			int val = p.durdur();
			unsigned char *t = toByte(val);
			const char data[5] = { ARDUINO_STOP, t[0], t[1], t[2], t[3] };
			size = 0;
			for (int i = 0; i < 5; i++){
				Serial.write(data);
			}
			delay(10);
			Serial.flush();
			break;
		}
	}
	if (p.isCalisiyor() && p.isWorkEnough()) {
		int val = p.durdur();
		unsigned char *t = toByte(val);
		const char data[] = { ARDUINO_STOP, t[0], t[1], t[2], t[3] };
		for (int i = 0; i < 5; i++){
			Serial.write(data[0]);//  .write(data[i]);
		}
		delay(10);
		Serial.flush();
	}
}

unsigned char *toByte(int n) {
	unsigned char bytes[4];

	bytes[0] = (n >> 24) & 0xFF;
	bytes[1] = (n >> 16) & 0xFF;
	bytes[2] = (n >> 8) & 0xFF;
	bytes[3] = n & 0xFF;
	return bytes;
}

unsigned int byteToInt(unsigned char b[], int start) {
	if (b == NULL)
		return 0x0;
	// ----------
	return (unsigned int) ( // NOTE: type cast not necessary for int
	(0xff & b[0]) << 24 | (0xff & b[1]) << 16 | (0xff & b[2]) << 8
			| (0xff & b[3]) << 0);
}
