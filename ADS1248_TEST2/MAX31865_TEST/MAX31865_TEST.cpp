// Do not remove the include below
#include "MAX31865_TEST.h"

#define DIGITAL_LOW_PASS_ACTIVE
/***************************************************************************
 * File Name: serial_MAX31865.h
 * Processor/Platform: Arduino Uno R3 (tested)
 * Development Environment: Arduino 1.0.5
 *
 * Designed for use with with Playing With Fusion MAX31865 Resistance
 * Temperature Device (RTD) breakout board: SEN-30202 (PT100 or PT1000)
 *   ---> http://playingwithfusion.com/productview.php?pdid=25
 *   ---> http://playingwithfusion.com/productview.php?pdid=26
 *
 * Copyright � 2014 Playing With Fusion, Inc.
 * SOFTWARE LICENSE AGREEMENT: This code is released under the MIT License.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * **************************************************************************
 * REVISION HISTORY:
 * Author			Date		Comments
 * J. Steinlage			2014Feb17	Original version
 * J. Steinlage                  2014Aug07       Reduced SPI clock to 1MHz - fixed occasional missing bit
 *                                               Fixed temp calc to give only unsigned resistance values
 *                                               Removed unused #defines for chip config (they're hard coded)
 *
 * Playing With Fusion, Inc. invests time and resources developing open-source
 * code. Please support Playing With Fusion and continued open-source
 * development by buying products from Playing With Fusion!
 *
 * **************************************************************************
 * ADDITIONAL NOTES:
 * This file configures then runs a program on an Arduino Uno to read a 2-ch
 * MAX31865 RTD-to-digital converter breakout board and print results to
 * a serial port. Communication is via SPI built-in library.
 *    - Configure Arduino Uno
 *    - Configure and read resistances and statuses from MAX31865 IC
 *      - Write config registers (MAX31865 starts up in a low-power state)
 *      - RTD resistance register
 *      - High and low status thresholds
 *      - Fault statuses
 *    - Write formatted information to serial port
 *  Circuit:
 *    Arduino Uno   Arduino Mega  -->  SEN-30201
 *    CS0: pin  9   CS0: pin  9   -->  CS, CH0
 *    CS1: pin 10   CS1: pin 10   -->  CS, CH1
 *    MOSI: pin 11  MOSI: pin 51  -->  SDI (must not be changed for hardware SPI)
 *    MISO: pin 12  MISO: pin 50  -->  SDO (must not be changed for hardware SPI)
 *    SCK:  pin 13  SCK:  pin 52  -->  SCLK (must not be changed for hardware SPI)
 *    GND           GND           -->  GND
 *    5V            5V            -->  Vin (supply with same voltage as Arduino I/O, 5V)
 ***************************************************************************/

// the sensor communicates using SPI, so include the hardware SPI library:
#include <SPI.h>
// include Playing With Fusion MAX31865 libraries
#include <PlayingWithFusion_MAX31865.h>              // core library
#include <PlayingWithFusion_MAX31865_STRUCT.h>       // struct library
#include <Nextion.h>
// CS pin used for the connection with the sensor
// other connections are controlled by the SPI library)
const int CS0_PIN = 9;
const int CS1_PIN = 10;

PWFusion_MAX31865_RTD rtd_ch0(CS0_PIN);
PWFusion_MAX31865_RTD rtd_ch1(CS1_PIN);

// structures for holding RTD values
static struct var_max31865 RTD_CH0;
struct var_max31865 *rtd_ptr;
double digitalLowPass(double last_smoothed, double new_value, double filterVal);
double lastVal;
double getTemp(double resistance);
void setNextionText(double temp, NexText *ptr);
void setNextionText2(double temp, NexText *ptr);
void setNextionText3(uint16_t val, NexText *ptr);
NexText t0(0, 1, "t0");
NexText t1(0, 2, "t1");
void setNextionUpdated();
unsigned long lastUpdated;
double getM();
double getC();
double getM2();
double getC2();
double getCalibratedTemp(double raw);
void setup() {
//	Serial.begin(115200);
	// setup for the the SPI library:
	SPI.begin();                            // begin SPI
	SPI.setClockDivider(SPI_CLOCK_DIV128); // SPI speed to SPI_CLOCK_DIV16 (1MHz)
	SPI.setDataMode(SPI_MODE1);             // MAX31865 works in MODE1 or MODE3

	// initalize the chip select pin
	pinMode(CS0_PIN, OUTPUT);
//  pinMode(CS1_PIN, OUTPUT);

	nexInit();
	Serial.println("Starting in 5 seconds....");
	delay(5000);
	// configure channel 0
	struct var_max31865 *rtd_ptr;
	rtd_ptr = &RTD_CH0;
	rtd_ch0.MAX31865_config(rtd_ptr, RTD_4_WIRE);
//  RTD_CH0.RTD_type = 1;                         // un-comment for PT100 RTD
	RTD_CH0.RTD_type = 2;                        // un-comment for PT1000 RTD

//  // configure channel 1
//  rtd_ptr = &RTD_CH1;
//  rtd_ch1.MAX31865_config(rtd_ptr, RTD_3_WIRE);
////  RTD_CH1.RTD_type = 1;                         // un-comment for PT100 RTD
//   RTD_CH0.RTD_type = 2;                        // un-comment for PT1000 RTD

	do {
		// give the sensor time to set up

		delay(3000);          // Update MAX31855 readings
		rtd_ptr = &RTD_CH0;
		rtd_ch0.MAX31865_full_read(rtd_ptr);
		// calculate RTD resistance
		lastVal = (double) RTD_CH0.rtd_res_raw;
		Serial.println(lastVal);
		Serial.print("Rrtd = ");                 // print RTD resistance heading
		Serial.print(lastVal);                        // print RTD resistance

		Serial.println(" ohm");
		// calculate RTD temperature (simple calc, +/- 2 deg C from -100C to 100C)
		// more accurate curve can be used outside that range;
		// print RTD temperature heading

		setNextionText2(lastVal, &t0);
		setNextionText2(lastVal, &t1);
	} while ((0 != RTD_CH0.status)); // end of no-fault handling

}

void loop() {

	delay(2000);                           // 1500ms delay... can be much faster

	rtd_ptr = &RTD_CH0;
	rtd_ch0.MAX31865_full_read(rtd_ptr);          // Update MAX31855 readings

	if (0 == RTD_CH0.status)              // no fault, print info to serial port
			{
		// calculate RTD resistance
		double newVal = (double) RTD_CH0.rtd_res_raw;
#ifdef  DIGITAL_LOW_PASS_ACTIVE
		newVal = digitalLowPass(lastVal, newVal, 0.9);
		lastVal = newVal;
#endif

		setNextionText2(getCalibratedTemp(newVal), &t1);
		setNextionText2(newVal, &t0);
		setNextionUpdated();
	}  // end of no-fault handling
	else {
		Serial.print("RTD Fault, register: ");
		Serial.print(RTD_CH0.status);
		if (0x80 & RTD_CH0.status) {
			Serial.println("RTD High Threshold Met"); // RTD high threshold fault
		} else if (0x40 & RTD_CH0.status) {
			Serial.println("RTD Low Threshold Met");  // RTD low threshold fault
		} else if (0x20 & RTD_CH0.status) {
			Serial.println("REFin- > 0.85 x Vbias");   // REFin- > 0.85 x Vbias
		} else if (0x10 & RTD_CH0.status) {
			Serial.println("FORCE- open"); // REFin- < 0.85 x Vbias, FORCE- open
		} else if (0x08 & RTD_CH0.status) {
			Serial.println("FORCE- open"); // RTDin- < 0.85 x Vbias, FORCE- open
		} else if (0x04 & RTD_CH0.status) {
			Serial.println("Over/Under voltage fault"); // overvoltage/undervoltage fault
		} else {
			Serial.println("Unknown fault, check connection"); // print RTD temperature heading
		}
	}  // end of fault handling

	// ******************** Print RTD 1 Information ********************
//  Serial.println("RTD Sensor 1:");              // Print RTD0 header
//
//  if(0 == RTD_CH1.status)                       // no fault, print info to serial port
//  {
//    if(1 == RTD_CH1.RTD_type)                   // handle values for PT100
//    {
//      // calculate RTD resistance
//      tmp = (double)RTD_CH1.rtd_res_raw * 400 / 32768;
//      Serial.print("Rrtd = ");                  // print RTD resistance heading
//      Serial.print(tmp);                        // print RTD resistance
//    }
//    else if(2 == RTD_CH1.RTD_type)              // handle values for PT1000
//    {
//      // calculate RTD resistance
//      tmp = (double)RTD_CH1.rtd_res_raw * 4000 / 32768;
//      Serial.print("Rrtd = ");                  // print RTD resistance heading
//      Serial.print(tmp);                        // print RTD resistance
//    }
//    Serial.println(" ohm");
//    // calculate RTD temperature (simple calc, +/- 2 deg C from -100C to 100C)
//    // more accurate curve can be used outside that range
//    tmp = ((double)RTD_CH1.rtd_res_raw / 32) - 256;
//    Serial.print("Trtd = ");                    // print RTD temperature heading
//    Serial.print(tmp);                          // print RTD resistance
//    Serial.println(" deg C");                   // print RTD temperature heading
//  }  // end of no-fault handling
//  else
//  {
//    Serial.print("RTD Fault, register: ");
//    Serial.print(RTD_CH1.status);
//    if(0x80 & RTD_CH1.status)
//    {
//      Serial.println("RTD High Threshold Met");  // RTD high threshold fault
//    }
//    else if(0x40 & RTD_CH1.status)
//    {
//      Serial.println("RTD Low Threshold Met");   // RTD low threshold fault
//    }
//    else if(0x20 & RTD_CH1.status)
//    {
//      Serial.println("REFin- > 0.85 x Vbias");   // REFin- > 0.85 x Vbias
//    }
//    else if(0x10 & RTD_CH1.status)
//    {
//      Serial.println("FORCE- open");             // REFin- < 0.85 x Vbias, FORCE- open
//    }
//    else if(0x08 & RTD_CH1.status)
//    {
//      Serial.println("FORCE- open");             // RTDin- < 0.85 x Vbias, FORCE- open
//    }
//    else if(0x04 & RTD_CH1.status)
//    {
//      Serial.println("Over/Under voltage fault");  // overvoltage/undervoltage fault
//    }
//    else
//    {
//      Serial.println("Unknown fault, check connection"); // print RTD temperature heading
//    }
//  }  // end of fault handling

}
double digitalLowPass(double last_smoothed, double new_value,
		double filterVal) {
	double smoothed = (new_value * (1 - filterVal))
			+ (last_smoothed * filterVal);
	return smoothed;
}
double getTemp(double res) {
	return (res / 32) - 256;
//	double val = res-1000;
//	return val/3.851;
}
void setNextionText(double temp, NexText *ptr) {
	int val1 = (int) temp;
	int val2 = ((int) (temp * 100)) % 100;
	char buffer[10];
	sprintf(buffer, "%d.%02d", val1, val2);

	ptr->setText(buffer);
	delay(100);
}
void setNextionText2(double temp, NexText *ptr) {
	uint32_t val = temp;
	uint32_t val2 = ((uint32_t) (temp * 10)) % 10;
	char buffer[10];
	sprintf(buffer, "%lu.%lu", val, val2);
	ptr->setText(buffer);
	delay(100);
}
void setNextionText3(uint16_t val, NexText *ptr) {
	char buffer[10];
	sprintf(buffer, "%d", val);

	ptr->setText(buffer);
	delay(100);
}
unsigned long updated = 0;
void setNextionUpdated() {
	NexNumber n0(0, 3, "n0");
	n0.setValue(updated++);
}
double getM() {
	return 30.6 / 976.0;
}
double getC() {
	return 8200 * getM();
}
double getM2() {
	return 30.86 / 979;
}
double getC2() {
	return getM2() * 9176 - 30.6;
}
double getCalibratedTemp(double raw) {
	//y  = mx-c;
	if (raw <= 9176)
		return getM() * raw - getC();
	else
		return getM2() * raw - getC2();
}
