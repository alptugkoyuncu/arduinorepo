/*
 * RecordSet.h
 *
 *  Created on: 4 Feb 2020
 *      Author: Alptug
 */

#ifndef RECORDSET_H_
#define RECORDSET_H_
typedef struct RecordSet{
	char wifi_ssd[32];
	char wifi_password[32];
	unsigned char ip[4];
	unsigned short port;
	unsigned short id;
};



#endif /* RECORDSET_H_ */
