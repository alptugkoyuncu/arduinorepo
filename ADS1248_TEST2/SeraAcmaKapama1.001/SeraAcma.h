/*
 * SeraAcma.h
 *
 *  Created on: 24 Tem 2015
 *      Author: WIN7
 */

#ifndef SERAACMA_H_
#define SERAACMA_H_

#include "Arduino.h"
#include"BasicTime.h"
#include <DS3231.h>
#include <Wire.h>
#include"My_Debug.h"

#define ACILIYOR 1
#define KAPANIYOR 2
#define DURUYOR 0
#define ACMA_KAPAMA_GECIS_SURESI 500

#define DAY_TIME_IN_SECOND 60*60*24;

struct DailyTimer {
	BasicTime *baslangic;
	uint16_t beklenenCalismaSuresi;
	DailyTimer(uint8_t saat, uint8_t dk, uint8_t sn, uint16_t sure) {
		this->baslangic = new BasicTime(saat, dk, sn);
		this->beklenenCalismaSuresi = sure;
	}
	DailyTimer(DS3231 *Clock, uint16_t sure) {
		this->beklenenCalismaSuresi = sure;
		bool a, b;
		this->baslangic = new BasicTime(Clock->getHour(a, b),
				Clock->getMinute(), Clock->getSecond());
	}
	uint16_t getCalisilanSure(DS3231 *Clock) {
		bool a, b;
		uint16_t h = Clock->getHour(a, b);
		uint16_t m = Clock->getMinute();
		uint16_t s = Clock->getSecond();
		uint16_t borc = 0;
		if (s < this->baslangic->sn) {
			//dakikadan borc al saniyeye yukle
			borc = 1;
			s += 60;
		}
		s = s - this->baslangic->sn;
		if ((m == 0 && borc == 1) || (m - borc) < this->baslangic->dk) {
			//saatten borc al dakikaya yukle saniyenin borcunu cikart
			m = m + 60 - borc;
			borc = 1;
		} else {
			m = m - borc;
			borc = 0;
		}
		m = m - this->baslangic->dk;
		if ((h == 0 && borc == 1) || (h - borc) < this->baslangic->saat) {
			//gunden borc al saate yukle dakikanin borcunu cikart
			h = h + 24 - borc;
			borc = 1;
		} else {
			h = h - borc;
			borc = 0;
		}
		h = h - this->baslangic->saat;
		uint16_t sure = (h * 60 * 60 + m * 60 + s);
//		Serial.print("calisma Suresi : ");
//		Serial.println(sure);
		return sure;
	}

};

#define NO_OF_KAPAMA_SWITCHES 10
typedef struct Switch {
private:
	uint8_t pinNo;
	bool lastPos;
	bool position;
	int16_t kapanma_Zamani;
public:
	void setPinNo(uint8_t pin) {
		this->pinNo = pin;
		this->position = digitalRead(this->pinNo) != LOW;
		this->lastPos = this->position;
		this->kapanma_Zamani = 0;
	}
	uint8_t getPos(){
		int pos = digitalRead(this->pinNo);
		if(pos!=this->position){
			this->lastPos = this->position;
		}
		this->position=pos;
	}
};
#define LW1 1
#define LW2 2
#define LW3 3
#define LW4 4
#define LW5 5
#define LW6 6
#define LW7 7
#define LW8 8
#define LW9 9
#define LW10 10

typedef struct SwitchSystem {
private:
	Switch sw[NO_OF_KAPAMA_SWITCHES];
	unsigned long firstSwitchOff;
public:
	SwitchSystem() {
		sw[0].setPinNo(LW1);
		sw[1].setPinNo(LW2);
		sw[2].setPinNo(LW3);
		sw[3].setPinNo(LW4);
		sw[4].setPinNo(LW5);
		sw[5].setPinNo(LW6);
		sw[6].setPinNo(LW7);
		sw[7].setPinNo(LW8);
		sw[8].setPinNo(LW9);
		sw[9].setPinNo(LW10);
		this->firstSwitchOff=0;
	}
	bool checkToStop() {

	}
};
class SeraAcma {
private:
	Switch swt[NO_OF_KAPAMA_SWITCHES];
	uint16_t maximumAciklikMiktari;
	const static uint16_t minimumAciklikMiktari = 0;
	uint16_t aciklikMiktari;
	uint16_t istenilenMiktar;
	uint8_t acmaPini;
	uint8_t kapamaPini;
	uint8_t calismaDurumu;
	DS3231 *Clock;
	DailyTimer *timer;
	uint16_t lastAciklikMiktari; //calismaya baslamadan onceki aciklik miktari
	void init(uint16_t maximumAciklikMiktari, uint16_t aciklikMiktari,
			uint8_t acmaPini, uint8_t kapamaPini, DS3231 *Clock);
	bool ac(uint16_t sure);
	bool kapat(uint16_t sure);
	void saatiAyarla(uint16_t);
	void setAciklikMiktari();
public:
	SeraAcma(uint16_t maximumAciklikMiktari, uint8_t acmaPini,
			uint8_t kapamaPini, DS3231 *Clock);
	SeraAcma(uint16_t maximumAciklikMiktari, uint16_t aciklikMiktari,
			uint8_t acmaPini, uint8_t kapamaPini, DS3231 *Clock);
	void setMaximumAciklikMiktari(uint16_t mam) {
		this->maximumAciklikMiktari = mam;
	}
	uint16_t getMaximumAciklikMiktari();
	uint16_t getMinimumAciklikMiktari();
	uint8_t calistir(uint16_t istenilenMiktar);
	uint16_t durdur(); //calisma suresini dondurur// true hemen kapatildigi zamanda false zamansiz
	uint8_t getCalismaDurumu();
	uint16_t getAciklikMiktari();
	uint16_t getIstenilenMiktar();
	bool isWorking();
	bool stopIfEnough();
	virtual ~SeraAcma();
	char *toString(int16_t &size);
};

#endif /* SERAACMA_H_ */
