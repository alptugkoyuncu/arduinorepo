/*
 * TimerListFile.h
 *
 *  Created on: 17 A�u 2015
 *      Author: WIN7
 */

#ifndef TIMERLISTFILE_H_
#define TIMERLISTFILE_H_

#include "File.h"
#include "TimerList.h"

class TimerListFile: public File {
public:
	TimerListFile(int baslamaAddresi);


	//bu indexe uygun verileri alir ve list item a donusturup geri dondurur
	int8_t readItemData(int listItemIndex,ListItem &item);


	void updateItemAt(const TimerList tl,int itemIndex);
	 void removeItem(const TimerList tl,int itemIndex);
	 void addItem(const TimerList tl,int itemIndex);
	// listenin sizeini dosyadan okur ve geri dondurur
	uint8_t getListSize();

	//listenin size ini dosyaya yazar
	void writeListSize(uint8_t size);

	// listeyi dosyaya yazar
	void writeTimerList(const TimerList list);

	//dosyadan tam liste icin veri okur ve geri dondurur
	TimerList readTimerListData();
private:
	//indexe uygun adresi bulur ve yazar
	int getAddressForItem(int index);
	//listeyi o indexten sonrasini gunceller bu indexe ekleme yapildiginda ya da bu indexten silme islemi gerceklestirildiginde kullanilir
	void updateList(const TimerList,int listItemIndex);
	// listedeki list item index li elemani dosyaya yazar
	void writeItemData(const TimerList list,int listItemIndex);

};

#endif /* TIMERLISTFILE_H_ */
