/*
 * Errors.h
 *
 *  Created on: 18 A�u 2015
 *      Author: WIN7
 */

#ifndef ERRORS_H_
#define ERRORS_H_

#define NO_ERROR 0
#define ERR_SERA_IS_WORKING 1
#define ERR_LIST_ITEM_NOT_FOUND 2
#define ERR_LIST_HAS_ITEM 3
#define ERR_GENERAL 4
#define ERR_ARRAY_OUT_OF_RANGE 5



#endif /* ERRORS_H_ */
