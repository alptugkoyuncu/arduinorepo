/*
 * BasicTime.h
 *
 *  Created on: 27 Tem 2015
 *      Author: WIN7
 */

#ifndef BASICTIME_H_
#define BASICTIME_H_

struct BasicTime {
	uint8_t sn;
	uint8_t dk;
	uint8_t saat;
	BasicTime(uint8_t saat, uint8_t dk, uint8_t sn) {
		this->sn = sn;
		this->dk = dk;
		this->saat = saat;
	}
	BasicTime() {
		this->sn = 0;
		this->dk = 0;
		this->saat = 0;
	}
	//return true if the day roll over else return false
	bool addSaniye(uint16_t s) {
		bool nextDay = false;
		uint16_t sure = s;
		uint8_t saat = (uint8_t) (sure / (24 * 60));
		sure = sure - (saat * 24 * 60);
		uint8_t dakika = (uint8_t) (sure / 60);
		sure = sure - (dakika * 60);
		uint8_t saniye = (uint8_t) sure;
		sn += saniye;
		dk += dakika;
		saat += saat;
		while (sn >= 60) {
			sn -= 60;
			sn++;
		}
		while (dk >= 60) {
			dk -= 60;
			saat++;
		}
		if (saat >= 24) {
			saat -= 24;
			nextDay = true;
		}
		return nextDay;
	}
	int8_t compareTo(BasicTime t) {
		if (this->saat > t.saat)
			return 1;
		if (this->saat < t.saat)
			return -1;
		if (this->dk > t.dk)
			return 1;
		if (this->dk < t.dk)
			return -1;
		if (this->sn > t.sn)
			return 1;
		if (this->sn < t.sn)
			return -1;
		return 0;
	}

};



#endif /* BASICTIME_H_ */
