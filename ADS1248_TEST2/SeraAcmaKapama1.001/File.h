/*
 * File.h
 *
 *  Created on: 29 Tem 2015
 *      Author: WIN7
 */

#ifndef FILE_H_
#define FILE_H_

#include<Wire.h>

#include"Arduino.h"
#include"My_Debug.h"
#define FILE_WAIT 25

class File {
public:
	File(int baslamaAdresi=0);
	byte readByte(const unsigned int address);
	byte readNextByte();
	uint16_t readShort(const unsigned int address);
	uint16_t readNextShort();
	void writeByte(const unsigned int address, byte data);
	void writeNextByte(byte data);
	void writeShort(const unsigned int address, uint16_t data);
	void writeNextShort(uint16_t data);
	bool hasData();
	void writeHeader();
	void resetAll();
	void resetSeek();
	void resetLastWrittenAddress();
protected:
	int baslamaAdresi;
	int deviceAddress;
	unsigned int seek;
	bool has_data;
	unsigned int lastWrittenAddress;
private:
	void i2c_eeprom_write_byte(int deviceaddress, unsigned int eeaddress,
			byte data) {
		int rdata = data;
		Wire.beginTransmission(deviceaddress);
		Wire.write((int) (eeaddress >> 8)); // MSB
		Wire.write((int) (eeaddress & 0xFF)); // LSB
		Wire.write(rdata);
		Wire.endTransmission();
	}

// WARNING: address is a page address, 6-bit end will wrap around
// also, data can be maximum of about 30 bytes, because the Wire library has a buffer of 32 bytes
	void i2c_eeprom_write_page(int deviceaddress, unsigned int eeaddresspage,
			byte* data, byte length) {
		Wire.beginTransmission(deviceaddress);
		Wire.write((int) (eeaddresspage >> 8)); // MSB
		Wire.write((int) (eeaddresspage & 0xFF)); // LSB
		byte c;
		for (c = 0; c < length; c++)
			Wire.write(data[c]);
		Wire.endTransmission();
	}

	byte i2c_eeprom_read_byte(int deviceaddress, unsigned int eeaddress) {
		byte rdata = 0xFF;
		Wire.beginTransmission(deviceaddress);
		Wire.write((int) (eeaddress >> 8)); // MSB
		Wire.write((int) (eeaddress & 0xFF)); // LSB
		Wire.endTransmission();
		Wire.requestFrom(deviceaddress, 1);
		if (Wire.available())
			rdata = Wire.read();
		return rdata;
	}

// maybe let's not read more than 30 or 32 bytes at a time!
	void i2c_eeprom_read_buffer(int deviceaddress, unsigned int eeaddress,
			byte *buffer, int length) {
		Wire.beginTransmission(deviceaddress);
		Wire.write((int) (eeaddress >> 8)); // MSB
		Wire.write((int) (eeaddress & 0xFF)); // LSB
		Wire.endTransmission();
		Wire.requestFrom(deviceaddress, length);
		int c = 0;
		for (c = 0; c < length; c++)
			if (Wire.available())
				buffer[c] = Wire.read();
	}
};
#endif /* FILE_H_ */
