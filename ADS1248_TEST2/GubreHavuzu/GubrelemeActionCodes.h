/*
 * GubrelemeActionCodes.h
 *
 *  Created on: 23 Eki 2016
 *      Author: alptug
 */

#ifndef GUBRELEMEACTIONCODES_H_
#define GUBRELEMEACTIONCODES_H_


/*
 * TO ARDUINO
 * <action_code>
 * FROM ARDUINO
 * <action_code err_code mem>
 */
#define ACTION_GET_MEM_INFO 1
#define ACTION_RESET_EEPROM 2

/**
 * TO ARDUINO
 * <action_code sure>
 * sure istege bagli girilmezse sinirsiz calisir
 * orn <101> yada <101 30>
 * FROM ARDUINO
 *
 */

#define ACTION_TIMER_START 101
/**
 * TO ARDUINO
 * <action_code>
 * timer durdur
 * FROM ARDUINO
 *
 */
#define ACTION_TIMER_STOP 102


/*
 * TO ARDUINO
 * <action_code otomatiklik_enable>
 * otomatiklik aktif olmasi icin 1 pasif olmasi icin 0
 */
#define ACTION_TIMER_SET_OTOMATIKLIK 103

/**
 * TO ARDUINO
 * <action_code >
 * FROM ARDUINO
 *
 * if (tt == NULL)
 		return sprintf(buffer, "<%d %d %d %d", ACTION_TIMER_INFO, errFlag,
				timer->isOtomatikEnabled(), timer->isRunning());
	else
		return sprintf(buffer, "<%d %d %d %d %d %d %d %d %d>",
				ACTION_TIMER_INFO, errFlag, timer->isOtomatikEnabled(),
				timer->isRunning(), tt->getRunningTime(), tt->getSaat(),
				tt->getDakika(), tt->getSure(), tt->isAktif());
 */
#define ACTION_TIMER_INFO 104

/**
 * TO ARDUINO
 * <action_code hh mm sure aktiflik>
 * aktiflik 0 olursa false digerlerinde 1
 */
#define ACTION_TIMER_ADD_TIMER_ITEM 111
/*
 * T0_ARDUINO
 * <action_code itemIndex>
 * veya
 * <action_code hh mm>
 */
#define ACTION_TIMER_REMOVE_TIMER_ITEM 112
/*
 * T0_ARDUINO
 * <action_code itemIndex newHH newMM newSure newAktiflik>
 * veya
 * <action_code oldHH oldMM newHH newMM newSure newAktiflik>
 */
#define ACTION_TIMER_UPDATE_TIMER_ITEM 113
/*
 * T0_ARDUINO
 * <acton_code timerItemAt>
 *
 * FROM_ARDUINO
 *<action_code errFlag hh mm sure aktiflik>
 */
#define ACTION_TIMER_GET_TIMER_ITEM_AT 114
/*
 * TO_ARDUINO
 * <action_code>
 *
 * FROM_ARDUINO
 * <action_code listSize>
 */
#define ACTION_TIMER_GET_TIMER_ITEM_SIZE 115

#define ACTION_TIMER_SET_DATE_TIME 121
#define ACTION_TIMER_GET_DATE_TIME 122
#define ACTION_TIMER_REQUEST_DATE_TIME 123

#endif /* GUBRELEMEACTIONCODES_H_ */
