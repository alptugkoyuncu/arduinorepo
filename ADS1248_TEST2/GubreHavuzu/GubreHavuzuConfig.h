/*
 * GubreHavuzuConfig.h
 *
 *  Created on: 25 Kas 2016
 *      Author: alptug
 */

#ifndef GUBREHAVUZUCONFIG_H_
#define GUBREHAVUZUCONFIG_H_

//#define GUBRE_HAVUZU_DEBUG

#ifdef GUBRE_HAVUZU_DEBUG
#define ghPrint(a) Serial.print(a)
#define ghPrintln(a) Serial.println(a)
#else
#define ghPrint(a) ;
#define ghPrintln(a) ;
#endif


#endif /* GUBREHAVUZUCONFIG_H_ */
