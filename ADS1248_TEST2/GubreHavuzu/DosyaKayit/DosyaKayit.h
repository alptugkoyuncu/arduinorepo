/*
 * DosyaKayit.h
 *
 *  Created on: 18 Kas 2016
 *      Author: alptug
 */

#ifndef DOSYAKAYIT_H_
#define DOSYAKAYIT_H_
#include"DosyaKayitConfig.h"
#include"TimerItemList.h"



struct DosyaKayitItem{
	uint16_t val;//12355 ilk deger 1 veya 0 otomatiklikEnabled 2. ve 3. deger saat 23 4. ve 5. deger dakika 55
	uint16_t sure; // calisma suresi
};
class DosyaKayit {
protected:
	virtual void saveTimerItemSize(uint8_t size)=0;
	uint8_t dosyaKayitItemListSize;
public:
	virtual void resetFile()=0;
	virtual void setOtomasyonEnabled(bool flag)=0;
	virtual bool isOtomasyonEnabled()=0;
	virtual uint8_t getTimerItemSize()=0;
	virtual bool addTimerItem(TimerItem val)=0;
	virtual bool removeTimerItem(TimerItem val)=0;

	virtual TimerItem getTimerItemAt(uint8_t dosyaItemIndex)=0;
    void saveTimerItemList(TimerItemList *l){
    	if(l==NULL){
    		this->saveTimerItemSize(0);
    		return;
    	}
    	this->saveTimerItemSize(l->getSize());
    	for(int i=0; i<l->getSize(); i++)
    		this->addTimerItem(l->getTimerItemAt(i));
    }
    void loadTimerItemList(TimerItemList *list){
    	if(list==NULL){
    		list=new TimerItemList();
    	}
    	bool isEmpty=list->getSize()==0;
    	for (uint8_t i = 0; i < this->dosyaKayitItemListSize; i++) {
    		list->addTimerItem(this->getTimerItemAt(i));
    	}
    	if(!isEmpty){
    		this->saveTimerItemList(list);
    	}
    	dkPrintln("Get Timer Item List successfully");
    }
	virtual ~DosyaKayit(){

	}
};


#endif /* DOSYAKAYIT_H_ */
