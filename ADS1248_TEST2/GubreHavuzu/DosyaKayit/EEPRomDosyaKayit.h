/*
 * EEPRomDosyaKayit.h
 *
 *  Created on: 18 Kas 2016
 *      Author: alptug
 */

#ifndef EEPROMDOSYAKAYIT_H_
#define EEPROMDOSYAKAYIT_H_

#include "DosyaKayit.h"

#define EEPROM_WRITE_DELAY 5
#define EEPROM_READ_DELAY 5

#define HAS_RECORD_VALUE 99
//#define MAXIMUM_EEPROM_KAYIT_GUNCELLEME 60000

class EEPRomDosyaKayit: public DosyaKayit {
public:
	EEPRomDosyaKayit();
	~EEPRomDosyaKayit();

	void setOtomasyonEnabled(bool flag) override;
	bool isOtomasyonEnabled()override;
	uint8_t getTimerItemSize() override;
	bool addTimerItem(TimerItem val) override;
	bool removeTimerItem(TimerItem val)override;

	TimerItem getTimerItemAt(uint8_t dosyaItemIndex) override;
	void resetFile()override;

protected:
//	void setOtomasyonKayitBaslamaAddresi();
	void saveTimerItemSize(uint8_t size) ;
private:
	int itemIndexToEEAddress(uint8_t dosyaItemIndex);// varsa pozitif deger dondurur yoksa -1
	void movedDosyaKayitInfo(int oldAddr,int newAddr);
	uint16_t readWord(int addr);
	void writeWord(int addr,uint16_t val);
	uint8_t readByte(int addr);
	void writeByte(int addr,uint8_t val);
	void writeDosyaKayit(int addr,DosyaKayitItem val);
	DosyaKayitItem getDosyaKayitItem(int addr);
//	void loadFileInfo();
	/*
	 * eeprom daki 0 addresindeki degeri okur deger 99 ise kayit vardir baska degerlerde kayit yoktur
	 */
	bool hasRecord();
	/*
	 * eepromdaki 1 degerini okur toplam kayitsayisi sinirisi astikca moved degeri 1 artar. Kayit baslangic ve bitis adresleri yenilenir
	 */
//	uint8_t movedDegeri;
//	uint8_t loadMovedDegeri();

	/*
	 *eepromdaki 2 degerini okur burada yeni kayitlarin baslangic addresini okur
	 */
//	uint16_t baslamaAddresi;
//	uint16_t loadBaslamaAddresi();

	/*
	 * yeni kayit+0 da bitis degeri bulunur
	 */
//	uint16_t bitisAddresi;
//	uint16_t loadBitisAddresi();
	/*
	 * yeni kayit+2 de toplam kayit degeri bulunur
	 * eeproma yapilan toplam kayit sayisina bakar bu deger siniri astiysa yeni kayitlar
	 *  simdiye kadar yapilan son kayit adresinin bir ilerisinden baslar.
	 *  Bu deger toplam kayit degerinin bir ilerisindeki addreste tutulur
	 */
//	uint16_t toplamKayitMiktari;
//	uint16_t loadToplamKayitMiktari();

	//burada otomasyo bilgilerinin kayitli tutuldugu alanlar
	uint16_t otomasyonKayitBaslamaAddresi;
};

#endif /* EEPROMDOSYAKAYIT_H_ */
