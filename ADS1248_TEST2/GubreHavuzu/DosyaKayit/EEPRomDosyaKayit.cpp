/*
 * EEPRomDosyaKayit.cpp
 *
 *  Created on: 18 Kas 2016
 *      Author: alptug
 */

#include "EEPRomDosyaKayit.h"
#include "EEPROM.h"

EEPRomDosyaKayit::EEPRomDosyaKayit():DosyaKayit() {
	if (!hasRecord()) {
		this->resetFile();
	}
	this->otomasyonKayitBaslamaAddresi = 1;
	this->getTimerItemSize();
}

EEPRomDosyaKayit::~EEPRomDosyaKayit() {
}

void EEPRomDosyaKayit::setOtomasyonEnabled(bool flag) {
	int addr = this->otomasyonKayitBaslamaAddresi;
	this->writeByte(addr, flag ? 1 : 0);
	dkPrintln("otomasyon enabled kaydedildi :");
}

bool EEPRomDosyaKayit::isOtomasyonEnabled() {
	int addr = this->otomasyonKayitBaslamaAddresi;
	bool flag= this->readByte(addr) != 0;
	dkPrintln("otomasyon enabled okundu ");
	return flag;
}

uint8_t EEPRomDosyaKayit::getTimerItemSize() {
	int addr = this->otomasyonKayitBaslamaAddresi + 1;
	 this->dosyaKayitItemListSize= this->readByte(addr);
	 dkPrintln("Dosya kayit item size okundu :");
	 return this->dosyaKayitItemListSize;
}

void EEPRomDosyaKayit::saveTimerItemSize(uint8_t size) {
	int addr = this->otomasyonKayitBaslamaAddresi + 1;
	this->writeByte(addr, size);
	this->dosyaKayitItemListSize=size;
	dkPrintln("Dosya Kayit item size kaydedildi : ");
}

bool EEPRomDosyaKayit::addTimerItem(TimerItem val) {
	DosyaKayitItem item;
	item.sure = val.getSure();
	item.val = val.isAktif() ? 10000 : 0;
	item.val = item.val + val.getSaat() * 100 + val.getDakika();

	int addr=itemIndexToEEAddress(this->dosyaKayitItemListSize);
	this->writeDosyaKayit(addr, item);
	this->saveTimerItemSize(this->dosyaKayitItemListSize+1);
	dkPrintln("Add Timer Item successfully");
	return true;
}

bool EEPRomDosyaKayit::removeTimerItem(TimerItem val) {
	TimerItem tmp;
	int addr=-1;
	for(uint8_t i=0; i<this->dosyaKayitItemListSize;i++){
		tmp = this->getTimerItemAt(i);
		if(tmp.compare(val)==0){
			addr = this->itemIndexToEEAddress(i);
			break;
		}
	}
	if(addr==-1)
		return false;
	this->movedDosyaKayitInfo(itemIndexToEEAddress(dosyaKayitItemListSize-1),addr);
	this->saveTimerItemSize(this->dosyaKayitItemListSize-1);
	dkPrintln("Remove timer Item successfully");
	return true;
}

TimerItem EEPRomDosyaKayit::getTimerItemAt(uint8_t dosyaItemIndex) {
	int addr =itemIndexToEEAddress(dosyaItemIndex) ;
	DosyaKayitItem item=this->getDosyaKayitItem(addr);
	uint8_t hh, dd;
	bool aktif = (item.val / 10000) > 0;
	if (aktif != 0)
		item.val = item.val % 10000;
	hh = item.val / 100;
	dd = (item.val - hh * 100);
	dkPrint("GET timer item at ");
	dkPrint(dosyaItemIndex);
	dkPrintln("sucessfully");
	return TimerItem(hh, dd, 0, item.sure,aktif);
}


void EEPRomDosyaKayit::movedDosyaKayitInfo(int oldAddr, int newAddr) {
	DosyaKayitItem val=this->getDosyaKayitItem(oldAddr);
	this->writeDosyaKayit(newAddr,val);
	dkPrintln("moved Dosya kayit info successfully");
}

void EEPRomDosyaKayit::resetFile() {
	this->writeByte(0,HAS_RECORD_VALUE);
	this->setOtomasyonEnabled(0);
	this->saveTimerItemSize(0);

}

int EEPRomDosyaKayit::itemIndexToEEAddress(uint8_t dosyaItemIndex) {
	return this->otomasyonKayitBaslamaAddresi + 2+ sizeof(DosyaKayitItem) * dosyaItemIndex;
}

bool EEPRomDosyaKayit::hasRecord() {
	dkPrintln("Has record");
	uint8_t val = this->readByte(0);
	return (val == HAS_RECORD_VALUE);
}

uint16_t EEPRomDosyaKayit::readWord(int addr) {
	uint8_t valL = this->readByte(addr);
	uint8_t valR = this->readByte(addr + 1);
	uint16_t val = valL<<8;
	val |= valR;
	dkPrint("read Word at :");
	dkPrint(addr);
	dkPrint(" Value : ");
	dkPrintln(val);
	return val;
}

void EEPRomDosyaKayit::writeWord(int addr, uint16_t val) {
	uint8_t valL = val >> 8;
	uint8_t valR = val;
	this->writeByte(addr, valL);
	this->writeByte(addr+1, valR);

	dkPrint("Write word to ");
	dkPrint(addr);
	dkPrint(" Value :");
	dkPrintln(val);
}

uint8_t EEPRomDosyaKayit::readByte(int addr) {
	uint8_t val = EEPROM.read(addr);
	delay(EEPROM_READ_DELAY);
	dkPrint("Read byte at :");
	dkPrint(addr);
	dkPrint(" Value :");
	dkPrintln(val);
	return val;
}

void EEPRomDosyaKayit::writeByte(int addr, uint8_t val) {
	EEPROM.write(addr, val);
	delay(EEPROM_WRITE_DELAY);
	dkPrint("Write Byte to :");
	dkPrint(addr);
	dkPrint(" Value :");
	dkPrintln(val);
}
void EEPRomDosyaKayit::writeDosyaKayit(int addr, DosyaKayitItem val) {
	EEPROM.put(addr, val);
	delay(EEPROM_WRITE_DELAY);
	dkPrint("Write Dosya Kayit to : ");
	dkPrint(addr);
	dkPrint(" Value :[");
	dkPrint(val.val);
	dkPrint(",");
	dkPrint(val.sure);
	dkPrintln("]");
}

DosyaKayitItem EEPRomDosyaKayit::getDosyaKayitItem(int addr) {
	DosyaKayitItem item;
	EEPROM.get(addr, item);
	delay(EEPROM_READ_DELAY);
	char buffer[50];
	sprintf(buffer,"Read Dosya Kayit at : %d Value:[%u,%u]",addr,item.val,item.sure);
	dkPrint(buffer);
	return item;
}
