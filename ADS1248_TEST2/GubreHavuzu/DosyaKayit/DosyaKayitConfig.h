/*
 * DosyaKayitConfig.h
 *
 *  Created on: 25 Kas 2016
 *      Author: alptug
 */

#ifndef DOSYAKAYITCONFIG_H_
#define DOSYAKAYITCONFIG_H_


//#define DOSYA_KAYIT_DEBUG_ENABLE

#ifdef DOSYA_KAYIT_DEBUG_ENABLE
#define dkPrint(a) Serial.print(a)
#define dkPrintln(a) Serial.println(a)
#else
#define dkPrint(a) ;
#define dkPrintln(a) ;
#endif



#endif /* DOSYAKAYITCONFIG_H_ */
