// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _GubreHavuzu_H_
#define _GubreHavuzu_H_
#include "Arduino.h"
#include"Timer.h"
#include"Wire.h"
#include"DataPackage.h"
#include "GubrelemeActionCodes.h"
#include "GubrelemeNextion/NextionMonitor.h"
#include "MemoryFree.h"
#include"DosyaKayit/EEPRomDosyaKayit.h"
#include"DosyaKayitTimerListener.h"
#include"GubreHavuzuConfig.h"
//add your includes for the project GubreHavuzu here


//end of add your includes here


//add your function definitions for the project GubreHavuzu here



void doAction(int16_t *array,uint8_t size);
int8_t str_action_timer_info(char *buffer);
int8_t str_action_timer_get_timer_item_at(char *buffer,uint8_t index);
int8_t str_action_timer_get_timer_item_size(char *buffer);
uint8_t str_action_timer_get_date_time(char *buffer);
TimerItemList * readFromFile();
uint8_t str_action_get_mem_info(char *);
//Do not add code below this line
#endif /* _GubreHavuzu_H_ */
