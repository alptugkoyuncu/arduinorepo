/*
 * NextionMainPanel.cpp
 *
 *  Created on: 3 Kas 2016
 *      Author: alptug
 */

#include "NextionMainPanel.h"
#include"NextionMonitor.h"
//NexButton* NextionMainPanel::getTimerListButton() {
//	return this->timerListButton;
//}

NextionMainPanel::NextionMainPanel(Timer* timer) :
		AbstractMyNextionPage(1, 0, "mainPage") {
	this->timer = timer;
	uint8_t pid = 1;

	this->saat = new NexText(pid, 2, "saat");
	this->otomatiklikAktif = new NexCheckbox(pid, 6, "c0");
	this->startStopBtn = new NexDSButton(pid, 5, "bt0");
	this->timerListButton = new NexButton(pid, 1, "btnTimerList");

	this->timerListButton->attachPop(destroyPage, this);

	this->otomatiklikAktif->attachPop(NextionMainPanel::aktiflikCheckBoxClicked,
			this);
	this->startStopBtn->attachPop(NextionMainPanel::startStopButtonClicked,
			this);
	this->saat->attachPop(destroyPage, this);
	NextionMonitor::clearNexListenList();
	NextionMonitor::createNexListenList(5);
	NextionMonitor::nex_listen_list[0] = this;
	NextionMonitor::nex_listen_list[1] = this->otomatiklikAktif;
	NextionMonitor::nex_listen_list[2] = this->startStopBtn;
	NextionMonitor::nex_listen_list[3] = this->timerListButton;
	NextionMonitor::nex_listen_list[4] = this->saat;
	NextionMonitor::nex_listen_list[5] = NULL;
	this->timer->addTimerListener(this);
}

NextionMainPanel::~NextionMainPanel() {
	NextionMonitor::clearNexListenList();
	this->timer->removeTimerListener(this);
	delete this->otomatiklikAktif;
	delete this->saat;
	delete this->startStopBtn;
	delete this->timerListButton;
	// TODO Auto-generated destructor stub
//	this->timerListButton->detachPop();
}

bool NextionMainPanel::setSaat(uint8_t hh, uint8_t mm, uint8_t ss) {
	char buff[10];
	sprintf(buff, "%02u:%02u:%02u", hh, mm, ss);
	return this->saat->setText(buff);
}

bool NextionMainPanel::setNextCalismaZamani(uint8_t hh, uint8_t mm, uint8_t ss,
		uint16_t sure) {
	char buff[20];
	if (sure == 0) {
		sprintf(buff, "%02u:%02u:%02u(-)", hh, mm, ss);
	} else
		sprintf(buff, "%02u:%02u:%02u(%u)", hh, mm, ss, sure);
	NexText txt(this->getObjPid(), 4, "nextCalisma");
	return txt.setText(buff);
}

void NextionMainPanel::setTimerStart() {
	timer->start();
}

void NextionMainPanel::setTimerStop() {
	timer->stop();
}

//NextionTimerList* NextionMainPanel::getNextionTimerList() {
//return this->ntl;
//}

bool NextionMainPanel::setCalismaZamani(uint16_t sure) {
	NexNumber n(this->getObjPid(), 3, "calismaZamani");
	return n.setValue(sure);
}

bool NextionMainPanel::setCalismaBilgileriVisible(bool flag) {
	NexNumber n(this->getObjPid(), 3, "calismaZamani");
	NexText txt(this->getObjPid(), 4, "nextCalisma");
	return n.setVisible(flag) && txt.setVisible(flag);
//			0535 672 55 10

}
//void loadPage(void* ptr) {
//	AbstractMyNextionPage *l = (AbstractMyNextionPage *) ptr;
//	l->pageLoaded();
//}

bool NextionMainPanel::pageLoaded() {
	DateTime now = timer->getNow();
	bool recvSuccess;
	recvSuccess = this->otomatiklikAktif->setValue(
			timer->isOtomatikEnabled() ? 1 : 0);

	recvSuccess &= this->setSaat(now.hour(), now.minute(), now.second());
	RunningTimerItem *rt = timer->getNextRunningTimerItem();
	if (rt != NULL) {
		recvSuccess &= this->setCalismaBilgileriVisible(true);
		recvSuccess &= this->setCalismaZamani(rt->getRunningTime());
		recvSuccess &= this->setNextCalismaZamani(rt->getSaat(),
				rt->getDakika(), rt->getSaniye(), rt->getSure());
	} else {
		recvSuccess &= this->setCalismaBilgileriVisible(false);
	}
	delay(10);
	recvSuccess &= this->startStopBtn->setValue(timer->isRunning() ? 1 : 0);
	return recvSuccess;
}

void NextionMainPanel::startStopButtonClicked(void* ptr) {
	NextionMainPanel *m = (NextionMainPanel *) ptr;
	bool isWorking;
	bool recvSuccess = m->getStartSopButtonValue(&isWorking);
	if (!recvSuccess)
		return;
	if (isWorking) {
		m->setTimerStart();
	} else
		m->setTimerStop();
}

void NextionMainPanel::aktiflikCheckBoxClicked(void* ptr) {
	NextionMainPanel *m = (NextionMainPanel *) ptr;
	bool isChecked;
	if (m->getOtomatikAktiflikCheckBoxValue(&isChecked)) {
		m->setOtomatiklikAktif(isChecked);
	}

}

bool NextionMainPanel::getStartSopButtonValue(bool *isWorking) {
	uint32_t val;
	bool flag = this->startStopBtn->getValue(&val);
	*isWorking = val != 0;
	return flag;
}

bool NextionMainPanel::getOtomatikAktiflikCheckBoxValue(bool *isChecked) {
	uint32_t val;
	bool flag = this->otomatiklikAktif->getValue(&val);
	*isChecked = val != 0;
	return flag;
}

void NextionMainPanel::setOtomatiklikAktif(bool val) {
	timer->setOtomatikEnabled(val);
}

void NextionMainPanel::dateChanged(uint16_t year, uint8_t month, uint8_t date) {
}

void NextionMainPanel::timerStateChanged(TimerStateEvent evt) {
	this->pageLoaded();
}

void NextionMainPanel::timerActionPerformed(TimerActionEvent evt) {
	this->pageLoaded();
}

void NextionMainPanel::timeChanged(uint8_t hh, uint8_t mm, uint8_t ss) {
	if (timer->isRunning()) {
		this->pageLoaded();
	}else
		this->setSaat(hh,mm,ss);
}
