/*
 * NextionSaatPage.cpp
 *
 *  Created on: 13 Kas 2016
 *      Author: alptug
 */

#include "NextionSaatUpdatePage.h"
#include"NextionMonitor.h"
NextionSaatUpdatePage::NextionSaatUpdatePage(Timer *t) :
		AbstractMyNextionPage(4, 0, "saatPage") {
	// TODO Auto-generated constructor stub
	this->timer = t;
	uint8_t pageId = 4;

	this->btnGeri = new NexButton(pageId, 14, "b0");
	this->btnKaydet = new NexButton(pageId, 15, "b1");

	NextionMonitor::clearNexListenList();
	NextionMonitor::createNexListenList(4);

	this->btnGeri->attachPop(destroyPage, this);
	this->btnKaydet->attachPop(NextionSaatUpdatePage::kaydetClicked, this);

	NextionMonitor::nex_listen_list[0] = this;
	NextionMonitor::nex_listen_list[1] = this->btnGeri;
	NextionMonitor::nex_listen_list[2] = this->btnKaydet;
	NextionMonitor::nex_listen_list[3] = NULL;
//	this->page->show();
}

bool NextionSaatUpdatePage::pageLoaded() {
	DateTime dt = this->timer->getNow();
	bool recvSuccess = true;
	uint8_t pageId = this->getObjPid();
	NexNumber *obj;

	//date objesini olustur
	obj = new NexNumber(pageId, 16, "n0");
	recvSuccess &= obj->setValue(dt.date());
	delete obj;

	//month objesini olustur
	obj = new NexNumber(pageId, 17, "n1");
	recvSuccess &= obj->setValue(dt.month());
	delete obj;

	//year objesini olustur
	obj = new NexNumber(pageId, 18, "n2");
	recvSuccess &= obj->setValue(dt.year());
	delete obj;

	//hour objesini olustur
	obj = new NexNumber(pageId, 19, "n13");
	recvSuccess &= obj->setValue(dt.hour());
	delete obj;

	//minute objesini olustur
	obj = new NexNumber(pageId, 20, "n14");
	recvSuccess &= obj->setValue(dt.minute());
	delete obj;

	//second objesini olustur
	obj = new NexNumber(pageId, 21, "n15");
	recvSuccess &= obj->setValue(dt.second());
	delete obj;

	return recvSuccess;
}

bool NextionSaatUpdatePage::kaydet() {
	uint32_t year, month, date, hh, mm, ss;
	bool recvSuccess = true;
	uint8_t pageId = this->getObjPid();
	NexNumber *obj;

	//date objesini olustur
	obj = new NexNumber(pageId, 16, "n0");
	recvSuccess &= obj->getValue(&date);
	delete obj;

	//month objesini olustur
	obj = new NexNumber(pageId, 17, "n1");
	recvSuccess &= obj->getValue(&month);
	delete obj;

	//year objesini olustur
	obj = new NexNumber(pageId, 18, "n2");
	recvSuccess &= obj->getValue(&year);
	delete obj;

	//hour objesini olustur
	obj = new NexNumber(pageId, 19, "n13");
	recvSuccess &= obj->getValue(&hh);
	delete obj;

	//minute objesini olustur
	obj = new NexNumber(pageId, 20, "n14");
	recvSuccess &= obj->getValue(&mm);
	delete obj;

	//second objesini olustur
	obj = new NexNumber(pageId, 21, "n15");
	recvSuccess &= obj->getValue(&ss);
	delete obj;

	if (recvSuccess) {
		DateTime dt(year, month, date, hh, mm, ss, 0);
		timer->updateClock(dt);
	}
	return recvSuccess;
}

NextionSaatUpdatePage::~NextionSaatUpdatePage() {
	// TODO Auto-generated destructor stub
	NextionMonitor::clearNexListenList();

	delete this->btnKaydet;
	delete this->btnGeri;
}

void NextionSaatUpdatePage::kaydetClicked(void* ptr) {
	NextionSaatUpdatePage *nsp = (NextionSaatUpdatePage *) ptr;
	nsp->kaydet();
	NextionSaatUpdatePage::destroyPage(nsp);
}
