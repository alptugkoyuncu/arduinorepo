/*
 * NewListItemPage.cpp
 *
 *  Created on: 7 Kas 2016
 *      Author: alptug
 */

#include "NewListItemPage.h"
#include"NextionMonitor.h"

NewListItemPage::NewListItemPage(TimerItemList *l) :
		AbstractMyNextionPage(3, 0, "page2") {
	// TODO Auto-generated constructor stub
	uint8_t pid = this->getObjPid();
	this->list = l;

	this->btnKaydet = new NexButton(pid, 22, "btnKaydet");
	this->btnSil = new NexButton(pid, 24, "btnSil");
	this->btnGeri = new NexButton(pid, 19, "b0");

	this->newOne = false;
	this->itemIndex = 0;
	this->btnGeri->attachPop(AbstractMyNextionPage::destroyPage);
	this->btnKaydet->attachPop(kaydetClicked, this);
	this->btnSil->attachPop(silClicked, this);
	NextionMonitor::clearNexListenList();
	NextionMonitor::createNexListenList(5);
	NextionMonitor::nex_listen_list[0] = this;
	NextionMonitor::nex_listen_list[1] = this->btnGeri;
	NextionMonitor::nex_listen_list[2] = this->btnSil;
	NextionMonitor::nex_listen_list[3] = this->btnKaydet;
	NextionMonitor::nex_listen_list[4] = NULL;
//	this->page->show();
}
NewListItemPage::~NewListItemPage() {
	// TODO Auto-generated destructor stub
	NextionMonitor::clearNexListenList();
	delete this->btnGeri;
	delete this->btnKaydet;
	delete this->btnSil;
}
bool NewListItemPage::setSaatNextion(uint8_t val) {
	NexNumber saatNextion(this->getObjPid(), 2, "n0");
	return saatNextion.setValue(val);
}

bool NewListItemPage::getSaat(uint8_t* val) {
	uint32_t tmp;
	bool recvSuccess;
	NexNumber saatNextion(this->getObjPid(), 2, "n0");
	recvSuccess = saatNextion.getValue(&tmp);
	*val = (uint8_t) tmp;
	return recvSuccess;
}

bool NewListItemPage::setDakikaNextion(uint8_t val) {
	NexNumber dakikaNextion(this->getObjPid(), 3, "n1");
	return dakikaNextion.setValue(val);
}

bool NewListItemPage::getDakika(uint8_t* val) {
	uint32_t tmp;
	NexNumber dakikaNextion(this->getObjPid(), 3, "n1");
	bool recvSuccess = dakikaNextion.getValue(&tmp);
	*val = (uint8_t) tmp;
	return recvSuccess;
}

bool NewListItemPage::setAktiflikNextion(bool isAktif) {
	NexCheckbox aktiflikNextion(this->getObjPid(), 21, "c0");
	return aktiflikNextion.setValue(isAktif ? 1 : 0);
}

bool NewListItemPage::isAktif(bool* isAktif) {
	NexCheckbox aktiflikNextion(this->getObjPid(), 21, "c0");
	uint32_t val;
	bool recvSuccess = aktiflikNextion.getValue(&val);
	*isAktif = val != 0;
	return recvSuccess;
}

bool NewListItemPage::setSureNextion(uint16_t val) {
	NexNumber sureNextion(this->getObjPid(), 5, "n2");
	return sureNextion.setValue(val);
}

bool NewListItemPage::getSure(uint16_t* val) {
	NexNumber sureNextion(this->getObjPid(), 5, "n2");
	uint32_t tmp;
	bool recvSuccess = sureNextion.getValue(&tmp);
	*val = (uint16_t) tmp;
	return recvSuccess;
}

bool NewListItemPage::loadItemIndex() {
	NexVariable itemIndexNextion(this->getObjPid(), 23, "itemIndex");
	uint32_t tmp;
	bool recvSucess = itemIndexNextion.getValue(&tmp);
	this->itemIndex = (uint8_t) tmp;
	return recvSucess;
}

bool NewListItemPage::loadNewOne() {
	NexVariable newOneNextion(this->getObjPid(), 25, "newOne");
	uint32_t tmp;
	bool recvSuccess = newOneNextion.getValue(&tmp);
	this->newOne = tmp != 0;
	return recvSuccess;
}

bool NewListItemPage::pageLoaded() {
	bool recvSuccess;
	TimerItem t;
	recvSuccess = this->loadNewOne();
	if (!recvSuccess)
		return false;
	this->btnSil->setVisible(!newOne);

	if (newOne)
		return recvSuccess;

	recvSuccess = this->loadItemIndex();
	if (!recvSuccess)
		return false;
	t = this->list->getTimerItemAt(itemIndex);

	recvSuccess = this->setSaatNextion(t.getSaat());
	recvSuccess &= this->setDakikaNextion(t.getDakika());
	recvSuccess &= this->setSureNextion(t.getSure());
	recvSuccess &= this->setAktiflikNextion(t.isAktif());

	return recvSuccess;
}

void NewListItemPage::deleteItem() {
	if (this->newOne)
		return;
	this->list->removeTimerItem(itemIndex);
}

void NewListItemPage::kaydetItem() {
	uint8_t hh, mm;
	bool aktif, recvSuccess;
	uint16_t sure;
	recvSuccess = this->getSaat(&hh) && this->getDakika(&mm)
			&& this->getSure(&sure) && this->isAktif(&aktif);
	if (!recvSuccess)
		return;
	TimerItem item(hh, mm, sure, aktif);

	if (newOne) {
		this->list->addTimerItem(item);
	} else {
		this->list->updateTimerItem(this->itemIndex, item);

	}
}
void kaydetClicked(void* ptr) {
	NewListItemPage *l = (NewListItemPage *) ptr;
	l->kaydetItem();
	AbstractMyNextionPage::destroyPage(l);
}

void silClicked(void* ptr) {
	NewListItemPage *l = (NewListItemPage *) ptr;
	l->deleteItem();
	AbstractMyNextionPage::destroyPage(l);
}
