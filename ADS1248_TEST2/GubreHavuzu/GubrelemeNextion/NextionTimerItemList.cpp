/*
 * NextionTimer.cpp
 *
 *  Created on: 31 Eki 2016
 *      Author: alptug
 */

#include"NextionMonitor.h"
#include"MemoryFree.h"

#include "NextionTimerItemList.h"
uint8_t getSaat(const char *str) {
	uint8_t onlar, birler;
	onlar = str[0] - '0';
	birler = str[1] - '0';
	return onlar * 10 + birler;
}
uint8_t getDakika(const char *str) {
	uint8_t onlar, birler;
	onlar = str[3] - '0';
	birler = str[4] - '0';
	return onlar * 10 + birler;
}

NextionTimerItem::NextionTimerItem(uint8_t itemIndex) {
	this->itemIndex = itemIndex;
	char buffer[5];
	sprintf(buffer, "m%u", itemIndex);
	touch =new NexHotspot(pid, 6 * itemIndex + 6, buffer);
}

NextionTimerItem::~NextionTimerItem() {
	delete this->touch;
}

NexHotspot* NextionTimerItem::getTouch() {
	return this->touch;
}

bool NextionTimerItem::setItem(TimerItem val, uint8_t currPage) {

	bool recvSuccess;

	char buffer[30];
	NexObject *obj;

// number item olustur
	sprintf(buffer, "numberItem%u", itemIndex);
	obj = new NexNumber(pid, 6 * itemIndex + 2, buffer);
	recvSuccess = ((NexNumber *) obj)->setValue(
			(currPage - 1) * 5 + itemIndex + 1);
	delete (NexNumber *)obj;
	obj=NULL;
// saat textini olurstur
	char saat[20]={0};

	//char *saat;
	//	saat = (char*) malloc((sizeof(char)) * 20);
	sprintf(saat, "%02u:%02u", val.getSaat(), val.getDakika());

	sprintf(buffer, "saatItem%u", itemIndex);
	obj = new NexText(pid, 6 * itemIndex + 3, buffer);
	recvSuccess &= ((NexText *) obj)->setText(saat);
//	free(saat);
	delete (NexText *)obj;
	obj=NULL;

// sure itemini olustur
	sprintf(buffer, "sureItem%u", itemIndex);
	obj = new NexNumber(pid, 6 * itemIndex + 4, buffer);
	recvSuccess &= ((NexNumber *) obj)->setValue(val.getSure());
	delete (NexNumber *)obj;
	obj=NULL;

//aktiflik itemini olsutur
	sprintf(buffer, "aktiflikItem%u", itemIndex);
	obj = new NexCheckbox(pid, 6 * itemIndex + 5, buffer);
	recvSuccess &= ((NexCheckbox *) obj)->setValue(val.isAktif() ? 1 : 0);
	delete (NexCheckbox *)obj;
	obj=NULL;

//elemani gorunur yapmak icin usutunde bulunan text boxi gorunmez yap
	sprintf(buffer, "vis%u", itemIndex);
	obj = new NexText(pid, 38 + itemIndex, buffer);
	recvSuccess &= ((NexText *) obj)->setVisible(false);
	delete (NexText *)obj;
	obj=NULL;

	return recvSuccess;
}

bool NextionTimerItem::setNoItem() {
	char buffer[20];
	sprintf(buffer, "vis%u", itemIndex);
	NexText val(pid, 38 + itemIndex, buffer);
	return val.setVisible(true);
}



NextionTimerItemList::NextionTimerItemList(TimerItemList* t) :
		AbstractMyNextionPage(2, 0, "listPage") {
	this->timerItemList = t;
	uint8_t pid = 2;
	this->currPage = 0;

	this->btnYeni = new NexButton(pid, 36, "btnYeni");
	this->btnRefresh = new NexButton(pid, 37, "btnRef");

	this->btnPgGeri = new NexButton(pid, 34, "btnPgGeri");
	this->btnPgIleri = new NexButton(pid, 32, "btnPgIleri");
	this->btnGeri = new NexButton(pid, 35, "btnGeri");

	for (uint8_t i = 0; i < ITEMS_PER_PAGE; i++) {
		this->items[i]=NULL;
		this->items[i] = new NextionTimerItem(i);
		this->items[i]->getTouch()->attachPop(AbstractMyNextionPage::destroyPage,
				this);
	}
	this->btnGeri->attachPop(AbstractMyNextionPage::destroyPage, this);
	this->btnYeni->attachPop(AbstractMyNextionPage::destroyPage, this);
	this->btnPgIleri->attachPop(NextionTimerItemList::nextPage, this);
	this->btnPgGeri->attachPop(NextionTimerItemList::prevPage, this);
	this->btnRefresh->attachPop(NextionTimerItemList::refreshPage, this);

	NextionMonitor::clearNexListenList();
	NextionMonitor::createNexListenList(6 + ITEMS_PER_PAGE);
	NextionMonitor::nex_listen_list[0] = this;
	NextionMonitor::nex_listen_list[1] = this->btnYeni;
	NextionMonitor::nex_listen_list[2] = this->btnPgGeri;
	NextionMonitor::nex_listen_list[3] = this->btnPgIleri;
	NextionMonitor::nex_listen_list[4] = this->btnRefresh;
	NextionMonitor::nex_listen_list[5] = this->btnGeri;

	for (uint8_t i = 0; i < ITEMS_PER_PAGE; i++)
		NextionMonitor::nex_listen_list[6 + i] = this->items[i]->getTouch();

	NextionMonitor::nex_listen_list[6 + ITEMS_PER_PAGE] = NULL;

	this->timerItemList->addTimerItemListener(this);
}

uint8_t NextionTimerItemList::getTotalPage() {
	return (timerItemList != NULL && timerItemList->getSize() > 0) ?
			(((timerItemList->getSize() - 1) / ITEMS_PER_PAGE) + 1) : 1;
}

NextionTimerItemList::~NextionTimerItemList() {
	for (uint8_t i = 0; i < ITEMS_PER_PAGE; i++){
		delete items[i];
		items[i]=NULL;
	}
	NextionMonitor::clearNexListenList();
	this->timerItemList->removeTimerItemListener(this);
	delete this->btnGeri;
	this->btnGeri=NULL;
	delete this->btnPgGeri;
	this->btnPgGeri=NULL;
	delete this->btnPgIleri;
	this->btnPgIleri=NULL;
	delete this->btnRefresh;
	this->btnRefresh=NULL;
	delete this->btnYeni;
	this->btnYeni=NULL;
}

bool NextionTimerItemList::setTimerTimeAt(uint8_t timerTimeIndex) {
	uint8_t targetPage = (timerTimeIndex / 5) + 1;
	if (currPage != targetPage)
		return false;
	uint8_t itemIndex = timerTimeIndex % ITEMS_PER_PAGE;
	TimerItem val = this->timerItemList->getTimerItemAt(timerTimeIndex);
	if(timerTimeIndex>=this->timerItemList->getSize())
		return false;
	this->items[itemIndex]->setItem(val, currPage);

	return true;
}

bool NextionTimerItemList::setCurrentPage(uint8_t pageIndex) {
	bool recvSuccess;
	uint8_t totalPage = this->getTotalPage();
	if (pageIndex > totalPage)
		pageIndex = 1;

	if (pageIndex != currPage) {
		char buffer[10];
		currPage = pageIndex;
		sprintf(buffer, "%u/%u", currPage, totalPage);
		NexText txt(this->getObjPid(), 33, "pageNumber");
		recvSuccess = txt.setText(buffer);
		recvSuccess &= this->btnPgGeri->setVisible(currPage != 1);
		recvSuccess &= this->btnPgIleri->setVisible(currPage != totalPage);
	}
	recvSuccess &= loadTimerPage();
	return recvSuccess;
}

bool NextionTimerItemList::loadTimerPage() {
	uint8_t totalPage = this->getTotalPage();
	if (currPage > totalPage)
		currPage = totalPage;
	if (currPage < 1)
		currPage = 1;
	uint8_t bas = (currPage - 1) * ITEMS_PER_PAGE;
	uint8_t son = currPage * ITEMS_PER_PAGE;
	uint8_t nextionIndex = 0;

	for (uint8_t i = bas; i < son; i++) {
		if (i < this->timerItemList->getSize()) {
			TimerItem tt = this->timerItemList->getTimerItemAt(i);
			this->items[nextionIndex]->setItem(tt, currPage);
		} else
			this->items[nextionIndex]->setNoItem();
		nextionIndex++;
	}

	return true;
}

void NextionTimerItemList::timerItemAdded(TimerItem val, uint8_t timerItemIndex) {
	setCurrentPage(getPageIndexOfItem(timerItemIndex));
}

void NextionTimerItemList::timerItemRemoved(TimerItem val, uint8_t timerItemIndex) {
	setCurrentPage(
			getPageIndexOfItem(timerItemIndex == 0 ? 0 : timerItemIndex - 1));
}

void NextionTimerItemList::timerItemUpdated(TimerItem oldVal, TimerItem newVal,
		uint8_t oldIndex, uint8_t newIndex) {
	setCurrentPage(getPageIndexOfItem(newIndex));
}

bool NextionTimerItemList::pageLoaded() {
	return this->setCurrentPage(1);
}

void NextionTimerItemList::nextPage(void* ptr) {
	NextionTimerItemList *nl = (NextionTimerItemList *) ptr;
	nl->setCurrentPage(nl->getCurrentPage() + 1);
	nl=NULL;
}

void NextionTimerItemList::prevPage(void* ptr) {
	NextionTimerItemList *nl = (NextionTimerItemList *) ptr;
	nl->setCurrentPage(nl->getCurrentPage() - 1);
	nl=NULL;
}

void NextionTimerItemList::refreshPage(void* ptr) {
	NextionTimerItemList *nl = (NextionTimerItemList *) ptr;
	nl->loadTimerPage();
	nl=NULL;
}

