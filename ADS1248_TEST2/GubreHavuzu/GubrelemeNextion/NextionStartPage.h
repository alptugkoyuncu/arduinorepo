/*
 * NextionStartPage.h
 *
 *  Created on: 24 Kas 2016
 *      Author: alptug
 */

#ifndef GUBRELEMENEXTION_NEXTIONSTARTPAGE_H_
#define GUBRELEMENEXTION_NEXTIONSTARTPAGE_H_
#include"AbstractMyNextionPage.h"
class NextionStartPage :public AbstractMyNextionPage {
private:
	NexButton*btn;
public:
	NextionStartPage();
	bool pageLoaded() override;
	~NextionStartPage();
};

#endif /* GUBRELEMENEXTION_NEXTIONSTARTPAGE_H_ */
