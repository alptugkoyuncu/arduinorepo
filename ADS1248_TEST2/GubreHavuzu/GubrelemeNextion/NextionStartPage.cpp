#include"NextionStartPage.h"
#include"NextionMonitor.h"

/*
 * NextionStartPage.cpp
 *
 *  Created on: 24 Kas 2016
 *      Author: alptug
 */


NextionStartPage::NextionStartPage() :
		AbstractMyNextionPage(0, 0, "startingPage") {
	btn = new NexButton(0, 2, "b0");
	btn->attachPop(AbstractMyNextionPage::destroyPage, this);
	NextionMonitor::clearNexListenList();
	NextionMonitor::createNexListenList(2);
	NextionMonitor::nex_listen_list[0] = btn;
	NextionMonitor::nex_listen_list[1] = NULL;
}

bool NextionStartPage::pageLoaded() {
	return true;
}

NextionStartPage::~NextionStartPage() {
	delete btn;
	btn=NULL;
}
