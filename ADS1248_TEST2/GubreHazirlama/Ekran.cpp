/*
 * Ekran.cpp
 *
 *  Created on: 17 Haz 2018
 *      Author: alptug
 */

#include "Ekran.h"

const unsigned char sol = 127;
const unsigned char sag = 126;
const unsigned char ust = 1;
const unsigned char alt = 2;
const unsigned char ent = 3;
const unsigned char esc = 4;
const unsigned char x = 5;
const unsigned char tick = 6;
const unsigned char ustc[] = { 0x04, 0x0E, 0x15, 0x04, 0x04, 0x04, 0x04, 0x00 };
const unsigned char altc[] = { 0x04, 0x04, 0x04, 0x04, 0x15, 0x0E, 0x04, 0x00 };
const unsigned char entc[] = { 0x01, 0x01, 0x05, 0x09, 0x1F, 0x08, 0x04, 0x00 };
const unsigned char escc[] = { 0x0F, 0x01, 0x01, 0x0F, 0x01, 0x01, 0x0F, 0x00 };
uint8_t xc[] = { 0x00, 0x11, 0x0A, 0x04, 0x0A, 0x11, 0x00, 0x00 };
uint8_t tickc[] = { 0x1F, 0x00, 0x01, 0x02, 0x14, 0x08, 0x00, 0x1F };

LiquidCrystal_I2C *lcd = new LiquidCrystal_I2C(0x27, 20, 4);
double *temp = NULL;
TaskOrganizer *to = new TaskOrganizer();
Ekran::EkranDurumu Ekran::ekranDurumu = Ekran::EkranDurumu::null;
void Ekran::AnaEkran::updateTempandToInfo(bool clear) {

	if (clear)
		clearLine(0);

	if (temp == NULL) {
		lcd->print("asd");
		return;
	}
//	int val = (int)(*temp*100);
//	int d =(int)*temp;
//	int f =(int)(val%100);
	lcd->setCursor(0, 0);
	lcd->print(*temp, 2);
	lcd->write(223);
	lcd->print('C');
	delay(10);
	lcd->setCursor(19, 0);
	lcd->write(to->isProgramStarted() ? tick : x);
	delay(10);
}

void Ekran::AnaEkran::updateTaskName(bool clear) {
	if (clear)
		clearLine(2);
	lcd->setCursor(0, 2);
	Task *t = to->getCurrentWorkingTask();
	if (t != NULL)
		lcd->print(t->getName());
}

void Ekran::AnaEkran::updateTaskInfo(bool clear) {
	if (clear)
		clearLine(3);
	Task *t = to->getCurrentWorkingTask();
	if (t != NULL) {

	}
}

void Ekran::showMainScreen() {
	ekranDurumu = EkranDurumu::ANA_EKRAN;
	Serial.println(F("main Screen"));
	lcd->clear();
	a.updateTempandToInfo(false);
	a.updateTaskName(false);
	a.updateTaskInfo(false);
}

void clearLine(uint8_t lineNumber) {
	lcd->setCursor(0, lineNumber);
	char c[21];
	for (int i = 0; i < 20; i++)
		c[i] = ' ';
	c[20] = '\0';
	lcd->print(c);
}
Ekran::MenuEkrani::MenuEkrani() {
	this->selecetedIndex = 0;
	this->minShowingIndex = 0;
	this->maximumShowingIndex = ITEMS_PER_PAGE - 1;
}
void Ekran::MenuEkrani::showProgram() {
	ekranDurumu = EkranDurumu::PROGRAM_LISTE_EKRANI;
	Serial.println(F("Program Menusu"));

	lcd->clear();
	char buffer[20];
	uint8_t size = to->getSize();

	for (int i = this->minShowingIndex;
			i <= this->maximumShowingIndex && i < size; i++) {
		lcd->setCursor(0, i - this->minShowingIndex);
		uint8_t size2 = snprintf(buffer, 20, " %d.%d)%s", i + 1, size,
				to->getTaskFromList(i)->getName());
		if (size2 >= 20)
			buffer[20] = '\0';
		else
			buffer[size2] = '\0';
		lcd->print(buffer);
	}

	this->drowItemIcon();
}

void Ekran::MenuEkrani::nextItem() {
	if (ekranDurumu != EkranDurumu::PROGRAM_LISTE_EKRANI)
		return;
	if (this->selecetedIndex >= to->getSize() - 1) {
		this->removeItemIcon();
		this->selecetedIndex = to->getSize() == 0 ? 0 : to->getSize() - 1;
		this->drowItemIcon();
		return;
	}
	if (this->selecetedIndex < this->maximumShowingIndex) {
		this->removeItemIcon();
		this->selecetedIndex++;
		this->drowItemIcon();
	} else {
		this->selecetedIndex++;
		this->minShowingIndex++;
		this->maximumShowingIndex++;
		this->showProgram();
	}
}

void Ekran::MenuEkrani::prevItem() {
	if (ekranDurumu != EkranDurumu::PROGRAM_LISTE_EKRANI)
		return;
	if (this->selecetedIndex == 0)
		return;
	if (this->selecetedIndex > this->minShowingIndex) {
		this->removeItemIcon();
		this->selecetedIndex--;
		this->drowItemIcon();
	} else {
		this->selecetedIndex--;
		this->minShowingIndex--;
		this->maximumShowingIndex--;
		this->showProgram();
	}
}

void Ekran::readKey(uint8_t key) {
	switch (ekranDurumu) {
	case EkranDurumu::ANA_EKRAN: {
		switch (key) {
		case 'A': {
			m.showProgram();
			return;
		}
		case 'B': {
			p.programIslemEkrani(to->getSize());
			return;
		}
		case esc: {
			to->stop();
			a.updateTempandToInfo(true);
			return;
		}
		case ent: {
			to->start();
			a.updateTempandToInfo(true);
			return;
		}
		default:
			return;
		}
		break;
	}
	case EkranDurumu::PROGRAM_LISTE_EKRANI: {
		switch (key) {
		case esc: {
			showMainScreen();
			return;
		}
		case 'B': {
			p.programIslemEkrani(m.getSelectedIndex());
			return;
		}
		case alt: {
			m.nextItem();
			return;
		}
		case ust: {
			m.prevItem();
			return;
		}
		default:
			return;
		}
		break;
	}
	case EkranDurumu::PROGRAM_ISLEM_EKRANI: {
		if (key == esc) {
			showMainScreen();
		} else if (key == 'A') {
			m.showProgram();
		} else {
			p.readKey(key);
		}
		break;
	}
	case EkranDurumu::SONLANDIRMA_EKRANI: {
		showMainScreen();
		break;
	}
	default:
		p.readKey(key);
		break;
	}

}

void Ekran::selectNextItem(bool flag) {
	flag ? m.nextItem() : m.prevItem();
}

void Ekran::MenuEkrani::drowItemIcon() {
	if (to->getSize() == 0 || this->selecetedIndex < this->minShowingIndex)
		return;
	lcd->setCursor(0, this->selecetedIndex - this->minShowingIndex);
	lcd->write(126);
}

void Ekran::MenuEkrani::removeItemIcon() {
	lcd->setCursor(0, this->selecetedIndex - this->minShowingIndex);
	lcd->write(' ');
}

Ekran::PrograIslem::PrograIslem() {
	this->newTask = '\0';
}
void Ekran::PrograIslem::programIslemEkrani(uint8_t index) {
	this->listIndex = index;
	lcd->clear();
	lcd->setCursor(0, 0);
	lcd->print(F("1-Yeni Ekle"));
	if (listIndex < to->getSize()) {
		lcd->setCursor(0, 1);
		lcd->print(F("2-Secileni Sil"));
	}
	if (to->getSize() > 0) {
		lcd->setCursor(0, 2);
		lcd->print(F("3-Hepsini Sil"));
	}
	ekranDurumu = EkranDurumu::PROGRAM_ISLEM_EKRANI;
}
void Ekran::PrograIslem::gorevSecmeEkrani() {
	lcd->clear();
	lcd->setCursor(0, 0);
	lcd->print(F("1-Isitma"));
	lcd->setCursor(0, 1);
	lcd->print(F("2-Isi Sabitleme"));
	lcd->setCursor(0, 2);
	lcd->print(F("3-Sogutma"));
	lcd->setCursor(0, 3);
	lcd->print(F("4-Bekletme"));
	ekranDurumu = EkranDurumu::GOREV_SECME_EKRANI;
}

void Ekran::PrograIslem::indexAyarlamaSayfasi() {
	if (ekranDurumu != EkranDurumu::INDEX_AYARLAMA_EKRANI) {
		lcd->clear();
		lcd->setCursor(0, 0);
		lcd->print(F("Gorev sirasini"));
		lcd->setCursor(0, 1);
		lcd->print(F("giriniz"));
	}
	char buffer[2];
	lcd->setCursor(0, 3);
	sprintf(buffer, "%02d", this->listIndex);
	lcd->print(buffer);
	ekranDurumu = EkranDurumu::INDEX_AYARLAMA_EKRANI;
}

void Ekran::PrograIslem::sicaklikSecimi() {
	if (ekranDurumu != EkranDurumu::SICAKLIK_AYARLAMA_EKRANI) {
		lcd->clear();
		lcd->setCursor(0, 0);
		lcd->print(F("Sicaklik giriniz"));
	}
	int t = 0;
	switch (newTask->getClass()) {
	case Class::ISITMA:
		t = ((IsitmaTask*) newTask)->getBitisSicaklik();
		break;
	case Class::SICAKLIK_TUTMA:
		t = ((IsiSabitleme*) newTask)->getBitisSicaklik();
		break;
	case Class::SOGUTMA:
		t = ((SogutmaTask *) newTask)->getBitisSicaklik();
		break;
	default:
		t = 0;
		break;
	}
	lcd->setCursor(0, 3);
	char buffer[3];
	sprintf(buffer, "%03d", t);
	lcd->print(buffer);
	ekranDurumu = EkranDurumu::SICAKLIK_AYARLAMA_EKRANI;

}

void Ekran::PrograIslem::sureSecimi() {
	if (ekranDurumu != EkranDurumu::SURE_SECME_EKRANIM) {
		lcd->clear();
		lcd->setCursor(0, 0);
		lcd->print(F("Sure giriniz(dk)"));
	}
	Class c = newTask->getClass();
	uint16_t s;
	switch (c) {
	case Class::ZAMAN:
		s = ((ZamanTask*) newTask)->getSure();
		break;
	case Class::SICAKLIK_TUTMA:
		s = ((IsiSabitleme *) newTask)->getSure();
		break;
	default:
		s = 0;
		break;
	}
	char buffer[4];
	sprintf(buffer, "%04d", s);
	lcd->setCursor(0, 3);
	lcd->print(buffer);
	ekranDurumu = EkranDurumu::SURE_SECME_EKRANIM;
}

void Ekran::PrograIslem::karistimaSecimi() {
	if (ekranDurumu != EkranDurumu::KARISTIRMA_SECME_EKRANI) {
		lcd->clear();
		lcd->setCursor(0, 0);
		lcd->print(F("Karistirma aktif mi"));
		lcd->setCursor(0, 1);
		lcd->print(F("1-aktif 0-pasif"));
	}
	lcd->setCursor(0, 2);
	lcd->print(newTask->isKaristiriciAktif() ? F("aktif") : F("pasif"));
	ekranDurumu = EkranDurumu::KARISTIRMA_SECME_EKRANI;
}

void Ekran::PrograIslem::sonlandirmaEkrani() {
	if (newTask != NULL) {
		to->addNewTask(newTask, this->listIndex);
		newTask = NULL;
	}
	lcd->clear();
	lcd->setCursor(0, 0);
	lcd->print(F("bitti"));
	ekranDurumu = EkranDurumu::SONLANDIRMA_EKRANI;
}

#define ISLEM_DURUMU_BASARISIZ 0
#define ISLEM_DURUMU_BASARILI 1
void Ekran::PrograIslem::readKey(uint8_t keypadKey) {
//	uint8_t durum;
//	char bilgi[4][20] = { 0 };
	switch (ekranDurumu) {
	case EkranDurumu::PROGRAM_ISLEM_EKRANI: {
		switch (keypadKey) {
		case '1': {
			gorevSecmeEkrani();
			break;
		}
		case '2': {
			this->newTask = to->removeTask(this->listIndex);
			if (this->newTask != NULL) {
				delete this->newTask;
				this->newTask = NULL;
//				*bilgi[0] = "Gorev Silindi";
//				durum = ISLEM_DURUMU_BASARILI;
				sonlandirmaEkrani();
			}
			break;
		}
		case '3': {
			to->removeAll();
//			*bilgi[0] = "Gorevler Silindi";
//			durum = ISLEM_DURUMU_BASARILI;
			sonlandirmaEkrani();
			break;
		}
		default: {
//			*bilgi[0] = "Hatali islem";
//			*bilgi[1] = "1-4 arasinda";
//			*bilgi[2] = "deger giriniz";
//			durum = ISLEM_DURUMU_BASARISIZ;
			break;

		}
		}
		break;
	}
	case EkranDurumu::GOREV_SECME_EKRANI: {
		if (newTask != NULL) {
			delete newTask;
			newTask = NULL;
		}
		switch (keypadKey) {
		case '1':
			newTask = new IsitmaTask();
			break;
		case '2':
			newTask = new IsiSabitleme();
			break;
		case '3':
			newTask = new SogutmaTask();
			break;
		case '4':
			newTask = new ZamanTask();
			break;
		case sol:
			sonlandirmaEkrani();
			return;
		default:
			return;
		}
		indexAyarlamaSayfasi();
		break;
	}
	case EkranDurumu::INDEX_AYARLAMA_EKRANI: {
		if (keypadKey >= '0' && keypadKey <= '9') {
			this->listIndex = this->listIndex * 10
					+ ((byte) keypadKey - (byte) '0');
			if (this->listIndex > to->getSize())
				this->listIndex = to->getSize();
			indexAyarlamaSayfasi();
		} else if (keypadKey == sol) {
			this->listIndex = this->listIndex / 10;
			indexAyarlamaSayfasi();
		} else if (keypadKey == ent) {
			Class c = newTask->getClass();
			if (c == Class::ZAMAN) {
				sureSecimi();
			} else
				sicaklikSecimi();
			return;
		} else if (keypadKey == esc) {
			gorevSecmeEkrani();
		}
		break;
	}
	case EkranDurumu::SICAKLIK_AYARLAMA_EKRANI: {

		if (keypadKey == ent) {
			Class c = newTask->getClass();
			if (c == Class::SICAKLIK_TUTMA) {
				sureSecimi();
			} else
				karistimaSecimi();
		} else if (keypadKey == esc) {
			indexAyarlamaSayfasi();
		} else {
			int t = 0;
			switch (newTask->getClass()) {
			case Class::ISITMA:
				t = ((IsitmaTask*) newTask)->getBitisSicaklik();
				break;
			case Class::SICAKLIK_TUTMA:
				t = ((IsiSabitleme*) newTask)->getBitisSicaklik();
				break;
			case Class::SOGUTMA:
				t = ((SogutmaTask *) newTask)->getBitisSicaklik();
				break;
			default:
				t = 0;
				break;
			}
			if (keypadKey >= '0' && keypadKey <= '9') {
				t = t * 10 + ((byte) keypadKey - (byte) '0');
				if (t > 200)
					t = 200;
			} else if (keypadKey == sol) {
				t = t / 10;
			}
			switch (newTask->getClass()) {
			case Class::ISITMA:
				((IsitmaTask*) newTask)->setBitisSicaklik(t);
				break;
			case Class::SICAKLIK_TUTMA:
				((IsiSabitleme*) newTask)->setBitisSicaklik(t);
				break;
			case Class::SOGUTMA:
				((SogutmaTask *) newTask)->setBitisSicaklik(t);
				break;
			default:
				break;
			}
			sicaklikSecimi();
		}
		break;
	}
	case EkranDurumu::SURE_SECME_EKRANIM: {
		if (keypadKey == ent) {
			karistimaSecimi();
		} else if (keypadKey == esc) {
			Class c = newTask->getClass();
			switch (c) {
			case Class::ZAMAN:
				indexAyarlamaSayfasi();
				break;

			case Class::SICAKLIK_TUTMA:
				sicaklikSecimi();
				break;
			default:
				break;
			}
		} else {
			uint16_t t = 0;
			switch (newTask->getClass()) {
			case Class::SICAKLIK_TUTMA:
				t = ((IsiSabitleme*) newTask)->getSure();
				break;
			case Class::ZAMAN:
				t = ((ZamanTask *) newTask)->getSure();
				break;
			default:
				t = 0;
				break;
			}
			if (keypadKey >= '0' && keypadKey <= '9') {
				t = t * 10 + ((byte) keypadKey - (byte) '0');
				if (t > 1000)
					t = 1000;
			} else if (keypadKey == sol) {
				t = t / 10;
			}
			switch (newTask->getClass()) {
			case Class::SICAKLIK_TUTMA:
				((IsiSabitleme*) newTask)->setSure(t);
				break;
			case Class::ZAMAN:
				((ZamanTask *) newTask)->setSure(t);
				break;
			default:
				break;
			}
			sureSecimi();
		}
		break;
	}
	case EkranDurumu::KARISTIRMA_SECME_EKRANI: {
		if (keypadKey == ent) {
			sonlandirmaEkrani();
		} else if (keypadKey == esc) {
			Class c = newTask->getClass();
			switch (c) {
			case Class::SICAKLIK_TUTMA:
			case Class::ZAMAN:
				sureSecimi();
				break;
			case Class::ISITMA:
			case Class::SOGUTMA:
				sicaklikSecimi();
				break;
			default:
				break;
			}
		} else {
			if (keypadKey == '1') {
				newTask->setKaristiriciAktif(true);
			} else if (keypadKey == '0') {
				newTask->setKaristiriciAktif(false);
			}
			karistimaSecimi();
		}
		break;
	}
	case EkranDurumu::SONLANDIRMA_EKRANI: {
		break;
	}
	default:
		break;
	}
}

void Ekran::PrograIslem::bilgiMesaji() {

}
