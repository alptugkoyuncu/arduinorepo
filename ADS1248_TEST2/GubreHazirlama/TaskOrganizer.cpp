/*
 * TaskList.cpp
 *
 *  Created on: 15 Haz 2018
 *      Author: alptug
 */

#include "TaskOrganizer.h"

void TaskOrganizer::addNewTask(Task* t, int8_t targetIndex) {
	if (this->size >= TASK_LIST_MAXIMUM_SIZE)
		return;
	if (targetIndex < 0) {
		list[size++] = t;
	} else {
		for (int i = size; i > targetIndex; i--) {
			list[i] = list[i - 1];
		}
		list[targetIndex] = t;
		size++;
	}
	to_event = TaskOrganizerEvent::NEW_ITEM_ADDED;
}

Task* TaskOrganizer::removeTask(uint8_t index) {
	if (index >= size)
		return NULL;
	Task *t = list[index];
	for (int i = index; i < size - 1; i++) {
		list[i] = list[i + 1];
	}
	size--;
	to_event = TaskOrganizerEvent::NEW_ITEM_REMOVED;
	return t;
}

bool TaskOrganizer::check() {
	if (this->currIndex >= this->size) {
		if (this->programStarted) {
			this->programStarted = false;
			return true;
		}
		return false;
	}
	if (this->programStarted) {
		if (list[this->currIndex]->getBaslamaZamani() == 0) {
			if (millis() - this->waitForStart > WAIT_SECOND_BETWEEN_TASKS) {
				list[this->currIndex]->start();
				Serial.println(F("Started"));
				return true;
			}
		} else {
			if (list[this->currIndex]->check(&this->temp2)) {
				list[this->currIndex++]->stop();
				Serial.println(F("Stop"));
				this->waitForStart = millis();
				return true;
			}
		}
	}else{
		if(list[this->currIndex]->isWorking())
			list[this->currIndex]->stop();
		return true;
	}
	return false;

}

Task* TaskOrganizer::getCurrentWorkingTask() {
	return this->programStarted && this->currIndex < this->size ?
			this->list[this->currIndex] : NULL;
}

extern void declarePins() {
}

extern void startIsitici() {
	Serial.println(F("Calisiyor - Isitici"));
}

extern void stopIsitici() {
	Serial.println(F("Durdu - Isitici"));
}

extern void startsogutucu() {
	Serial.println(F("Calisiyor - Sogutucu"));
}

extern void stopSogutucu() {
	Serial.println(F("Durdu - Sogutucu"));
}

extern void startKaristirici() {
	Serial.println(F("Calisiyor - Karistirici"));
}

extern void stopKaristirici() {
	Serial.println(F("Durdu - Karisitirici"));
}

void TaskOrganizer::removeAll() {
	//Serial.print("Reading all : ");
	//Serial.println(this->size);
//	char buffer[100];
//	for (int i = 0; i < this->size; i++) {
//		sprintf(buffer, "Reading %d.index -> name : %s", i,this->list[i]->getName());
	//Serial.println(buffer);
//	}
//	Serial.println("Remove all");
//	delay(500);
	for (int i = 0; i < this->size; i++) {
//		Serial.print("Remove :");
//		Serial.print(i);
//		Serial.print(". index : ");
//		Serial.println(list[i]->getName());
		delete list[i];
		this->list[i] = NULL;
	}
	this->size = 0;
	delay(20);
}

void TaskOrganizer::start() {
	this->programStarted = true;
}

void TaskOrganizer::stop() {
	this->programStarted = false;
	this->currIndex = 0;
}
