################################################################################
# Automatically-generated file. Do not edit!
################################################################################

INO_SRCS := 
ASM_SRCS := 
O_UPPER_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
PDE_SRCS := 
CC_SRCS := 
AR_SRCS := 
C_SRCS := 
C_UPPER_DEPS := 
PDE_DEPS := 
C_DEPS := 
AR := 
CC_DEPS := 
AR_OBJ := 
C++_DEPS := 
LINK_OBJ := 
CXX_DEPS := 
ASM_DEPS := 
HEX := 
INO_DEPS := 
SIZEDUMMY := 
S_UPPER_DEPS := 
ELF := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
. \
core\core \
libraries\DHT \
libraries\DS3231 \
libraries\DS3231\Examples\DS3231_TEST \
libraries\EEPROM\examples\eeprom_clear \
libraries\EEPROM\examples\eeprom_crc \
libraries\EEPROM\examples\eeprom_get \
libraries\EEPROM\examples\eeprom_iteration \
libraries\EEPROM\examples\eeprom_put \
libraries\EEPROM\examples\eeprom_read \
libraries\EEPROM\examples\eeprom_update \
libraries\EEPROM\examples\eeprom_write \
libraries\ITEADLIB_Arduino_Nextion-master \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompButton \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompCrop \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompDualStateButton \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompGauge \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompHotspot \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompNumber \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompPage \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompPicture \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompProgressBar \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompSlider \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompText \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompTimer \
libraries\ITEADLIB_Arduino_Nextion-master\examples\CompWaveform \
libraries\MemoryFree \
libraries\MemoryFree\examples\FreeMemory \
libraries\Wire\examples\SFRRanger_reader \
libraries\Wire\examples\digital_potentiometer \
libraries\Wire\examples\master_reader \
libraries\Wire\examples\master_writer \
libraries\Wire\examples\slave_receiver \
libraries\Wire\examples\slave_sender \
libraries\Wire\src \
libraries\Wire\src\utility \
libraries\sht1xalt-master\examples\sht1xalt_simple \
libraries\sht1xalt-master \

