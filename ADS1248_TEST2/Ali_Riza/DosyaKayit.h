/*
 * MyEEPROM.h
 *
 *  Created on: 12 �ub 2016
 *      Author: Alptug
 */

#ifndef DOSYAKAYIT_H_
#define DOSYAKAYIT_H_
#include<EEPROM.h>
#include"OtomasyonBirimi.h"
#include"Arduino.h"
struct DosyaBilgileri {
	uint16_t kayitSayisi;
	uint8_t isiticiOtomatik;
	uint8_t sogutucuOtomatik;
	uint8_t nemlendiriciOtomatik;
	uint8_t isiklandiriciOtomatik;
	uint8_t isiticiBaslatma;
	uint8_t isiticiBitirme;
	uint8_t sogutucuBaslatma;
	uint8_t sogutucuBitirme;
	uint8_t nemlendiriciBaslatma;
	uint8_t nemlendiriciBitirme;
	uint8_t isiklandirmaBaslatmaSaat;
	uint8_t isiklandirmaBaslatmaDakika;
	uint8_t isiklandirmaBitirmeSaat;
	uint8_t isiklandirmaBitirmeDakika;
};
class OtomasyonKayit {
private:
	const static int HAS_RECORD_ADDRESS=0;
	const static int MOVED_ADDRESS=1;
	const static bool kayitEnabled = true;
	const static int DELAY_WAIT = 100;
	DosyaBilgileri db;
protected:
	// datalarin kac defa yer degistirdigi moved de kayitli tutulacak 60 000 uzerine yazma da bir (100 000 cycle sinirindan dolayi) moved bir artacak
	//startIndex hesaplamasi da moved*sizeof(DosyaBilgileri) +1 seklinde olacak
	//kac�nc� �ndex den balad�g�n� kontrol edecek kayitSayisi 60000 i  gecince yeni yere yazilacak. hesaplamasi sizeof(Dosyabilgileri)*startIndex+1;

	const int getStartAddress() {
		uint8_t moved = this->getMovedDegeri();
		if(db.kayitSayisi>60000){
			moved++;
			EEPROM.write(MOVED_ADDRESS,moved);
			db.kayitSayisi=0;
		}
		return sizeof(DosyaBilgileri) * moved + 2;
	}
public:
	OtomasyonKayit() {
		uint8_t val = EEPROM.read(0);
		bool hasRecord = val == 1;
		if (!hasRecord) {
			Serial.println("HAS NO RECORD CONSTRUCTOR");
			EEPROM.write(HAS_RECORD_ADDRESS, 1);	//has record =true
			EEPROM.write(MOVED_ADDRESS, 0);	// ilk moved degerini ata
			db = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			int addr = this->getStartAddress();
			EEPROM.put(addr, db);
		} else {
			Serial.print("RECORD : ");
			Serial.println(val);
			this->readFromEEPROM();
		}
	}
	uint8_t getMovedDegeri(){
		return  EEPROM.read(1);
	}
	uint16_t getKayitSayisi() {
		return db.kayitSayisi;
	}
	uint8_t getDosyaBilgileriSize(){
		return sizeof(DosyaBilgileri);
	}
	void loadToOtomasyonBirimi(OtomasyonBirimi &isitici, Sogutucu &sogutucu,
			OtomasyonBirimi &nemlendirici, Isiklandirma &isiklandirma) {

		DosyaBilgileri db2 = db;
		isitici.set(db2.isiticiOtomatik, db2.isiticiBaslatma,
				db2.isiticiBitirme);
		sogutucu.set(db2.sogutucuOtomatik, db2.sogutucuBaslatma,
				db2.sogutucuBitirme);
		nemlendirici.set(db2.nemlendiriciOtomatik, db2.nemlendiriciBaslatma,
				db2.nemlendiriciBitirme);
		isiklandirma.set(db2.isiklandiriciOtomatik,
				db2.isiklandirmaBaslatmaSaat, db2.isiklandirmaBaslatmaDakika,
				db2.isiklandirmaBitirmeSaat, db2.isiklandirmaBitirmeDakika);
	}
	void readFromEEPROM() {
		int eeAddress = getStartAddress();
		Serial.print("eeAdress : ");
		Serial.println(eeAddress);
		delay(DELAY_WAIT);
		EEPROM.get(eeAddress, db);
		delay(DELAY_WAIT);
		printDB(db);

	}
	int saveSogutucu(OtomasyonBirimi sogutucu) {
		if (db.sogutucuOtomatik == sogutucu.isOtomatikAktif()
				&& db.sogutucuBaslatma == sogutucu.getBaslamaDegeri()
				&& db.sogutucuBitirme == sogutucu.getBitisDegeri())
			return -1;

		int eeAddress = getStartAddress();

		db.kayitSayisi++;
		db.sogutucuOtomatik = sogutucu.isOtomatikAktif();
		db.sogutucuBaslatma = (uint8_t) sogutucu.getBaslamaDegeri();
		db.sogutucuBitirme = (uint8_t) sogutucu.getBitisDegeri();

		printDB(db);
		if (kayitEnabled) {
			delay(DELAY_WAIT);
			EEPROM.put(eeAddress, db);
			delay(DELAY_WAIT);
		}
		return db.kayitSayisi;
	}
	int saveIsitici(OtomasyonBirimi isitici) {
		if (db.isiticiOtomatik == isitici.isOtomatikAktif()
				&& db.isiticiBaslatma == isitici.getBaslamaDegeri()
				&& db.isiticiBitirme == isitici.getBitisDegeri())
			return -1;

		int eeAddress = getStartAddress();

		db.kayitSayisi++;
		db.isiticiOtomatik = isitici.isOtomatikAktif();
		db.isiticiBaslatma = (uint8_t) isitici.getBaslamaDegeri();
		db.isiticiBitirme = (uint8_t) isitici.getBitisDegeri();

		printDB(db);
		if (kayitEnabled) {
			delay(DELAY_WAIT);
			EEPROM.put(eeAddress, db);
			delay(DELAY_WAIT);
		}

		return db.kayitSayisi;
	}
	int saveNemlendirici(OtomasyonBirimi nemlendirici) {

		if (db.nemlendiriciOtomatik == nemlendirici.isOtomatikAktif()
				&& db.nemlendiriciBaslatma == nemlendirici.getBaslamaDegeri()
				&& db.nemlendiriciBitirme == nemlendirici.getBitisDegeri())
			return -1;

		int eeAddress = getStartAddress();

		db.kayitSayisi++;
		db.nemlendiriciOtomatik = nemlendirici.isOtomatikAktif();
		db.nemlendiriciBaslatma = (uint8_t) nemlendirici.getBaslamaDegeri();
		db.nemlendiriciBitirme = (uint8_t) nemlendirici.getBitisDegeri();

		printDB(db);
		if (kayitEnabled) {
			delay(DELAY_WAIT);
			EEPROM.put(eeAddress, db);
			delay(DELAY_WAIT);
		}
		return db.kayitSayisi;
	}
	int saveIsiklandirma(Isiklandirma isiklandirma) {

		if (db.isiklandiriciOtomatik == isiklandirma.isOtomatikAktif()
				&& db.isiklandirmaBaslatmaSaat == isiklandirma.getBaslamaSaati()
				&& db.isiklandirmaBaslatmaDakika
						== isiklandirma.getBaslamaDakika()
				&& db.isiklandirmaBitirmeSaat == isiklandirma.getBitisSaati()
				&& db.isiklandirmaBitirmeDakika
						== isiklandirma.getBitisDakikasi())
			return -1;

		int eeAddress = getStartAddress();

		db.kayitSayisi++;
		db.isiklandiriciOtomatik = isiklandirma.isOtomatikAktif();
		db.isiklandirmaBaslatmaSaat = (uint8_t) isiklandirma.getBaslamaSaati();
		db.isiklandirmaBaslatmaDakika =
				(uint8_t) isiklandirma.getBaslamaDakika();
		db.isiklandirmaBitirmeSaat = (uint8_t) isiklandirma.getBitisSaati();
		db.isiklandirmaBitirmeDakika =
				(uint8_t) isiklandirma.getBitisDakikasi();

		printDB(db);
		if (kayitEnabled) {
			delay(DELAY_WAIT);
			EEPROM.put(eeAddress, db);
			delay(DELAY_WAIT);
		}
		return db.kayitSayisi;

	}

	void printDB(DosyaBilgileri db) {
		char buffer[100];
		Serial.println("READ EEPROM:");
		memset(buffer, 0, sizeof(buffer));
		sprintf(buffer, "Isitici : %d %d %d", db.isiticiOtomatik,
				db.isiticiBaslatma, db.isiticiBitirme);
		Serial.println(buffer);
		memset(buffer, 0, sizeof(buffer));
		sprintf(buffer, "Sogutucu: %d %d %d", db.sogutucuOtomatik,
				db.sogutucuBaslatma, db.sogutucuBitirme);
		Serial.println(buffer);
		memset(buffer, 0, sizeof(buffer));
		sprintf(buffer, "Nemlendirici : %d %d %d", db.nemlendiriciOtomatik,
				db.nemlendiriciBaslatma, db.nemlendiriciBitirme);
		Serial.println(buffer);
		memset(buffer, 0, sizeof(buffer));
		sprintf(buffer, "Isiklandirma : %d %d:%d %d:%d",
				db.isiklandiriciOtomatik, db.isiklandirmaBaslatmaSaat,
				db.isiklandirmaBaslatmaDakika, db.isiklandirmaBitirmeSaat,
				db.isiklandirmaBitirmeDakika);
		Serial.println(buffer);
	}
	~OtomasyonKayit() {
	}
};

#endif /* DOSYAKAYIT_H_ */
