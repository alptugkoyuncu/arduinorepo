/*
 * MyNex.h
 *
 *  Created on: 26 Oca 2016
 *      Author: Alptug
 */

#ifndef MYNEX_H_
#define MYNEX_H_

#include"arduino.h"
#include "Nextion.h"
#include"Sensorler.h"

#define DBG_MYNEX

#ifdef DBG_MYNEX
#define nexprint(a) Serial.print(a)
#define nexprintln(a) Serial.println(a)

#else
#define nexprint(a) ;
#define nexprintln(a) ;
#endif

class MyNex {
	private:
	int picWifiOn=17;
	int picWifiOff=18;
protected:
	NexNumber *getMinElement(uint8_t pageId);
	NexNumber *getMaxElement(uint8_t pageId);
	NexNumber *getMin2Element(uint8_t pageId);
	NexNumber *getMax2Element(uint8_t pageId);

	NexDSButton *getIsWorkingElement(uint8_t pageId);
	NexDSButton *getIsOtomatikElement(uint8_t pageId);

	NexNumber *getGMinElement(uint8_t pageId);
	NexNumber *getGMaxElement(uint8_t pageId);
	NexNumber *getGMin2Element(uint8_t pageId);
	NexNumber *getGMax2Element(uint8_t pageId);
	NexNumber *getGIsOtomatikElement(uint8_t pageId);

	NexText *getNemText();
	NexText *getSicaklikText();
	NexText *getSaatText();
	NexText *getTarihText();

	//listener eklenecek buttunlar

public:
	 NexDSButton *dsbIsiticiOnOff;
		 NexDSButton *dsbSogutucuOnOff;
		 NexDSButton *dsbNemlendiriciOnOff;
		 NexDSButton *dsbIsiklandirmaOnOff;

		 NexButton *kaydetIsitici;
		 NexButton *kaydetSogutucu;
		 NexButton *kaydetNemlendirici;
		 NexButton *kaydetIsiklandirma;
	const static uint8_t ID_MAIN_PAGE = 0;
	 const static uint8_t ID_ISITICI_PAGE = 1;
	 const static uint8_t ID_SOGUTUCU_PAGE = 2;
	 const	 static uint8_t ID_NEMLENDIRICI_PAGE = 3;
	 const static uint8_t ID_ISIKLANDIRMA_PAGE = 4;
	 const	 static uint8_t ID_NUMBER_PAD_PAGE = 5;
	 const	 static uint8_t ID_UPDATE_PAGE = 6;
	bool hasError = false;
	MyNex();
	~MyNex();

	NexPage getPage(const uint8_t pageId);
	uint8_t getCurrentPageId();
	static const uint8_t getPageIdByFromObject(NexObject *obj);
	static const uint8_t getRelatedPageIdFormDSButton(NexDSButton *dsbtn);
	bool setWifi(bool avaible);
	bool setMainPageValues(float sicaklik, float nem,DateTime dt);
	bool setWorkingValues(bool isIsiticiWorking,bool isSogutucuWorking, bool isNemlendiriciWorking,bool isIsiklandirmaWorking);
	bool setOtomasyonPageValues(uint8_t pageId,uint16_t min, uint16_t max, bool isOtomatik);
	bool setMinValue(uint8_t pageId, uint16_t min);
	bool setMaxValue(uint8_t pageId,uint16_t max);
	bool setOtomatiklikAktif(uint8_t pageId,bool isOtomatik);
	bool setWorkingValue(uint8_t pageId,bool isWorking);
	bool gotoPage(uint8_t pageId);
	bool getMinValue(uint8_t pageId, uint32_t &min);
	bool getMaxValue(uint8_t pageId,uint32_t &max);
	bool isOtomatik(uint8_t pageId, bool &isOtomatik);
	bool isWorking(uint8_t pageId, bool &isWorking);
	bool setNem(float nem);
	bool setSicaklik(float sicaklik);
	bool setSaat(uint8_t saat, uint8_t dakika);
	bool setTarih(uint16_t yil, uint8_t ay, uint8_t gun);
	void getObjInfo(NexObject obj, char *buffer, int size);

};

#endif /* MYNEX_H_ */
