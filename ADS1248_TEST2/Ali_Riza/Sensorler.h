/*
 * Sensorler.h
 *
 *  Created on: 17 Oca 2016
 *      Author: Alptug
 */

#ifndef SENSORLER_H_
#define SENSORLER_H_
#include "Arduino.h"
#include "SHT1xalt.h"
#include <Wire.h>         // this #include still required because the RTClib depends on it
#include "DS3231.h"
#include"MemoryFree.h"
#include<DHT.h>
class SensorError {
protected:
	uint8_t err;
public:
	static const uint8_t RTC_NOT_FOUND = 0;
	static const uint8_t RTC_NOT_RUNNING = 1;
	static const uint8_t STH10_NO_ACK = 2;
	static const uint8_t SH10_TIMEOUT = 3;

	SensorError(uint8_t errNo) {
		this->err = errNo;
	}
	uint8_t getErrorNo() {
		return this->err;
	}
	void toString(char buffer[], int &size) {
		switch (err) {
		case RTC_NOT_FOUND: {
			size = sprintf(buffer, "RTC not found");
			return;
		}
		case RTC_NOT_RUNNING: {
			size = sprintf(buffer, "RTC is not running");
			return;
		}
		case STH10_NO_ACK: {
			size = sprintf(buffer, "SHT10 failed to acknowledge a command!");
			return;
		}
		case SH10_TIMEOUT: {
			size = sprintf(buffer, "SHT10 failed to produce a measurement!");
			return;
		}
		default:
			size = sprintf(buffer, "UNKNOWN ERROR: Error No = %d", this->err);
			return;
		}
	}
};
class SensorErrorHandler {
private:
	uint8_t id;
public:
	virtual void handleError(SensorError err);
	void setId(uint8_t id) {
		this->id = id;
	}
	uint8_t getId() {
		return this->id;
	}
};

class SensorEvent {
protected:
	uint8_t type;
public:
	static const uint8_t SENS0R_TEMPERATURE = 1;
	static const uint8_t SENSOR_HUMIDITY = 2;
	static const uint8_t SENSOR_DATE = 3;
	static const uint8_t SENSOR_TIME = 4;
	static const uint8_t SENSOR_SECOND = 5;
	static const uint8_t SENSOR_DATE_TIME = 6;

	SensorEvent(uint8_t type) {
		this->type = type;
	}
	uint8_t getType() {
		return this->type;
	}
};

class SensorListener {
	uint8_t sensorType;
	uint8_t id;
public:
	virtual void valueChanged(SensorEvent evt);
	void setId(uint8_t id) {
		this->id = id;
	}
	uint8_t getId() {
		return this->id;
	}

};

#define MAXIMUM_SENSOR_LISTENER 5
#define MAXIMUM_ERROR_HANDLER 5

class Sensor {
protected:
	unsigned long waitMillis;
	unsigned long lastUpdate;
	SensorListener *listener[5];
	SensorErrorHandler *handler[5];
	uint8_t handlerIndex;
	uint8_t listenerIndex;
	void resetLastUpdate() {
		this->lastUpdate = millis();
	}

	void fireEvent(SensorEvent evt) {
		for (int i = 0; i < this->listenerIndex; i++) {
//			Serial.print(i);
//			Serial.println(F(". sensor event fired"));
			this->listener[i]->valueChanged(evt);
		}
	}
	void handleError(uint8_t errNo) {
		SensorError evt(errNo);
		for (int i = 0; i < this->handlerIndex; i++)
				handler[i]->handleError(evt);
	}
public:
	Sensor(){
		this->handlerIndex=0;
		this->listenerIndex=0;
	}
	~Sensor(){

	}
	virtual void begin()=0;
	void addErrorHandler(SensorErrorHandler *eh) {
		eh->setId(this->handlerIndex);
		this->handler[this->handlerIndex++] = eh;
	}
	void removeHandler(SensorErrorHandler *eh) {
		uint8_t id = eh->getId();
		for (int i = id; i < this->handlerIndex - 1; i++) {
			this->handler[id] = this->handler[i + 1];
		}
		delete eh;
		this->handler[this->handlerIndex] = NULL;
		this->handlerIndex--;
	}
	void addListener(SensorListener *l) {
		if(this->listenerIndex>=5){
			Serial.println("Index Out of range");
			return;
		}
		l->setId(this->listenerIndex);
		this->listener[this->listenerIndex++] = l;
	}
	void removeListener(SensorListener *l) {
		uint8_t id = l->getId();
		for (int i = id; i < this->listenerIndex - 1; i++) {
			this->listener[id] = this->listener[i + 1];
		}
		delete l;
		this->listener[this->listenerIndex] = NULL;
		this->listenerIndex--;
	}
	virtual bool update()=0;
	void setUpdateSecond(uint16_t waitSec) {
		this->waitMillis = waitSec;
	}
};
class TemperatureHumiditySensor: public Sensor {
protected:
	float temp, rh;

private:
	bool isEqual(float val1, float val2) {
		switch (precision) {
		case NO_PRECISION: {
			uint8_t v1 = val1;
			uint8_t v2 = val2;
			return v1 == v2;
		}
		case PRECISION_1: {
			uint16_t v1 = (val1 * 10);
			uint16_t v2 = (val2 * 10);
			return v1 == v2;
		}
		case PRECISION_2: {
			uint16_t v1 = (val1 * 100);
			uint16_t v2 = (val2 * 100);
			return v1 == v2;
		}
		default:
			return val1 == val2;
		}
	}
public:
	static const uint8_t PRECISION_1 = 1;
	static const uint8_t PRECISION_2 = 2;
	static const uint8_t NO_PRECISION = 0;
	uint8_t precision;
	TemperatureHumiditySensor() {
		Serial.println("TEMP HUM constructor");
		this->precision = NO_PRECISION;
		temp=-100;
		rh = -100;
	}


	void setTemp(float t) {
		Serial.print("Sicaklik :");
		Serial.println(t);
		if(this->isEqual(this->temp,t))
		return;
		this->temp=t;
		SensorEvent evt(SensorEvent::SENS0R_TEMPERATURE);
		this->fireEvent(evt);
	}
	void setHumidity(float rh) {
		Serial.print("Hum : ");
		Serial.println(rh);
		if(this->isEqual(this->rh,rh))
		return;
		this->rh=rh;

		SensorEvent evt(SensorEvent::SENSOR_HUMIDITY);
		this->fireEvent(evt);
	}
	float getTemp() {
		return this->temp;
	}
	float getHumidity() {
		return this->rh;
	}
};

#define DHTPIN 8
#define DHTTYPE DHT22
class DHT_Sensor:public TemperatureHumiditySensor{
private :
	DHT *dht;
	bool started;
public:
	DHT_Sensor(){
		Serial.println(" constructor DHT sensor");
		dht = new DHT(DHTPIN, DHTTYPE);
		started = false;
		this->begin();
	}
	void begin() override{
		if(started)
			return;
		dht->begin();
		this->lastUpdate=millis();
		this->waitMillis=4000;
		Serial.println("DHT22 begin");
		started = true;
	}
	bool update() override{
		if(millis()>this->waitMillis+this->lastUpdate){
			float t = dht->readTemperature(false);
			float h = dht->readHumidity();
			this->lastUpdate = millis();
			if(isnan(t) || isnan(h)){
				return false;
			}
			this->setTemp(dht->readTemperature(false));
			this->setHumidity(dht->readHumidity());
			return true;
		}
		return false;
	}
	~DHT_Sensor(){
		delete dht;
	}
};

// Set these to whichever pins you connected the SHT1x to:
#define dataPin 6
#define clockPin 7

// Set this number larger to slow down communication with the SHT1x, which may
// be necessary if the wires between the Arduino and the SHT1x are long:
#define clockPulseWidth 2

// The next lines are fine if you're using a 5V Arduino. If you're using a 3.3V
// Arduino (such at the Due), comment the next line and uncomment the one after:

#define supplyVoltage sht1xalt::VOLTAGE_5V
//#define supplyVoltage sht1xalt::VOLTAGE_3V5

// If you want to report temperature units in Fahrenheit instead of Celcius,
// comment the next line and uncomment the one after:
#define temperatureUnits sht1xalt::UNITS_CELCIUS
//#define temperatureUnits sht1xalt::UNITS_FAHRENHEIT

class SHT10Sensor : public TemperatureHumiditySensor{
private:
	sht1xalt::Sensor *sensor;
	bool started = false;
public:
	SHT10Sensor(){
		Serial.println("sht10 constructor");
		this->begin();
	}
	void begin() override {
		if(started)
			return;
		sensor = new sht1xalt::Sensor( dataPin, clockPin, clockPulseWidth,
		                         supplyVoltage, temperatureUnits );
		sensor->configureConnection();
		  // Reset the SHT1x, in case the Arduino was reset during communication:
		  sensor->softReset();
		  this->lastUpdate=millis();
		  this->waitMillis=10000;
		  Serial.println("SHT10 begin");
		started = true;
	}
	bool update() override {
		if(millis()>this->waitMillis+this->lastUpdate) {
			sht1xalt::error_t err;
			float t,h;
			err = sensor->measure(t, h);
			uint8_t errNo;
			this->lastUpdate = millis();
			if (err) {
				switch (err) {
					case sht1xalt::ERROR_NO_ACK:
					errNo=SensorError::STH10_NO_ACK;
					Serial.println("SHT1x failed to acknowledge a command!");
					break;
					case sht1xalt::ERROR_MEASUREMENT_TIMEOUT:
					errNo=SensorError::SH10_TIMEOUT;
					Serial.println("SHT1x failed to produce a measurement!");
					break;
				}
				this->handleError(errNo);
				sensor->softReset();
				started = false;
				return false;
			}
			else {
				this->setTemp(t);
				this->setHumidity(h);
				return true;
			}
		}
		else
		return false;
	}
	~SHT10Sensor(){
		delete sensor;
	}
};
class MyTime: public Sensor {
private:
	DS3231 RTC; //Create the DS3231 object
	bool started = false;
	uint8_t lastDayEventFired;
protected:

public:
	bool timerIsInterrupted;

	MyTime() {
		Wire.begin();
		RTC.begin();
		timerIsInterrupted = false;
		lastDayEventFired = 32;
	}
	void begin() override {
		//Enable Interrupt
		RTC.enableInterrupts(EveryMinute); //interrupt at  EverySecond, EveryMinute, EveryHour
		// or this
		//RTC.enableInterrupts(18,4,0);    // interrupt at (h,m,s);
	}
	bool update() override {
		RTC.clearINTStatus();
		if (timerIsInterrupted) {
			timerIsInterrupted = false;
//			alarm();
			DateTime dt = RTC.now();
			uint8_t evtNumber;
			if (dt.date() != this->lastDayEventFired) {
				this->lastDayEventFired = dt.date();
				evtNumber = SensorEvent::SENSOR_DATE_TIME;
			} else {
				evtNumber = SensorEvent::SENSOR_TIME;
			}
			SensorEvent evt(evtNumber);
			this->fireEvent(evt);
			return true;
		}
		return false;
	}
	DateTime getDateTime() {
		return this->RTC.now();
	}
	void adjustRtc(DateTime dt) {
		RTC.adjust(dt);
		timerIsInterrupted =true;
		this->update();
	}
};

#endif /* SENSORLER_H_ */
