/*
 * StateChangeListener.h
 *
 *  Created on: 13 Oca 2016
 *      Author: Alptug
 */

#ifndef STATECHANGELISTENER_H_
#define STATECHANGELISTENER_H_


#define EVT_WORKING_STATU_CHANGED 0
#define EVT_OTOMATIKLIK_CHANGED 1
#define EVT_MIN_DEGER_CHANGED 2
#define EVT_MAX_DEGER_CHANGED 3
#define EVT_SET_ALL 4
class StateEvent {
private:
	uint8_t type;
	uint16_t value;
public:
	StateEvent(uint8_t type,uint16_t value){
		this->type = type;
		this->value = value;
	}
	const uint8_t getType() {
		return this->type;
	}
	const uint16_t getValue(){
		return this->value;
	}
	uint8_t toString(char buffer[],int size){
		memset(buffer,0,size);
		return snprintf(buffer,size,"type : %d ; value : %d",this->getType(),this->getValue());
	}
	 ~StateEvent(){

	 }
};

class StateChangeListener {
private:
	unsigned char id;
public:
	virtual void changeState(StateEvent ev){
		Serial.println("DEFAULT");
	};
	void setId(unsigned char id) {
		this->id=id;
	}
	unsigned char getId() {
		return id;
	}

	~StateChangeListener(){};
};



#endif /* STATECHANGELISTENER_H_ */
