// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _SeraAcmaKapama_H_
#define _SeraAcmaKapama_H_
#include "My_Debug.h"
#include "Arduino.h"
//add your includes for the project SeraAcmaKapama here
#include"SeraAcma.h"
#include"TimerList.h"
//#include <Adafruit_CC3000.h>
//#include <ccspi.h>
//#include <SPI.h>
#include <string.h>
#include"TimerListFile.h"
#include"AcmaKapamaFile.h"
#include"Errors.h"
#include"BasicTime.h"
#include"TimerList.h"
#include"Shared.h"
#include"DataPackage.h"
#include"MemoryFree.h"
//end of add your includes here
#ifdef __cplusplus
extern "C" {
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif


//action functions
int8_t action_hemen_durdur();
int8_t action_tam_ac();
int8_t action_tam_kapat();
int8_t action_calistir(uint16_t sure);
int8_t action_sent_sera_acma_info();
int8_t action_listeden_sil(int8_t index) ;
int8_t action_listeden_sil2(uint8_t s,uint8_t dk,uint8_t sn);
int8_t action_listeye_ekle(uint8_t isActive, uint8_t h, uint8_t m, uint8_t s,uint16_t miktar);
int8_t action_listeyi_guncelle(int8_t pos, uint8_t isActive, uint8_t h,	uint8_t m, uint8_t s, uint16_t miktar);
int8_t action_get_timer_list_item_at(int index);
int8_t action_reset_timer_list_and_file();
int8_t action_initialize_clock(byte year, byte month, byte date, byte dow,
		byte h, byte m, byte s);
int8_t action_get_timer_list_size();
uint8_t action_load_timer_list();
int8_t action_print_time();
int8_t action_sent_memory_info() ;
int8_t action_gecersiz_komut();
int8_t action_request_time() ;
int8_t action_send_ping_server() ;
int8_t action_receive_ping_server();
void doActionCommands();
int8_t action_send_mac_address();
//add your function definitions for the project SeraAcmaKapama here
bool sendToWireless(int deneme, char *buff, int size);
int8_t action_initialize_clock(byte year,byte month,byte date,byte dow,byte h,byte m,byte s=0);
int listItemToCharArray(char *buff ,const ListItem l);
ListItem *getNextTimerListItem(BasicTime *t1, BasicTime *t2 = NULL);
void updateListOnClockChange();
bool updateListOnDateStart();
void communicateWithSerial();
TimerList createDumpData();
//void writeItemData(const ListItem item);
//ListItem *readNextItemData();
//TimerList readTimerListData();
void printMemory();
//void writeTimerListData();
void action_reset_esp(long t = 550);
uint8_t action_start_esp() ;
uint16_t action_sent_maximum_aciklik_miktari() ;
uint16_t action_sent_system_elements();
int8_t action_sent_limit_switch_aciklik_miktarlar();
int8_t action_sent_switch_system_info();
int8_t action_sent_switch_group_info(int type);
//Do not add code below this line
#endif /* _SeraAcmaKapama_H_ */
