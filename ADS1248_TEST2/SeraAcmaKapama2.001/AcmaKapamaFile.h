/*
 * AcmaKapamaFile.h
 *
 *  Created on: 17 A�u 2015
 *      Author: WIN7
 */

#ifndef ACMAKAPAMAFILE_H_
#define ACMAKAPAMAFILE_H_

#include "File.h"
#include "SeraAcma.h"
class AcmaKapamaFile: public File {
	private:
	int aciklikMiktariAdresi;
    int istenilenMiktarAdresi;
	public :
	AcmaKapamaFile(int baslamaAdresi):File(baslamaAdresi){

		aciklikMiktariAdresi = baslamaAdresi+2;
		istenilenMiktarAdresi = baslamaAdresi+4;
	}
	uint16_t getKayitliAciklikMiktari() {
		return this->readShort(this->aciklikMiktariAdresi);
	}
	uint16_t getKayitliIstenilenMiktar() {
		return this->readShort(this->istenilenMiktarAdresi);
	}
	void kaydetAciklikMiktari(uint16_t acilanMiktar){
		if(!this->hasData())
			this->writeHeader();
		this->writeShort(this->aciklikMiktariAdresi,acilanMiktar);
	}
	void kaydetIstenilenMiktar(uint16_t istenilenMiktar){
		if(!this->hasData())
			this->writeHeader();
		this->writeShort(this->istenilenMiktarAdresi,istenilenMiktar);
	}
	void seraAcmaBilgileriniKaydet(SeraAcma sa) {
		if(!this->hasData())
			 writeHeader();
		kaydetAciklikMiktari(sa.getAciklikMiktari());
		kaydetIstenilenMiktar(sa.getIstenilenMiktar());
	}
};

#endif /* ACMAKAPAMAFILE_H_ */
