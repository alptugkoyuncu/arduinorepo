/*
 * SwitchGroup.cpp
 *
 *  Created on: 24 Eki 2015
 *      Author: WIN7
 */

#include "SwitchGroup.h"

SwitchGroup::SwitchGroup(SwitchType groupId, uint16_t aciklikMiktari,
		LimitSwitch *switches, const uint8_t switchSize) {
	// TODO Auto-generated constructor stub
	this->type = groupId;
	this->setSwitchAciklikMiktari(aciklikMiktari);
	this->switchSize = switchSize;
	this->switches = switches;
}

SwitchGroup::~SwitchGroup() {
	// TODO Auto-generated destructor stub
	delete[] switches;

}

void SwitchGroup::setSwitchAciklikMiktari(uint16_t aciklikMiktari) {
	if (aciklikMiktari < MINIMUM_YEKPARE_ACIKLIK_MIKTARI
			|| aciklikMiktari > MAXIMUM_YEKPARE_ACIKLIK_MIKTARI) {
		switch (this->type) {
		case SWT_ACMA:
			this->aciklikMiktari = MAXIMUM_YEKPARE_ACIKLIK_MIKTARI;
			break;
		case SWT_KAPAMA:
			this->aciklikMiktari = MINIMUM_YEKPARE_ACIKLIK_MIKTARI;
			break;
		default:
			this->aciklikMiktari = MINIMUM_YEKPARE_ACIKLIK_MIKTARI;
			break;
		}
	} else
		this->aciklikMiktari = aciklikMiktari;
}
uint16_t SwitchGroup::getAciklikMiktari() {
	return this->aciklikMiktari;
}
bool SwitchGroup::checkToStop() {
	for (int i = 0; i < this->switchSize; i++)
		if (this->switches[i].isClosed() == HIGH)
			return true;
	return false;
}
void SwitchGroup::getSystemInfo(char *str,int &str_size) {

	str_size=0;
	if(this->switchSize<=0){
		str=NULL;
		return;
	}
	char temp[20];
	int size=20;
	for(int i=0; i<this->switchSize;i++){
		this->switches[i].getSystemInfo(temp,size);
		if(i==0){
			str_size = sprintf(str,"{%s}",this->getType(),temp);
		}else
			str_size+=sprintf(str+str_size," {%s}",temp);
	}
	str[str_size]=NULL;
}

KapamaSwitchGroup::KapamaSwitchGroup(uint16_t aciklikMiktari,
		KapamaKontrolSwitch switches[], uint8_t switchSize) :
		SwitchGroup(SWT_KAPAMA, aciklikMiktari, switches, switchSize) {
	this->firstSwitchOffTime = 0;
}

bool KapamaSwitchGroup::checkToStop() {
	// �lk kapanan sw�tch yoksa tara  ve f�rst sw�tch e kaydet
	if (this->firstSwitchOffTime == 0) {
		for (int i = 0; i < this->switchSize; i++) {
			if (switches[i].isClosed() == HIGH) {
				// �lk sw�tch kapand�g� zaman once butun sw�tchler�n ver�ler�n� resetle
				this->resetSwitchValues();
				this->firstSwitchOffTime = millis();
				int16_t fark = 0;
				((KapamaKontrolSwitch) switches[i]).setSureFarki(fark);
			}
		}

	}
	// �lk kapanan sw�tch yoksa �sleme durdurma
	if (this->firstSwitchOffTime == 0) {
		return false;
	} else {
		//�lk kapanan sw�tch varsa sure fark�n� al
		int16_t sureFarki = this->getSureFarki();
		// sure fark� mx�mumu gecm�s m� gecd�yse durdur
		if (sureFarki > MAXIMUM_KAPANMA_SWICTH_SURE_FARKI)
			return true;
		else {
			// sure fark� max�mumu gecmed�yse ilk defa kapanan sw�tcler� kontrol et ve sure farklar�n� guncelle
			bool canStop = true;
			for (int i = 0; i < this->switchSize; i++) {
				if (switches[i].isClosed() == HIGH) {
					if (((KapamaKontrolSwitch) switches[i]).getSureFarki() < 0)
						((KapamaKontrolSwitch) switches[i]).setSureFarki(
								sureFarki);
				} else {
					canStop = false;
				}
			}
			return canStop;
		}
	}
}

void KapamaSwitchGroup::resetSwitchValues() {
	for (int i = 0; i < this->switchSize; i++)
		((KapamaKontrolSwitch) switches[i]).resetSureFarki();
}

int16_t KapamaSwitchGroup::getSureFarki() {
	if (this->firstSwitchOffTime == 0)
		return 0;
	unsigned long curr = millis();
	if ((signed long) (curr - this->firstSwitchOffTime) < 0) {
		signed long temp = (signed long) this->firstSwitchOffTime;
		return (int16_t) (curr - temp);
	} else
		return (int16_t) (curr - this->firstSwitchOffTime);
}

SwitchType SwitchGroup::getType() {
	return this->type;
}
