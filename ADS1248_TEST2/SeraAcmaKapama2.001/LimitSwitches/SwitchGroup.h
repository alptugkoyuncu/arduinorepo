/*
 * SwitchGroup.h
 *
 *  Created on: 24 Eki 2015
 *      Author: WIN7
 */

#ifndef LIMITSWITCHES_SWITCHGROUP_H_
#define LIMITSWITCHES_SWITCHGROUP_H_

#include"LimitSwitch.h"

#define MAXIMUM_YEKPARE_ACIKLIK_MIKTARI 240
#define MINIMUM_YEKPARE_ACIKLIK_MIKTARI 0

#define MAXIMUM_KAPANMA_SWICTH_SURE_FARKI 8000 //ms olarak


typedef enum SwitchType {
	SWT_ACMA=1, SWT_KAPAMA=2
};

class SwitchGroup {
private:
	SwitchType type;
	uint16_t aciklikMiktari;
	LimitSwitch *switches;
	const int switchSize;
public:
	SwitchGroup(SwitchType groupId, uint16_t aciklikMiktari,
			LimitSwitch *switches,const uint8_t switchSize);
	virtual ~SwitchGroup();
	bool checkToStop();
	uint16_t getAciklikMiktari();
	void setSwitchAciklikMiktari(uint16_t aciklikMiktari);
	void getSystemInfo(char *str,int &str_size);
	SwitchType getType();
};
class KapamaSwitchGroup:public SwitchGroup{
private:
	unsigned long firstSwitchOffTime;
	int16_t getSureFarki();
public:
	KapamaSwitchGroup(uint16_t aciklikMiktari,KapamaKontrolSwitch switches[],uint8_t switchSize);
	bool checkToStop();
	void resetSwitchValues();
};
#endif /* LIMITSWITCHES_SWITCHGROUP_H_ */
