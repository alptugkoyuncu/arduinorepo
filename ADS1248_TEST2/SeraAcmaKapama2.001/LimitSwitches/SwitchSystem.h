/*
 * SwitchSystem.h
 *
 *  Created on: 23 Eki 2015
 *      Author: WIN7
 */

#ifndef SWITCHSYSTEM_H_
#define SWITCHSYSTEM_H_

#include "SwitchGroup.h"

#define KAPAMA_KONTROL_LIMIT_SWITCHES 1
#define ACMA_KONTROL_LIMIT_SWITCHES 1

#define NO_OF_KAPAMA_SWITCHES 10
#define NO_OF_ACMA_SWITCHES 2
//kapamay� kontrol eden l�m�t switch pinleri
#define LW_K1 1
#define LW_K2 2
#define LW_K3 3
#define LW_K4 4
#define LW_K5 5
#define LW_K6 6
#define LW_K7 7
#define LW_K8 8
#define LW_K9 9
#define LW_K10 10

// Acmayi kontrol eden limit switch pinleri
#define LW_A1 11
#define LW_A2 12

#define DEFAULT_ACMA_SWITCH_ACIKLIK_MIKTARI 210
#define DEFAULT_KAPAMA_SWITCH_ACIKLIK_MIKTARI 0

typedef enum SwitchSystemErrors{
	SSE_NO_ERROR=0,
	SSE_MULTIPLE_AKTIF_SG
};

class SwitchSystem {
private:
	SwitchGroup *groups;
	int groupSize;
	SwitchSystemErrors err;
public:
	//eger akt�f b�r sw�tch grubu varsa onun ac�kl�kM�ktar�n� dondurur yoksa -1
	SwitchSystem();
	virtual ~SwitchSystem();

	//1 den fazla ayn� anda akt�f sw�tch varsa errflag 1 olur yoksa 0 doner
	// akt�f sw�th yoksa deger -1 doner yoksa akt�f sw�tch�n ac�kl�kM�ktar� doner
	int16_t getAktifSwitchAciklikMiktari();
	bool hasLimitSwitchAt(int16_t aciklikMiktari);
	int getGroupSize(){
		return this->groupSize;
	}
	SwitchSystemErrors checkFatalError();
	void resetKapanmaSwitches();
	bool checkToStop(int16_t istenilenAciklikMiktari);
	int16_t getSwitchAciklikMiktari(SwitchType type);
	void getSystemInfo(char *str,int &size);
	void getGroupSystemInfo(char *str,int&str_size,SwitchType type);
	void setSwitchAciklikMiktari(SwitchType type,int16_t miktar);
	SwitchGroup *getSwitchGroupByAciklikMiktari(uint16_t aciklikMiktari);
	SwitchGroup *getSwitchGroupByType(SwitchType);
	SwitchGroup *getSwitchGroupByIndex(int index);
};

#endif /* SWITCHSYSTEM_H_ */
