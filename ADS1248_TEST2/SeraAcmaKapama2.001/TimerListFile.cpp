/*
 * TimerListFile.cpp
 *
 *  Created on: 17 A�u 2015
 *      Author: WIN7
 */

#include "TimerListFile.h"
TimerListFile::TimerListFile(int baslamaAdresi) :
		File(baslamaAdresi) {

}
TimerList TimerListFile::readTimerListData() {
#ifdef DEBUG
	DEBUG_S("Reading data from eeprom : ");
#endif
	TimerList l;

	if (!this->hasData()) {
#ifdef LOG
		LOG_S("has no data");
#endif
		return l;
	}
	resetSeek();
	byte size = this->getListSize();
	if (size <= 0) {
#ifdef LOG
		LOG_S("size <=0");
#endif
		return l;
	} else {
#ifdef DEBUG
//		char ch[20];
//		int s =
		sprintf(my_dbg_buf,"Size : %d",size);
//		ch[s]=NULL;
		DEBUG_S(my_dbg_buf);
#endif
	}
	ListItem item;
	for (int i = 0; i < size; i++) {
		int8_t err = this->readItemData(i, item);
		if (err < 0)
			continue;
		l.add(item);
	}
	return l;
}
void TimerListFile::writeTimerList(const TimerList list) {
#ifdef DEBUG
	DEBUG_S("Writing Data to eeprom");
#endif
	resetAll();
	byte size = list.size < 0 ? 0 : list.size;
	if (list.size <= 0) {
		this->resetAll();
		return;
	}
	writeHeader();
	writeListSize(size);

	for (int i = 0; i < size; i++)
		writeItemData(list, i);
}
void TimerListFile::updateList(const TimerList list, int listItemIndex) {
	int addr;
	ListItem item;
	this->writeListSize(list.size);
	if (list.size <= 0) {
		this->resetAll();
		return;
	}
	this->has_data = true;
	for (int i = listItemIndex; i < list.size; i++) {
		this->writeItemData(list, listItemIndex);
	}
}
void TimerListFile::writeItemData(const TimerList list, int listItemIndex) {
	this->has_data = true;
	ListItem item = list.list[listItemIndex];
	int addr = this->getAddressForItem(listItemIndex);
	this->lastWrittenAddress = addr - 1;
	BasicTime t = item.baslama;
	uint16_t value = item.aciklikMiktari;

	writeNextByte(item.isActive);
	writeNextByte(t.saat);
	writeNextByte(t.dk);
	writeNextByte(t.sn);
	writeNextShort(value);
}
//her item icin 6 byte data kaydeder

int8_t TimerListFile::readItemData(int listItemIndex, ListItem &item) {
#ifdef DEBUG
	DEBUG_S("TimerListFile : read item");
#endif
	if (this->getListSize() <= listItemIndex)
		return -1;
	int addr = this->getAddressForItem(listItemIndex);
	this->seek = addr;

	byte isActive = readNextByte();
	byte h = readNextByte();
	byte m = readNextByte();
	byte s = readNextByte();
	uint16_t value = readNextShort();
	if (h < 0 || h > 23 || m < 0 || m > 59 || s < 0 || s > 59)
		return -1;

	item.aciklikMiktari = value;
	BasicTime t(h, m, s);
	item.baslama = t;
	item.isActive = isActive;
	return 0;
}
uint8_t TimerListFile::getListSize() {
#ifdef DEBUG
	DEBUG_S("TimerListFile : get list size");
#endif
	if (this->hasData())
		return this->readByte(baslamaAdresi + 2);
	else
		return 0;
}
void TimerListFile::writeListSize(uint8_t size) {
#ifdef DEBUG
	DEBUG_S("TimerListFile : write list size");
#endif
	this->writeByte(this->baslamaAdresi + 2, size);
}
int TimerListFile::getAddressForItem(int listItemIndex) {
	// 1 data size icin 6 byte her item icin
	return (this->baslamaAdresi + 3) + listItemIndex * 6;
}
void TimerListFile::addItem(const TimerList tl, int itemIndex) {
#ifdef DEBUG
	DEBUG_S("TimerListFile : add item");
#endif
	this->updateList(tl, itemIndex);
}
void TimerListFile::removeItem(const TimerList tl, int itemIndex) {
#ifdef DEBUG
	DEBUG_S("TimerListFile : remove item");
#endif
	this->updateList(tl, itemIndex);
}
void TimerListFile::updateItemAt(const TimerList tl, int itemIndex) {
#ifdef DEBUG
	DEBUG_S("TimerListFile : update item");
#endif
	this->writeItemData(tl, itemIndex);
}
