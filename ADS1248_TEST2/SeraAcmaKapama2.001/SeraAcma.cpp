/*
 * SeraAcma.cpp
 *
 *  Created on: 24 Tem 2015
 *      Author: WIN7
 */

#include "SeraAcma.h"
bool h12, PM;
void SeraAcma::init(const uint16_t maximumAciklikMiktari,
		uint16_t aciklikMiktari, uint8_t acmaPini, uint8_t kapamaPini,
		DS3231 *Clock) {
	this->maximumAciklikMiktari = maximumAciklikMiktari;
	this->aciklikMiktari = aciklikMiktari;
	this->lastAciklikMiktari = aciklikMiktari;
	this->acmaPini = acmaPini;
	this->kapamaPini = kapamaPini;
	pinMode(acmaPini, OUTPUT);
	pinMode(kapamaPini, OUTPUT);
	digitalWrite(acmaPini, LOW);
	digitalWrite(kapamaPini, LOW);
	this->calismaDurumu = DURUYOR;
	this->timer = NULL;
	this->Clock = Clock;
}
SeraAcma::SeraAcma(const uint16_t maximumAciklikMiktari, uint8_t acmaPini,
		uint8_t kapamaPini, DS3231 *Clock) {
	// TODO Auto-generated constructor stub
	init(maximumAciklikMiktari, 0, acmaPini, kapamaPini, Clock);
}
SeraAcma::SeraAcma(const uint16_t maximumAciklikMiktari,
		uint16_t aciklikMiktari, uint8_t acmaPini, uint8_t kapamaPini,
		DS3231 *Clock) {
	init(maximumAciklikMiktari, aciklikMiktari, acmaPini, kapamaPini, Clock);
}
SeraAcma::~SeraAcma() {
	// TODO Auto-generated destructor stub
}
uint16_t SeraAcma::getIstenilenMiktar() {
	return this->istenilenMiktar;
}

void SeraAcma::switchWorking(uint16_t calismaSuresi) {
	this->durdur();
	delay(ACMA_KAPAMA_GECIS_SURESI);
	if (this->getCalismaDurumu() == ACILIYOR)
		this->kapat(calismaSuresi);
	else
		this->ac(calismaSuresi);
}

void SeraAcma::setDurmaOption(DurmaOption opt) {
	SwitchSystemErrors err = this->swt.checkFatalError();
	if (durmaOpt != DO_NO_LIMIT && err!=SSE_NO_ERROR)
		this->durmaOpt=DO_WITH_TIMER;
	else
		this->durmaOpt = durmaOpt;
}

uint8_t SeraAcma::calistir(uint16_t istenilenMiktar, DurmaOption durmaOpt) {

	if (istenilenMiktar < this->minimumAciklikMiktari)
		this->istenilenMiktar = this->minimumAciklikMiktari;
	else if (istenilenMiktar > this->maximumAciklikMiktari)
		this->istenilenMiktar = this->maximumAciklikMiktari;
	else
		this->istenilenMiktar = istenilenMiktar;
	//eger durma seceneg�n� sadece sw�tche verd�lerse ve de sw�tch yoksa otomat�k olarak durma seceneg�n� zamanlay�c�ya atar
	this->setDurmaOption(durmaOpt);
	switch (this->durmaOpt) {

	case DO_WITH_SWITCH: {
		this->setAciklikMiktari();
		uint8_t calismaDurumu = this->getCalismaDurumu();
		uint16_t calismaSuresi =
				this->istenilenMiktar > this->aciklikMiktari ?
						this->istenilenMiktar - this->aciklikMiktari :
						this->aciklikMiktari - this->istenilenMiktar;
		if (calismaDurumu == ACILIYOR) {
			if (this->istenilenMiktar <= aciklikMiktari)
				this->switchWorking(calismaSuresi);
			else
				this->ac(calismaSuresi);
		} else if (calismaDurumu == KAPANIYOR) {
			if (this->istenilenMiktar >= aciklikMiktari)
				this->switchWorking(calismaSuresi);
			else
				this->kapat(calismaSuresi);
		} else {
			if (this->istenilenMiktar >= aciklikMiktari)
				this->ac(calismaSuresi);
			else
				this->kapat(calismaSuresi);
		}
		break;
	}
	case DO_NO_LIMIT:
		if (this->istenilenMiktar == this->maximumAciklikMiktari) {
			if (this->getCalismaDurumu() == KAPANIYOR)
				this->switchWorking(this->maximumAciklikMiktari);
			else
				this->ac(this->maximumAciklikMiktari);

		} else {
			if (this->getCalismaDurumu() == KAPANIYOR)
				this->kapat(this->maximumAciklikMiktari);
			else
				this->switchWorking(this->maximumAciklikMiktari);
		}
		break;

	case DO_WITH_TIMER:
	case DO_WITH_TIMER_OR_SWITCH:
		this->calistirWithTimer(istenilenMiktar);
		break;
	}
	return this->getCalismaDurumu();
}
uint8_t SeraAcma::calistirWithTimer(uint16_t istenilenMiktar) {
	uint16_t aciklikMiktari = this->getAciklikMiktari(); //ayni zamanda calismaSuresini de gunceller

	if (aciklikMiktari == this->istenilenMiktar) {
		if (this->isWorking())
			this->durdur();
		return DURUYOR;
	}
	uint16_t calisilanSure =
			this->timer != NULL && this->timer->baslangic != NULL ?
					this->timer->getCalisilanSure(this->Clock) : 0;

	uint16_t calismaSuresi =
			aciklikMiktari < this->istenilenMiktar ?
					this->istenilenMiktar - aciklikMiktari :
					aciklikMiktari - this->istenilenMiktar;
	if (!this->isWorking()) {
		if (aciklikMiktari < this->istenilenMiktar) {
			this->ac(calismaSuresi);
		} else
			this->kapat(calismaSuresi);
	} else {
		if (((aciklikMiktari < this->istenilenMiktar)
				&& (this->getCalismaDurumu() == ACILIYOR))
				|| ((aciklikMiktari > this->istenilenMiktar)
						&& this->getCalismaDurumu() == KAPANIYOR)) {

			this->timer->beklenenCalismaSuresi = calismaSuresi + calisilanSure;

		} else {

			if (aciklikMiktari < this->istenilenMiktar) {
				this->durdur(); //istenilen miktari degistirdigi icin if icin alinir
				delay(ACMA_KAPAMA_GECIS_SURESI);
				this->ac(calismaSuresi);
			} else {
				this->durdur(); //istenilen miktari degistirdigi icin if icin alinir
				delay(ACMA_KAPAMA_GECIS_SURESI);
				this->kapat(calismaSuresi);
			}
		}
	}
	/*} else if (aciklikMiktari > istenilenMiktar) {
	 if (this->getCalismaDurumu() == ACILIYOR) {
	 this->durdur(true);
	 }
	 this->calismaSuresi = aciklikMiktari - istenilenMiktar;
	 this->kapat();
	 } else {
	 if (this->getCalismaDurumu() == KAPANIYOR) {
	 this->durdur(true);
	 }
	 this->calismaSuresi = istenilenMiktar - aciklikMiktari;
	 this->ac();
	 }*/
	return this->calismaDurumu;

}
// kapatma islemi suruyorsa calisma suresini guncelle
//duruyorsa kapatmata basla
bool SeraAcma::kapat(uint16_t calismaSuresi) {

	if (this->isWorking())
		return false;
	if (digitalRead(acmaPini) == HIGH) {
		digitalWrite(acmaPini, LOW);
		delay(ACMA_KAPAMA_GECIS_SURESI);
	}
	digitalWrite(kapamaPini, HIGH);

	this->lastAciklikMiktari = this->getAciklikMiktari();
	this->saatiAyarla(calismaSuresi);
	this->calismaDurumu = KAPANIYOR;

	char t[100];
//	int s =
	sprintf(t,
			"<%d:%d:%d>Kapanmaya basladi(%d. pin=%d)  : Aciklik Miktari : %d\tSure : %d",
			this->timer->baslangic->saat, this->timer->baslangic->dk,
			this->timer->baslangic->sn, kapamaPini, digitalRead(kapamaPini),
			this->lastAciklikMiktari, this->timer->beklenenCalismaSuresi);
//	t[s]=NULL;
	Serial.println(t);

	return true;
}
// acma islemi devam ediyorsa suresi guncelle
//duruyorsa acmaya basla
bool SeraAcma::ac(uint16_t calismaSuresi) {

	if (this->isWorking())
		return false;
	if (digitalRead(kapamaPini) == HIGH) {
		digitalWrite(kapamaPini, LOW);
		delay(ACMA_KAPAMA_GECIS_SURESI);
	}

	this->lastAciklikMiktari = this->getAciklikMiktari();
	this->saatiAyarla(calismaSuresi);

	digitalWrite(acmaPini, HIGH);

	this->calismaDurumu = ACILIYOR;

	char t[100];
//	int s =
	sprintf(t,
			"<%d:%d:%d>Ac�lmaya basladi(%d. pin=%d) : Aciklik Miktari : %d\tSure : %d",
			this->timer->baslangic->saat, this->timer->baslangic->dk,
			this->timer->baslangic->sn, acmaPini, digitalRead(acmaPini),
			this->lastAciklikMiktari, this->timer->beklenenCalismaSuresi);
//	t[s]=NULL;
	Serial.println(t);

	return true;
}
// forced : true ise timer in bitis zamani yenilenir .
//
uint16_t SeraAcma::durdur() {

	//her �ht�male kars� p�nler low yap�l�r
	digitalWrite(kapamaPini, LOW);
	digitalWrite(acmaPini, LOW);

	if (this->calismaDurumu == DURUYOR)
		return 0;
	bool a, b;
	this->timer->beklenenCalismaSuresi = this->timer->getCalisilanSure(
			this->Clock);

	// eger �lg�l� sw�tch akt�f �se �sten�len m�ktar� d�rek ac�kl�k m�ktar� olarak ayarlar�z yoksa ac�kl�k m�ktar� zamanla hesaplan�r
	int16_t tmp = this->swt.getAktifSwitchAciklikMiktari();
	if (tmp > 0)
		this->aciklikMiktari = tmp;
	else
		this->setAciklikMiktari();

	this->calismaDurumu = DURUYOR;

	this->istenilenMiktar = this->aciklikMiktari;
	char t[100];
//	int s =
	sprintf(t, "<%d:%d:%d>Durdu : Aciklik Miktari : %d\tSure : %d",
			this->timer->baslangic->saat, this->timer->baslangic->dk,
			this->timer->baslangic->sn, aciklikMiktari,
			this->timer->beklenenCalismaSuresi);
//	t[s]=NULL;
	Serial.println(t);

	return this->timer->beklenenCalismaSuresi;
}
uint8_t SeraAcma::getCalismaDurumu() {
	return this->calismaDurumu;
}

void SeraAcma::setAciklikMiktari() {
	if (this->getCalismaDurumu() == ACILIYOR) {
		this->aciklikMiktari = this->lastAciklikMiktari
				+ this->timer->getCalisilanSure(this->Clock);
	}
	if (this->getCalismaDurumu() == KAPANIYOR) {
		this->aciklikMiktari = this->lastAciklikMiktari
				- this->timer->getCalisilanSure(this->Clock);
	}
	if (this->aciklikMiktari < this->minimumAciklikMiktari) {
		this->aciklikMiktari = this->minimumAciklikMiktari;

	}
	if (this->aciklikMiktari > this->maximumAciklikMiktari) {
		this->aciklikMiktari = this->maximumAciklikMiktari;

	}
}
//cagirildigi zamanki aciklik miktari dondurulur. Eger islem devam ediyorsa belirli bir sure sonra bu miktar degismis olacaktir
uint16_t SeraAcma::getAciklikMiktari() {
	if (this->getCalismaDurumu() != DURUYOR) {
		this->setAciklikMiktari();
	}
	return this->aciklikMiktari;
}
bool SeraAcma::stopIfEnough() {
	if (this->getCalismaDurumu() == DURUYOR) {
		// her �ht�male kars� durudur fonk cag�r�l�r bu fonks�yon cal�sma durumu duruyordayken sadece p�nler� low yap�p fonk dan c�kar
		this->durdur();
		return false;
	}

	uint16_t calisilanSure = this->timer->getCalisilanSure(this->Clock);
	this->setAciklikMiktari();
	bool switchFlag = this->swt.checkToStop(this->istenilenMiktar);
	// bu sayede
	if(this->durmaOpt!=DO_NO_LIMIT && this->swt.checkFatalError()!=SSE_NO_ERROR)
		this->durmaOpt=DO_WITH_TIMER;
	switch (this->durmaOpt) {
	case DO_NO_LIMIT:
		return false;
	case DO_WITH_SWITCH:
		if (switchFlag) {
			this->durdur();
			return true;
		}
		return false;

	case DO_WITH_TIMER:
		if (this->aciklikMiktari < this->minimumAciklikMiktari) {
			this->aciklikMiktari = this->minimumAciklikMiktari;
			this->durdur();
			return true;
		}
		if (this->aciklikMiktari > this->maximumAciklikMiktari) {
			this->aciklikMiktari = this->maximumAciklikMiktari;
			this->durdur();
			return true;
		}
		if (calisilanSure >= this->timer->beklenenCalismaSuresi) {
			this->durdur();
			return true;
		}
		return false;

	case DO_WITH_TIMER_OR_SWITCH:
	default:
		if (switchFlag) {
			this->durdur();
			return true;
		}
		if (this->aciklikMiktari < this->minimumAciklikMiktari) {
			this->aciklikMiktari = this->minimumAciklikMiktari;
			this->durdur();
			return true;
		}
		if (this->aciklikMiktari > this->maximumAciklikMiktari) {
			this->aciklikMiktari = this->maximumAciklikMiktari;
			this->durdur();
			return true;
		}
		if (calisilanSure >= this->timer->beklenenCalismaSuresi) {
			this->durdur();
			return true;
		}
		return false;
	}
	return false;
}
void SeraAcma::saatiAyarla(uint16_t sure) {
	bool a, b;
	if (this->timer != NULL) {
		if (this->timer->baslangic != NULL) {
			free(this->timer->baslangic);
			this->timer->baslangic = NULL;
		}

		free(timer);
		timer = NULL;
	}
	this->timer = new DailyTimer(this->Clock, sure);
}
uint16_t SeraAcma::getMaximumAciklikMiktari() {
	return this->maximumAciklikMiktari;
}
uint16_t SeraAcma::getMinimumAciklikMiktari() {
	return this->minimumAciklikMiktari;
}
bool SeraAcma::isWorking() {
	return this->getCalismaDurumu() != DURUYOR;
}
char *SeraAcma::toString(int16_t &size) {
	char buff[256];
	size = sprintf(buff, "%d %d %d", (uint16_t) this->getAciklikMiktari(),
			(uint16_t) this->getIstenilenMiktar(),
			(uint16_t) this->getCalismaDurumu());
	buff[size] = NULL;
	return buff;
}
