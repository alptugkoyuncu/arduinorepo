// Do not remove the include below
#include "ESP_ALI_RIZA.h"
/**TODO:
 * 1-PING alma gonderme islemi iptal edildi. Gerekirse ileride daha duzenli bi sistem gelecek
 * 2-board reset sonsuz while dongusu ile gerceklesiyor yapilabilirse daha duzenli bi reset islemi gerekli
 * 3-server ya da arduino ya gonderilen datalar gidip gitmedigi kontrol edilmiyor ileride daha bi duzenli data alisverisi kontrolu yapilmali
 *
 */
 const char* ESP_STARTED="<97>";
 const char *ESP_RESET="<96>";

 void boslukAtla(char *data, uint8_t *index, uint8_t size) ;
 //s�radak� �ndexden sonrak�(varsa bosluklar� atlar) datay� returnVal a kayderer index datadan sonrak� �lk karakter� goster�r
bool getNextData(char *data, uint8_t *index, uint8_t size,
    uint16_t *returnVal) ;
    // servera oda numaras�n� gonder�r basar�l� olursa loopa g�rer
    // basar�s�z durumda reset atar
bool sentRoomNumber();
void resetBoard();
uint8_t connectServer();
uint8_t connectWifi(const char* ssid, const char* password);
//INIT
const char* ssid     = "KOYUNCU_CATI";
//const char *ssid = "KOYUNCU_DIS";
const char* password = "5458798185";
#define IP_ADDRESS = 192,168,1,11
#define PORT 5555

#define AK_DEBUG 0
#define RS_NO_PROBLEM 0
#define RS_WIFI_CONN_OK 11
#define RS_TCP_CONN_OK 12
#define RS_SENT_HEADER_OK 13
#define RS_SENT_MAC_OK 14
#define RS_WIFI_PROBLEM 21
#define RS_TCP_CONN_PROBLEM 22
#define RS_TYPE_PROBLEM 23
#define RS_MAC_ADDRESS_PROBLEM 24
#define RS_DATA_NOT_COMPLETE 5
#define RS_SENT_PROBLEM 6



#define ACTION_RESET_ESP 96//action
#define ACTION_ESP_STARTED 97//action
//#define MAC_SERVER
//#define UNIX_SERVER
const char* SERVER_END_OF_LINE =
#ifdef MAC_SERVER
  "\r";
#else
#ifdef   UNIX_SERVER
  "\n";
#else
  "\r\n";
#endif
#endif
#define d_print(STR)\
  {\
    if(AK_DEBUG) Serial.print(STR); \
  }
#define d_println(STR)\
  {\
    if(AK_DEBUG) Serial.println(STR);\
  }




WiFiClient client;
uint8_t ERR_CODE;

struct ReceivedPackage {
#define MAX_PACK_SIZE 255
#define BASLANGIC_AYIRACI 1
#define BITIS_AYIRACI 2
#define GERCEK_DATA 3
#define TANIMSIZ_DATA 4
  public:
    byte data[MAX_PACK_SIZE];
    bool isFinished;
    bool isStarted;
    bool isOverlapped;
    uint8_t size;
    void init() {
      for (int i = 0; i < MAX_PACK_SIZE; i++)
        data[i] = 0x0; //NULL
      isFinished = false;
      isStarted = false;
      size = 0;
    }
    uint8_t setData(byte d) {
      if (size >= MAX_PACK_SIZE - 3) {
        d_println("Overlapped");
        init();
        return TANIMSIZ_DATA;
      }
      if (d == '\0' || d == '>' || d == '\r' || d == '\n') {
        if (!isStarted) {
          d_print("Tanimsiz Data byte code:");
          d_println((int)d);
          d_println("Data daha baslamadi");
          return TANIMSIZ_DATA;
        }
        if(isFinished){
          d_print("Tanimsiz Data byte code:");
          d_println((int)d);
          d_println("Data zaten bitik");
          return BITIS_AYIRACI;
        }
        isFinished = true;
        isStarted = false;
        data[size++] = '>';
        d_print("End of data : ");
        d_println((char *)data);
        return BITIS_AYIRACI;
      } else if (d == '<') {
        d_print("Baslangic Ayiraci : ");
        d_println((char)d);
        isStarted = true;
        isFinished = false;
        data[size++] = d;
        return BASLANGIC_AYIRACI;
      } else {
        if (!isStarted) {
          d_print("Tanimsiz Data :");
          d_println((int)d);
          d_println("Baslangic datasi daha gelmedi");
          return TANIMSIZ_DATA;
        }
        d_print("Gecerli Data:");
        d_println((int)d);
        data[size++] = d;
        return GERCEK_DATA;
      }
    }
    uint8_t getDataType(byte d) {
      if (d == '\0' || d == '>' || d == '\r' || d == '\n') {
        return  isFinished ? TANIMSIZ_DATA : BITIS_AYIRACI;
      } else if (d == '<') {
        return BASLANGIC_AYIRACI;
      } else if(d==' ' || d=='\t' ||(d>='0' && d<='9')){
        return isStarted ? GERCEK_DATA : TANIMSIZ_DATA;
      }else
      return TANIMSIZ_DATA;
    }
    void addServerEndOfLine(){
        const char *tmp = SERVER_END_OF_LINE;
        for (int i = 0; i < sizeof(tmp); i++)
          data[size++] = tmp[i];
        data[size] = 0x0; //NULL
    }
};
ReceivedPackage pck_recv, pck_sent;

uint8_t connectServer() {
  uint8_t flag;
  IPAddress ip(192, 168, 1, 11);
  int count = 0;
  d_print("Tcp Connection:");
  client.stopAll();
  while (count < 10) {
    if (client.connect(ip, 5555) > 0) {
      d_println("Established");

      return ERR_CODE = RS_NO_PROBLEM;
    }
    delay(100);
    count++;
  }
  d_println("Failed");
  return ERR_CODE = RS_TCP_CONN_PROBLEM;
}

// Fucntion to connect WiFi
uint8_t connectWifi(const char* ssid, const char* password) {
  int WiFiCounter = 0;
  // We start by connecting to a WiFi network
  d_print("Connecting to ");
  d_println(ssid);
  WiFi.disconnect();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    WiFiCounter++;
    d_print(".");
    if (WiFiCounter >= 30) {
      d_println("Can not connect to the wifi");
      return ERR_CODE = RS_WIFI_PROBLEM;
    }
  }
  d_println("");
  d_println("WiFi connected");
  d_print("IP address: ");
  d_println(WiFi.localIP());
  return ERR_CODE = RS_NO_PROBLEM;
}

bool sentRoomNumber(){

	while(client.available() > 0) {
	    byte dt = client.read();
	    uint8_t flag = pck_recv.setData(dt);

	    if (flag == BITIS_AYIRACI) {
	      size_t t = Serial.write((uint8_t *)pck_recv.data, pck_recv.size);
	      Serial.flush();
	      pck_recv.init();
	    }
	  }
}

// Inifinite loop - Causes to reset self
void resetBoard() {
  client.write(ESP_RESET,sizeof(ESP_RESET));
  client.write(SERVER_END_OF_LINE,sizeof(SERVER_END_OF_LINE));
  client.flush();
  client.stopAll();
  WiFi.disconnect();
  d_println("Reseting");
  while (1) {}
}
void sentArduinoOk(){
  Serial.write(ESP_STARTED, sizeof(ESP_STARTED));
  Serial.write(SERVER_END_OF_LINE,sizeof(SERVER_END_OF_LINE));
  Serial.flush();
}
void emptySerialData(){
  while(Serial.available()>0)
    Serial.read();
}
void emptyClientData(){
  while(client.available()>0)
    client.read();
}
////////////////////////// SETUP //////////////////////////////////////////////////////

void setup() {
  pck_recv.init();
  pck_sent.init();
  Serial.begin(115200);  //Start Serial
  Serial.flush();

  d_println("Started...");
  delay(1000);
  uint8_t fails = 0;
  // Start WiFi
  do {
    connectWifi(ssid, password);
    if (fails == 4)
      resetBoard();
    fails++;
  } while (ERR_CODE != RS_NO_PROBLEM);

  fails = 0;
  //connect TCP SERVER
  do {
    connectServer();
    if (fails == 4)
      resetBoard();
    fails++;
  } while (ERR_CODE != RS_NO_PROBLEM);
  fails = 0;
  //send header to the server

  //  server.begin();  // Start Server

  emptyClientData();
  emptySerialData();
  sentArduinoOk();
  d_println("Setup Done!");

  pck_recv.init();
  pck_sent.init();
}


////////////////////////// LOOP //////////////////////////////////////////////////////



void loop() {
  while (Serial.available() > 0) {
    byte dt = Serial.read();
    uint8_t flag = pck_sent.setData(dt);

    if (flag == BITIS_AYIRACI) {
      pck_sent.addServerEndOfLine();
      size_t t = client.write((uint8_t *)pck_sent.data, pck_sent.size);
      client.flush();
      pck_sent.init();
    }
  }
  delay(10);
  while(client.available() > 0) {
    byte dt = client.read();
    uint8_t flag = pck_recv.setData(dt);

    if (flag == BITIS_AYIRACI) {
      size_t t = Serial.write((uint8_t *)pck_recv.data, pck_recv.size);
      Serial.flush();
      pck_recv.init();
    }
  }
  delay(10);

}
//s�radak� �ndexden sonrak�(varsa bosluklar� atlar) datay� returnVal a kayderer index datadan sonrak� �lk karakter� goster�r
bool getNextData(char *data, uint8_t *index, uint8_t size,
    uint16_t *returnVal) {
  Serial.print("Parse data  : ");
  char act[6];
  uint8_t actIndex = 0;
  boslukAtla(data, index, size);
  while (1) {
    if (*index >= size || data[*index] == '\0' || data[*index] == ' ') {
      if (actIndex == 0) {
        *returnVal = 0;
      } else {
        act[actIndex] = '\0';
        *returnVal = atoi(act);
      }
      Serial.print("Next index=");
      Serial.print(*index);
      Serial.print(" ;Next Data value = ");
      Serial.println(*returnVal);
      return true;
    } else
      act[actIndex++] = data[*index];
    (*index)++;
  }
  return false;
}
// bosluk olmayan �lk �ndex� indexe kaydeder
void boslukAtla(char *data, uint8_t *index, uint8_t size) {
  uint8_t i = *index;
  for (; i < size; i++)
    if (data[i] == ' ')
      continue;
    else {
      *index = i;
      return;
    }
  *index = size;
  return;
}

