
/**TODO:
   1-PING alma gonderme islemi iptal edildi.Bu islem' dolayli yoldan  arduino uzerinden yaapilacak
   2-board reset sonsuz while dongusu ile gerceklesiyor yapilabilirse daha duzenli bi reset islemi gerekli
   3-server ya da arduino ya gonderilen datalar gidip gitmedigi kontrol edilmiyor ileride daha bi duzenli data alisverisi kontrolu yapilmali
   4- room/client numbar islemi iptal edildi yine bu islem dolayli yoldan arduino uzerinden gerceklesecek.
   1. ve 4. maddelerdeki degisikleriden oturu artik esp guncellemesine gerek kalmayacak yukle esplerin hepsi herbir arduno uzerinden kontrol edilebilecek
   5-room client işlemi tekrar yerleştirildi.Arduino içinde loopta sürekli kontrol etmek  manasız
*/


#include <ESP8266WiFi.h>
#include<WifiClient.h>

#define AK_DEBUG 0


//INIT
const char *ssid = "KOYUNCU_CATI";
//const char *ssid = "KOYUNCU_YENI";
//const char *ssid = "KOYUNCU_DIS";
const char* password = "5458798185";
#define IP_ADDRESS 192,168,1,11
#define PORT 5555
const int16_t ARDUINO_ID = 1001;

#define RS_NO_PROBLEM 0
#define RS_WIFI_CONN_OK 11
#define RS_TCP_CONN_OK 12
#define RS_WIFI_PROBLEM 21
#define RS_TCP_CONN_PROBLEM 22
#define RS_HEADER_PROBLEM 23
#define RS_DATA_NOT_COMPLETE 5
#define RS_SENT_PROBLEM 6

const int16_t REQUEST_ID = 50;
const int16_t REQUEST_ID_OK = 51;
const char* ESP_STARTED = "<97>";
const char *ESP_RESET = "<96>";
const char *WIFI_CONNECTED = "<98>";
const char *SERVER_CONNECTED = "<99>";
const char *ACTION_DISCONNECT_FROM_SERVER = "<100>";
//varıable defınes

#define ACTION_RESET_ESP 96//action
#define ACTION_ESP_STARTED 97//action

// servera oda numarasını gonderır basarılı olursa loopa gırer
// basarısız durumda reset atar
#define MAX_FAIL 3
#define NO_TCP_TRY 10
#define NO_WIFI_TRY 30
unsigned long lastReceiveArduino, lastReceiveServer;
const unsigned long MAX_WAIT_MS = 300000; //5 dakika icinde data gelmezse resetler

void resetBoard();
uint8_t connectServer();
uint8_t connectWifi(const char* ssid, const char* password);
uint8_t sentHeader();
void fromShort(int16_t val, uint8_t *newVal, boolean isLittleEndian);
int16_t toShort(uint8_t *val, boolean isLittleEndian);
//#define MAC_SERVER
//#define UNIX_SERVER
const char* SERVER_END_OF_LINE =
#ifdef MAC_SERVER
  "\r";
#else
#ifdef   UNIX_SERVER
  "\n";
#else
  "\r\n";
#endif
#endif

#define d_print(STR)\
  {\
    if(AK_DEBUG) Serial.print(STR); \
  }
#define d_println(STR)\
  {\
    if(AK_DEBUG) Serial.println(STR);\
  }

WiFiClient client;
uint8_t ERR_CODE;

struct ReceivedPackage {
#define MAX_PACK_SIZE 255
#define BASLANGIC_AYIRACI 1
#define BITIS_AYIRACI 2
#define GERCEK_DATA 3
#define TANIMSIZ_DATA 4
  public:
    byte data[MAX_PACK_SIZE];
    bool isFinished;
    bool isStarted;
    bool isOverlapped;
    uint8_t size;
    void init() {
      for (int i = 0; i < MAX_PACK_SIZE; i++)
        data[i] = 0x0; //NULL
      isFinished = false;
      isStarted = false;
      size = 0;
    }
    void copyJustData(char *dt, int *len) {
      *len = 0;
      for (int i = 0; i < size; i++) {
        char d = data[i];
        if (d == ' ' || d == '\t') {
          dt[(*len)++] = ' ';
        }
        else if (d <= '9' && d >= '0') {
          dt[(*len)++] = d;
        }
      }
    }
    uint8_t setData(byte d) {
      if (size >= MAX_PACK_SIZE - 3) {
        d_println("Overlapped");
        init();
        return TANIMSIZ_DATA;
      }
      if (d == '\0' || d == '>' || d == '\r' || d == '\n') {
        if (!isStarted) {
          d_print("Tanimsiz Data byte code:");
          d_println((int)d);
          d_println("Data daha baslamadi");
          return TANIMSIZ_DATA;
        }
        if (isFinished) {
          d_print("Tanimsiz Data byte code:");
          d_println((int)d);
          d_println("Data zaten bitik");
          return BITIS_AYIRACI;
        }
        isFinished = true;
        isStarted = false;
        data[size++] = '>';
        d_print("End of data : ");
        d_println((char *)data);
        return BITIS_AYIRACI;
      } else if (d == '<') {
        d_print("Baslangic Ayiraci : ");
        d_println((char)d);
        isStarted = true;
        isFinished = false;
        data[size++] = d;
        return BASLANGIC_AYIRACI;
      } else {
        if (!isStarted) {
          d_print("Tanimsiz Data :");
          d_println((int)d);
          d_println("Baslangic datasi daha gelmedi");
          return TANIMSIZ_DATA;
        }
        d_print("Gecerli Data:");
        d_println((int)d);
        data[size++] = d;
        return GERCEK_DATA;
      }
    }
    uint8_t getDataType(byte d) {
      if (d == '\0' || d == '>' || d == '\r' || d == '\n') {
        return  isFinished ? TANIMSIZ_DATA : BITIS_AYIRACI;
      } else if (d == '<') {
        return BASLANGIC_AYIRACI;
      } else if (d == ' ' || d == '\t' || (d >= '0' && d <= '9')) {
        return isStarted ? GERCEK_DATA : TANIMSIZ_DATA;
      } else
        return TANIMSIZ_DATA;
    }
    void addServerEndOfLine() {
      const char *tmp = SERVER_END_OF_LINE;
      for (int i = 0; i < sizeof(tmp); i++)
        data[size++] = tmp[i];
      data[size] = 0x0; //NULL
    }
};
ReceivedPackage pck_recv, pck_sent;

uint8_t connectServer() {
  uint8_t flag;
  IPAddress ip(IP_ADDRESS);
  int count = 0;
  d_print("Tcp Connection:");
  client.stopAll();
  while (count < NO_TCP_TRY) {
    d_print(".");
    if (client.connect(ip, PORT) > 0) {
      d_println("Established");

      return ERR_CODE = RS_NO_PROBLEM;
    }
    yield();
    delay(10);
    count++;
  }
  d_println("Failed");
  return ERR_CODE = RS_TCP_CONN_PROBLEM;
}

// Fucntion to connect WiFi
uint8_t connectWifi(const char* ssid, const char* password) {
  int WiFiCounter = 0;
  // We start by connecting to a WiFi network
  d_print("Connecting to ");
  d_println(ssid);
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    yield();
    delay(500);
    WiFiCounter++;
    d_print(".");
    if (WiFiCounter >= NO_WIFI_TRY) {
      d_println("Can not connect to the wifi");
      return ERR_CODE = RS_WIFI_PROBLEM;
    }
  }
  d_println("");
  d_println("WiFi connected");
  d_print("IP address: ");
  d_println(WiFi.localIP());
  return ERR_CODE = RS_NO_PROBLEM;
}
uint8_t sentHeader() {

  d_println("Waiting for request");
  unsigned long maxWaitSecond = 3000;
  int8_t errTry = 20;
  unsigned long strt = millis();
  uint8_t tmp[2];
  while ( errTry > 0) {
    if (millis() - strt > maxWaitSecond) {
      fromShort(ARDUINO_ID, tmp, false);
      d_print("Timeout : Sending arduino_id :");
      d_print(ARDUINO_ID);
      client.write((uint8_t *)tmp, 2);
      client.flush();
      d_println("    ok");
      strt = millis();
      errTry--;
    }

    if (client.available() >= 2) {
      size_t n = client.readBytes(tmp, 2);
      if (n == 2)
        strt = millis();
      int16_t value = toShort( tmp, false);
      d_print("Yeni data : ");
      d_println(value);
      if (value == REQUEST_ID) {
        d_println("Value is request id");
        fromShort(ARDUINO_ID, tmp, false);
        d_print("Sending arduino_id :");
        d_print(ARDUINO_ID);
        client.write((uint8_t *)tmp, 2);
        client.flush();
        d_println(" Sent Completed");
      }
      else if (value == ARDUINO_ID) {
        d_println("value is arduino id");
        fromShort(REQUEST_ID_OK, tmp, false);
        d_print("Sending request_id_ok :");
        d_print(REQUEST_ID_OK);
        client.write((uint8_t *)tmp, 2);
        client.flush();
        d_println("Sending completed");
        d_println("Header transfer completed");
        return ERR_CODE = RS_NO_PROBLEM;
      } else {
        d_println("value hatali");
        fromShort(ARDUINO_ID, tmp, false);
        client.write((uint8_t *)tmp, 2);
        client.flush();
        errTry--;
      }
    }
  yield();
  }
  d_println("Header transfer failed");
  return ERR_CODE = RS_HEADER_PROBLEM;
}

// Inifinite loop - Causes to reset self
void resetBoard() {
  client.write(ACTION_DISCONNECT_FROM_SERVER, sizeof(ACTION_DISCONNECT_FROM_SERVER));
  client.write(SERVER_END_OF_LINE, sizeof(SERVER_END_OF_LINE));
  delay(10);
  Serial.write(ESP_RESET, sizeof(ESP_RESET));
  Serial.write(SERVER_END_OF_LINE, sizeof(SERVER_END_OF_LINE));
  delay(10);
  yield();
  client.flush();
  Serial.flush();
  client.stopAll();
  WiFi.disconnect();
  d_println("Reseting");
//  ESP.restart();
  delay(100);do{}while(1);
}
void sentArduinoOk() {
  Serial.write(ESP_STARTED, sizeof(ESP_STARTED));
  Serial.write(SERVER_END_OF_LINE, sizeof(SERVER_END_OF_LINE));
  Serial.flush();
}
void emptySerialData() {
  while (Serial.available() > 0)
    Serial.read();
}
void emptyClientData() {
  while (client.available() > 0)
    client.read();
}
////////////////////////// SETUP //////////////////////////////////////////////////////


void setup() {
  pck_recv.init();
  pck_sent.init();
  Serial.begin(115200);  //Start Serial
  Serial.flush();

  d_println("Started...");
  sentArduinoOk();
  yield();
  delay(100);
  uint8_t fails = 0;
  // Start WiFi
  do {

    if (fails == MAX_FAIL)
      resetBoard();
    connectWifi(ssid, password);
    yield();
    fails++;
  } while (ERR_CODE != RS_NO_PROBLEM);


  Serial.write(WIFI_CONNECTED, sizeof(WIFI_CONNECTED));
  Serial.write(SERVER_END_OF_LINE, sizeof(SERVER_END_OF_LINE));
  yield();
  delay(100);
  fails = 0;
  //connect TCP SERVER
  do {
    if (fails == MAX_FAIL)
      resetBoard();
    connectServer();
    yield();
    fails++;
  } while (ERR_CODE != RS_NO_PROBLEM);
  sentHeader();
  if (ERR_CODE != RS_NO_PROBLEM)
    resetBoard();
  Serial.write(SERVER_CONNECTED, sizeof(SERVER_CONNECTED));
  Serial.write(SERVER_END_OF_LINE, sizeof(SERVER_END_OF_LINE));
  yield();
  delay(100);
  fails = 0;
  emptyClientData();
  emptySerialData();


  pck_recv.init();
  pck_sent.init();
  lastReceiveArduino = millis();
  lastReceiveServer = millis();

  d_println("Setup Done!");
}


////////////////////////// LOOP //////////////////////////////////////////////////////



void loop() {
  yield();
  delay(2);
  if ((millis() - lastReceiveArduino) > MAX_WAIT_MS || (millis() - lastReceiveServer) > MAX_WAIT_MS) {
    d_println("Timeout!");
    resetBoard();
  }
  while (Serial.available() > 0) {
    lastReceiveArduino = millis();
    byte dt = Serial.read();
    uint8_t flag = pck_sent.setData(dt);
    if (flag == BITIS_AYIRACI) {
      pck_sent.addServerEndOfLine();
      size_t t = client.write((uint8_t *)pck_sent.data, pck_sent.size);
      pck_sent.init();
      break;
    }
  }
  while (client.available() > 0) {
    lastReceiveServer = millis();
    byte dt = client.read();
    uint8_t flag = pck_recv.setData(dt);

    if (flag == BITIS_AYIRACI) {
      size_t t = Serial.write((uint8_t *)pck_recv.data, pck_recv.size);
      pck_recv.init();
      break;
    }
  }
}
void fromShort(int16_t val, uint8_t *newVal, boolean isLittleEndian) {
  // Little Endian
  //        byte ret[2];
  if (isLittleEndian) {
    newVal[0] = (int8_t) val;
    newVal[1] = (int8_t) (val >> 8);
  } else {
    // Big Endian generally network conversion
    newVal[0] = (int8_t) (val >> 8);
    newVal[1] = (int8_t) val;
  }
  //        return ret;
}
int16_t toShort(uint8_t *val, boolean isLittleEndian) {
  if (isLittleEndian) {
    return val[0] | val[1] << 8;
  } else
    return val[1] | val[0] << 8;
}
