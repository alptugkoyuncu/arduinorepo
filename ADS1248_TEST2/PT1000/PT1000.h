// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _PT1000_H_
#define _PT1000_H_
#include "Arduino.h"
//add your includes for the project PT1000 here
#include "RTDModule.h"
#include"OneWire.h"
#include "SHT1xalt.h"

//end of add your includes here


//add your function definitions for the project PT1000 here


double digitalLowPass(double last_smoothed, double new_value, double filterVal);

//Do not add code below this line
#endif /* _PT1000_H_ */
