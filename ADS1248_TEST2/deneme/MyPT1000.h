/*
 * MyPt1000.h
 * 4 four wire 3 pt1000 reading
 * PGA=4 SPS=20
 *
 *  Created on: 17 Tem 2017
 *      Author: alptug
 */

#ifndef MYPT1000_H_
#define MYPT1000_H_

#include<Arduino.h>
#include <SPI.h>
#include <ads12xx.h>
#include<MemoryFree.h>
//end of add your includes here

enum ADSBoardSensorType {
	boardTempUpdated, temp1Updated, temp2Updated, temp3Updated
};

class ADSBoardEvent {
public:
	ADSBoardEvent(ADSBoardSensorType type, double val, double min, double max) {
		this->type = type;
		this->val = val;
		this->min = min;
		this->max = max;
	}
	double getVal() {
		return val;
	}
	double getMin() {
		return min;
	}
	double getMax() {
		return max;
	}
	ADSBoardSensorType getType() {
		return type;
	}
private:
	double val, min, max;
	ADSBoardSensorType type;
};
class ADSBoardListener {
public:
	virtual void valueUpdated(ADSBoardEvent val)=0;
};

//do reading ile sira ile sicakliklari olcup ortalamasini alir ve listenerlari
// gerektiginde update eder
//getBoardTemp fonksiyonu cagirilirsa isBoardTempRequested true olur
//okunmaktaki olan temp reading bittikten sonra temp okunur ve gonderilir temp reading bolunmez.
//temp reading belirlenen miktarda temp okunur hatali bilgi ayiklanip geri kalanlarin ortalamasi alinir ve listener fire edilir.

#define MINIMUM_WAIT_SECOND_TO_NEW_READ 1000
#define PGA PGA2_4 // PGA 4V/V
#define SPS DOR3_5  //20
#define AKIM IMAG2_100 //i 100 ua
class MyPt1000 {
public:
	MyPt1000(int8_t resetPin = -1);
	void begin(int CS, int start, int DRDY);
	void setListener(ADSBoardListener *l);
	void doReading();
	void setNumberOfPTReading(uint8_t val);
	void requestBoardTemp();
	void fireListener(ADSBoardSensorType type, double avr, double min,
			double max) {
		ADSBoardEvent evt(type, avr, min, max);
		this->fireListener(evt);
	}
	void stop();

	double boardTemp, temp1, temp2, temp3;
private:
	unsigned long lastRead;
	ads12xx ADS;
	bool isBoardTempRequested;
	ADSBoardListener *l;
	ADSBoardSensorType currentReadingSensor;
	void fireListener(ADSBoardEvent evt);
	uint8_t numberOfPtReading;
	const uint8_t numberOfTotalBoardTempReading = 5;
	const static uint8_t MAXnumberOfPtReading = 20;
	double ptValued[MAXnumberOfPtReading];
	uint8_t readingIndex = 0;
	void changeRegistersForNewReading(ADSBoardSensorType type);
	double getPtTemp();
	double getBoardTemp();
	int8_t resetPin;
};

#endif /* MYPT1000_H_ */
