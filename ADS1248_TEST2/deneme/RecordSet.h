/*
 * RecordSet.h
 *
 *  Created on: 4 Feb 2020
 *      Author: Alptug
 */

#ifndef RECORDSET_H_
#define RECORDSET_H_

typedef struct RecordSet {
	char flag;//daha once kayit var mi diye bakar
	char wifi_ssd[32];
	char wifi_password[32];
	char ip[16]; //192.168.1.11 seklindeki string formati
	char port[6];
	char id[6];
	RecordSet(){
		flag=99;//bu deger kayit var demek baska degerler kayit yok demek
		memset(wifi_ssd,0,32);
		memset(wifi_password,0,32);
		memset(ip,0,16);
		memset(port,0,6);
		memset(id,0,6);
	}
	bool hasRecord(){
		return flag==99;
	}
};

#endif /* RECORDSET_H_ */
