/*
 * NextionMonitor.cpp
 *
 *  Created on: 4 Feb 2020
 *      Author: Alptug
 */

#include "NextionMonitor.h"
#include"Defines.h"
#ifndef DERECE
#define DERECE 176
#endif
size_t dToStr(char *ch, double val);
void setSensorValue(NexText txt, double val);
NextionMonitor::NextionMonitor(unsigned char enablePin,
		bool multiSerialEnable) {
	pinMode(enablePin, INPUT);
	this->enable = digitalRead(enablePin) || multiSerialEnable;
	this->btn = new NexButton(1, 16, "b0");
	NexTouch *a[] = { this->getButton(), 0x0 };
	listenList = a;
}

void NextionMonitor::connect() {
	if (this->enable)
		nexInit();
}

void NextionMonitor::disconnect() {
	if (this->enable) {
		nexSerial.end();
	}
}

void NextionMonitor::setSensor1(double val) {
	if (!this->enable)
		return;
	NexText txt(0, 8, "pageMain.t6");
	setSensorValue(txt, val);
}

void NextionMonitor::setSensor2(double val) {
	if (!this->enable)
		return;
	NexText txt(0, 9, "pageMain.t7");
	setSensorValue(txt, val);
}

void NextionMonitor::setSensor3(double val) {
	if (!this->enable)
		return;
	NexText txt(0, 10, "pageMain.t8");
	setSensorValue(txt, val);
}

size_t dToStr(char *ch, double val) {
	int i = (int) val;
	val = val - (double) i;
	val = val * 100;
	int f = (int) val;
	return sprintf(ch, "%d.%02d", i, f);
}

void setSensorValue(NexText txt, double val) {
	char str[20];
	int len = dToStr(str, val);
	sprintf(str + len, "%cC", char(DERECE));
	txt.setText(str);
}

void NextionMonitor::listen() {
	if (this->enable) {
		nexLoop(listenList);
	}
}

RecordSet NextionMonitor::getAyarlar() {
	if (!this->enable)
		return RecordSet();
	RecordSet s;
	int len=0;
	NexText txt(1, 11, "pageAyarlar0.tSsid");
	len=txt.getText(s.wifi_ssd, 32);
	memset(s.wifi_ssd+len,0,32-len);

	txt = NexText(1, 12, "pageAyarlar0.tPass");
	len=txt.getText(s.wifi_password, 32);
	memset(s.wifi_password+len,0,32-len);

	txt = NexText(1, 13, "pageAyarlar0.tIp");
	len=txt.getText(s.ip, 16);
	memset(s.ip+len,0,16-len);

	txt = NexText(1, 14, "pageAyarlar0.tPort");
	len=txt.getText(s.port, 6);
	memset(s.port+len,0,6-len);

	txt = NexText(1, 15, "pageAyarlar0.tId");
	len=txt.getText(s.id, 6);
	memset(s.id+len,0,6-len);
	return s;
}

void NextionMonitor::sendAyarlar(RecordSet recordSet) {
	if (!this->enable)
		return;

#ifdef SERIAL_DEBUG_ENABLE
	Serial.println("send ayarlar:");
	Serial.println(recordSet.hasRecord());
	Serial.println(recordSet.wifi_ssd);
	Serial.println(recordSet.wifi_password);
	Serial.println(recordSet.ip);
	Serial.println(recordSet.port);
	Serial.println(recordSet.id);
#endif
	NexText txt(1, 11, "pageAyarlar0.tSsid");
	txt.setText(recordSet.wifi_ssd);

	txt = NexText(1, 12, "pageAyarlar0.tPass");
	txt.setText(recordSet.wifi_password);

	txt = NexText(1, 13, "pageAyarlar0.tIp");
	txt.setText(recordSet.ip);

	txt = NexText(1, 14, "pageAyarlar0.tPort");
	txt.setText(recordSet.port);

	txt = NexText(1, 15, "pageAyarlar0.tId");
	txt.setText(recordSet.id);
}

NexButton* NextionMonitor::getButton() {
	return btn;
}

bool NextionMonitor::isEnable() {
	return this->enable;
}
