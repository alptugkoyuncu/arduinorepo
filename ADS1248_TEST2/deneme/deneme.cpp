// Do not remove the include below
#include "Defines.h"
#include <Arduino.h>
#include"MyPt1000.h"
#include"ESPWifi.h"
#include"NextionMonitor.h"
#include <EEPROM.h>

//add your function definitions for the project SeraProjectWithProMini3_3 here

int parseNextInt(char *str, uint8_t *bas);
void doActions(char *buffer);

MyPt1000 *pt;
SeraWifi *wifi;
NextionMonitor *mon;
size_t dToStr2(char *ch, double val);

uint8_t ADS1248_START_PIN = A0;
uint8_t ADS1248_CS_PIN = 10;
uint8_t ADS1248_DRDY_PIN = 2;
int8_t ADS1248_RESET_PIN = A1;
int8_t ESP8266_RESET_PIN = 9;
NexButton b0(1, 16, "b0");
NexTouch *nexListenList[] = { &b0, NULL };
bool isEspEnable() {
//	return !mon->isEnable()||MULTI_SERIAL_ENABLE;
	return false;
}
bool isNextionEnable() {
	return mon->isEnable() || MULTI_SERIAL_ENABLE;
}
class ADSListener: public ADSBoardListener {
public:
	void valueUpdated(ADSBoardEvent val) {
		DataPackage sp;
		sp.init();
		char buffer[40];
		size_t n = sprintf(buffer, "<1%d ", val.getType());
		n = n + dToStr2(buffer + n, val.getVal());
		buffer[n++] = ' ';
		n = n + dToStr2(buffer + n, val.getMin());
		buffer[n++] = ' ';
		n = n + dToStr2(buffer + n, val.getMax());
		buffer[n++] = '>';
		buffer[n] = '\0';
		if (isEspEnable())
			wifi->println(buffer);
#ifdef SERIAL_DEBUG_ENABLE
		char b[20];
		sprintf(b, "Temp Reading Type: [%d] Value :", val.getType());
		Serial.print(b);
		Serial.println(val.getVal(), 2);
#endif
		switch (val.getType()) {
		case ADSBoardSensorType::temp1Updated:
#ifdef SERIAL_DEBUG_ENABLE
			unsigned long son;
			unsigned long bas;
			bas = micros();
#endif
			mon->setSensor1(val.getVal());
#ifdef SERIAL_DEBUG_ENABLE
			son = micros();
			Serial.print(F("deger : "));
			Serial.println(son - bas);
#endif
			break;
		case ADSBoardSensorType::temp2Updated:
			mon->setSensor2(val.getVal());
			break;
		case ADSBoardSensorType::temp3Updated:
			mon->setSensor3(val.getVal());
			break;
		case ADSBoardSensorType::boardTempUpdated:
//			monitor->setBoardTemp(val.getVal());
			break;
		default:
#ifdef SERIAL_DEBUG_ENABLE
			Serial.println(F("Unkown Type"));
#endif
			break;
		}
	}
};
ADSListener *ptListener;
DataPackage pck;
//The setup function is called once at startup of the sketch
RecordSet record;
#ifdef SERIAL_DEBUG_ENABLE
DataPackage serialPackage;
#endif
void PopNexAyarlarKaydetButton(void *ptr) {
#ifdef SERIAL_DEBUG_ENABLE
	Serial.println("button clicked");
#endif
	record = mon->getAyarlar();
	EEPROM.put(RECORD_EEPROM_START_INDEX, record);
}

unsigned long now;
void setup() {

#ifdef SERIAL_DEBUG_ENABLE
	Serial.begin(9600);
	Serial.println(F("Started"));
#endif
	wifi = new ESPWifi(&Serial3, 115200, ESP8266_RESET_PIN);
	mon = new NextionMonitor(NEXTION_ENABLE_PIN, MULTI_SERIAL_ENABLE);
	EEPROM.get(RECORD_EEPROM_START_INDEX, record);
	if (isNextionEnable()) {
		mon->connect();
		b0.attachPop(PopNexAyarlarKaydetButton, &b0);
//		mon->getButton()->attachPush(PopNexAyarlarKaydetButton,mon->getButton());
		if (record.hasRecord())
			mon->sendAyarlar(record);
	}
	if (isEspEnable()) {
		Serial.println("Setup Ends");
		wifi->begin();
		Serial.println("Setup Ends");
		wifi->connect();
//		delay(1000);
//	monitor->gotoPage(MonitorPages::openingScreen);

//		delay(2000);
//	monitor->gotoPage(MonitorPages::MainScreen);
		Serial.println("Setup Ends");
	}
	ptListener = new ADSListener();
	pt = new MyPt1000(ADS1248_RESET_PIN);
	pt->begin(ADS1248_CS_PIN, ADS1248_START_PIN, ADS1248_DRDY_PIN);
#ifdef SERIAL_DEBUG_ENABLE
	Serial.println("Setup Ends");
#endif
	pt->setListener(ptListener);
// Add your initialization code here
	now = millis();
//	pt->setNumberOfPTReading(1);
}

// The loop function is called in an endless loop

void loop() {
	if (millis() - now > BOARD_TEMP_REQUEST_TIME) {
		pt->requestBoardTemp();
		now = millis();
	}

	if (isNextionEnable()) {
		nexLoop(nexListenList);
	}
	if (isEspEnable()) {
		wifi->readAll(&pck);
		if (pck.isFinished) {
			char buffer[100];
			memset(buffer,0,100);
			int len = 0;
			pck.copyJustData(buffer, &len);
			pck.init();
			doActions(buffer);

		}
	}
#ifdef SERIAL_DEBUG_ENABLE

	int i = 0;
	if (Serial.available()) {
		serialPackage.setData(Serial.read());
	}
	if (serialPackage.isFinished) {
		char buffer[100];
		memset(buffer,0,100);
		int len = 0;
		serialPackage.copyJustData(buffer, &len);
		serialPackage.init();
		doActions(buffer);

	}
#endif
	pt->doReading();
	delay(5);
//Add your repeated code here
}
void doActions(char *buffer) {
	uint8_t actCode;
	uint8_t nextIndex = 0;
#ifdef SERIAL_DEBUG_ENABLE
	Serial.print("Next Index : ");
	Serial.println(buffer + nextIndex);
#endif
	actCode = parseNextInt(buffer, &nextIndex);
#ifdef SERIAL_DEBUG_ENABLE
	Serial.print("Act Code : ");
	Serial.println(actCode);
#endif
	char buffer2[30];
	switch (actCode) {
	case AC_SEND_WIFI_SSID: {
		int len = sprintf(buffer, "<%d %s>", actCode, record.wifi_ssd);

		break;
	}
	case AC_SEND_PASS: {
		int len = sprintf(buffer, "<%d %s>", actCode, record.wifi_password);
		break;
	}
	case AC_SEND_IP: {
		int len = sprintf(buffer, "<%d %s>", actCode, record.ip);
		break;
	}
	case AC_SEND_PORT: {
		int len = sprintf(buffer, "<%d %s>", actCode, record.port);
		break;
	}
	case AC_SEND_ID: {
		int len = sprintf(buffer, "<&d %s>", actCode, record.id);
		break;
	}
	default:
		break;
	}
	wifi->println(buffer);
#ifdef SERIAL_DEBUG_ENABLE
	Serial.println(buffer);
#endif
}
int parseNextInt(char *str, uint8_t *bas) {
	//baslangicta bosluk varsa temizle ilk data indexi kaydet
//	bool baslangicBosluk = true;
	uint8_t startIndex = *bas, sonIndex = 0;
	for (; str[startIndex] != '\0'; startIndex++) {
		if (str[startIndex] == ' ' || str[startIndex] == '\t' || str[startIndex] == '\r'
				|| str[startIndex] == '\n')
			continue;
		else {
			break;
		}
	}
	sonIndex=startIndex;
	//bosluk veya satir sonuna kadar ilerle bulunca son indexi kaydet
	for (; str[sonIndex] != '\0'; sonIndex++) {
		if (str[sonIndex] == ' ' || str[sonIndex] == '\t' || str[sonIndex] == '\r'
				|| str[sonIndex] == '\n') {
			break;
		}
	}
	if (sonIndex == 0 || startIndex == sonIndex)
		return 0;
	char tmp[10];
	memset(tmp, 0, 10);
	strncpy(tmp, str + startIndex, sonIndex - startIndex);
	*bas=sonIndex;
	return atoi(tmp);
}
size_t dToStr2(char *ch, double val) {
	int i = (int) val;
	val = val - (double) i;
	val = val * 100;
	int f = (int) val;
	return sprintf(ch, "%d.%02d", i, f);
}

