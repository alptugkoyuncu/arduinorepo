/*
 * ESPWifi.h
 *
 *  Created on: 20 Tem 2017
 *      Author: alptug
 */

#ifndef ESPWIFI_H_
#define ESPWIFI_H_

#include "SeraWifi.h"
#include<Arduino.h>
class ESPWifi: public SeraWifi {
public:

	ESPWifi(HardwareSerial *serial, unsigned long int uart,
			int8_t resetPin = -1);
	void begin();
	void connect();
	virtual void setParamaters(RecordSet s);
	void disconnect();
	void write(DataPackage);
	void sendPig();
	byte readByte(DataPackage *pck = 0x0);
	size_t readAll(DataPackage *pck);
	void println(char *ch);
	bool isAvaible();

private:
	HardwareSerial *ser;
	unsigned long int uart;
	int8_t resetPin;
	bool sentAndCheck(unsigned short code,const char *str) ;
};

#endif /* ESPWIFI_H_ */
