/*
 * NextionMonitor.h
 *
 *  Created on: 4 Feb 2020
 *      Author: Alptug
 */

#ifndef NEXTIONMONITOR_H_
#define NEXTIONMONITOR_H_
#include<Arduino.h>
#include"RecordSet.h"
#include "Nextion/Nextion.h"
#define MYSERIAL Serial
class NextionMonitor {
public:
	NextionMonitor(unsigned char enablePin,bool multiSerialEnable);
	void connect();
	void disconnect();
	void setSensor1(double val);
	void setSensor2(double val);
	void setSensor3(double val);
	void setTime(void *time);
	RecordSet getAyarlar();
	void sendAyarlar(RecordSet);
	void listen();
	NexButton* getButton();
	bool isEnable();
private:
	bool  enable;
	NexTouch **listenList;
	NexButton *btn;
};

#endif /* NEXTIONMONITOR_H_ */
