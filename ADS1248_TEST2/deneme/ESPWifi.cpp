/*
 * ESPWifi.cpp
 *
 *  Created on: 20 Tem 2017
 *      Author: alptug
 */

#include "ESPWifi.h"
#include "Defines.h"
ESPWifi::ESPWifi(HardwareSerial *seri, unsigned long int uart,
		int8_t resetPin) {
	this->ser = seri;
	this->uart = uart;
	this->resetPin = resetPin;
}
void ESPWifi::begin() {
	if (this->resetPin >= 0) {
		pinMode(resetPin, OUTPUT);
		digitalWrite(resetPin, LOW);
		delay(10);
	}
	this->ser->begin(uart);
	this->ser->flush();
}

void ESPWifi::connect() {
	if (this->resetPin >= 0) {
		pinMode(resetPin, OUTPUT);
		digitalWrite(resetPin, HIGH);
		delay(10);
	}
}

void ESPWifi::disconnect() {
	if (this->resetPin >= 0) {
		digitalWrite(resetPin, LOW);
	}
	this->ser->flush();
	this->ser->end();
}

void ESPWifi::write(DataPackage dataPackage) {
	dataPackage.addServerEndOfLine();
	Serial.write(dataPackage.data, dataPackage.size);
}

byte ESPWifi::readByte(DataPackage *pck) {
	if (this->isAvaible())
		if (pck == 0x0)
			return this->ser->read();
		else {
			byte d = this->ser->read();
			pck->setData(d);
			return d;
		}
	else
		return 0x0;

}

size_t ESPWifi::readAll(DataPackage *pck) {
	if (pck == 0x0)
		return 0;
	unsigned long now = millis();
	size_t val = 0;
	while (millis() - now < MAX_ALLOACTE_TIME && this->isAvaible()) {
		pck->setData(this->ser->read());
		val++;
		if (pck->isFinished)
			return val;
	}
	return val;
}

void ESPWifi::println(char *ch) {
	this->ser->write(ch);
	this->ser->write("\r\n");
}

void ESPWifi::sendPig() {
	char buffer[10];
	sprintf(buffer, "<%d>", WIFI_PING);
	this->println(buffer);
}

bool ESPWifi::isAvaible() {
	return this->ser->available();
}

void ESPWifi::setParamaters(RecordSet s) {
	sentAndCheck(AC_SEND_WIFI_SSID,s.wifi_ssd);
	sentAndCheck(AC_SEND_PASS,s.wifi_password);
	sentAndCheck(AC_SEND_IP,s.ip);
	sentAndCheck(AC_SEND_PORT,s.port);
	sentAndCheck(AC_SEND_ID,s.id);

}
bool ESPWifi::sentAndCheck(unsigned short code, const char*str) {
	char buffer[30];
		int len = sprintf(buffer, "<%d %s>",  code, str);
		buffer[len] = 0x0;
	int no_try = 10;
	int i = 0;
	char buffer2[30];
	memset(buffer2, 0x0, 30);
	unsigned long waitForNewData = 500;
	unsigned long lastSentTime = millis();
	bool finished = false;
	bool error=false;
	while (no_try > 0) {
		if (lastSentTime - millis() > 500) {
			no_try--;
			ser->write(buffer, len);
			ser->write(SERVER_END_OF_LINE, sizeof(SERVER_END_OF_LINE));
			finished=false;
		}
		while (ser->available() > 0 &&!finished) {
			char c = Serial.read();
			if (c == '<') {
				buffer2[i++] = c;
			} else if (c == '>') {
				if (i > 0) {
					buffer2[i++] = c;
					finished = true;
					break;
				}
			} else {
				if (i > 0)
					buffer[i++] = c;
			}
		}
		if(finished){
			if(len==i){
				bool flag=true;
				for(int j=0; j<i; j++){
					if(buffer[j]!=buffer2[j]){
						flag=false;
						break;
					}
				}
				if(flag)
					return true;
			}
		}
		delay(10);
	}
	return false;
}
