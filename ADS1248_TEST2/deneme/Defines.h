/*
 * Defines.h
 *
 *  Created on: 5 Feb 2020
 *      Author: Alptug
 */

#ifndef DEFINES_H_
#define DEFINES_H_
#define MULTI_SERIAL_ENABLE 1//multiserial enable olursa ESP serial3 nextion serial2 yi kullaniyor
							//disable olursa sistem baslangicta bir defaligina
							//nextion enable pin kontrol edilir. enable pin aktif olunca serial baglantisi
							// sadece nextion tarafindan kontrol edilir. disabla durumunda da sadece esp
							// tarafindan kontrol edilir. pin kontrolu sadece setup da nextion
							//konstructor da yapildigi icin her pin durumu degisiminde manual reset atilmasi
							//sarttir.
#define NEXTION_ENABLE_PIN 9

#define AC_SEND_WIFI_SSID 101
#define AC_SEND_PASS 102
#define AC_SEND_IP 103
#define AC_SEND_PORT 104
#define AC_SEND_ID 105

#define BOARD_TEMP_REQUEST_TIME 30000

#define RECORD_EEPROM_START_INDEX 0
//#define NEXTION_SOFTWARE_SERIAL_AVAIBLE
#define SERIAL_DEBUG_ENABLE

#ifdef NEXTION_SOFTWARE_SERIAL_AVAIBLE
#define NEXTION_TX_PIN
#define NEXTION_RX_PIN
#endif




#endif /* DEFINES_H_ */
