/*
 * NokiaMonitor.cpp
 *
 *  Created on: 18 Tem 2017
 *      Author: alptug
 */

#include "NokiaMonitor.h"

NokiaMonitor::NokiaMonitor(int8_t DC, int8_t CS, int8_t RST) {
	// TODO Auto-generated constructor stub
	display = new Adafruit_PCD8544(DC, CS, RST);
//	display = new Adafruit_PCD8544(6,7,DC, CS, RST);

}

NokiaMonitor::~NokiaMonitor() {
	// TODO Auto-generated destructor stub
	delete this->display;
}

void NokiaMonitor::begin() {
	this->display->begin(NM_CONSTRATS, NM_BIAS);
	this->display->setTextSize(1);
	this->display->setTextColor(BLACK);
	this->openOpeningPage();
}
const char *celcisus=" C";
bool NokiaMonitor::setTemp1(double val) {
	if (this->currentPage != MonitorPages::MainScreen)
		return false;
	this->clearRest(5,14,0);
	this->gotoChar(5,0);
	display->print(val, 2);
	display->print(celcisus);
	display->display();
	return true;
}

bool NokiaMonitor::setTemp2(double val) {
	if (this->currentPage != MonitorPages::MainScreen)
		return false;
	this->clearRest(5,14,1);
	this->gotoChar(5,1);
	display->print(val, 2);
	display->print(celcisus);
	display->display();
	return true;
}
bool NokiaMonitor::setTemp3(double val) {
	if (this->currentPage != MonitorPages::MainScreen)
		return false;
	this->clearRest(5,14,2);
	this->gotoChar(5,2);
	display->print(val, 2);
	display->print(celcisus);
	display->display();
	return true;
}

bool NokiaMonitor::setBoardTemp(double val) {
	if (this->currentPage != MonitorPages::AdminPage)
		return false;
	this->clearRest(5,14,0);
	this->gotoChar(5,0);
	display->print(val, 2);
	display->print(celcisus);
	display->display();
	return true;
}

void NokiaMonitor::openMainPage() {
	display->setTextSize(1);
	display->clearDisplay();
	this->gotoChar(0,0);
	display->print(F("T1 : "));
	this->gotoChar(0,1);
	display->print(F("T2 : "));
	this->gotoChar(0,2);
	display->print(F("T3 : "));

	display->display();
	this->currentPage = MonitorPages::MainScreen;
}

void NokiaMonitor::openOpeningPage() {
	display->clearDisplay();
	this->currentPage = MonitorPages::openingScreen;
	display->setTextSize(2);
	display->println(F("OPENING....."));
	display->display();
}

void NokiaMonitor::openAdminPage() {
	display->setTextSize(1);
	display->clearDisplay();
	this->gotoChar(0,0);
	display->print(F("BT : "));
	display->display();
	this->currentPage = MonitorPages::AdminPage;
}

void NokiaMonitor::clearRest(int startCharIndex, int stopCharIndex, int yLine) {
	this->gotoChar(startCharIndex,yLine);
	display->fillRect(startCharIndex*6,yLine*8,(stopCharIndex-startCharIndex)*6,8,WHITE);

}

void NokiaMonitor::gotoChar(uint8_t x, uint8_t y) {
	display->setCursor(x*6,y*8);
}
