/*
 * SeraWifi.h
 *
 *  Created on: 20 Tem 2017
 *      Author: alptug
 */

#ifndef SERAWIFI_H_
#define SERAWIFI_H_

#include<Arduino.h>

#define PACKAGE_DEBUG 0
#define MAX_ALLOACTE_TIME 50 //50 ms


//ACTION CODES
#define WIFI_PING 0
#define WIFI_IS_ALIVE 1



#define SERVER_END_OF_LINE  "\r\n"


#define d_print(STR)\
  {\
    if(PACKAGE_DEBUG) Serial.print(STR); \
  }
#define d_println(STR)\
  {\
    if(PACKAGE_DEBUG) Serial.println(STR);\
  }

struct DataPackage {
#define MAX_PACK_SIZE 255
#define BASLANGIC_AYIRACI 1
#define BITIS_AYIRACI 2
#define GERCEK_DATA 3
#define TANIMSIZ_DATA 4
protected:

    bool isServerEndOfLineAdded;
  public:
    byte data[MAX_PACK_SIZE];
    bool isFinished;
    bool isStarted;
    bool isOverlapped;
    uint8_t size;
    void init() {
      for (int i = 0; i < MAX_PACK_SIZE; i++)
        data[i] = 0x0; //NULL
      isFinished = false;
      isStarted = false;
      isServerEndOfLineAdded=false;
      size = 0;
    }
    void copyJustData(char *dt, int *len) {
      *len = 0;
      for (int i = 0; i < size; i++) {
        char d = data[i];
        if (d == ' ' || d == '\t'){
          dt[(*len)++] = ' ';
        }
        else if (d <= '9' && d >= '0'){
          dt[(*len)++] = d;
        }
      }
    }
    uint8_t setData(byte d) {
      if (size >= MAX_PACK_SIZE - 3) {
        d_println("Overlapped");
        init();
        return TANIMSIZ_DATA;
      }
      if (d == '\0' || d == '>' || d == '\r' || d == '\n') {
        if (!isStarted) {
          d_print("Tanimsiz Data byte code:");
          d_println((int)d);
          d_println("Data daha baslamadi");
          return TANIMSIZ_DATA;
        }
        if (isFinished) {
          d_print("Tanimsiz Data byte code:");
          d_println((int)d);
          d_println("Data zaten bitik");
          return BITIS_AYIRACI;
        }
        isFinished = true;
        isStarted = false;
        data[size++] = '>';
        d_print("End of data : ");
        d_println((char *)data);
        return BITIS_AYIRACI;
      } else if (d == '<') {
        d_print("Baslangic Ayiraci : ");
        d_println((char)d);
        isStarted = true;
        isFinished = false;
        data[size++] = d;
        return BASLANGIC_AYIRACI;
      } else {
        if (!isStarted) {
          d_print("Tanimsiz Data :");
          d_println((int)d);
          d_println("Baslangic datasi daha gelmedi");
          return TANIMSIZ_DATA;
        }
        d_print("Gecerli Data:");
        d_println((int)d);
        data[size++] = d;
        return GERCEK_DATA;
      }
    }
    uint8_t getDataType(byte d) {
      if (d == '\0' || d == '>' || d == '\r' || d == '\n') {
        return  isFinished ? TANIMSIZ_DATA : BITIS_AYIRACI;
      } else if (d == '<') {
        return BASLANGIC_AYIRACI;
      } else if (d == ' ' || d == '\t' || (d >= '0' && d <= '9')) {
        return isStarted ? GERCEK_DATA : TANIMSIZ_DATA;
      } else
        return TANIMSIZ_DATA;
    }
    void addServerEndOfLine() {
    	if(isServerEndOfLineAdded)
    		return;
      const char *tmp = SERVER_END_OF_LINE;
      for (int i = 0; i < sizeof(tmp); i++)
        data[size++] = tmp[i];
      data[size] = 0x0; //NULL
      this->isServerEndOfLineAdded=true;
    }
};

typedef enum{OK=0}SWResult;

class SeraWifi {
public:
	SeraWifi(){
		this->errCode = SWResult::OK;
	};
	virtual void begin()=0;
	virtual void connect()=0;
	virtual void disconnect()=0;
	virtual void write(DataPackage)=0;
	virtual void println(char *ch)=0;
	virtual void sendPig();
	virtual byte readByte(DataPackage *pck=0x0)=0;
	virtual size_t readAll(DataPackage *pck)=0;
	virtual bool isAvaible()=0;
	SWResult getErrorCode(){
		return errCode;
	}
protected:
	SWResult errCode;
};

#endif /* SERAWIFI_H_ */
