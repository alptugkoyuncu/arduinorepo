/*
 * ESPWifi.cpp
 *
 *  Created on: 20 Tem 2017
 *      Author: alptug
 */

#include "ESPWifi.h"

ESPWifi::ESPWifi(HardwareSerial *serial, unsigned long int uart,
		int8_t resetPin) {
	this->serial = serial;
	this->uart = uart;
	this->resetPin = resetPin;
}
void ESPWifi::begin() {
	if (this->resetPin >= 0){
		pinMode(resetPin, OUTPUT);
		digitalWrite(resetPin,LOW);
		delay(10);
	}
	this->serial->begin(uart);
	this->serial->flush();
}

void ESPWifi::connect() {
	if (this->resetPin >= 0) {
		digitalWrite(resetPin, HIGH);
		delay(50);
	}
}

void ESPWifi::disconnect() {
	if (this->resetPin >= 0) {
		digitalWrite(resetPin, LOW);
		delay(50);
	}
	this->serial->flush();
	this->serial->end();
}

void ESPWifi::write(DataPackage dataPackage) {
	dataPackage.addServerEndOfLine();
	Serial.write(dataPackage.data, dataPackage.size);
}

byte ESPWifi::readByte(DataPackage *pck) {
	if (this->isAvaible())
		if (pck == 0x0)
			return this->serial->read();
		else {
			byte d = this->serial->read();
			pck->setData(d);
			return d;
		}
	else
		return 0x0;

}

size_t ESPWifi::readAll(DataPackage *pck) {
	if (pck == 0x0)
		return 0;
	unsigned long now = millis();
	size_t val = 0;
	while (millis() - now < MAX_ALLOACTE_TIME && this->isAvaible()) {
		pck->setData(this->serial->read());
		val++;
		if (pck->isFinished)
			return val;
	}
	return val;
}

void ESPWifi::println(char* ch) {
	this->serial->write(ch);
	this->serial->write("\r\n");
}

void ESPWifi::sendPig() {
	char buffer[10];
	sprintf(buffer, "<%d>", WIFI_PING);
	this->println(buffer);
}

bool ESPWifi::isAvaible() {
	return this->serial->available();
}
