/*
 * ESPWifi.h
 *
 *  Created on: 20 Tem 2017
 *      Author: alptug
 */

#ifndef ESPWIFI_H_
#define ESPWIFI_H_

#include "SeraWifi.h"


class ESPWifi: public SeraWifi {
public:

	ESPWifi(HardwareSerial *serial,unsigned long int uart,int8_t resetPin=-1);
	void begin();
	void connect();
	void disconnect();
	void write(DataPackage);
    void sendPig();
	byte readByte(DataPackage *pck=0x0);
	size_t readAll(DataPackage *pck);
	void println(char *ch);
	bool isAvaible();

private:
	HardwareSerial *serial;
	unsigned long int uart;
	int8_t resetPin;
};

#endif /* ESPWIFI_H_ */
