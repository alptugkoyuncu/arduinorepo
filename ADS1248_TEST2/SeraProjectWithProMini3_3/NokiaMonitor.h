/*
 * NokiaMonitor.h
 *
 *  Created on: 18 Tem 2017
 *      Author: alptug
 */

#ifndef NOKIAMONITOR_H_
#define NOKIAMONITOR_H_

#include "Monitor.h"
#include"libraries/Adafruit_GFX.h"
#include"libraries/Adafruit_PCD8544.h"
#include"libraries/SPI.h"

#define NM_CONSTRATS 60
#define NM_BIAS 0x04
class NokiaMonitor: public Monitor {
public:
	NokiaMonitor(int8_t DC,int8_t CS,int8_t RST);
	~NokiaMonitor();
	void begin();
	bool setTemp1(double val);
	bool setTemp2(double val);
	bool setTemp3(double val);
	bool setBoardTemp(double val);

protected:
	void openMainPage();
	void openOpeningPage();
	void openAdminPage();
private:
	 Adafruit_PCD8544 *display ;
	 void clearRest(int startCharIndex,int stopCharIndex,int yLine);
	 void gotoChar(uint8_t x,uint8_t y);
};

#endif /* NOKIAMONITOR_H_ */
