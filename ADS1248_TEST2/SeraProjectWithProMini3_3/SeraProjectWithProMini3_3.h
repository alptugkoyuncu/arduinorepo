// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _SeraProjectWithProMini3_3_H_
#define _SeraProjectWithProMini3_3_H_
#include <Arduino.h>
#include"MyPt1000.h"
#include"NokiaMonitor.h"
#include"ESPWifi.h"
//add your function definitions for the project SeraProjectWithProMini3_3 here



int parseNextInt(char *str,uint8_t *bas);
void doActions(char *buffer);
//Do not add code below this line
#endif /* _SeraProjectWithProMini3_3_H_ */
