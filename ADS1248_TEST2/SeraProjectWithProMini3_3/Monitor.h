/*
 * Monitor.h
 *
 *  Created on: 17 Tem 2017
 *      Author: alptug
 */

#ifndef MONITOR_H_
#define MONITOR_H_
typedef enum {openingScreen,MainScreen,AdminPage} MonitorPages;
class Monitor {
public:
	Monitor(){};
	virtual void begin()=0;
	virtual bool setTemp1(double val)=0;
	virtual bool setTemp2(double val)=0;
	virtual bool setTemp3(double val)=0;
	virtual bool setBoardTemp(double val)=0;
	void gotoPage(MonitorPages p){
		switch(p){
		case MonitorPages::openingScreen:this->openOpeningPage();break;
		case MonitorPages::MainScreen:this->openMainPage();break;
		case MonitorPages::AdminPage:this->openAdminPage();break;
		default:this->openMainPage();break;
		}
		this->currentPage=p;
	}
protected:
	MonitorPages currentPage;
	virtual void openMainPage()=0;
	virtual void openOpeningPage()=0;
	virtual void openAdminPage()=0;
};

#endif /* MONITOR_H_ */
