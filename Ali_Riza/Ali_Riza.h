// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _Ali_Riza_H_
#define _Ali_Riza_H_
#include "Arduino.h"
//add your includes for the project Ali_Riza here

#include "OtomasyonBirimi.h"
#include"MemoryFree.h"
#include"Sensorler.h"
#include"SerialData.h"
#include"MyNex.h"
#include"DosyaKayit.h"
//end of add your includes here

// add defines here

//p�n def�nes
#define ISITICI_PIN 31
#define SOGUTUCU_PIN 33
#define NEMLENDIRICI_PIN 35
#define ISIKLANDIRMA_PIN 37

#define ESP_RESET_PIN 3


//act�on def�nes
// Ser�al and w�f� act�on commands
#define SET_OB_ALL 10 // <action type min max isOtomatikAktif>
#define SET_OB_WORKING 11 //<action type isWorking>
#define SET_TIMER 12 //<action year month day hour minute second dayOfWeek>

#define GET_ALL_DATA_INFO 20 //<action>
#define GET_AYALAR_INFOR 21 //<action type>
#define GET_TEMP_INFO 22
#define GET_HUM_INFO 23
#define GET_TIMER 24//<action>
//#define GET_OB_MIN 6//<action type>
//#define GET_OB_MAX 7//<action type>
//#define GET_OB_IS_AUTO 8//<action type>
#define GET_OB_IS_WORKING 25//<action type>
#define GET_FREE_MEMORY 26//<action>
#define GET_DOSYA_KAYIT_INFO 27//<action> giden ise <action moved kayitMiktari sizeof(db)>
#define UPDATE_TIMER 31 //act�on
#define ACTION_REQUEST_ROOM_NUMBER 90 //action
#define ACTION_SENT_ROOM_NUMBER 91//action
#define ACTION_RESET_ESP 96//action
#define ACTION_ESP_STARTED 97//action
#define ACTION_PING_TO_SERVER 98//action
#define ACTION_PING_FROM_SERVER 99//action
#define ACTION_DISCONNECT_FROM_SERVER 100//action
//var�able def�nes
#define ROOM_NUMBER 11
#define WAIT_FOR_NEXT_EVENT 1500
#define wifi Serial3
#define SERIAL_ALL 4
#define WIFI_UART 19200

#define PING_SENT_INTERVAL 77000
#define PING_REPLY_INTERVAL 10000
//end of defines

//add your function definitions for the project Ali_Riza here
void printMemory();
void kaydetButtonClicked(void *ptr);
void acKapaButtonClicked(void *ptr);
void blockPage();
void checkBlockage();
uint8_t getNexCurrentPage() ;
void printLog(const char *str, NexObject *ptr);
void boslukAtla(char *data, uint8_t *index, uint8_t size);
bool getNextData(char *data, uint8_t *index, uint8_t size,uint16_t *returnVal);
OtomasyonBirimi *getOtomasyonBirimiByType(uint8_t otomasyontype);
uint8_t getNexPageIdByObType(uint8_t obType) ;
uint8_t sentToUart(uint8_t serialNo, char*buffer);
uint8_t sendOBIsWorking(uint8_t serial, uint8_t otomasyonType) ;
uint8_t sendOBIsOtomatik(uint8_t serial, uint8_t otomasyonType) ;
uint8_t sendOBMax(uint8_t serial, uint8_t otomasyonType);
uint8_t sendOBMin(uint8_t serial, uint8_t otomasyontype);
uint8_t sendTempInfo(uint8_t serialNo=SERIAL_ALL);
uint8_t sendHumInfo(uint8_t serialNo=SERIAL_ALL);
uint8_t sendObInfo(uint8_t serial, uint8_t otomasyonType) ;
uint8_t sendTime(uint8_t serial) ;
uint8_t sendFreeMemory(uint8_t serial) ;
void floattoChar(char buffer[],float val,int floatSize=2);
void doAction(char *buff,int size, uint8_t serialNo=SERIAL_ALL);
void INT0_ISR();
void action_reset_esp(long t = 550);
uint8_t requestTimerUpdateFromServer(uint8_t serialNo=SERIAL_ALL);
void checkWireless() ;
int8_t action_send_ping_server();
uint8_t sentRoomNuber(uint8_t serialNo=SERIAL_ALL);
uint8_t action_start_esp() ;
int8_t action_receive_ping_server() ;
uint8_t sendToSerial(char *buffer);
uint8_t sendToWifi(char *buffer);
uint8_t sendAllDataInfo(uint8_t serial=SERIAL_ALL);
uint8_t sendDosyaKayitInfo(uint8_t serialNo=SERIAL_ALL);
//Do not add code below this line
#endif /* _Ali_Riza_H_ */
