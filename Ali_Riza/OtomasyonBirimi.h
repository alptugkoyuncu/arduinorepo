/*
 * Otomasyon.h
 *
 *  Created on: 13 Oca 2016
 *      Author: Alptug
 */

#ifndef OTOMASYONBIRIMI_H_
#define OTOMASYONBIRIMI_H_
#include "Arduino.h"
#include "StateChangeListener.h"

class OtomasyonBirimi {
private:
	const static uint8_t max_listener_size = 10;
	StateChangeListener *listeners[max_listener_size];
	uint8_t listenerIndex;bool elleMudahale = false;
protected:
	uint8_t pinNo;bool lastValue;bool otomatikAktif;
	uint16_t baslamaDegeri;
	uint16_t bitisDegeri;
	uint16_t deger;bool hasDeger;
	void setWorkingAutomatically(bool isWorking) {
		if (isWorking && digitalRead(this->pinNo) == LOW) {
			digitalWrite(this->pinNo, HIGH);
			StateEvent evt(EVT_WORKING_STATU_CHANGED, 1);
			this->fireEvent(evt);
		}
		if (!isWorking && digitalRead(this->pinNo) == HIGH) {
			digitalWrite(this->pinNo, LOW);
			StateEvent evt(EVT_WORKING_STATU_CHANGED, 0);
			this->fireEvent(evt);
		}
	}
	bool isOtomatiklikChanged(bool isOtomatik) {
		bool flag = this->otomatikAktif == isOtomatik;
		if (flag)
			return false;
		else {
			this->otomatikAktif = isOtomatik;
			return true;
		}
	}
	bool isBaslamaChanged(uint16_t baslama) {
		bool flag = baslama == this->baslamaDegeri;
		if (flag) {
			return false;
		} else {
			this->baslamaDegeri = baslama;
			return true;
		}
	}
	bool isBitisChanged(uint16_t bitis) {
		bool flag = bitis == this->bitisDegeri;
		if (flag) {
			return false;
		} else {
			this->bitisDegeri = bitis;
			return true;
		}
	}
	virtual void checkForOtomatikStartStop() {
		if (this->isOtomatikAktif()) {
			if (this->hasDeger && (this->deger < this->baslamaDegeri)) {
				this->setWorkingAutomatically(true);
				return;
			}
			if (this->hasDeger && (this->deger > this->bitisDegeri)) {
				this->setWorkingAutomatically(false);
				return;
			}
		} else
			this->setWorkingAutomatically(false);

	}

	void fireEvent(StateEvent evt) {
		Serial.println(F("[---FIRE EVENT---]"));
		for (int i = 0; i < this->listenerIndex; i++) {
			Serial.print(i);
			Serial.print("[. event fired->]");
			Serial.println(evt.getType());
			listeners[i]->changeState(evt);
			Serial.println("]. event fired[");
		}
		Serial.println(F("]---FIRE EVENT---["));
	}
public:
	const static uint8_t OB_TYPE_ALL=0;
	const static uint8_t OB_TYPE_ISITICI = 1;
	const static uint8_t OB_TYPE_SOGUTUCU = 2;
	const static uint8_t OB_TYPE_NEMLENDIRICI = 3;
	const static uint8_t OB_TYPE_ISIKLANDIRMA = 4;

	OtomasyonBirimi(uint8_t pinNo) {
		this->pinNo = pinNo;
		this->listenerIndex = 0;
		this->lastValue = false;
		this->otomatikAktif = false;
		pinMode(this->pinNo, OUTPUT);
		digitalWrite(this->pinNo, LOW);
		this->baslamaDegeri = 0;
		this->bitisDegeri = 0;
		this->deger = 0;
		this->hasDeger = false;
	}

	~OtomasyonBirimi() {

	}
	void addListener(StateChangeListener *l) {
		Serial.println("add listener");
		if (this->listenerIndex >= 10)
			return;
		l->setId(this->listenerIndex);
		listeners[this->listenerIndex++] = l;
	}
	void removeListener(StateChangeListener *l) {
		uint8_t index = l->getId();

		for (int i = index; i < this->listenerIndex - 1; i++) {
			listeners[i] = listeners[i + 1];
			listeners[i]->setId(i);
		}
		listeners[this->listenerIndex] = NULL;
		delete l;
		this->listenerIndex--;
	}
	void setOtomatikAktif(bool isOtomatik) {
		if (this->isOtomatiklikChanged(isOtomatik)) {
			StateEvent evt(EVT_OTOMATIKLIK_CHANGED, isOtomatik);
			this->fireEvent(evt);
			this->checkForOtomatikStartStop();
		}
	}

	void setWorking(bool isWorking) {
		this->setOtomatikAktif(false);
		delay(1);
		this->setWorkingAutomatically(isWorking);

	}

	void setDeger(uint16_t deger) {
		this->deger = deger;
		this->hasDeger = true;
		this->checkForOtomatikStartStop();
	}

	void setBaslamaDegeri(uint16_t min) {
		if (this->isBaslamaChanged(min)) {
			StateEvent evt(EVT_MIN_DEGER_CHANGED, min);
			this->fireEvent(evt);
			this->checkForOtomatikStartStop();
		}
	}
	void setBitisDegeri(uint16_t max) {
		if (this->isBitisChanged(max)) {
			StateEvent evt(EVT_MAX_DEGER_CHANGED, max);
			this->fireEvent(evt);
			this->checkForOtomatikStartStop();
		}
	}
	void set(uint8_t isOtomatiklik, uint16_t baslamaDegeri,
			uint16_t bitisDegeri) {
		Serial.print("OTOMASYON SET  = isOtomatik : ");
		Serial.print(isOtomatiklik);
		Serial.print(" baslama : ");
		Serial.print(baslamaDegeri);
		Serial.print(" Bitis : ");
		Serial.println(bitisDegeri);

		if (this->isOtomatiklikChanged(isOtomatiklik)
				| this->isBaslamaChanged(baslamaDegeri)
				| this->isBitisChanged(bitisDegeri)) {
			StateEvent evt(EVT_SET_ALL, 0);
			this->fireEvent(evt);
			this->checkForOtomatikStartStop();
		}
	}
	uint16_t getBaslamaDegeri() {
		return this->baslamaDegeri;
	}
	uint16_t getBitisDegeri() {
		return this->bitisDegeri;
	}

	uint16_t getDeger() {
		return this->deger;
	}

	bool isWorking() {
		return digitalRead(this->pinNo);
	}
	bool isOtomatikAktif() {
		return this->otomatikAktif;
	}
	void print(){
		Serial.print("Otomatiklik : ");
		Serial.print(this->isOtomatikAktif());
		Serial.print(" Baslama : ");
		Serial.print(this->getBaslamaDegeri());
		Serial.print(" Bitis : ");
		Serial.println(this->getBitisDegeri());
	}

};
class Sogutucu: public OtomasyonBirimi {
protected:
	virtual void checkForOtomatikStartStop() {
		if (this->isOtomatikAktif()) {
			if (this->hasDeger && (this->deger > this->baslamaDegeri)) {
				this->setWorkingAutomatically(true);
				return;
			}
			if (this->hasDeger && (this->deger < this->bitisDegeri)) {
				this->setWorkingAutomatically(false);
				return;
			}
		} else {
			this->setWorkingAutomatically(false);
			return;
		}
	}
public:
	Sogutucu(uint8_t pin) :
			OtomasyonBirimi(pin) {
	}
	;
};

class Isiklandirma: public OtomasyonBirimi {
protected:
	uint16_t dakika, saat;
	virtual void checkForOtomatikStartStop() {
		if (this->isOtomatikAktif()) {
			if (this->hasDeger) {
				this->setWorkingAutomatically(this->getDeger()>=this->getBaslamaDegeri() && this->getDeger()<this->getBitisDegeri());
			}
		}else
			this->setWorkingAutomatically(false);

	}
public:
	Isiklandirma(uint8_t pin) :
			OtomasyonBirimi(pin) {
		dakika = 0;
		saat = 0;
	}
	void setDeger(uint16_t saat, uint16_t dakika) {
		OtomasyonBirimi::setDeger(saat * 100 + dakika);
	}
	uint16_t getDakika() {
		return getDeger() % 100;
	}
	uint16_t getSaat() {
		return getDeger() / 100;
	}
	void setBaslamaDegeri(uint16_t saat, uint16_t dakika) {
		uint16_t val = saat * 100 + dakika;
		OtomasyonBirimi::setBaslamaDegeri(val);
	}
	void setBitisDegeri(uint16_t saat, uint16_t dakika) {
		uint16_t val = saat * 100 + dakika;
		OtomasyonBirimi::setBitisDegeri(val);
	}
	void set(uint8_t isOtomatik, uint16_t basSaat, uint16_t basDakika,
			uint16_t bitisSaat, uint16_t bitisDakika) {
		uint16_t baslama = basSaat * 100 + basDakika;
		uint16_t bitis = bitisSaat * 100 + bitisDakika;
		OtomasyonBirimi::set(isOtomatik, baslama, bitis);
	}
	uint16_t getBaslamaDakika() {
		uint16_t val = OtomasyonBirimi::getBaslamaDegeri();
		return val % 100;
	}
	uint16_t getBitisDakikasi() {
		uint16_t val = OtomasyonBirimi::getBitisDegeri();
		return val % 100;
	}
	uint16_t getBaslamaSaati() {
		uint16_t val = OtomasyonBirimi::getBaslamaDegeri();
		return val / 100;
	}
	uint16_t getBitisSaati() {
		uint16_t val = OtomasyonBirimi::getBitisDegeri();
		return val / 100;
	}
};

#endif /* OTOMASYONBIRIMI_H_ */
