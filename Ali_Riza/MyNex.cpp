/*
 * MyNex.cpp
 *
 *  Created on: 26 Oca 2016
 *      Author: Alptug
 */

#include "MyNex.h"

NexPage MyNex::getPage(const uint8_t pageId) {
	switch (pageId) {
	case MyNex::ID_MAIN_PAGE:
		return NexPage(0, 0, "Main");
	case MyNex::ID_ISITICI_PAGE:
		return NexPage(1, 0, "I");
	case MyNex::ID_SOGUTUCU_PAGE:
		return NexPage(2, 0, "S");
	case MyNex::ID_NEMLENDIRICI_PAGE:
		return NexPage(3, 0, "N");
	case MyNex::ID_ISIKLANDIRMA_PAGE:
		return NexPage(4, 0, "IS");
	case MyNex::ID_NUMBER_PAD_PAGE:
		return NexPage(5, 0, "NP");
	case MyNex::ID_UPDATE_PAGE:
		return NexPage(6, 0, "update");
	default:
		return NexPage(0, 0, "Main");
	}
}

uint8_t MyNex::getCurrentPageId() {
	NexNumber cp(0, 32, "Main.val45");
	uint32_t val;
	if (cp.getValue(&val))
		return val;
	else
		return 0;
}

bool MyNex::setWorkingValues(bool isIsiticiWorking, bool isSogutucuWorking,
bool isNemlendiriciWorking, bool isIsiklandirmaWorking) {
	return this->setWorkingValue(MyNex::ID_ISITICI_PAGE, isIsiticiWorking)
			& this->setWorkingValue(MyNex::ID_SOGUTUCU_PAGE, isSogutucuWorking)
			& this->setWorkingValue(MyNex::ID_NEMLENDIRICI_PAGE,
					isNemlendiriciWorking)
			& this->setWorkingValue(MyNex::ID_ISIKLANDIRMA_PAGE,
					isIsiklandirmaWorking);
}

bool MyNex::setOtomasyonPageValues(uint8_t pageId, uint16_t min, uint16_t max,
bool isOtomatik) {
	return this->setMaxValue(pageId, max) & this->setMinValue(pageId, min)
			& this->setOtomatiklikAktif(pageId, isOtomatik);
}

bool MyNex::setMinValue(uint8_t pageId, uint16_t min) {

	NexNumber *sval = NULL;
	NexNumber *val = NULL;
	NexNumber *sval2 = NULL;
	NexNumber *val2 = NULL;

	char buffer[100];
	bool flag = true;

	sval = this->getGMinElement(pageId);
	val = this->getMinElement(pageId);
	if (pageId == ID_ISIKLANDIRMA_PAGE) {
		sval2 = this->getGMin2Element(pageId);
		val2 = this->getMin2Element(pageId);
		uint16_t saat = min / 100;
		if (!sval->setValue(saat)) {
			getObjInfo(*sval, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}

		if (!val->setValue(saat)) {
			getObjInfo(*val, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
		uint16_t dakika = min % 100;
		if (!sval2->setValue(dakika)) {
			getObjInfo(*sval2, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
		if (!val2->setValue(dakika)) {
			getObjInfo(*val2, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
	} else {
		if (!val->setValue(min)) {
			getObjInfo(*val, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
		if (!sval->setValue(min)) {
			getObjInfo(*sval2, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
	}
	if (val)
		delete val;
	if (val2)
		delete val2;
	if (sval)
		delete sval;
	if (sval2)
		delete sval2;
	return flag;
}

bool MyNex::setMaxValue(uint8_t pageId, uint16_t max) {
	NexNumber *sval = NULL;
	NexNumber *val = NULL;
	NexNumber *sval2 = NULL;
	NexNumber *val2 = NULL;

	char buffer[100];
	bool flag = true;

	sval = this->getGMaxElement(pageId);
	val = this->getMaxElement(pageId);
	if (pageId == ID_ISIKLANDIRMA_PAGE) {
		sval2 = this->getGMin2Element(pageId);
		val2 = this->getMin2Element(pageId);
		uint16_t saat = max / 100;
		if (!sval->setValue(saat)) {
			getObjInfo(*sval, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}

		if (!val->setValue(saat)) {
			getObjInfo(*val, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
		uint16_t dakika = max % 100;
		if (!sval2->setValue(dakika)) {
			getObjInfo(*sval2, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
		if (!val2->setValue(dakika)) {
			getObjInfo(*val2, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
	} else {
		if (!val->setValue(max)) {
			getObjInfo(*val, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
		if (!sval->setValue(max)) {
			getObjInfo(*sval2, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
	}
	if (val)
		delete val;
	if (val2)
		delete val2;
	if (sval)
		delete sval;
	if (sval2)
		delete sval2;
	return flag;
}

bool MyNex::setOtomatiklikAktif(uint8_t pageId, bool isOtomatik) {

	NexNumber *val;
	val = this->getGIsOtomatikElement(pageId);
	NexDSButton *btn;
	btn = this->getIsOtomatikElement(pageId);
	if (val == NULL || btn == NULL) {
		nexprint("otomatiklik yanlis pageId : ");
		nexprintln(pageId);
		if (val)
			delete val;
		if (btn)
			delete btn;
	}
	char buffer[100];
	bool flag = true;
	if (val && !val->setValue(isOtomatik)) {
		getObjInfo(*val, buffer, sizeof(buffer));
		nexprintln(buffer);
		flag = false;
	}
	if (pageId == this->getCurrentPageId()) {
		if (!btn->setValue(isOtomatik)) {
			getObjInfo(*btn, buffer, sizeof(buffer));
			nexprintln(buffer);
			flag = false;
		}
	}
	if (val)
		delete val;
	if (btn)
		delete btn;
	return flag;
}

bool MyNex::setWorkingValue(uint8_t pageId, bool isWorking) {
	NexDSButton *btn;
	btn = this->getIsWorkingElement(pageId);
	if (btn == NULL) {
		nexprint("isWorking yanlis pageId : ");
		nexprintln(pageId);

	}
	char buffer[100];
	bool flag = true;

	if (!btn->setValue(isWorking)) {
		getObjInfo(*btn, buffer, sizeof(buffer));
		nexprintln(buffer);
		flag = false;
	}

	btn = NULL;
	return flag;
}

bool MyNex::gotoPage(uint8_t pageId) {
	NexPage pg = this->getPage(pageId);
	return pg.show();
}

bool MyNex::getMinValue(uint8_t pageId, uint32_t &min) {
	NexNumber *n = this->getMinElement(pageId);
	bool flag = n->getValue(&min);
	if (flag) {
		if (pageId == ID_ISIKLANDIRMA_PAGE) {
			min = min * 100;
			NexNumber *n2 = this->getMin2Element(pageId);
			uint32_t val;
			flag = n2->getValue(&val);
			min = flag ? min + val : 0;
			delete n2;
		}
	}
	delete n;
	return flag;
}

bool MyNex::getMaxValue(uint8_t pageId, uint32_t& max) {
	NexNumber *n = this->getMaxElement(pageId);
	bool flag = n->getValue(&max);
	if (flag) {
		if (pageId == ID_ISIKLANDIRMA_PAGE) {
			max = max * 100;
			NexNumber *n2 = this->getMin2Element(pageId);
			uint32_t val;
			flag = n2->getValue(&val);
			max = flag ? max + val : 0;
			delete n2;
		}
	}
	delete n;
	return flag;
}

bool MyNex::isOtomatik(uint8_t pageId, bool& isOtomatik) {
	NexDSButton *ds = this->getIsOtomatikElement(pageId);
	uint32_t val;
	bool flag = false;
	if (ds) {
		if (ds->getValue(&val)) {
			isOtomatik = val != 0;
			flag = true;
		}
	}
	delete ds;
	return flag;
}

bool MyNex::isWorking(uint8_t pageId, bool& isWorking) {
	NexDSButton *ds = this->getIsWorkingElement(pageId);
	uint32_t val;
	bool flag = false;
	if (ds) {
		if (ds->getValue(&val)) {
			isWorking = val != 0;
			flag = true;
		}
	}
	return flag;
}

bool MyNex::setNem(float nem) {
	NexText *t = this->getNemText();
	bool flag = false;
	if (t) {
		char buffer[30];
//		memset(buffer,0,sizeof(buffer));
		int size = 0;
		char c = '%';
		size = sprintf(buffer, "%d%c", (int) nem, c);
		buffer[size] = '\0';
		if (t->setText(buffer))
			flag = true;
	}
	delete t;
	return flag;

}

bool MyNex::setSicaklik(float sicaklik) {
	NexText *t = this->getSicaklikText();
	bool flag = false;
	if (t) {
		char buffer[30];
		memset(buffer, 0, sizeof(buffer));
//		memset(buffer,0,sizeof(buffer));
		sprintf(buffer, "%d�C", (int) sicaklik);
		if (t->setText(buffer))
			flag = true;
	}
	delete t;
	return flag;
}

bool MyNex::setSaat(uint8_t saat, uint8_t dakika) {
	NexText *t = this->getSaatText();
	bool flag = false;
	if (t) {
		char buffer[30];
//		memset(buffer,0,sizeof(buffer));
		int size = 0;
		size = sprintf(buffer, "%.2d:%.2d", saat, dakika);
		buffer[size] = '\0';
		if (t->setText(buffer))
			flag = true;
	}
	delete t;
	return flag;
}

bool MyNex::setTarih(uint16_t yil, uint8_t ay, uint8_t gun) {
	NexText *t = this->getTarihText();
	bool flag = false;
	if (t) {
		char buffer[30];
		//		memset(buffer,0,sizeof(buffer));
		int size = 0;
		size = sprintf(buffer, "%.2d-%.2d-%d", gun, ay, yil);
		buffer[size] = '\0';
		if (t->setText(buffer))
			flag = true;
	}
	delete t;
	return flag;
}

NexNumber* MyNex::getMinElement(uint8_t pageId) {
	NexNumber *val;
	switch (pageId) {
	case ID_ISITICI_PAGE: {
		val = new NexNumber(1, 3, "I.n0");
		break;
	}
	case ID_SOGUTUCU_PAGE: {
		val = new NexNumber(2, 3, "S.n0");
		break;
	}
	case ID_NEMLENDIRICI_PAGE: {
		val = new NexNumber(3, 3, "N.n0");
		break;
	}
	case ID_ISIKLANDIRMA_PAGE: {
		val = new NexNumber(4, 11, "IS.n0");
		break;
	}
	default:
		val = NULL;
	}
	return val;
}

NexNumber* MyNex::getMaxElement(uint8_t pageId) {

	//Kayded�lecek ver�ler�n oldugu componentlar
	switch (pageId) {
	case ID_ISITICI_PAGE:
		return new NexNumber(1, 7, "I.n1");
	case ID_SOGUTUCU_PAGE:
		return new NexNumber(2, 7, "S.n1");
	case ID_NEMLENDIRICI_PAGE:
		return new NexNumber(3, 7, "N.n1");
	case ID_ISIKLANDIRMA_PAGE:
		return new NexNumber(4, 15, "IS.n2");
	default:
		return NULL;
	}
}

NexNumber* MyNex::getMin2Element(uint8_t pageId) {
	NexNumber *val2;
	switch (pageId) {
	case ID_ISIKLANDIRMA_PAGE: {
		val2 = new NexNumber(4, 14, "IS.n1");
		break;
	}
	default:
		val2 = NULL;
	}
	return val2;
}

NexNumber* MyNex::getMax2Element(uint8_t pageId) {
	switch (pageId) {
	case ID_ISIKLANDIRMA_PAGE: {
		return new NexNumber(4, 16, "IS.n3");

	}
	default:
		return NULL;
	}
}

NexDSButton* MyNex::getIsWorkingElement(uint8_t pageId) {
	switch (pageId) {
	case ID_ISITICI_PAGE:
		return this->dsbIsiticiOnOff;
	case ID_ISIKLANDIRMA_PAGE:
		return this->dsbIsiklandirmaOnOff;
	case ID_SOGUTUCU_PAGE:
		return this->dsbSogutucuOnOff;
	case ID_NEMLENDIRICI_PAGE:
		return this->dsbNemlendiriciOnOff;
	default:
		return NULL;
	}
}

NexDSButton* MyNex::getIsOtomatikElement(uint8_t pageId) {

	switch (pageId) {
	case ID_ISITICI_PAGE: {
		return new NexDSButton(1, 10, "I.bt0");

	}
	case ID_SOGUTUCU_PAGE: {
		return new NexDSButton(2, 10, "S.bt0");

	}
	case ID_NEMLENDIRICI_PAGE: {
		return new NexDSButton(3, 10, "N.bt0");

	}
	case ID_ISIKLANDIRMA_PAGE: {
		return new NexDSButton(4, 8, "IS.bt0");
	}
	default:
		return NULL;
	}
}

NexNumber* MyNex::getGMinElement(uint8_t pageId) {
	NexNumber *sval;
	switch (pageId) {
	case ID_ISITICI_PAGE: {
		sval = new NexNumber(0, 18, "Main.val10");
		break;
	}
	case ID_SOGUTUCU_PAGE: {
		sval = new NexNumber(0, 23, "Main.val20");
		break;
	}
	case ID_NEMLENDIRICI_PAGE: {
		sval = new NexNumber(0, 26, "Main.val30");
		break;
	}
	case ID_ISIKLANDIRMA_PAGE: {
		sval = new NexNumber(0, 31, "Main.val40");
		break;
	}
	default:
		sval = NULL;
	}
	return sval;
}

NexNumber* MyNex::getGMaxElement(uint8_t pageId) {
	switch (pageId) {
	case ID_ISITICI_PAGE:
		return new NexNumber(0, 19, "Main.val11");
	case ID_SOGUTUCU_PAGE:
		return new NexNumber(0, 21, "Main.val21");
	case ID_NEMLENDIRICI_PAGE:
		return new NexNumber(0, 24, "Main.val31");
	case ID_ISIKLANDIRMA_PAGE:
		return new NexNumber(0, 28, "Main.val42");
	default:
		return NULL;
	}
}

NexNumber* MyNex::getGMin2Element(uint8_t pageId) {
	NexNumber *sval2;
	switch (pageId) {
	case ID_ISIKLANDIRMA_PAGE: {
		sval2 = new NexNumber(0, 27, "Main.val41");
		break;
	}
	default:
		sval2 = NULL;

	}
	return sval2;
}

NexNumber* MyNex::getGMax2Element(uint8_t pageId) {
	switch (pageId) {
	case ID_ISIKLANDIRMA_PAGE: {
		return new NexNumber(0, 29, "Main.val43");
	}
	default:
		return NULL;
	}
}

NexNumber* MyNex::getGIsOtomatikElement(uint8_t pageId) {
	NexNumber *val = NULL;
	switch (pageId) {
	case ID_ISITICI_PAGE: {
		val = new NexNumber(0, 20, "Main.val12");
		break;
	}
	case ID_SOGUTUCU_PAGE: {
		val = new NexNumber(0, 22, "Main.val22");
		break;
	}
	case ID_NEMLENDIRICI_PAGE: {
		val = new NexNumber(0, 25, "Main.val32");
		break;
	}
	case ID_ISIKLANDIRMA_PAGE: {
		val = new NexNumber(0, 30, "Main.val44");
		break;
	}
	default:
		val = NULL;
	}
	return val;
}

MyNex::MyNex() {
	dsbIsiticiOnOff = new NexDSButton(0, 10, "Main.bt0");
	dsbSogutucuOnOff = new NexDSButton(0, 15, "Main.bt1");
	dsbNemlendiriciOnOff = new NexDSButton(0, 16, "Main.bt2");
	dsbIsiklandirmaOnOff = new NexDSButton(0, 17, "Main.bt3");

	kaydetIsitici = new NexButton(1, 11, "I.b4");
	kaydetSogutucu = new NexButton(2, 11, "S.b4");
	kaydetNemlendirici = new NexButton(3, 11, "N.b4");
	kaydetIsiklandirma = new NexButton(4, 9, "IS.b4");
}

MyNex::~MyNex() {
	delete dsbIsiklandirmaOnOff;
	delete dsbSogutucuOnOff;
	delete dsbNemlendiriciOnOff;
	delete dsbIsiklandirmaOnOff;
	delete kaydetIsitici;
	delete kaydetSogutucu;
	delete kaydetNemlendirici;
	delete kaydetIsiklandirma;
}

bool MyNex::setMainPageValues(float sicaklik, float nem, DateTime dt) {
	return this->setSicaklik(sicaklik) & this->setNem(nem)
			& this->setSaat(dt.hour(), dt.minute())
			& this->setTarih(dt.year(), dt.month(), dt.date());
}

bool MyNex::setWifi(bool avaible) {
	NexPicture pic(0, 33, "p0");
	return pic.setPic(avaible ? this->picWifiOn : this->picWifiOff);
}

void MyNex::getObjInfo(NexObject obj, char* buffer, int size) {
	memset(buffer, 0, size);
	int s = snprintf(buffer, size, "[%d,%d,%s]", obj.getObjPid(),
			obj.getObjCid(), obj.getObjName());
	buffer[s] = '\0';
}

NexText* MyNex::getNemText() {

	return new NexText(0, 2, "Main.t1");
}

NexText* MyNex::getSicaklikText() {
	return new NexText(0, 1, "Main.t0");
}

NexText* MyNex::getSaatText() {
	return new NexText(0, 3, "Main.t2");
}

NexText* MyNex::getTarihText() {
	return new NexText(0, 4, "Main.t3");
}

const uint8_t MyNex::getPageIdByFromObject(NexObject* obj) {
	return obj->getObjPid();
}

const uint8_t MyNex::getRelatedPageIdFormDSButton(NexDSButton* dsbtn) {
	switch (dsbtn->getObjCid()) {
	case 10:
		return MyNex::ID_ISITICI_PAGE;
	case 15:
		return MyNex::ID_SOGUTUCU_PAGE;
	case 16:
		return MyNex::ID_NEMLENDIRICI_PAGE;
	case 17:
		return MyNex::ID_ISIKLANDIRMA_PAGE;

	default:
		return 0;
	}
}
