/*
 * SerialData.h
 *
 *  Created on: 22 Oca 2016
 *      Author: Alptug
 */

#ifndef SERIALDATA_H_
#define SERIALDATA_H_
#include "arduino.h"

class SerialData {
private:
	bool DEBUG_ENABLED=false;
	//#define MAC_SERVER
	//#define UNIX_SERVER
	const char* SERVER_END_OF_LINE =
	#ifdef MAC_SERVER
			"\r";
	#else
	#ifdef   UNIX_SERVER
			"\n";
	#else
			"\r\n";
	#endif
	#endif
	#define d_print(STR)\
	  {\
	    if(DEBUG_ENABLED) Serial.print(STR); \
	  }
	#define d_println(STR)\
	  {\
	    if(DEBUG_ENABLED) Serial.println(STR);\
	  }
	const static int MAX_PACK_SIZE = 255;
	uint8_t data[MAX_PACK_SIZE];
	uint8_t size;
	bool finished;
	bool started;
	bool overlapped;
	void resetNextBytes(int startIndex){
		for(int i=startIndex;i<this->MAX_PACK_SIZE;i++)
			this->data[i]=0x0;
	}
public:
	const static int BASLANGIC_AYIRACI = 1;
	const static int BITIS_AYIRACI = 2;
	const static int GERCEK_DATA = 3;
	const static int TANIMSIZ_DATA = 4;
	const static int OVERLAPPED = 5;
	void init() {
		this->resetNextBytes(0);
		finished = false;
		started = false;
		overlapped = false;
		size = 0;
	}
	uint8_t setData(uint8_t d) {
		if (size >= MAX_PACK_SIZE - 3) {
			d_println("Overlapped");
			overlapped = true;
			this->init();
			return OVERLAPPED;
		}
		if (d == '\0' || d == '>' || d == '\r' || d == '\n') {
			if (!started) {
				d_print("Tanimsiz Data byte code:");
				d_println((int )d);
				d_println("Data daha baslamadi");
				return TANIMSIZ_DATA;
			}
			if (finished) {
				d_print("Tanimsiz Data byte code:");
				d_println((int )d);
				d_println("Data zaten bitik");
				return BITIS_AYIRACI;
			}
			overlapped = false;
			finished = true;
			started = false;
			this->resetNextBytes(size);
			d_print("End of data : ");
			d_println((char * )data);
			return BITIS_AYIRACI;
		} else if (d == '<') {
			d_print("Baslangic Ayiraci : ");
			d_println((char )d);
			overlapped = false;
			started = true;
			finished = false;
			return BASLANGIC_AYIRACI;
		} else {
			if (!started) {
				d_print("Tanimsiz Data :");
				d_println((int )d);
				d_println("Baslangic datasi daha gelmedi");
				return TANIMSIZ_DATA;
			}
			if(this->getDataType(d)==GERCEK_DATA){
			d_print("Gecerli Data:");
			d_println((int )d);
			data[size++] = d;
			return GERCEK_DATA;
			}else{
				this->init();
				return TANIMSIZ_DATA;
			}
		}
	}
	uint8_t getDataType(byte d) {
		if (d == '\0' || d == '>' || d == '\r' || d == '\n') {
			return finished ? TANIMSIZ_DATA : BITIS_AYIRACI;
		} else if (d == '<') {
			return BASLANGIC_AYIRACI;
		} else if(d=='\t' || d==' ' || (d>='0' && d<='9')){
			return started ? GERCEK_DATA : TANIMSIZ_DATA;
		}else
			return TANIMSIZ_DATA;
	}

	const byte* getData() const {
		return data;
	}

	bool isFinished() const {
		return finished;
	}

	bool isOverlapped() const {
		return overlapped;
	}

	uint8_t getSize() const {
		return size;
	}

	bool isStarted() const {
		return started;
	}
};

#endif /* SERIALDATA_H_ */
