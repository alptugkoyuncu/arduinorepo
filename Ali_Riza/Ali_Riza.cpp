// Do not remove the include below
#include "Ali_Riza.h"

/*
 * Declare a dual state button object [page id:0,component id:1, component name: "dsbIsiticiOnOff"].
 */

volatile bool pageBlockedAktif = false;

unsigned long lastEventTime;

MyNex myNex;

StateChangeListener *isitmaListener, *sogutmaListener, *nemListener,
		*isikListener;

OtomasyonBirimi isitici( ISITICI_PIN);
Sogutucu sogutucu(SOGUTUCU_PIN);
OtomasyonBirimi nemlendirici(NEMLENDIRICI_PIN);
Isiklandirma isiklandirma(ISIKLANDIRMA_PIN);
/*
 * Register a dual state button object to the touch event list.
 */
NexTouch *nex_listen_list[] = { myNex.dsbIsiticiOnOff, myNex.dsbSogutucuOnOff,

myNex.dsbNemlendiriciOnOff, myNex.dsbIsiklandirmaOnOff, myNex.kaydetIsitici,
		myNex.kaydetSogutucu, myNex.kaydetNemlendirici,
		myNex.kaydetIsiklandirma, NULL };

TemperatureHumiditySensor *tempSens;
MyTime *timer;

SerialData sd0, sd3;
int minMemory, maxMemory;
const bool hasSerial = true;
bool hasWifi = false;

bool isPingSent = false;
unsigned long pingSentTime;
bool timerIsUpdated = false;
OtomasyonKayit *kayit;

class OtomasyonStateListener: public StateChangeListener {
private:
	uint8_t obType;

public:
	OtomasyonStateListener(uint8_t otomasyonBirimiType) {
		this->obType = otomasyonBirimiType;
	}
	uint8_t getObType() {
		return obType;
	}

	void changeState(StateEvent ev) override {
		delay(50);
		//blockPage();
		char buffer[50];
		ev.toString(buffer, sizeof(buffer));
		Serial.print(" Event obj :");
		Serial.println(buffer);
		uint8_t pageId = getNexPageIdByObType(getObType());
		switch (ev.getType()) {
//		case EVT_MIN_DEGER_CHANGED: {
//			Serial.println("EVT_MIN_CHANGED");
//			//nexi bilgilendir
//			myNex.setMinValue(pageId, ev.getValue());
//
//			// seriallari bilgilendir
//			sendOBMin(SERIAL_ALL, this->getObType());
//			break;
//		}
//		case EVT_MAX_DEGER_CHANGED: {
//			Serial.println("EVT_MAX_CHANGED");
//			//nexi bilgilendir
//			myNex.setMaxValue(pageId, ev.getValue());
//			// seriallari bilgilendir
//			sendOBMax(SERIAL_ALL, this->getObType());
//			break;
//		}
//		case EVT_OTOMATIKLIK_CHANGED: {
//			Serial.println("EVT_OTOMATIKLIK_CHANGED");
//			bool val = ev.getValue() > 0;
//			//nexi bilgilendir
//			myNex.setOtomatiklikAktif(pageId, val);
//			// seriallari bilgilendir
//			sendOBIsOtomatik(SERIAL_ALL, this->getObType());
//			break;
//		}
		case EVT_WORKING_STATU_CHANGED: {
			Serial.println("EVT_WORKING_STATUR CHANGED");
			bool val = ev.getValue() > 0;
			//nexi bilgilendir
			myNex.setWorkingValue(pageId, val);
			// seriallari bilgilendir
			sendOBIsWorking(SERIAL_ALL, this->getObType());
			break;
		}
		case EVT_OTOMATIKLIK_CHANGED:
		case EVT_MIN_DEGER_CHANGED:
		case EVT_MAX_DEGER_CHANGED:
		case EVT_SET_ALL: {
			Serial.println("EVT_SET_ALL");
			OtomasyonBirimi *tmp = getOtomasyonBirimiByType(getObType());

			sendObInfo(SERIAL_ALL, getObType());
			myNex.setOtomasyonPageValues(pageId, tmp->getBaslamaDegeri(),
					tmp->getBitisDegeri(), tmp->isOtomatikAktif() > 0);

			break;
		}
		default: {
			break;
		}
		}

		if (ev.getType() != EVT_WORKING_STATU_CHANGED) {
			int val = 0;
			switch (this->getObType()) {
			case OtomasyonBirimi::OB_TYPE_ISITICI: {
				val = kayit->saveIsitici(isitici);
				break;
			}
			case OtomasyonBirimi::OB_TYPE_SOGUTUCU: {
				val = kayit->saveSogutucu(sogutucu);
				break;
			}
			case OtomasyonBirimi::OB_TYPE_NEMLENDIRICI: {
				val = kayit->saveNemlendirici(nemlendirici);
				break;
			}
			case OtomasyonBirimi::OB_TYPE_ISIKLANDIRMA: {
				val = kayit->saveIsiklandirma(isiklandirma);
				break;
			}
			default: {
				break;
			}
			}
			if (val > 0) {
				sendDosyaKayitInfo(SERIAL_ALL);
			}
		}
		lastEventTime = millis();
		Serial.println("END OF OTOMASYON STATE LISTENER");
		sendFreeMemory(SERIAL_ALL);
		delay(50);
	}
};

int counter = 0;
class TimerListener: public SensorListener {
	void valueChanged(SensorEvent evt) override {
		counter++;
		Serial.println(F("-----SENSOR LISTENER-------"));
		Serial.print(F("Event type : "));
		Serial.print(evt.getType());
		Serial.print(F("   counter :"));
		Serial.println(counter);
		switch (evt.getType()) {
		case SensorEvent::SENSOR_TIME: {
			DateTime dt = timer->getDateTime();
			isiklandirma.setDeger(dt.hour(), dt.minute());
			myNex.setSaat(dt.hour(), dt.minute());
			sendTime(SERIAL_ALL);
			break;
		}
		case SensorEvent::SENSOR_DATE: {
			DateTime dt = timer->getDateTime();
			myNex.setTarih(dt.year(), dt.month(), dt.date());
			sendTime(SERIAL_ALL);
			break;
		}
		case SensorEvent::SENSOR_SECOND: {
			break;
		}
		case SensorEvent::SENSOR_DATE_TIME: {
			DateTime dt = timer->getDateTime();
			isiklandirma.setDeger(dt.hour(), dt.minute());
			delay(50);
			myNex.setSaat(dt.hour(), dt.minute());
			delay(50);
			myNex.setTarih(dt.year(), dt.month(), dt.date());
			sendTime(SERIAL_ALL);
			break;
		}
		default:
			Serial.print(F("Sensor listener hata : "));
			Serial.println(evt.getType());
			break;
		}
		Serial.println(F("-----------"));
	}
};
class OBSensorListener: public SensorListener {
public:
	void valueChanged(SensorEvent evt) override {
		Serial.println(F("-----SENSOR LISTENER-------"));
		Serial.print(F("Event type : "));
		Serial.println(evt.getType());
		switch (evt.getType()) {
		case SensorEvent::SENS0R_TEMPERATURE: {
			isitici.setDeger(tempSens->getTemp());
			sogutucu.setDeger(tempSens->getTemp());
			myNex.setSicaklik(tempSens->getTemp());
			sendTempInfo(SERIAL_ALL);
			break;
		}
		case SensorEvent::SENSOR_HUMIDITY: {
			nemlendirici.setDeger(tempSens->getHumidity());
			myNex.setNem(tempSens->getHumidity());
			sendHumInfo(SERIAL_ALL);
			break;
		}
		default:
			Serial.print(F("Sensor listener hata : "));
			Serial.println(evt.getType());
			break;
		}

		delay(50);
	}
};

OBSensorListener obl;
TimerListener tl;
void setup(void) {

	/* Set the baudrate which is for debug and communicate with Nextion screen. */
	nexInit();
#ifndef DEBUG_SERIAL_ENABLE
	Serial.begin(115200);
#endif
	Serial.println("SETUP STARTED");
	delay(5000);
	kayit = new OtomasyonKayit();
	Serial.println("SETUP STARTED");
	tempSens = new DHT_Sensor();
	tempSens->addListener(&obl);

	/*Initialize INT0 for accepting interrupts from timer */
	PORTD |= 0x04;
	DDRD &= ~0x04;
	attachInterrupt(0, INT0_ISR, FALLING);
	Serial.println("SETUP STARTED");
	timer = new MyTime();
	timer->addListener(&tl);

	/* Register the pop event callback function of the dual state button component. */
	isitmaListener = new OtomasyonStateListener(
			OtomasyonBirimi::OB_TYPE_ISITICI);
	sogutmaListener = new OtomasyonStateListener(
			OtomasyonBirimi::OB_TYPE_SOGUTUCU);
	nemListener = new OtomasyonStateListener(
			OtomasyonBirimi::OB_TYPE_NEMLENDIRICI);
	isikListener = new OtomasyonStateListener(
			OtomasyonBirimi::OB_TYPE_ISIKLANDIRMA);

	isitici.addListener(isitmaListener);
	sogutucu.addListener(sogutmaListener);
	nemlendirici.addListener(nemListener);
	isiklandirma.addListener(isikListener);

	kayit->loadToOtomasyonBirimi(isitici, sogutucu, nemlendirici, isiklandirma);

	isitici.print();

	myNex.dsbIsiticiOnOff->attachPop(acKapaButtonClicked,
			myNex.dsbIsiticiOnOff);
	myNex.dsbSogutucuOnOff->attachPop(acKapaButtonClicked,
			myNex.dsbSogutucuOnOff);
	myNex.dsbNemlendiriciOnOff->attachPop(acKapaButtonClicked,
			myNex.dsbNemlendiriciOnOff);
	myNex.dsbIsiklandirmaOnOff->attachPop(acKapaButtonClicked,
			myNex.dsbIsiklandirmaOnOff);

	myNex.kaydetIsitici->attachPop(kaydetButtonClicked, myNex.kaydetIsitici);
	myNex.kaydetSogutucu->attachPop(kaydetButtonClicked, myNex.kaydetSogutucu);
	myNex.kaydetNemlendirici->attachPop(kaydetButtonClicked,
			myNex.kaydetNemlendirici);
	myNex.kaydetIsiklandirma->attachPop(kaydetButtonClicked,
			myNex.kaydetIsiklandirma);

	minMemory = freeMemory();
	maxMemory = minMemory;
	sendFreeMemory(SERIAL_ALL);

//	isitici.setBaslamaDegeri(11);
//	isitici.setBitisDegeri(99);
//	isitici.setOtomatikAktif(1);
//
//	sogutucu.setBaslamaDegeri(22);
//	sogutucu.setBitisDegeri(88);
//	sogutucu.setOtomatikAktif(0);
//
//	nemlendirici.setBaslamaDegeri(33);
//	nemlendirici.setBitisDegeri(77);
//	nemlendirici.setOtomatikAktif(1);
//
//	isiklandirma.setBaslamaDegeri(6, 0);
//	isiklandirma.setBitisDegeri(18, 0);
//	isiklandirma.setOtomatikAktif(0);
//	isiticiSonAyarlariYukle();
//	sogutucuSonAyarlariYukle();
//	nemlendiriciSonAyarlariYukle();
//	isiklandirmaSonAyarlariYukle();
//	tempSens->setTemp(38);
//	tempSens->setHumidity(66);

	/*	checkBlockage();
	 delay(10000);
	 checkBlockage();
	 char t[100]={0};
	 txtSicaklik.getText(t,sizeof(t));
	 Serial.println(t);
	 memset(t,0,sizeof(t));
	 txtNem.getText(t,sizeof(t));
	 Serial.println(t);
	 memset(t,0,sizeof(t));
	 txtSaat.getText(t,sizeof(t));
	 Serial.println(t);
	 memset(t,0,sizeof(t));
	 txtTarih.getText(t,sizeof(t));
	 Serial.println(t);
	 Serial.println("now dpne");
	 while(1)
	 delay(1);*/
	timer->begin();
	tempSens->begin();
	pinMode(ESP_RESET_PIN, OUTPUT);
	action_reset_esp(1000);
}
void loop(void) {
	/*
	 * When a pop or push event occured every time,
	 * the corresponding component[right page id and component id] in touch event list will be asked.
	 */
	if (!timerIsUpdated) {
		requestTimerUpdateFromServer();
	}
	checkWireless();

	nexLoop(nex_listen_list);
	tempSens->update();
	if (timer->update()) {
//		sendTime(SERIAL_ALL);
//		DateTime dt = timer->getDateTime();
//		myNex.setSaat(dt.hour(),dt.minute());
	}
	while (hasSerial && Serial.available() > 0) {
		uint8_t data = Serial.read();
		sd0.setData(data);
		if (sd0.isFinished()) {
			doAction((char *) sd0.getData(), sd0.getSize());
			sd0.init();
		}
	}
	while (wifi.available() > 0) {
		uint8_t data = wifi.read();
		sd3.setData(data);
		if (sd3.isFinished()) {
			doAction((char *) sd3.getData(), sd3.getSize());
			sd3.init();
		}
	}
//checkBlockage();

}
void doAction(char *buff, int size, uint8_t serialNo) {
	Serial.println(F("[-----DOACTION-----]"));
	Serial.print(" Data : ");
	Serial.print(buff);
	Serial.print(" - Size : ");
	Serial.println(size);
	bool switchEnable = true;
	if (buff == NULL) {
		switchEnable = false;
	}
	uint8_t index = 0;
	uint16_t action = 0;
	if (!getNextData(buff, &index, size, &action)) {
		Serial.println(F("action bulunamadi"));
		switchEnable = switchEnable & false;
	} else {
		Serial.print("action = ");
		Serial.println(action);
	}
	OtomasyonBirimi *tmp;
	if (switchEnable)
		switch (action) {
		case SET_OB_ALL: {
			Serial.println(F("[---SET_OB_ALL---]"));
			uint16_t type = 0;
			if (!getNextData(buff, &index, size, &type)) {
				Serial.println("type bulunamadi");
				break;
			}
			tmp = getOtomasyonBirimiByType(type);
			uint16_t baslama, bitis, otomatikAktif;
			if (!getNextData(buff, &index, size, &baslama)) {
				Serial.println("min bulunamadi");
				break;
			}
			if (!getNextData(buff, &index, size, &bitis)) {
				Serial.println("max bulunamadi");
				break;
			}
			if (!getNextData(buff, &index, size, &otomatikAktif)) {
				Serial.println("isotomatik bulunamadi");
				break;
			}
			tmp->set(otomatikAktif, baslama, bitis);
//			tmp->setBaslamaDegeri(baslama);
//			tmp->setBitisDegeri(bitis);
//			tmp->setOtomatikAktif(otomatikAktif);
			Serial.println(F("]---SET_OB_ALL---["));
			break;
		}
		case SET_OB_WORKING: {
			uint16_t type;
			Serial.println(F("[---SET_OB_WORKING---]"));
			if (!getNextData(buff, &index, size, &type)) {
				Serial.println("Type bulunamadi");
				break;
			}
			tmp = getOtomasyonBirimiByType(type);

			uint16_t working;
			if (!getNextData(buff, &index, size, &working)) {
				Serial.println("working bulunamadi");
				break;
			}
			tmp->setWorking(working > 0);
			Serial.println(F("]---SET_OB_WORKING---["));
			break;
		}
		case SET_TIMER: {
			uint16_t year = 0, month = 0, day = 0, hour = 0, minute = 0,
					second = 0, dayOfWeek;
			if (!getNextData(buff, &index, size, &year)) {
				Serial.println("yil bulunamadi");
				break;
			}
			if (!getNextData(buff, &index, size, &month)) {
				Serial.println("ay bulunamadi");
				break;
			}
			if (!getNextData(buff, &index, size, &day)) {
				Serial.println("gun bulunamadi");
				break;
			}
			if (!getNextData(buff, &index, size, &hour)) {
				Serial.println("saat bulunamadi");
				break;
			}
			if (!getNextData(buff, &index, size, &minute)) {
				Serial.println("dakika bulunamadi");
				break;
			}
			if (!getNextData(buff, &index, size, &second)) {
				Serial.println("saniye bulunamadi");
				break;
			}
			if (!getNextData(buff, &index, size, &dayOfWeek)) {
				Serial.println(F("Haftanin gunu bulunamadi"));
			}
			DateTime dt(year, month, day, hour, minute, second, dayOfWeek);
			timer->adjustRtc(dt);
			timerIsUpdated = true;
			break;
		}
		case GET_ALL_DATA_INFO: {
			sendAllDataInfo(SERIAL_ALL);
			break;
		}
		case GET_AYALAR_INFOR: {
			uint16_t type;
			if (!getNextData(buff, &index, size, &type)) {
				Serial.println("type bulunamadi");
				break;
			}
			sendObInfo(SERIAL_ALL, type);
			break;
		}

		case GET_TEMP_INFO: {
			sendTempInfo(SERIAL_ALL);
			break;
		}
		case GET_HUM_INFO: {
			sendHumInfo(SERIAL_ALL);
			break;
		}
		case GET_TIMER: {
			sendTime(SERIAL_ALL);
			break;
		}
//		case GET_OB_MIN: {
//			uint16_t type;
//			if (!getNextData(buff, &index, size, &type)) {
//				Serial.println("type bulunamadi");
//				break;
//			}
//			sendOBMin(serialNo, type);
//			break;
//		}
//		case GET_OB_MAX: {
//			uint16_t type;
//			if (!getNextData(buff, &index, size, &type)) {
//				Serial.println("type bulunamadi");
//				break;
//			}
//			sendOBMax(serialNo, type);
//			break;
//		}
//		case GET_OB_IS_AUTO: {
//			uint16_t type;
//			if (!getNextData(buff, &index, size, &type)) {
//				Serial.println("type bulunamadi");
//				break;
//			}
//			sendOBIsOtomatik(serialNo, type);
//			break;
//		}
		case GET_OB_IS_WORKING: {
			uint16_t type;
			if (!getNextData(buff, &index, size, &type)) {
				Serial.println("type bulunamadi");
				break;
			}
			sendOBIsWorking(SERIAL_ALL, type);
			break;
		}
		case GET_FREE_MEMORY: {
			sendFreeMemory(SERIAL_ALL);
			break;
		}
		case GET_DOSYA_KAYIT_INFO: {
			sendDosyaKayitInfo(SERIAL_ALL);
			break;
		}
		case ACTION_REQUEST_ROOM_NUMBER: {
			sentRoomNuber(SERIAL_ALL);
			break;
		}
		case ACTION_SENT_ROOM_NUMBER: {
			break;
		}
		case ACTION_ESP_STARTED: {
			action_start_esp();
			break;
		}
		case ACTION_RESET_ESP: {
			action_reset_esp(1000);
			break;
		}
		case ACTION_PING_TO_SERVER: {
			action_send_ping_server();
			break;
		}
		case ACTION_PING_FROM_SERVER: {
			action_receive_ping_server();
			break;
		}

		case ACTION_DISCONNECT_FROM_SERVER: {
			action_reset_esp(1000);
			break;
		}
		default:
			Serial.println("Do action error");
			break;
		}
	Serial.println(F("]____DOACTION----["));

}
// bosluk olmayan �lk �ndex� indexe kaydeder
void boslukAtla(char *data, uint8_t *index, uint8_t size) {
	uint8_t i = *index;
	for (; i < size; i++)
		if (data[i] == ' ')
			continue;
		else {
			*index = i;
			return;
		}
	*index = size;
	return;
}

OtomasyonBirimi *getOtomasyonBirimiByType(uint8_t otomasyontype) {
	switch (otomasyontype) {
	case OtomasyonBirimi::OB_TYPE_ISITICI:
		return &isitici;
	case OtomasyonBirimi::OB_TYPE_SOGUTUCU:
		return &sogutucu;
	case OtomasyonBirimi::OB_TYPE_NEMLENDIRICI:
		return &nemlendirici;
	case OtomasyonBirimi::OB_TYPE_ISIKLANDIRMA:
		return &isiklandirma;
	default:
		return NULL;
	}
}
uint8_t sendToSerial(char *buffer) {
	Serial.print("TO SERIAL : ");
	size_t t = Serial.println(buffer);
	return t;

}
uint8_t sendToWifi(char *buffer) {
	Serial.print("TO WIFI:");
	Serial.println(buffer);
	size_t t = wifi.println(buffer);
	return t;
}
uint8_t sentToUart(uint8_t serial, char*buffer) {
	switch (serial) {
	case 0: {
		if (hasSerial) {
			return sendToSerial(buffer);
		} else
			return 0;
	}
	case 1:
		return 0;
	case 2:
		return 0;
	case 3:
		if (hasWifi) {
			return sendToWifi(buffer);
		} else
			return 0;
	case SERIAL_ALL: {
		size_t t = 0;
		if (hasSerial) {
			t = sendToSerial(buffer);
		}
		if (hasWifi) {
			t = sendToWifi(buffer);
		}
		return t;
	}
	default:
		return 0;
	}
}
uint8_t sendOBIsWorking(uint8_t serial, uint8_t otomasyonType) {
	uint8_t action = GET_OB_IS_WORKING;
	char buffer[100];
	int size;
	if (otomasyonType == OtomasyonBirimi::OB_TYPE_ALL) {
		size = sprintf(buffer, "<%d %d %d %d %d %d>", action, otomasyonType,
				isitici.isWorking(), sogutucu.isWorking(),
				nemlendirici.isWorking(), isiklandirma.isWorking());
	} else {
		OtomasyonBirimi *tmp = getOtomasyonBirimiByType(otomasyonType);

		size = sprintf(buffer, "<%d %d %d>", action, otomasyonType,
				tmp->isWorking());

		tmp = NULL;
	}
	buffer[size] = '\0';
	return sentToUart(serial, buffer);
}
//uint8_t sendOBIsOtomatik(uint8_t serial, uint8_t otomasyonType) {
//	uint8_t action = GET_OB_IS_AUTO;
//	OtomasyonBirimi *tmp = getOtomasyonBirimiByType(otomasyonType);
//	char buffer[100];
//	int size = sprintf(buffer, "<%d %d %d>", action, otomasyonType,
//			tmp->isOtomatikAktif());
//	buffer[size] = '\0';
//	tmp = NULL;
//	return sentToUart(serial, buffer);
//}
//uint8_t sendOBMax(uint8_t serial, uint8_t otomasyonType) {
//	uint8_t action = GET_OB_MAX;
//	OtomasyonBirimi *tmp = getOtomasyonBirimiByType(otomasyonType);
//	char buffer[100];
//	memset(buffer, 0, sizeof(buffer));
//	snprintf(buffer, sizeof(buffer), "<%d %d %d>", action, otomasyonType,
//			tmp->getBitisDegeri());
//
//	return sentToUart(serial, buffer);
//}
//
//uint8_t sendOBMin(uint8_t serial, uint8_t otomasyontype) {
//
//	uint8_t action = GET_OB_MIN;
//	OtomasyonBirimi *tmp = getOtomasyonBirimiByType(otomasyontype);
//	char buffer[100];
//	memset(buffer, '\0', sizeof(buffer));
//	snprintf(buffer, sizeof(buffer), "<%d %d %d>", action, otomasyontype,
//			tmp->getBaslamaDegeri());
//	return sentToUart(serial, buffer);
//}
uint8_t sendTempInfo(uint8_t serial) {

	uint8_t action = GET_TEMP_INFO;
	char buffer[100];
	char temp[20];
	memset(temp, 0, 20);
	floattoChar(temp, tempSens->getTemp(), 0);
	int size = sprintf(buffer, "<%d %s>", action, temp);
	buffer[size] = '\0';
	return sentToUart(serial, buffer);
}
uint8_t sendHumInfo(uint8_t serial) {
	uint8_t action = GET_HUM_INFO;
	char buffer[100];
	char hum[20];
	memset(hum, 0, 20);
	floattoChar(hum, tempSens->getHumidity(), 0);
	int size = sprintf(buffer, "<%d %s>", action, hum);
	buffer[size] = '\0';
	return sentToUart(serial, buffer);
}
uint8_t sendAllDataInfo(uint8_t serial) {
	sendTempInfo(serial);
	delay(50);
	sendHumInfo(serial);
	delay(50);
	sendTime(serial);
	delay(50);
	sendOBIsWorking(serial, OtomasyonBirimi::OB_TYPE_ALL);

}
uint8_t sendObInfo(uint8_t serial, uint8_t otomasyonType) {
	uint8_t action = GET_AYALAR_INFOR;
	char buffer[200];
	int size = -1;
	if (otomasyonType == OtomasyonBirimi::OB_TYPE_ALL) {
		size = sprintf(buffer, "<%d %d %d %d %d %d %d %d %d %d %d %d %d %d>",
				action, otomasyonType, isitici.getBaslamaDegeri(),
				isitici.getBitisDegeri(), isitici.isOtomatikAktif(),
				sogutucu.getBaslamaDegeri(), sogutucu.getBitisDegeri(),
				sogutucu.isOtomatikAktif(), nemlendirici.getBaslamaDegeri(),
				nemlendirici.getBitisDegeri(), nemlendirici.isOtomatikAktif(),
				isiklandirma.getBaslamaDegeri(), isiklandirma.getBitisDegeri(),
				isiklandirma.isOtomatikAktif());
	} else {
		OtomasyonBirimi *tmp = getOtomasyonBirimiByType(otomasyonType);

		size = sprintf(buffer, "<%d %d %d %d %d %d>", action, otomasyonType,
				tmp->getBaslamaDegeri(), tmp->getBitisDegeri(),
				tmp->isOtomatikAktif(), tmp->isWorking());

		tmp = NULL;
	}
	if (size > 0) {
		buffer[size] = '\0';
		return sentToUart(serial, buffer);
	} else
		return 0;

}
uint8_t sendTime(uint8_t serial) {
	uint8_t action = GET_TIMER;
	DateTime dt = timer->getDateTime();
	char buffer[100];
	int size = sprintf(buffer, "<%d %d %d %d %d %d %d>", action, dt.year(),
			dt.month(), dt.date(), dt.hour(), dt.minute(), dt.second());
	buffer[size] = '\0';
	return sentToUart(serial, buffer);

}
uint8_t sendFreeMemory(uint8_t serial) {
	uint8_t action = GET_FREE_MEMORY;
	int mem = freeMemory();
	if (mem > maxMemory)
		maxMemory = mem;
	if (mem < minMemory)
		minMemory = mem;
	char buffer[100];
	int size = sprintf(buffer, "<%d %d %d %d>", action, mem, maxMemory,
			minMemory);
	buffer[size] = '\0';
	return sentToUart(serial, buffer);
}
uint8_t getOtomasyonTypeByPageId(uint8_t myNexPageId) {
	switch (myNexPageId) {
	case MyNex::ID_ISIKLANDIRMA_PAGE:
		return OtomasyonBirimi::OB_TYPE_ISIKLANDIRMA;
	case MyNex::ID_ISITICI_PAGE:
		return OtomasyonBirimi::OB_TYPE_ISITICI;
	case MyNex::ID_NEMLENDIRICI_PAGE:
		return OtomasyonBirimi::OB_TYPE_NEMLENDIRICI;
	case MyNex::ID_SOGUTUCU_PAGE:
		return OtomasyonBirimi::OB_TYPE_SOGUTUCU;
	default:
		return 0;
	}
}
void kaydetButtonClicked(void *ptr) {
	Serial.println("Kaydet Button clicked");
	NexButton *btn = (NexButton *) ptr;
	uint8_t pageId = btn->getObjPid();
	btn = NULL;
	uint32_t baslama, bitis;
	bool isOtomatik;

	myNex.getMinValue(pageId, baslama);
	myNex.getMaxValue(pageId, bitis);
	myNex.isOtomatik(pageId, isOtomatik);
	uint8_t obType = getOtomasyonTypeByPageId(pageId);
	OtomasyonBirimi *temp;
//blockPage();

	switch (obType) {
	case OtomasyonBirimi::OB_TYPE_ISITICI: {
		temp = &isitici;
		break;
	}
	case OtomasyonBirimi::OB_TYPE_SOGUTUCU: {
		temp = &sogutucu;
		break;
	}
	case OtomasyonBirimi::OB_TYPE_NEMLENDIRICI: {
		temp = &nemlendirici;
		break;
	}
	case OtomasyonBirimi::OB_TYPE_ISIKLANDIRMA: {
		temp = &isiklandirma;
		break;
	}
	default:
		temp = NULL;
		break;
	}
	if (temp) {
		temp->set(isOtomatik, baslama, bitis);
//		temp->setBaslamaDegeri(baslama);
//		temp->setBitisDegeri(bitis);
//		temp->setOtomatikAktif(isOtomatik);
		temp = NULL;
	}
	lastEventTime = millis();
//	page->show();
}

/*
 * Dual state button component pop callback function.
 * In this example,the button's text value will plus one every time when it is released.
 */

void acKapaButtonClicked(void *ptr) {
	NexDSButton *btn = (NexDSButton *) ptr;
	uint8_t pageId = MyNex::getRelatedPageIdFormDSButton(btn);
	bool isWorking;
	myNex.isWorking(pageId, isWorking);
	uint8_t obType = getOtomasyonTypeByPageId(pageId);
	switch (obType) {
	case OtomasyonBirimi::OB_TYPE_ISITICI:
		isitici.setWorking(isWorking);
		break;
	case OtomasyonBirimi::OB_TYPE_SOGUTUCU:
		sogutucu.setWorking(isWorking);
		break;
	case OtomasyonBirimi::OB_TYPE_NEMLENDIRICI:
		nemlendirici.setWorking(isWorking);
		break;
	case OtomasyonBirimi::OB_TYPE_ISIKLANDIRMA:
		isiklandirma.setWorking(isWorking);
		break;
	default: {
		char temp[100];
		memset(temp, 0, sizeof(temp));
		Serial.print("Farkli Component id :");
		myNex.getObjInfo(*btn, temp, sizeof(temp));
		Serial.println(temp);
	}
	}
}

void blockPage() {
	if (!pageBlockedAktif) {
		myNex.gotoPage(MyNex::ID_UPDATE_PAGE);
		pageBlockedAktif = true;
	}
	lastEventTime = millis();

}
void checkBlockage() {
	if (!pageBlockedAktif) {
		return;
	}
	unsigned long diff = millis() - lastEventTime;
	if (diff >= WAIT_FOR_NEXT_EVENT) {
		pageBlockedAktif = false;
		myNex.gotoPage(myNex.getCurrentPageId());
	}
}

void printLog(const char *str, NexObject *ptr) {
	Serial.print("ERROR :");
	Serial.print(str);
	Serial.print("->");
	ptr->printObjInfo();
}
bool getNextData(char *data, uint8_t *index, uint8_t size,
		uint16_t *returnVal) {
	Serial.print("Parse data  : ");
	char act[6];
	uint8_t actIndex = 0;
	boslukAtla(data, index, size);
	while (1) {
		if (*index >= size || data[*index] == '\0' || data[*index] == ' ') {
			if (actIndex == 0) {
				*returnVal = 0;
			} else {
				act[actIndex] = '\0';
				*returnVal = atoi(act);
			}
			Serial.print("Next index=");
			Serial.print(*index);
			Serial.print(" ;Next Data value = ");
			Serial.println(*returnVal);
			return true;
		} else
			act[actIndex++] = data[*index];
		(*index)++;
	}
	return false;
}
//Interrupt service routine for external interrupt on INT0 pin conntected to /INT
void INT0_ISR() {
//Keep this as short as possible. Possibly avoid using function calls
	timer->timerIsInterrupted = true;
}
void floattoChar(char buffer[], float val, int floatSize) {
	if (floatSize > 4)
		floatSize = 4;
	int d = val;
	float ondalik = val - d;
	for (int i = 0; i < floatSize; i++)
		ondalik = ondalik * 10;
	int d2 = ondalik;
	switch (floatSize) {
	case 0:
		sprintf(buffer, "%d", d);
		return;
	case 1:
		sprintf(buffer, "%d.%d", d, d2);
		return;
	case 2:
		sprintf(buffer, "%d.%.2d", d, d2);
		return;
	case 3:
		sprintf(buffer, "%d.%.3d", d, d2);
		return;
	default:
		sprintf(buffer, "%d.%.4d", d, d2);
		return;
	}
}
uint8_t getNexPageIdByObType(uint8_t obType) {
	switch (obType) {
	case OtomasyonBirimi::OB_TYPE_ISITICI:
		return MyNex::ID_ISITICI_PAGE;
	case OtomasyonBirimi::OB_TYPE_SOGUTUCU:
		return MyNex::ID_SOGUTUCU_PAGE;
	case OtomasyonBirimi::OB_TYPE_NEMLENDIRICI:
		return MyNex::ID_NEMLENDIRICI_PAGE;
	case OtomasyonBirimi::OB_TYPE_ISIKLANDIRMA:
		return MyNex::ID_ISIKLANDIRMA_PAGE;
	default:
		return 0;
	}
}
void action_reset_esp(long t) {
	if (t < 550)
		t = 550;
	if (t > 3000)
		t = 3000;
	unsigned long dly = 10;
	wifi.end();
	digitalWrite(ESP_RESET_PIN, LOW);
	delay(dly);
	digitalWrite(ESP_RESET_PIN, HIGH);
	delay(10);
	wifi.begin(WIFI_UART);
	delay(10);
	hasWifi = false;
	isPingSent = false;
	pingSentTime = millis();
	myNex.setWifi(hasWifi);
	Serial.println("RESET ESP");
}
uint8_t action_start_esp() {
	Serial.println("Wireless avaible");
	hasWifi = true;
	myNex.setWifi(hasWifi);
	isPingSent = false;
	pingSentTime = millis();
	delay(10);
	return 0;
}
void checkWireless() {
	if (isPingSent) {
		if (millis() - pingSentTime > PING_REPLY_INTERVAL) {
			action_reset_esp();
			return;
		}
	} else {
		if (millis() - pingSentTime > PING_SENT_INTERVAL) {
			action_send_ping_server();
		}
	}
}

int8_t action_send_ping_server() {

	pingSentTime = millis();
	isPingSent = true;
	char buffer[50];
	memset(buffer, 0, sizeof(buffer));
	int size;
	int act = ACTION_PING_TO_SERVER;
	size = sprintf(buffer, "<%d>", act);

	buffer[size] = '\0';
	return sentToUart(SERIAL_ALL, buffer);
}
int8_t action_receive_ping_server() {
	isPingSent = false;
	pingSentTime = millis();
	return 0;
}

uint8_t sentRoomNuber(uint8_t serialNo) {
	char buffer[50];
	memset(buffer, 0, sizeof(buffer));
	int size = sprintf(buffer, "<%d %d>", ACTION_SENT_ROOM_NUMBER,
	ROOM_NUMBER);
	buffer[size] = '\0';
	return sentToUart(serialNo, buffer);
}
unsigned long sendms = millis();
uint8_t requestTimerUpdateFromServer(uint8_t serialNo) {
	if (millis() > sendms + 10000) {
		char buffer[10];
		memset(buffer, 0, sizeof(buffer));
		int size = sprintf(buffer, "<%d>", UPDATE_TIMER);
		buffer[size] = '\0';
		sendms = millis();

		return sentToUart(serialNo, buffer);
	}
	return 0;
}
uint8_t sendDosyaKayitInfo(uint8_t serialNo) {
	int action = GET_DOSYA_KAYIT_INFO;
	char buffer[40];
	uint8_t movedSayisi = kayit->getMovedDegeri();
	uint16_t kayitSayisi = kayit->getKayitSayisi();
	uint8_t dosyaSize = kayit->getDosyaBilgileriSize();
	int size = sprintf(buffer, "<%d %d %d %d>", action, movedSayisi,
			kayitSayisi, dosyaSize);
	buffer[size] = '\0';
	return sentToUart(serialNo, buffer);
}
