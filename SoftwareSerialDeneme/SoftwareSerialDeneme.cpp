#include <Arduino.h>
#include"Nextion.h"
SoftwareSerial HMISerial(8,7);
void buttonClicked(void *ptr);
NexButton btn(0,2,"b0");
NexNumber num(0,2,"n0");
NexTouch *nex_listen_list[] = {&btn, NULL};
uint32_t i=0;
void setup() {
	nexInit();
	btn.attachPop(buttonClicked,&btn);
	dbSerialPrintln("Setup done");
}

void buttonClicked(void *ptr){
	dbSerialPrintln("button clicked");
	uint32_t *val;
	*val=0;
	;
	if(num.getValue(val)){
		dbSerialPrintln("value  get");
	}else
		dbSerialPrintln("value NOT get");
	i=i+1;
	num.setValue(*val);
}
void loop() {
	nexLoop(nex_listen_list);
}
