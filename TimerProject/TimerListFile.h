/*
 * TimerListFile.h
 *
 *  Created on: 11 Oca 2016
 *      Author: Alptug
 */

#ifndef TIMERLISTFILE_H_
#define TIMERLISTFILE_H_

#include "File.h"
#include "TimerItemList.h"
class TimerListFile: public File {
	public:
	TimerListFile(int baslamaAdresi):File(baslamaAdresi){

	}
	TimeItem readItemAt(int index){
		TimeItem it;
		unsigned int basAddr = this->getAdressForIndex(index);
		it.isAktif = this->readByte(basAddr);
		it.dk = this->readByte(basAddr+1);
		it.saat = this->readByte(basAddr+2);
		it.calismaSuresi = this->readByte(basAddr+3);
		return it;
	}

	void saveTimerList(TimerItemList l){
		for(uint8_t i=0; i<l.getListSize();i++)
			this->saveTimerItem(l.getItemAt(i),i);
	}
	void saveTimerItem(TimeItem it,unsigned int index){
		if(!this->hasData())
			this->writeHeader();

		this->saveTimeItemsAktiflik(it.isAktif,index);
		this->saveTimerItemsCalismaSuresi(it.calismaSuresi,index);
		this->saveTimerItemsDakika(it.dk,index);
		this->saveTimerItemsSaat(it.saat,index);
	}
	void saveTimerItemsDakika(uint8_t dakika,unsigned int index){
		unsigned int baseAddr = this->getAdressForIndex(index)+1;
		this->writeByte(baseAddr,dakika);
	}
	void saveTimerItemsSaat(uint8_t saat,unsigned int index){
		unsigned int baseAddr = this->getAdressForIndex(index)+2;
		this->writeByte(baseAddr,saat);
	}
	void saveTimerItemsCalismaSuresi(uint8_t calismaSuresi,unsigned int index){
		unsigned int baseAddr = this->getAdressForIndex(index)+3;
		this->writeByte(baseAddr,calismaSuresi);
	}
	void saveTimeItemsAktiflik(uint8_t isAktif,unsigned int index){
		unsigned int baseAddr = this->getAdressForIndex(index);
				this->writeByte(baseAddr,isAktif);
	}
	private:
	unsigned int getAdressForIndex(int index){
		return this->baslamaAdresi+index*4;
	}
};

#endif /* TIMERLISTFILE_H_ */
