/*
 * Timer.h
 *
 *  Created on: 11 Oca 2016
 *      Author: Alptug
 */

#ifndef TIMERITEMLIST_H_
#define TIMERITEMLIST_H_
#include "RTClib.h"
#define ARRAY_OUT_OF_SIZE 1
#define ARRAY_HAS_MULTIPLE_ITEM 2
struct TimeItem{
public:
	uint8_t dk;
	uint8_t saat;
	uint8_t calismaSuresi; //dakika olarak
	uint8_t isAktif;
	TimeItem(){
		this->dk=0;
		this->saat=0;
		this->calismaSuresi=0;
		this->isAktif=0;
	}
	int8_t compare(TimeItem other){
		if(this->saat>other.saat)
			return 1;
		if(this->saat<other.saat)
			return -1;
		if(this->dk>other.dk)
			return 1;
		if(this->dk<other.dk)
			return -1;
		return 0;
	}
};

class TimerItemList{
private:
	static const uint8_t size=8;
	TimeItem list[size];

public:

	uint8_t getListSize(){
		return size;
	}
	TimeItem getItemAt(uint8_t index){

		return list[index];
	}
	int8_t setItemAt(TimeItem it,uint8_t index){
		if(index<0 || index>size)
			return ARRAY_OUT_OF_SIZE;
		for(int i=0; i<size; i++)
			if(i!=index && list[i].compare(it)==0){
				list[index] = it;
				return ARRAY_HAS_MULTIPLE_ITEM;
			}
		list[index] = it;
		return 0;
	}
	int8_t getNextItem(DateTime now){
		int8_t index=-1;
		for(uint8_t i=0; i<size; i++){
			if(list[i].isAktif){
				if(index<0)
					index=i;
				else{
					if(list[index].compare(list[i])>0)
						index = i;
				}
			}
		}
		return index;
	}
};

#endif /* TIMERITEMLIST_H_ */
