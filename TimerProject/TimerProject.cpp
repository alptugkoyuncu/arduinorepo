// Do not remove the include below
#include "TimerProject.h"

TimerItemList l;
DateTime now;
RTC_DS3231 rtc;
//The setup function is called once at startup of the sketch
void setup()
{

	rtc.begin();

	now = rtc.now();
	l.getNextItem(now);
	TimerListFile file(0);
	if(file.hasData()){
		for(uint8_t i=0; i<l.getListSize();i++)
			l.setItemAt(file.readItemAt(i),i);
	}else{
		TimeItem it;
		for(uint8_t i=0; i<l.getListSize();i++)
					l.setItemAt(it,i);
		file.saveTimerList(l);
	}
// Add your initialization code here
}

// The loop function is called in an endless loop
void loop()
{
//Add your repeated code here
}
