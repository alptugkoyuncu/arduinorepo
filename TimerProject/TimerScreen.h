/*
 * TimerScreen.h
 *
 *  Created on: 11 Oca 2016
 *      Author: Alptug
 */

#ifndef TIMERSCREEN_H_
#define TIMERSCREEN_H_

#include <LiquidCrystal.h>
#include <LCDKeypad.h>

#define MAIN_SCREEN 0
#define TIMER_SCREEN 1
class TimerScreen {
	private:
	LiquidCrystal lcd;
	uint8_t screenPos;
	uint8_t rowPos=0;
	uint8_t columnPos=0;
	//static  const int adc_key_val[5] = { 50, 200, 400, 600, 800 };
			int NUM_KEYS = 5;
			int adc_key_in;
			int key = -1;
			int oldkey = -1;
public:
	TimerScreen(){
    		 lcd = LiquidCrystal(8, 13, 9, 4, 5, 6, 7);
    		lcd.begin(16, 2);
    		lcd.clear();
    		lcd.setCursor(0, 0);
     }
	void setMainScreen(){
		this->screenPos = 0;
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("Main Screen");
		lcd.setCursor(0,1);
		lcd.print("Next Timer");
	}
	void setTimerScreen(TimerItemList l,int index){

	}
	void changeSceen(){
		switch(this->screenPos){
		case 0:
		}
	}
	// Convert ADC value to key number
	int get_key(unsigned int input) {
		int k;
		for (k = 0; k < NUM_KEYS; k++) {
			if (input < adc_key_val[k]) {
				return k;
			}
		}
		if (k >= NUM_KEYS)
			k = -1;  // No valid key pressed
		return k;
	}

};

void setup() {
	lcd.print("     helle! ");
	lcd.print("      welcome!");
	lcd.setCursor(0, 1);
	lcd.print("   LinkSprite");
	lcd.print("    LCD Shield");
	delay(1000);

	lcd.setCursor(0, 0);
	for (char k = 0; k < 26; k++) {
		lcd.scrollDisplayLeft();
		delay(400);
	}
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("ADC key testing");
}
void loop() {
	adc_key_in = analogRead(0);    // read the value from the sensor
	key = get_key(adc_key_in);  // convert into key press
	if (key != oldkey)   // if keypress is detected
			{
		delay(50);  // wait for debounce time
		adc_key_in = analogRead(0);    // read the value from the sensor
		key = get_key(adc_key_in);    // convert into key press
		if (key != oldkey) {
			lcd.setCursor(0, 1);
			oldkey = key;
			if (key >= 0) {
				lcd.print(msgs[key]);
			}
		}
	}
	delay(100);
}

#endif /* TIMERSCREEN_H_ */
