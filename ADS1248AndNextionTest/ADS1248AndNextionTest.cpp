// Do not remove the include below
#include "ADS1248AndNextionTest.h"

#define SERIAL_DEBUG_ENABLE
//#define NEXTION_ENABLE
int START = 8;
int CS = 7;
int DRDY = 2;
#define PGA DOR3_5
//Define which ADC to use in the ads12xx.h file

ads12xx ADS;
//The setup function is called once at startup of the sketch
void setup() {
#ifdef NEXTION_ENABLE
	nexInit();
#else
#ifdef SERIAL_DEBUG_ENABLE
	Serial.begin(115200);
	while (!Serial) {

	}
#endif

#endif

	Serial.println("Serial online");
	pinMode(20, OUTPUT);
	digitalWrite(20, HIGH); //Force other Chip into CS high
	ADS.begin(CS, START, DRDY);  //initialize ADS as object of the ads12xx class

	ADS.Reset();

	ADS.SetRegisterValue(MUX0, MUX_SP2_AIN5 | MUX_SN2_AIN7);
	ADS.SetRegisterValue(VBIAS, VBIAS_RESET);
	ADS.SetRegisterValue(MUX1, REFSELT1_REF1 | VREFCON1_ON); //ADS Reference on REF1, Internal Reference on
	ADS.SetRegisterValue(SYS0, PGA2_0 | DOR3_5);
	ADS.SetRegisterValue(IDAC0, IMAG2_250);			 //	IDAC at 0,250mA current
	ADS.SetRegisterValue(IDAC1, I1DIR_AIN1|I2DIR_OFF);		// IDAC Currentsink on AIN1
	ADS.SetRegisterValue(GPIOCFG, 0);
	ADS.SetRegisterValue(GPIODIR, 0);
	ADS.SetRegisterValue(GPIODAT, 0);

#ifdef SERIAL_DEBUG_ENABLE
	Serial.println("Setup ends");
#endif
// Add your initialization code here
}

// The loop function is called in an endless loop
void loop() {
	unsigned long volt_val = ADS.GetConversion();
	Serial.println(volt_val, DEC);

	double voltage = (1.54 / (16777216 / 2)) * volt_val;
	double ohm = voltage / (0.0015 * 8);
	Serial.print("Resistance:");
	Serial.print(ohm, DEC);
	Serial.println("Ohm");
//Add your repeated code here
}
