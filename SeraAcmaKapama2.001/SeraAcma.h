/*
 * SeraAcma.h
 *
 *  Created on: 24 Tem 2015
 *      Author: WIN7
 */

#ifndef SERAACMA_H_
#define SERAACMA_H_

#include "Arduino.h"
#include"BasicTime.h"
#include <DS3231.h>
#include <Wire.h>
#include"My_Debug.h"
#include"LimitSwitches/SwitchSystem.h"

#define ACILIYOR 1
#define KAPANIYOR 2
#define DURUYOR 0

#define ACMA_KAPAMA_GECIS_SURESI 500

#define DAY_TIME_IN_SECOND 60*60*24;

struct DailyTimer {
	BasicTime *baslangic;
	uint16_t beklenenCalismaSuresi;
	DailyTimer(uint8_t saat, uint8_t dk, uint8_t sn, uint16_t sure) {
		this->baslangic = new BasicTime(saat, dk, sn);
		this->beklenenCalismaSuresi = sure;
	}
	DailyTimer(DS3231 *Clock, uint16_t sure) {
		this->beklenenCalismaSuresi = sure;
		bool a, b;
		this->baslangic = new BasicTime(Clock->getHour(a, b),
				Clock->getMinute(), Clock->getSecond());
	}
	uint16_t getCalisilanSure(DS3231 *Clock) {
		bool a, b;
		uint16_t h = Clock->getHour(a, b);
		uint16_t m = Clock->getMinute();
		uint16_t s = Clock->getSecond();
		uint16_t borc = 0;
		if (s < this->baslangic->sn) {
			//dakikadan borc al saniyeye yukle
			borc = 1;
			s += 60;
		}
		s = s - this->baslangic->sn;
		if ((m == 0 && borc == 1) || (m - borc) < this->baslangic->dk) {
			//saatten borc al dakikaya yukle saniyenin borcunu cikart
			m = m + 60 - borc;
			borc = 1;
		} else {
			m = m - borc;
			borc = 0;
		}
		m = m - this->baslangic->dk;
		if ((h == 0 && borc == 1) || (h - borc) < this->baslangic->saat) {
			//gunden borc al saate yukle dakikanin borcunu cikart
			h = h + 24 - borc;
			borc = 1;
		} else {
			h = h - borc;
			borc = 0;
		}
		h = h - this->baslangic->saat;
		uint16_t sure = (h * 60 * 60 + m * 60 + s);
//		Serial.print("calisma Suresi : ");
//		Serial.println(sure);
		return sure;
	}

};

enum DurmaOption {
	DO_WITH_SWITCH, DO_WITH_TIMER, DO_WITH_TIMER_OR_SWITCH, DO_NO_LIMIT
};
class SeraAcma {
private:
	DurmaOption durmaOpt;
	uint16_t maximumAciklikMiktari;
	const static uint16_t minimumAciklikMiktari = 0;
	uint16_t aciklikMiktari;
	uint16_t istenilenMiktar;
	uint8_t acmaPini;
	uint8_t kapamaPini;
	uint8_t calismaDurumu;
	DS3231 *Clock;
	DailyTimer *timer;
	uint16_t lastAciklikMiktari; //calismaya baslamadan onceki aciklik miktari
	void init(uint16_t maximumAciklikMiktari, uint16_t aciklikMiktari,
			uint8_t acmaPini, uint8_t kapamaPini, DS3231 *Clock);
	bool ac(uint16_t sure);
	bool kapat(uint16_t sure);
	void saatiAyarla(uint16_t);
	void setAciklikMiktari();
	void switchWorking(uint16_t calimaSuresi);
	uint8_t calistirWithTimer(uint16_t istenilenMiktar);
public:
	void setDurmaOption(DurmaOption opt);
	SwitchSystem swt;
	SeraAcma(uint16_t maximumAciklikMiktari, uint8_t acmaPini,
			uint8_t kapamaPini, DS3231 *Clock);
	SeraAcma(uint16_t maximumAciklikMiktari, uint16_t aciklikMiktari,
			uint8_t acmaPini, uint8_t kapamaPini, DS3231 *Clock);
	void setMaximumAciklikMiktari(uint16_t mam) {
		this->maximumAciklikMiktari = mam;
	}
	uint16_t getMaximumAciklikMiktari();
	uint16_t getMinimumAciklikMiktari();
	uint8_t calistir(uint16_t istenilenMiktar, DurmaOption opt);
	uint16_t durdur(); //calisma suresini dondurur// true hemen kapatildigi zamanda false zamansiz
	uint8_t getCalismaDurumu();
	uint16_t getAciklikMiktari();
	uint16_t getIstenilenMiktar();
	bool isWorking();
	bool stopIfEnough();
	virtual ~SeraAcma();
	char *toString(int16_t &size);
};

#endif /* SERAACMA_H_ */
