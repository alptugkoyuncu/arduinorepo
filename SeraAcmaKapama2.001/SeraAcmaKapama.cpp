// Do not remove the include below
#include "SeraAcmaKapama.h"
//The setup function is called once at startup of the sketch
#define ACTION_GECERSIZ_KOMUT 0

#define ACTION_SENT_SYSTEM_ELEMENTS 1

//sera acma kapama komutlari
#define ACTION_HEMEN_DURDUR 11
#define ACTION_TAM_AC 12
#define ACTION_TAM_KAPAT 13
#define ACTION_CALISTIR 14
#define ACTION_GET_SERA_ACMA_INFO 15
#define ACTION_SET_MAXIMUM_ACIKLIK_MIKTARI 16
#define ACTION_SENT_MAXIMUM_ACIKLIK_MIKTARI 17

//sera bilgilerini dosyalama komutlari
#define ACTION_LISTEDEN_SIL 21
#define ACTION_LISTEDEN_SIL2 22
#define ACTION_LISTEYE_EKLE 23
#define ACTION_LISTEYI_GUNCELLE 24
#define ACTION_RESET_TIMER_LIST_AND_FILE 25
#define ACTION_GET_TIMER_LIST_SIZE 26
#define ACTION_GET_TIMER_LIST_ITEM_AT 27
#define ACTION_LOAD_TIMER_LIST 28
#define ACTIN_PRINT_TIMER_LIST 29
//saat ve memory ayarlama komutlari
#define ACTION_INITIALIZE_CLOCK 31
#define ACTION_PRINT_TIME 32
#define ACTION_SENT_MEMORY_INFO 33
#define ACTION_GET_TIME 34
#define ACTION_SEND_MAC_ADDRESS 35

#define ACTION_SET_LIMIT_SWITCH_ACIKLIK_MIKTARI 41
#define ACTION_SENT_LIMIT_SWITCH_ACIKLIK_MIKTARI 42
#define ACTION_RESET_K_LIMIT_SWITCH_SURE_FARKI 43
#define ACTION_SENT_SWITCH_SYSTEM_INFO 44
#define ACTION_SENT_SWITCH_GROUP_INFO 45

#define ACTION_RESET_ESP 91
#define ACTION_ESP_STARTED 92
#define ACTION_PING_TO_SERVER 98
#define ACTION_PING_FROM_SERVER 99

#define WAIT_TO_SAVE 1000
#define WAIT_TO_SEND 10000

#define ACMA_PINI 5
#define KAPAMA_PINI 6
#define MAXIMUM_ACIKLIK_MIKTARI 240

#define ESP_RESET_PIN 8

//struct Data{
//	const byte header;
//	const byte action;
//	const byte data[100];
//	const byte size;
//	Data(byte data[],int size){
//		int ayirac1=0; // : icin ayirac
//		int ayirac2=0; // bosluk icin ayirac
//		for(int i=0; i<size; i++){
//			if(data[i])
//		}
//		header = 12;
//		action=33;
//	}
//};
//TODO:
//#define ACTION_SENT_MEMORY_INFO HEM SERIAL(BAGLANTI VAR SA ) A HEM DE WIRELESS A GONDER sadece kalan memory i gonder duruma gore restart at

//simdiki zamanla listedeki zamanlar kiyaslanirken(siradaki acma kapamayi yapmak icin)
//loop icindeki sureden dolayi item kacirilabilir
//bunun onlemek icin aralik degeri koyuldu deger saniye cinsinden
//#define ARALIK 3

int maxMemory = 0;
int minMemory = 32000;
DS3231 Clock;
SeraAcma *sa;
TimerList timerList;
AcmaKapamaFile *fileAcmaKapama;
TimerListFile *fileTimerList;
uint8_t errNo;
unsigned long cur1, cur2;
//WifiConnection *conn;
#define WIFI_SENT_TRY 1
bool isPingSent = false;
unsigned long pingSentTime;
#define PING_SENT_INTERVAL 600000
#define PING_REPLY_INTERVAL 20000
bool isSerialAvaible = true;
bool isWirelessAvaible = false;
void resetTimerList();
void printTimerList();
DataPackage pck(RECEIVED);

#define wlConn Serial1
#define SERIAL_UART 115200
uint8_t w_fails = 0;
//HardwareSerial wlConn = Serial2;

long getBasicTimeDiffer(BasicTime t1, BasicTime t2) {
	long val1 = t1.saat * 60 * 60 + t1.dk * 60 + t1.sn;
	long val2 = t2.saat * 60 * 60 + t2.dk * 60 + t2.sn;
	return val2 - val1;
}
//saniye olarak farki dondurur
long getTimeDifferFromNextTimerItem() {
	bool a, b;
#define NEXT_DAY 60*60*24
	BasicTime t(Clock.getHour(a, b), Clock.getMinute(), Clock.getSecond());
	long minFark = NEXT_DAY;
	int minIndex = -1;
	for (int i = 0; i < timerList.size; i++) {
		if (timerList.list[i].isActive) {
			BasicTime t2 = timerList.list[i].baslama;
			long fark = getBasicTimeDiffer(t, t2);
			if (fark == 0)
				return 0;
			if (fark < 0)
				fark += NEXT_DAY;
			if (minFark > fark) {
				minFark = fark;
				minIndex = i;
			}
		}
	}
	return minFark;
}

void printMemory() {
#ifdef LOG
	char buffer[100];
	int size;
	int mem = getFreeRam();
	if (mem > maxMemory)
	maxMemory = mem;
	if (mem < minMemory) {
		minMemory = mem;
	}
//	size_my_dbg_buf =
	sprintf(buffer, "Memory : %d  Min : %d Max : %d ", mem, minMemory,
			maxMemory);
	Serial.println(buffer);
#endif
}
//uint16_t getKayitliAciklikMiktari();
//uint16_t getKayitliIstenilenMiktar();
//void seraAcmaBilgileriniKaydet();

void setup() {
// Add your initialization code here
// Start the I2C interface

	if (isSerialAvaible)
		Serial.begin(115200);
	w_fails = 0;
	pinMode(ESP_RESET_PIN, OUTPUT);
	action_reset_esp();
//#ifdef DEBUG

//Serial.println("setup start");

	delay(5000);
//#endif

	akto.set(0);
	printMemory();
	Wire.begin();
#ifdef DEBUG
	DEBUG_S(" file acma kapama : ");
#endif
	fileAcmaKapama = new AcmaKapamaFile(0);
#ifdef DEBUG
	DEBUG_S("timer list file : ");
#endif
	fileTimerList = new TimerListFile(10);
//	pck = new ReceivedPackage();

//		fileAcmaKapama->resetAll();
//	fileTimerList->resetAll();
//	delay(100000);

	Clock.setClockMode(false);

	uint16_t aciklikMiktari = 0;
	uint16_t istenileMiktar = 0;
	if (fileAcmaKapama->hasData()) {
#ifdef DEBUG
		DEBUG_S("file acma data var");
#endif
		aciklikMiktari = fileAcmaKapama->getKayitliAciklikMiktari();
		istenileMiktar = fileAcmaKapama->getKayitliIstenilenMiktar();
#ifdef DEBUG
//		char ch[20];
//		int s =
		sprintf(my_dbg_buf, "aciklik Mik: %d Istenilen Mik: %d", aciklikMiktari,
				istenileMiktar);
//		ch[s] = NULL;
		DEBUG_S(my_dbg_buf);
#endif
	}
	if (fileTimerList->hasData()) {
#ifdef DEBUG
		DEBUG_S("File timer list has data");
#endif
		timerList = fileTimerList->readTimerListData();
	}
	sa = new SeraAcma(MAXIMUM_ACIKLIK_MIKTARI, aciklikMiktari, ACMA_PINI,
	KAPAMA_PINI, &Clock);

//	conn = new CC3300_BOARD(CT_DATA_SERA);
//	conn->setWaitms(60000);
//	conn->completeConnect();

	printTimerList();

	action_sent_memory_info();
	//Serial.print("istenilenMiktar ");
	//Serial.println(istenileMiktar);
	sa->calistir(istenileMiktar);
	//eger sera calisiyorsa yarim saniye de bir veriyi dosyaya kaydet
	cur1 = millis();
	cur2 = millis();
	while (sa->isWorking()) {
		if (isSerialAvaible && Serial.available())
			communicateWithSerial();
		//calisan elemanin durmasi gerekiyorsa durdur
		if (wlConn.available()) {
			byte dt = wlConn.read();
			bool flag = pck.setData(dt);
			if (flag == BITIS_AYIRACI) {
				if (pck.size != 0)
					doActionCommands();
				pck.init();
			}
			if (flag == OVERLAPPED) {
				//DO NOTHING
				//Overlapped oldugunda verileri yedek bi yere kaydedecegiz bunun icin memory de kalan yeri belirleyecegiz loop icinde
				//kullanilan ve minimum memory limitini asmayacak sekilde sirayla yer acacagiz . Mesela bi kuyruk tasarlanip son gelen eleman
				// en sona eklenip bastaki eleman baska bi yerde yedeklenebilir
			}
		}
		if (sa->stopIfEnough() != false) {
			printTimerList();
			//			action_sent_memory_info();
			//sera islemi durdugunda verileri kaydet
			fileAcmaKapama->kaydetAciklikMiktari(sa->getAciklikMiktari());
			action_sent_sera_acma_info();
		} else {
			if ((millis() - cur1) > WAIT_TO_SAVE) {
				cur1 = millis();
				action_sent_sera_acma_info();
				fileAcmaKapama->kaydetAciklikMiktari(sa->getAciklikMiktari());
			}
		}
	}
//	if (!Clock.oscillatorCheck()) {
//		action_request_time();
//#ifdef LOG
//		LOG_S("Oscilator hatasi : ");
//#endif
//	}
#ifdef DEBUG
	DEBUG_S("SETUP DONE!");
#endif
}

void checkWireless() {
	if (isPingSent) {
		if (millis() - pingSentTime > PING_REPLY_INTERVAL) {
			action_reset_esp();
			isPingSent = false;
			pingSentTime = millis();
			return;
		}
	} else {
		if (millis() - pingSentTime > PING_SENT_INTERVAL) {
			action_send_ping_server();
			pingSentTime = millis();
			isPingSent = true;
		}
	}

}
// The loop function is called in an endless loop
void loop() {
//Add your repeated code here

	checkWireless();
//	unsigned long fark=millis();
//	Serial.println("loop baslangici");
//	Serial.println(millis());
	if (sa->isWorking())
		akto.set(750);
	else
		akto.set(7500);
	if (isSerialAvaible && Serial.available()) {
//		Serial.println("serial avaible");
		communicateWithSerial();
	}
	/**
	 * eski kod
	 *
	 *
	 if (false && !conn->hasProblem()&& conn->isAvaible()) {
	 Serial.println("receive");
	 uint8_t receivedData[50];
	 for (int i = 0; i < 50; i++)
	 receivedData[i] = NULL;

	 uint32_t size = conn->receive(receivedData, 50, 50);
	 if (size > 0 && receivedData != NULL) {
	 //		Serial.print("Data Received : ");
	 //		Serial.print((char *) receivedData);
	 //		Serial.print("   size : ");
	 //		Serial.println(size);
	 receivedData[size] = NULL;
	 char c;
	 for (int i = 0; i < size && (c = receivedData[i]) != NULL; i++) {
	 ////			if (c != NULL) {
	 uint8_t flag2 = pck->setData(c);
	 //			if (flag2 == 3) {
	 //				Serial.print("OK ");
	 //			}
	 //			Serial.println("");
	 if (pck->isFinished) {
	 //				Serial.print("Data Finished : ");
	 //				Serial.println(pck->data);
	 doActionCommands();
	 pck->init();
	 }
	 //			}
	 }
	 }
	 }
	 */
	while (wlConn.available() && !akto.isTimeOver()) {
		byte dt = wlConn.read();
		bool flag = pck.setData(dt);
		if (pck.isFinished) {
			doActionCommands();
			pck.init();
		}
		if (flag == OVERLAPPED) {
			//DO NOTHING
			//Overlapped oldugunda verileri yedek bi yere kaydedecegiz bunun icin memory de kalan yeri belirleyecegiz loop icinde
			//kullanilan ve minimum memory limitini asmayacak sekilde sirayla yer acacagiz . Mesela bi kuyruk tasarlanip son gelen eleman
			// en sona eklenip bastaki eleman baska bi yerde yedeklenebilir
		}
	}
//Serial.println("is sa wworking");
	//eger sera calisiyorsa yarim saniye de bir veriyi dosyaya kaydet
	if (sa->isWorking()) {
		//calisan elemanin durmasi gerekiyorsa durdur
		if (sa->stopIfEnough() != false) {
			printTimerList();
//			action_sent_memory_info();
			//sera islemi durdugunda verileri kaydet
			fileAcmaKapama->kaydetAciklikMiktari(sa->getAciklikMiktari());
			action_sent_sera_acma_info();
		} else {
			if ((millis() - cur1) > WAIT_TO_SAVE) {
				cur1 = millis();
				action_sent_sera_acma_info();
				fileAcmaKapama->kaydetAciklikMiktari(sa->getAciklikMiktari());

			}
		}

	} else {
//		Serial.println("Mac sent Basarili2222 : ");
//		delay(20);
		// yeni gun geldiyse diye islem baslatiliyor
//		updateListOnDateStart();
		// listeden siradaki elemani cekmek islemi yapiliyor
		if (!Clock.oscillatorCheck()) {
			//Serial.println("Oscilator hatasi");
			action_request_time();
#ifdef LOG
			LOG_S("Oscilator hatasi : ");
#endif
		}
		if (millis() - cur2 > WAIT_TO_SEND) {
			//Serial.println("Mac sent Basarili3333333 : ");
			int16_t size;
			size = 0;
			char * buff = sa->toString(size);
#ifdef DEBUG
//			char cch[100];
//			int cchs =
			sprintf("Sera Acma bilgileri : %s", buff);
//			cch[cchs] = NULL;
			DEBUG_S(my_dbg_buf);
#endif
			if (size <= 0) {
#ifdef LOG
				LOG_S("sera acma bilgileri hatasi : size 0 veya daha kucuk");
#endif
			}
			cur2 = millis();
			action_sent_sera_acma_info();

		}
		bool a, b;

		uint8_t saat, dakika, saniye;
		saat = Clock.getHour(a, b);
		dakika = Clock.getMinute();
		saniye = Clock.getSecond();
		BasicTime *t1 = new BasicTime(saat, dakika, saniye);

		//en yakin olan saati getir
		// eger 1 dk dan fazla zaman varsa interneti kontrol edip tekrar baglanti denenebilir
		ListItem *temp = NULL;

#ifdef ARALIK
		saniye+=ARALIK;
		BasicTime *t2 = new BasicTime(saat,dakika,saniye);
		temp = getNextTimerListItem(t1,t2);
		free(t2);
		t2= NULL;
#else
//		Serial.println("before get next timer");
		temp = getNextTimerListItem(t1);
#endif
		//calismaya baslayinca kaydet
		if (temp != NULL) {
			sa->calistir(temp->aciklikMiktari);
			fileAcmaKapama->seraAcmaBilgileriniKaydet(*sa);
			action_sent_sera_acma_info();
			cur1 = millis();
#ifdef DEBUG
//			char tt[30];
//			int tts =
			sprintf(my_dbg_buf, "Listeden Secildi : %d-->",
					temp->aciklikMiktari);
//			tt[tts] = NULL;
			DEBUG_S(my_dbg_buf);
			printMemory();
#endif
		} else {

//			if (conn->hasProblem() && t > 120) {
//				conn->setWaitms(90000);
//				conn->autoSolveProblem();
//			}

		}

		//	free(temp);
		temp = NULL;
		free(t1);
		t1 = NULL;
	}
	if (!sa->isWorking() && !akto.isTimeOver()) {
		long t = akto.getTimeLeft() < 500 ? akto.getTimeLeft() : 500;
		delay(t);
	}

}

//int8_t action_send_mac_address() {
//#ifdef DEBUG
//	DEBUG_S("send mac address");
//#endif
//	int8_t flag = NO_ERROR;
//	int act = ACTION_SEND_MAC_ADDRESS;
//	int size;
//	char buffer[100];
//	if (sa->isWorking()) {
//		flag = ERR_SERA_IS_WORKING;
//		size = sprintf(buffer, "%d %d", act, flag);
//	} else {
//		uint8_t macAddress[6];
//		conn.getMacAddress(macAddress);
//		size = sprintf(buffer, "%d %d:%d %d %d %d %d %d", act, flag,
//				macAddress[0], macAddress[1], macAddress[2], macAddress[3],
//				macAddress[4], macAddress[5]);
//	}
//
//	buffer[size] = NULL;
//	if (isSerialAvaible)
//		Serial.println(buffer);
//	if (isWirelessAvaible) {
//		sendToWireless(WIFI_SENT_TRY, buffer, size);
//	}
//	return flag;
//}

//TimerList readTimerListData() {
//	Serial.println("Reading data from eeprom : ");
//	TimerList l;
//	fileTimerList->resetSeek();
//	byte size = fileTimerList->readNextByte();
//	ListItem *temp;
//	for (int i = 0; i < size; i++) {
//		temp = readNextItemData();
//		if (temp != NULL)
//			l.add(temp);
//	}
//	return l;
//}
//void writeTimerListData() {
//	Serial.println("Writing Data to eeprom");
//	fileTimerList->resetAll();
//	fileTimerList->writeHeader();
//	if (timerList.size < 0)
//		timerList.size = 0;
//	byte size = timerList.size;
//	fileTimerList->writeNextByte(size);
//	if (timerList.size <= 0) {
//		return;
//	}
//	for (int i = 0; i < size; i++)
//		writeItemData(timerList.list[i]);
//}
//ListItem *readNextItemData() {
//	ListItem *item = new ListItem();
//	byte h = fileTimerList->readNextByte();
//	byte m = fileTimerList->readNextByte();
//	byte s = fileTimerList->readNextByte();
//	uint16_t value = fileTimerList->readNextShort();
//	byte isActive = fileTimerList->readNextByte();
//	if (h < 0 || h > 23 || m < 0 || m > 59 || s < 0 || s > 59)
//		return NULL;
//	item->aciklikMiktari = value;
//	item->baslama = new BasicTime(h, m, s);
//	item->isActive = isActive;
//	return item;
//}
//void writeItemData(const ListItem item) {
//	BasicTime *t = item.baslama;
//	fileTimerList->writeNextByte(t->saat);
//	fileTimerList->writeNextByte(t->dk);
//	fileTimerList->writeNextByte(t->sn);
//	uint16_t value = item.aciklikMiktari;
//	fileTimerList->writeNextShort(value);
//	fileTimerList->writeNextByte(item.isActive);
//}
//uint16_t getKayitliAciklikMiktari() {
//	return fileAcmaKapama->readNextShort();
//}
//uint16_t getKayitliIstenilenMiktar() {
//	return fileAcmaKapama->readNextShort();
//}
//void seraAcmaBilgileriniKaydet() {
//	fileAcmaKapama->resetAll();
//	fileAcmaKapama->writeHeader();
//	uint16_t miktar = sa->getAciklikMiktari();
//	fileAcmaKapama->writeNextShort(miktar);
//	fileAcmaKapama->writeNextShort(sa->getIstenilenMiktar());
//}
void communicateWithSerial() {

	long action = Serial.parseInt();
#ifdef DEBUG
//	char ch[50];
//	int ch_size;
//	size_my_dbg_buf =
	sprintf(my_dbg_buf, "Gelen Data : %d", action);
//	ch[ch_size] = NULL;
	DEBUG_S(my_dbg_buf);
#endif
	switch (action) {
	case ACTION_HEMEN_DURDUR:
		action_hemen_durdur();
		break;
	case ACTION_TAM_AC:
		action_tam_ac();
		break;
	case ACTION_TAM_KAPAT:
		action_tam_kapat();
		break;
	case ACTION_CALISTIR:
		uint16_t sure;
		sure = sa->getAciklikMiktari();
		while (true) {
			sure = Serial.parseInt();
			break;
		}
		action_calistir(sure);
		break;
	case ACTION_GET_SERA_ACMA_INFO: {
		action_sent_sera_acma_info();
		break;
	}
	case ACTION_SET_MAXIMUM_ACIKLIK_MIKTARI: {
		uint16_t miktar = MAXIMUM_ACIKLIK_MIKTARI;
		while (true) {
			miktar = Serial.parseInt();
			break;
		}
		sa->setMaximumAciklikMiktari(miktar);
		break;
	}
	case ACTION_SENT_MAXIMUM_ACIKLIK_MIKTARI: {
		action_sent_maximum_aciklik_miktari();
		break;
	}
	case ACTION_LISTEDEN_SIL:
		uint8_t deleteIndex;
		while (true) {
			deleteIndex = Serial.parseInt();
			break;
		}
		action_listeden_sil(deleteIndex);
		printTimerList();
		action_sent_memory_info();
		break;
	case ACTION_LISTEDEN_SIL2:
		while (true) {
			uint8_t h1 = Serial.parseInt();
			uint8_t m1 = Serial.parseInt();
			uint8_t s1 = Serial.parseInt();
			action_listeden_sil2(h1, m1, s1);
			break;
		}
		printTimerList();
		break;
	case ACTION_LISTEYE_EKLE:
		while (true) {
			uint8_t isActive = Serial.parseInt();
			uint8_t h = Serial.parseInt();
			uint8_t m = Serial.parseInt();
			uint8_t s = Serial.parseInt();
			uint16_t miktar = Serial.parseInt();
			action_listeye_ekle(isActive, h, m, s, miktar);
			break;
		}

		printTimerList();
		action_sent_memory_info();
		break;
	case ACTION_LISTEYI_GUNCELLE:
		while (true) {
			uint8_t pos = Serial.parseInt();
			uint8_t isActive = Serial.parseInt();
			uint8_t h = Serial.parseInt();
			uint8_t m = Serial.parseInt();
			uint8_t s = Serial.parseInt();
			uint16_t miktar = Serial.parseInt();
			action_listeyi_guncelle(pos, isActive, h, m, s, miktar);
			break;
		}

		printTimerList();
		break;
	case ACTION_RESET_TIMER_LIST_AND_FILE:
		action_reset_timer_list_and_file();
		printTimerList();
		break;
	case ACTION_GET_TIMER_LIST_SIZE:
		action_get_timer_list_size();
		printTimerList();
		break;
	case ACTION_GET_TIMER_LIST_ITEM_AT: {
		uint8_t index = Serial.parseInt();
		action_get_timer_list_item_at(index);
		break;
	}
	case ACTION_LOAD_TIMER_LIST:
		timerList = fileTimerList->readTimerListData();
		printTimerList();
		action_load_timer_list();
		break;
	case ACTIN_PRINT_TIMER_LIST:
		printTimerList();
		break;

	case ACTION_INITIALIZE_CLOCK:
		uint8_t y, m, d, dow, h, min, s;
		while (true) {
			y = Serial.parseInt();
			m = Serial.parseInt();
			d = Serial.parseInt();
			dow = Serial.parseInt();
			h = Serial.parseInt();
			min = Serial.parseInt();
			s = Serial.parseInt();
			break;
		}
		action_initialize_clock(y, m, d, dow, h, min, s);

		break;
	case ACTION_PRINT_TIME:
		action_print_time();
		break;
	case ACTION_SENT_MEMORY_INFO:
		action_sent_memory_info();
		break;
	case ACTION_GET_TIME:
		action_request_time();
		break;
	case ACTION_SEND_MAC_ADDRESS:
//		action_send_mac_address();
		break;
	case ACTION_SENT_SYSTEM_ELEMENTS:
		action_sent_system_elements();
		break;
	case ACTION_SET_LIMIT_SWITCH_ACIKLIK_MIKTARI: {
		Serial.println("bu action daha yapilmadi");
		break;
	}
	case ACTION_SENT_LIMIT_SWITCH_ACIKLIK_MIKTARI: {
		action_sent_limit_switch_aciklik_miktarlar();
		break;
	}
	case ACTION_RESET_K_LIMIT_SWITCH_SURE_FARKI: {
		sa->swt.resetKapanmaSwitches();
		break;
	}
	case ACTION_RESET_ESP: {
		action_reset_esp();
		break;
	}
	case ACTION_ESP_STARTED: {
		action_start_esp();
		break;
	}
	case ACTION_PING_TO_SERVER: {
		action_send_ping_server();
		break;
	}
	case ACTION_PING_FROM_SERVER: {
		action_receive_ping_server();
		break;
	}
	default:
		action_gecersiz_komut();
		break;
	}
}

void doActionCommands() {
	//Serial.print("Received Data : ");
	//Serial.println((char *)pck.data);
	//parse the package
	const char headerParser = ':';
	const char araParser = ' ';
	uint8_t headerParserCount = 0;
	uint8_t araParserCount = 0;
	//header0 verinin geldigi baglantiyi veriri
	//header1 komutu ve komut sonucunu verir basarili veya hata sonucunu
	//header2 valuelari veriri

	char sendertype[4];
	uint8_t senderTypeCount = 0;
	char actionCommand[4];
	uint8_t actionCommanSize = 0;
	char hataKodu[4];
	uint8_t hataKoduSize = 0;
	char values[50];
	uint8_t valueCount = 0;
	for (int i = 0; i < pck.size; i++) {
		char next = pck.data[i];
		if (next == headerParser) {
			headerParserCount++;
			araParserCount = 0;
		} else if (next == araParser)
			araParserCount++;
		else {
			if (headerParserCount == 0)
				sendertype[senderTypeCount++] = next;
			else if (headerParserCount == 1) {
				if (araParserCount == 0)
					actionCommand[actionCommanSize++] = next;
				else if (araParserCount == 1)
					hataKodu[hataKoduSize++] = next;
				else {
#ifdef LOG
					LOG_S("HATA ARA PARSELDE");
#endif
				}
			} else if (headerParserCount == 2) {
				for (int j = i; j < pck.size; j++)
					values[valueCount++] = pck.data[j];
				break;
			} else {
#ifdef LOG
				LOG_S("HATA HEADER PARSER DE");
#endif
			}
		}

	}
	/*
	 if (senderTypeCount < 0) {
	 Serial.println("senderype header de sorun");
	 }
	 if (actionCommanSize < 0) {
	 Serial.println("action command size da sorun");
	 }
	 if (hataKoduSize < 0)
	 Serial.println("hata kodu size da sorun");
	 if (valueCount < 0)
	 Serial.println("value count da  sorun");
	 */

	if (senderTypeCount < 0 || actionCommanSize < 0 || hataKoduSize < 0
			|| valueCount < 0)
		return;

	sendertype[senderTypeCount] = NULL;
	actionCommand[actionCommanSize] = NULL;
	hataKodu[hataKoduSize] = NULL;
	if (valueCount > 0)
		values[valueCount] = NULL;
#ifdef DEBUG
	DEBUG_S("buraya kadar geldik");
#endif
	uint8_t sender = senderTypeCount > 0 ? atoi(sendertype) : 0;
	uint8_t actCmd = actionCommanSize > 0 ? atoi(actionCommand) : 0;
	uint8_t hata_kodu = hataKoduSize > 0 ? atoi(hataKodu) : 0;

	char ch[50];
	int ch_size;
	if (valueCount > 0)
		//	ch_size=
		sprintf(ch, "sender : %d\tact: %d hata: %d value: %s", sender, actCmd,
				hata_kodu, values);
	else
		//	ch_size =
		sprintf(ch, "sender : %d\tact: %d hata: %d value:", sender, actCmd,
				hata_kodu);
	Serial.println(ch);

	switch (actCmd) {
	case ACTION_HEMEN_DURDUR:
		action_hemen_durdur();
		break;
	case ACTION_TAM_AC:
		action_tam_ac();
		break;
	case ACTION_TAM_KAPAT:
		action_tam_kapat();
		break;
	case ACTION_CALISTIR: {
		uint16_t miktar = atoi(values);
		action_calistir(miktar);
		break;
	}
	case ACTION_GET_SERA_ACMA_INFO:
		action_sent_sera_acma_info();
		break;
	case ACTION_SET_MAXIMUM_ACIKLIK_MIKTARI: {
		uint16_t miktar = atoi(values);
		sa->setMaximumAciklikMiktari(miktar);
		break;
	}
	case ACTION_SENT_MAXIMUM_ACIKLIK_MIKTARI: {
		action_sent_maximum_aciklik_miktari();
		break;
	}
//sera bilgilerini dosyalama komutlari
	case ACTION_LISTEDEN_SIL: {
		uint8_t index = atoi(values);
		action_listeden_sil(index);
		printTimerList();
		break;
	}
	case ACTION_LISTEDEN_SIL2: {
		uint8_t h;
		char c_hour[3];
		uint8_t chcount = 0;

		uint8_t m;
		char c_minute[3];
		uint8_t cmcount = 0;

		uint8_t s;
		char c_sec[3];
		uint8_t cseccount = 0;

		araParserCount = 0;

		for (int i = 0; i < valueCount && values[i] != NULL; i++) {
			char dt = values[i];
			if (dt == araParser)
				araParserCount++;
			else {
				if (araParserCount == 0)
					c_hour[chcount++] = dt;
				else if (araParserCount == 1)
					c_minute[cmcount++] = dt;
				else if (araParserCount == 2)
					c_sec[cseccount++] = dt;
				else {
#ifdef LOG
					LOG_S(
							"ACTION LISTEDEN_SIL2 ARA PARSER COUNT HATASI");
#endif
				}
			}
		}
		if ((chcount <= 0 || chcount > 2) || (cmcount <= 0 || cmcount > 2)
				|| (cseccount <= 0 || cseccount > 2)) {
#ifdef LOG
			LOG_S("ACTION LISTEDEN SIL2 GENEL PARSER HATASI");
#endif
			return;
		}
		c_hour[chcount] = NULL;
		c_minute[cmcount] = NULL;
		c_sec[cseccount] = NULL;
		h = atoi(c_hour);
		m = atoi(c_minute);
		s = atoi(c_sec);

		action_listeden_sil2(h, m, s);
		printTimerList();
		break;
	}

	case ACTION_LISTEYE_EKLE: {
		araParserCount = 0;
		uint8_t isActive;

		uint8_t h;
		char c_hour[3];
		uint8_t chcount = 0;

		uint8_t m;
		char c_minute[3];
		uint8_t cmcount = 0;

		uint8_t s;
		char c_sec[3];
		uint8_t cseccount = 0;

		uint16_t miktar;
		char c_miktar[6];
		uint8_t cmiktarcount = 0;

		int iactive = values[0] - '0';
		isActive = iactive <= 0 ? 0 : 1;
//		araParserCount++;
		char dt;
		for (int i = 1; i < valueCount && (dt = values[i]) != NULL; i++) {
			if (dt == araParser)
				araParserCount++;
			else {
				if (araParserCount == 1)
					c_hour[chcount++] = dt;
				else if (araParserCount == 2)
					c_minute[cmcount++] = dt;
				else if (araParserCount == 3)
					c_sec[cseccount++] = dt;
				else if (araParserCount == 4)
					c_miktar[cmiktarcount++] = dt;
				else {
#ifdef LOG
					LOG_S(
							"ACTION LISTEYE_EKLE ARA PARSER COUNT HATASI");
#endif
				}
			}
		}
		if ((chcount <= 0 || chcount > 2) || (cmcount <= 0 || cmcount > 2)
				|| (cseccount <= 0 || cseccount > 2) || cmiktarcount <= 0) {
#ifdef LOG
			LOG_S("ACTION LISTEYE_EKLE GENEL PARSER HATASI");
#endif
			return;
		}
		c_hour[chcount] = NULL;
		c_minute[cmcount] = NULL;
		c_sec[cseccount] = NULL;
		c_miktar[cmiktarcount] = NULL;
		h = atoi(c_hour);
		m = atoi(c_minute);
		s = atoi(c_sec);
		miktar = atoi(c_miktar);

		action_listeye_ekle(isActive, h, m, s, miktar);
		printTimerList();
		break;
	}
	case ACTION_LISTEYI_GUNCELLE: {
		araParserCount = 0;

		uint8_t pos;
		char c_pos[4];
		uint8_t cposcount = 0;

		uint8_t isActive;

		uint8_t h;
		char c_hour[3];
		uint8_t chcount = 0;

		uint8_t m;
		char c_minute[3];
		uint8_t cmcount = 0;

		uint8_t s;
		char c_sec[3];
		uint8_t cseccount = 0;

		uint16_t miktar;
		char c_miktar[6];
		uint8_t cmiktarcount = 0;

		bool lastIsAraParser = false;
		for (int i = 0; i < valueCount && values[i] != NULL; i++) {
			char dt = values[i];
			if (lastIsAraParser && dt == araParser)
				continue;
			if (dt == araParser) {
				araParserCount++;
				lastIsAraParser = true;
			} else {
				lastIsAraParser = false;
				if (araParserCount == 0)
					c_pos[cposcount++] = dt;
				else if (araParserCount == 1)
					isActive = dt - '0';
				else if (araParserCount == 2)
					c_hour[chcount++] = dt;
				else if (araParserCount == 3)
					c_minute[cmcount++] = dt;
				else if (araParserCount == 4)
					c_sec[cseccount++] = dt;
				else if (araParserCount == 5)
					c_miktar[cmiktarcount++] = dt;
				else {

					Serial.println(
							"ACTION LISTEYI_GUNCELLE ARA PARSER COUNT HATASI");

				}
			}
		}
		if ((cposcount <= 0 || cposcount > 3) || (chcount <= 0 || chcount > 2)
				|| (cmcount <= 0 || cmcount > 2)
				|| (cseccount <= 0 || cseccount > 2) || cmiktarcount <= 0) {

			Serial.println("ACTION LISTEYI_GUNCELLE GENEL PARSER HATASI");

			return;
		}
		c_pos[cposcount] = NULL;
		c_hour[chcount] = NULL;
		c_minute[cmcount] = NULL;
		c_sec[cseccount] = NULL;
		c_miktar[cmiktarcount] = NULL;

		pos = atoi(c_pos);
		h = atoi(c_hour);
		m = atoi(c_minute);
		s = atoi(c_sec);
		miktar = atoi(c_miktar);

		action_listeyi_guncelle(pos, isActive, h, m, s, miktar);

		printTimerList();
		break;
	}
	case ACTION_RESET_TIMER_LIST_AND_FILE:
		action_reset_timer_list_and_file();

		printTimerList();
		break;
	case ACTION_GET_TIMER_LIST_SIZE:
		action_get_timer_list_size();

		printTimerList();
		break;

	case ACTION_GET_TIMER_LIST_ITEM_AT: {
		uint8_t index = atoi(values);
		action_get_timer_list_item_at(index);
		break;
	}
	case ACTION_LOAD_TIMER_LIST:
		timerList = fileTimerList->readTimerListData();
		action_load_timer_list();
		printTimerList();
		break;

	case ACTIN_PRINT_TIMER_LIST:
		printTimerList();
		break;
//saat ve memory ayarlama komutlari
	case ACTION_INITIALIZE_CLOCK: {

		char c_y[3];
		uint8_t c_ycount = 0;

		char c_month[3];
		uint8_t c_monthcount = 0;

		char c_d[3];
		uint8_t c_dcount = 0;
		// Sets the Day of the Week (1-7);
		int idow;

		char c_h[3];
		uint8_t c_hcount = 0;

		char c_min[3];
		uint8_t c_mincount = 0;

		char c_s[3];
		uint8_t c_scount = 0;

		araParserCount = 0;
		char dt;
		for (int i = 0; i < valueCount && (dt = values[i]) != NULL; i++) {
			if (dt == araParser)
				araParserCount++;
			else {
				if (araParserCount == 0)
					c_y[c_ycount++] = dt;
				else if (araParserCount == 1)
					c_month[c_monthcount++] = dt;
				else if (araParserCount == 2)
					c_d[c_dcount++] = dt;
				else if (araParserCount == 3)
					idow = dt - '0';
				else if (araParserCount == 4)
					c_h[c_hcount++] = dt;
				else if (araParserCount == 5)
					c_min[c_mincount++] = dt;
				else if (araParserCount == 6)
					c_s[c_scount++] = dt;
				else {
#ifdef LOG
					LOG_S(
							"ACTION_INITIALIZE_CLOCK ARA PARSER COUNT HATASI");
#endif
				}
			}
		}
		if ((c_ycount <= 0 || c_ycount > 2)
				|| (c_monthcount <= 0 || c_monthcount > 2)
				|| (c_dcount <= 0 || c_dcount > 2) || (idow < 1 || idow > 7)
				|| (c_hcount < 0 || c_hcount > 2)
				|| (c_mincount < 0 || c_mincount > 2)
				|| (c_scount < 0 || c_scount > 2)) {
#ifdef LOG
			LOG_S("ACTION_INITIALIZE_CLOCK GENEL PARSER HATASI");
#endif
			return;
		}
		c_y[c_ycount] = NULL;
		c_month[c_monthcount] = NULL;
		c_d[c_dcount] = NULL;
		c_h[c_hcount] = NULL;
		c_min[c_mincount] = NULL;
		c_s[c_scount] = NULL;

		uint8_t y = atoi(c_y), m = atoi(c_month), d = atoi(c_d), dow = idow, h =
				atoi(c_h), min = atoi(c_min), s = atoi(c_s);

		action_initialize_clock(y, m, d, dow, h, min, s);
		break;
	}

	case ACTION_PRINT_TIME:
		action_print_time();
		break;
	case ACTION_SENT_MEMORY_INFO:
		action_sent_memory_info();
		break;
	case ACTION_GET_TIME:
		action_request_time();
		break;
	case ACTION_SEND_MAC_ADDRESS:
//		action_send_mac_address();
		break;
	case ACTION_SENT_SYSTEM_ELEMENTS:
		action_sent_system_elements();
		break;
	case ACTION_SET_LIMIT_SWITCH_ACIKLIK_MIKTARI: {
		const char esitlikParser = '=';
		char temp[10];
		uint8_t tempIndex = 0;
		uint8_t type[5];
		uint8_t typeIndex = 0;
		uint16_t miktar[5];
		uint8_t miktarIndex = 0;
		int i = 0;
		while (values[i] == araParser && i < valueCount)
			i++;
		for (; i < valueCount; i++) {
			char c = values[i];
			if (c == araParser) {
				int j = i + 1;
				if (j < valueCount && values[j] == araParser)
					continue;
				temp[tempIndex] = NULL;
				miktar[miktarIndex] = atoi(temp);
				tempIndex = 0;
			} else if (c == esitlikParser) {
				temp[tempIndex] = NULL;
				type[typeIndex] = atoi(temp);
				tempIndex = 0;
			} else if (c >= '0' && c <= '9') {
				temp[tempIndex++] = c;
			} else {

			}
		}
		if (typeIndex == miktarIndex) {

			enum SwitchType t;

			for (int i = 0; i < typeIndex; i++) {
				switch (type[i]) {
				case SWT_ACMA:
					sa->swt.setSwitchAciklikMiktari(SWT_ACMA, miktar[i]);
					break;
				case SWT_KAPAMA:
					sa->swt.setSwitchAciklikMiktari(SWT_KAPAMA, miktar[i]);
					break;
				default:
					break;
				}
			}
			action_sent_limit_switch_aciklik_miktarlar();
		} else {
			//hata kodu gonder
		}
		break;
	}
	case ACTION_SENT_LIMIT_SWITCH_ACIKLIK_MIKTARI: {
		action_sent_limit_switch_aciklik_miktarlar();
		break;
	}
	case ACTION_RESET_K_LIMIT_SWITCH_SURE_FARKI: {
		sa->swt.resetKapanmaSwitches();
		break;
	}
	case ACTION_SENT_SWITCH_SYSTEM_INFO: {
		action_sent_switch_system_info();
		break;
	}
	case ACTION_SENT_SWITCH_GROUP_INFO:{
		int val = atoi(values);
		action_sent_switch_group_info(val);
		break;
	}
	case ACTION_RESET_ESP: {
		action_reset_esp();
		break;
	}
	case ACTION_ESP_STARTED: {
		action_start_esp();
		break;
	}
	case ACTION_PING_TO_SERVER: {
		action_send_ping_server();
		break;
	}
	case ACTION_PING_FROM_SERVER: {
		action_receive_ping_server();
		break;
	}
	case ACTION_GECERSIZ_KOMUT:
	default:
		action_gecersiz_komut();
		break;
	}

}
int8_t action_request_time() {
#ifdef DEBUG
	DEBUG_S("request time");
#endif
	int8_t flag = NO_ERROR;
	int act = ACTION_GET_TIME;
	int size;
	char buffer[100];
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
	}
	size = sprintf(buffer, "%d %d", act, flag);
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible) {
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	}
	return flag;
}
int8_t action_hemen_durdur() {
	int8_t flag = NO_ERROR;
	sa->durdur();
	fileAcmaKapama->seraAcmaBilgileriniKaydet(*sa);
	int16_t size1, size;
	size1 = 0;
	char buff[512];
	int act = ACTION_HEMEN_DURDUR;
	size = sprintf(buff, "%d %d:%s", act, flag, sa->toString(size1));
	buff[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buff);
	if (isWirelessAvaible) {
		sendToWireless(WIFI_SENT_TRY, buff, size);
	}
	return flag;
}
int8_t action_tam_ac() {
	int8_t flag = NO_ERROR;
	sa->calistir(sa->getMaximumAciklikMiktari());
	fileAcmaKapama->seraAcmaBilgileriniKaydet(*sa);
	int16_t size1, size;
	size1 = 0;
	char buff[512];
	int act = ACTION_TAM_AC;
	size = sprintf(buff, "%d %d:%s", act, flag, sa->toString(size1));
	buff[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buff);
	if (isWirelessAvaible) {
		sendToWireless(WIFI_SENT_TRY, buff, size);
	}
	return flag;
}
int8_t action_tam_kapat() {

	int8_t flag = NO_ERROR;
	sa->calistir(sa->getMinimumAciklikMiktari());
	fileAcmaKapama->seraAcmaBilgileriniKaydet(*sa);
	int16_t size1, size;
	size1 = 0;
	char buff[512];
	int act = ACTION_TAM_KAPAT;
	size = sprintf(buff, "%d %d:%s", act, flag, sa->toString(size1));
	buff[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buff);
	if (isWirelessAvaible) {
		sendToWireless(WIFI_SENT_TRY, buff, size);
	}
	return flag;
}
int8_t action_calistir(uint16_t sure) {

	int8_t flag = NO_ERROR;
	sa->calistir(sure);
	fileAcmaKapama->seraAcmaBilgileriniKaydet(*sa);
	int16_t size1, size;
	size1 = 0;
	char buff[512];
	int act = ACTION_CALISTIR;
	size = sprintf(buff, "%d %d:%s", act, flag, sa->toString(size1));
	buff[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buff);
	if (isWirelessAvaible) {
		sendToWireless(WIFI_SENT_TRY, buff, size);
	}
	return flag;
}
int8_t action_sent_sera_acma_info() {
	int8_t flag = NO_ERROR;
	int16_t size1, size;
	size1 = 0;
	char buff[512];
	int act = ACTION_GET_SERA_ACMA_INFO;
	size = sprintf(buff, "%d %d:%s", act, flag, sa->toString(size1));
	buff[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buff);
	if (isWirelessAvaible) {
		sendToWireless(WIFI_SENT_TRY, buff, size);
	}
	return flag;
}
int8_t action_listeden_sil(int8_t index) {
	int8_t flag = NO_ERROR;
	int act = ACTION_LISTEDEN_SIL;
	int size;
	char buffer[100];
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		ListItem li;
		int8_t err = timerList.remove(index, li);
		if (err < 0) {
			flag = ERR_LIST_ITEM_NOT_FOUND;
			size = sprintf(buffer, "%d %d", act, flag);
		} else {
			fileTimerList->removeItem(timerList, index);

			int8_t isActive = li.isActive == false ? 0 : 1;
			size = sprintf(buffer, "%d %d:%d %d %d %d %d %d", act, flag, index,
					isActive, li.baslama.saat, li.baslama.dk, li.baslama.sn,
					li.aciklikMiktari);

		}
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);

	return flag;
}
int8_t action_listeden_sil2(uint8_t saat, uint8_t dk, uint8_t sn) {
	BasicTime t(saat, dk, sn);
	char buffer[100];
	int size;
	int act = ACTION_LISTEDEN_SIL2;
	int8_t flag = NO_ERROR;
	ListItem li;
	BasicTime time;
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		int8_t index = timerList.hasItemAtTime(t);
		if (index < 0) {
			flag = ERR_LIST_ITEM_NOT_FOUND;
			size = sprintf(buffer, "%d %d", act, flag);
		} else {
			int8_t err = timerList.remove(index, li);
			if (err < 0) {
				flag = ERR_LIST_ITEM_NOT_FOUND;
				size = sprintf(buffer, "%d %d", act, flag);
			} else {
				fileTimerList->removeItem(timerList, index);
				time = li.baslama;
				int8_t isActive = li.isActive == false ? 0 : 1;
				size = sprintf(buffer, "%d %d:%d %d %d %d %d %d", act, flag,
						index, isActive, time.saat, time.dk, time.sn,
						li.aciklikMiktari);

			}
		}
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);

	return flag;
}
int8_t action_listeye_ekle(uint8_t isActive, uint8_t h, uint8_t m, uint8_t s,
		uint16_t miktar) {
	int8_t flag = NO_ERROR;
	char buffer[100];
	int size;
	int act = ACTION_LISTEYE_EKLE;
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		BasicTime t1(h, m, s);
		if (timerList.hasItemAtTime(t1) >= 0) {
			flag = ERR_LIST_HAS_ITEM;
			size = sprintf(buffer, "%d %d", act, flag);
		} else {
//			bool a, b;
//			BasicTime t2(Clock.getHour(a, b), Clock.getMinute(),
//					Clock.getSecond());
//			bool isDone = true;
//			if (t1.compareTo(&t2) < 0)
//				isDone = false;
//			int8_t pos = timerList.add(isActive, h, m, s, miktar, isDone);

			int8_t pos = timerList.add(isActive, h, m, s, miktar);
			if (pos == -1) {
				flag = ERR_SERA_IS_WORKING;
				size = sprintf(buffer, "%d %d", act, flag);
			} else {
				fileTimerList->addItem(timerList, pos);
				int active = timerList.list[pos].isActive == false ? 0 : 1;
				size = sprintf(buffer, "%d %d:%d %d %d %d %d %d", act, flag,
						pos, active, h, m, s, miktar);
			}
		}
	}

	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_listeyi_guncelle(int8_t pos, uint8_t isActive, uint8_t h,
		uint8_t m, uint8_t s, uint16_t miktar) {

	int8_t flag = NO_ERROR;
	char buffer[100];
	sprintf(buffer, "guncelleme oncesi : %d. %d %d %d %d %d", pos, isActive, h,
			m, s, miktar);
	Serial.println(buffer);
	int size;
	int act = ACTION_LISTEYI_GUNCELLE;
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		if (pos >= timerList.size || pos < 0) {
			flag = ERR_ARRAY_OUT_OF_RANGE;
			size = sprintf(buffer, "%d %d", act, flag);
		} else {
//			BasicTime t1(h, m, s);
//			bool h12 = false, PM = false;
//			BasicTime t2(Clock.getHour(h12, PM), Clock.getMinute(),
//					Clock.getSecond());

//			bool isDone = true;
//			if (t1.compareTo(&t2) > 0)
//				isDone = false;
//			int8_t newPos = (timerList.update(pos, isActive, h, m, s, miktar,
//					isDone));
			int8_t newPos = (timerList.update(pos, isActive, h, m, s, miktar));
			if (newPos < 0) {
				flag = ERR_GENERAL;
				size = sprintf(buffer, "%d %d", act, flag);
			} else {
				uint8_t start, end;
				if (pos > newPos) {
					start = newPos;
					end = pos;
				} else {
					start = pos;
					end = newPos;
				}
				for (int i = start; i <= end; i++)
					fileTimerList->updateItemAt(timerList, i);
				flag = NO_ERROR;
				int active = isActive == 0 ? 0 : 1;
				size = sprintf(buffer, "%d %d:%d %d %d %d %d %d %d", act, flag,
						pos, newPos, active, h, m, s, miktar);
			}
		}
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_reset_timer_list_and_file() {
	int8_t flag = NO_ERROR;
	char buffer[100];
	int size;
	int act = ACTION_RESET_TIMER_LIST_AND_FILE;

	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		flag = NO_ERROR;
		size = sprintf(buffer, "%d %d", act, flag);
		resetTimerList();
		fileTimerList->resetAll();
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}

int8_t action_initialize_clock(byte year, byte month, byte date, byte dow,
		byte h, byte m, byte s) {

	int8_t flag = NO_ERROR;
//	char buffer[100];
//	int size;
//	int act = ACTION_INITIALIZE_CLOCK;
#ifdef LOG
//	char buffer[100];
//	size_my_dbg_buf =
	sprintf(my_dbg_buf, "Tarih : %d %d %d %d %d %d %d", year, month, date, dow,
			h, m, s);
	LOG_S(my_dbg_buf);
#endif
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
//		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		Clock.setSecond(s); //Set the second
		Clock.setMinute(m); //Set the minute
		Clock.setHour(h);  //Set the hour
		Clock.setDoW(dow);    //Set the day of the week
		Clock.setDate(date);  //Set the date of the month
		Clock.setMonth(month);  //Set the month of the year
		Clock.setYear(year);  //Set the year (Last two digits of the year)
//		updateListOnClockChange();
//		flag = NO_ERROR;
//		size = sprintf(buffer, "%d %d", act, flag);

	}
//	buffer[size] = NULL;
//	if (isSerialAvaible)
//		Serial.println(buffer);
//	if (isWirelessAvaible)
//		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_get_timer_list_size() {

	const int BUFFER_SIZE = 512;

	int8_t flag = NO_ERROR;
	char buffer[BUFFER_SIZE];
	int size;
	int act = ACTION_GET_TIMER_LIST_SIZE;

	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		int totalSize = 0;
		uint8_t itemIndex = 0;
		size = sprintf(buffer, "%d %d:%d", act, flag, timerList.size);

//bufferi gonder
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_get_timer_list_item_at(int index) {
	int8_t flag = NO_ERROR;
	char buffer[100];
	int size;
	int act = ACTION_GET_TIMER_LIST_ITEM_AT;
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		if (index >= timerList.size && index < 0) {
			flag = ERR_ARRAY_OUT_OF_RANGE;
			size = sprintf(buffer, "%d %d", act, flag);
		} else {
			ListItem lt = timerList.list[index];
			BasicTime t = lt.baslama;
			int active = lt.isActive == false ? 0 : 1;
			size = sprintf(buffer, "%d %d:%d %d %d %d %d %d", act, flag, index,
					active, t.saat, t.dk, t.sn, lt.aciklikMiktari);
		}
	}

	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_print_time() {

	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_PRINT_TIME;

	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		bool a, b;
		int size = sprintf(buffer, "%d %d: %d %d %d", act, flag,
				Clock.getHour(a, b), Clock.getMinute(), Clock.getSecond());
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	isWirelessAvaible = false;
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	isWirelessAvaible = true;
	return flag;
}

int8_t action_sent_memory_info() {
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_SENT_MEMORY_INFO;

	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		int mem = freeMemory();
		if (mem > maxMemory)
			maxMemory = mem;
		if (mem < minMemory) {
			minMemory = mem;
		}
		size = sprintf(buffer, "%d %d:%d %d %d", act, flag, mem, minMemory,
				maxMemory);
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_send_ping_server() {
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_PING_TO_SERVER;

	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		size = sprintf(buffer, "%d %d", act, flag);
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_receive_ping_server() {
	isPingSent = false;
	return true;
}
int8_t action_gecersiz_komut() {
#ifndef HAS_GECERSIZ_KOMUT
	return 0;
#endif;
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_GECERSIZ_KOMUT;
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		size = sprintf(buffer, "%d %d", act, flag);
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_sent_limit_switch_aciklik_miktarlar() {
	int8_t flag = NO_ERROR;
	char buffer[100];
	uint8_t bufferIndex = 0;
	int act = ACTION_SENT_LIMIT_SWITCH_ACIKLIK_MIKTARI;
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		bufferIndex = sprintf(buffer, "%d %d", act, flag);
	} else {
		bufferIndex += sprintf(buffer, "%d %d", act, flag);
		SwitchGroup *grp;
		for (int i = 0; i < sa->swt.getGroupSize(); i++) {
			grp = sa->swt.getSwitchGroupByIndex(i);
			SwitchType type = grp->getType();
			uint16_t miktar = grp->getAciklikMiktari();
			if (i == 0)
				bufferIndex += sprintf(buffer + bufferIndex, "%d=%d", type,
						miktar);
			else
				bufferIndex += sprintf(buffer + bufferIndex, " %d=%d", type,
						miktar);
		}
	}
	buffer[bufferIndex] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, bufferIndex);
	return flag;
}

void resetTimerList() {
#ifdef LOG
	LOG_S("Resetting timer list");
#endif
	while (timerList.size > 0) {

		timerList.remove(0);
		delay(10);
#ifdef LOG
		LOG_S("timer listten eleman siliniyor");
#endif
	}
	timerList.size = 0;

}
void printTimerList() {

	if (timerList.size <= 0) {
		Serial.println("NO ITEM");
		return;
	}
	char item[50];
	for (int i = 0; i < timerList.size; i++) {
//		char ch[100];
		listItemToCharArray(item, timerList.list[i]);
		sprintf(my_dbg_buf, "<%d>\t%s", i, item);
		Serial.println(my_dbg_buf);
//		free(item);
//		item=NULL;
	}
}

ListItem * getNextTimerListItem(BasicTime * t1, BasicTime * t2) {
	if (t2 == NULL) {
		for (int i = 0; i < timerList.size; i++) {

			if ((timerList.list[i].isActive)
					&& (t1->compareTo(timerList.list[i].baslama)) == 0)
				return &timerList.list[i];
		}
	} else {
		for (int i = 0; i < timerList.size; i++) {
			if (timerList.list[i].isActive
					&& t1->compareTo(timerList.list[i].baslama) >= 0
					&& t2->compareTo(timerList.list[i].baslama) >= 0)
				return &timerList.list[i];
		}
	}
	return NULL;
}
/*void updateListOnClockChange() {
 bool a, b;
 BasicTime *t = new BasicTime(Clock.getHour(a, b), Clock.getMinute(),
 Clock.getSecond());
 for (int i = 0; i < timerList.size; i++) {
 if (timerList.list[i].baslama->compareTo(t) < 0) {
 timerList.list[i].isDoneToday = true;
 } else {
 timerList.list[i].isDoneToday = false;
 }
 timerList.resetDay = Clock.getDate();
 }
 free(t);
 t = NULL;
 }
 */
/*bool updateListOnDateStart() {
 if (timerList.resetDay == Clock.getDate())
 return false;
 Serial.println("vvvvvvvv");
 for (int i = 0; i < timerList.size; i++)
 timerList.list[i].isDoneToday = false;
 timerList.resetDay = Clock.getDate();
 return true;
 }
 */
int listItemToCharArray(char * buff, const ListItem item) {

	uint16_t m = item.aciklikMiktari;
//	char ch[30];
	int size = sprintf(buff, "(%c)\t%d:%d:%d\t%d", (item.isActive ? '+' : '-'),
			item.baslama.saat, item.baslama.dk, item.baslama.sn, m);
//	buff =(char *) malloc((size+1)*sizeof(char));
//	memcpy(buff,ch,size+1);
//	buff[size]=NULL;
//	free(ch);

	return size;
}

TimerList createDumpData() {
	TimerList timerList;
	bool h12, PM;
	ListItem item;
	byte dk = Clock.getMinute();

	for (int i = 0; i < 10; i++) {
		item.aciklikMiktari = 10 * (i + 1);
		++dk;
		dk = dk % 60;
		BasicTime t(Clock.getHour(h12, PM), dk, 0);
		item.baslama = t;
		uint16_t m = timerList.list[i].aciklikMiktari;
		item.isActive = true;
		timerList.add(item);
	}
	return timerList;
}
bool sendToWireless(int deneme, char *buff, int size) {
	int notry = 0;
	if (deneme <= 0)
		deneme = 1;
	while (!akto.isTimeOver()) {
		if (notry >= deneme)
			break;
		notry++;
		//Serial.print("try to send on wireless : ");
		buff[size] = NULL;
		char buff2[size + 2];
		int size2 = sprintf(buff2, "<%s>", buff);
		buff2[size2] = NULL;
		//Serial.println(buff2);
//		bool flag = conn->sendLine(buff2, size2);

		int s = wlConn.write((uint8_t *) buff2, size2);
//		bool flag = conn->sendLine(buff, size);
//		delay(1000);
		wlConn.flush();
		if (s == size2) {
			//Serial.println("Sent OK");
			w_fails = 0;
			return true;
		}
		long t = akto.getTimeLeft();
		if (t <= 0)
			break;
		if (t > 1000) {
			delay(500);
		} else {
			delay(100);
		}
	}
	//Serial.println("sent failed");
	w_fails++;
	if (w_fails >= 3) {
		//Serial.println("w fails hatasi");
		if (sa->isWorking()) {
			action_reset_esp();
		} else {
			long t = getTimeDifferFromNextTimerItem();
			action_reset_esp(t);
		}
		w_fails = 0;
	}
	return false;
}

uint8_t action_start_esp() {
	//Serial.println("Wireless avaible");
	isWirelessAvaible = true;
	isPingSent = false;
	pingSentTime = millis();
	return NO_ERROR;
}
uint8_t action_load_timer_list() {
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_LOAD_TIMER_LIST;

	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		size = sprintf(buffer, "%d %d", act, flag);
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
uint16_t action_sent_maximum_aciklik_miktari() {
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_SENT_MAXIMUM_ACIKLIK_MIKTARI;

	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		size = sprintf(buffer, "%d %d:%d", act, flag,
				sa->getMaximumAciklikMiktari());
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
uint16_t action_sent_system_elements() {
	int8_t flag = NO_ERROR;
	char buffer[250];
	int size;
	int act = ACTION_SENT_SYSTEM_ELEMENTS;
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		//t�mer b�lg�s�n� al
		String tmp1 = "tim1 ";
		// switch bilgilerini al
		sa
		tmp1 += tmp;
		size = sprintf(buffer, "%d %d:", act, flag);
		tmp1.toCharArray(&buffer[size], 250);
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
uint8_t action_sent_switch_system_info() {
	int8_t flag = NO_ERROR;
	char buffer[250];
	int size = 250;
	int act = ACTION_SENT_SWITCH_SYSTEM_INFO;
	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
		size = sprintf(buffer, "%d %d", act, flag);
	} else {
		sa->swt.getSystemInfo(buffer, size);
		size = sprintf(buffer, "%d %d:", act, flag);
		int size2 = 250 - size;
		sa->swt.getSystemInfo(buffer + size, size2);
		size += size2;
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
uint8_t action_sent_switch_group_info(int type) {
	int8_t flag = NO_ERROR;
	char buffer[250];
	int size = 250;
	int act = ACTION_SENT_SWITCH_GROUP_INFO;
	SwitchType t;
	switch (type) {
	case SWT_ACMA:
		t = SWT_ACMA;
		break;
	case SWT_KAPAMA:
		t = SWT_KAPAMA;
		break;
	default:
		flag = ERR_SWITCH_GROUP_NOT_FOUND;
		size = sprintf(buffer, "%d %d", act, flag);
	}

	if (sa->isWorking()) {
		flag = ERR_SERA_IS_WORKING;
	}
	if(flag!=NO_ERROR){
		size = sprintf(buffer, "%d %d", act, flag);
	}
	else {
		sa->swt.getSystemInfo(buffer, size);
		size = sprintf(buffer, "%d %d:", act, flag);
		int size2 = 250 - size;
		sa->swt.getGroupSystemInfo(buffer + size, size2,t);
		size += size2;
	}
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
void action_reset_esp(long t) {
	if (t < 550)
		t = 550;
	if (t > 3000)
		t = 3000;
	unsigned long dly = t < 3000 ? t - 500 : 3000;
	wlConn.end();
	digitalWrite(ESP_RESET_PIN, LOW);
	delay(dly);
	digitalWrite(ESP_RESET_PIN, HIGH);
	delay(50);
	wlConn.begin(SERIAL_UART);
	delay(50);
	while (wlConn.available())
		wlConn.read();
	isWirelessAvaible = false;
	//Serial.println("RESET ESP");
}

