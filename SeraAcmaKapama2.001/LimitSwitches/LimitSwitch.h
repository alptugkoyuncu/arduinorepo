/*
 * LimitSwitch.h
 *
 *  Created on: 22 Eki 2015
 *      Author: WIN7
 */

#ifndef LIMITSWITCH_H_
#define LIMITSWITCH_H_

#include"Arduino.h"
class LimitSwitch {
private:
	uint8_t pinNo;
protected:
	uint8_t place;
public:
	LimitSwitch() {
		this->pinNo = 0;
		this->place = 0;
	}
	LimitSwitch(uint8_t pinNo, uint8_t place) {
		this->pinNo = pinNo;
		this->place = place;
	}
	void initialize(uint8_t pinNo, uint8_t place) {
		this->pinNo = pinNo;
		this->place = place;
	}
	void setPinNo(uint8_t pin) {
		this->pinNo = pin;
	}
	uint8_t isClosed() {
		return digitalRead(this->pinNo);
	}
	void getSystemInfo(char *str,int &str_size){
		str_size=sprintf(str,"%d %d",this->place,this->isClosed());
		str[str_size]=NULL;
	}
};

class KapamaKontrolSwitch: public LimitSwitch {
private:
	int16_t sureFarki;
public:
	KapamaKontrolSwitch(uint8_t pin, uint8_t place, int16_t sureFarki = -1) :
			LimitSwitch(pin, place) {
		this->sureFarki = sureFarki;
	}
	/*void setSureFarki(unsigned long start){
	 unsigned long curr = millis();
	 if((signed long)(curr-start)<0){
	 signed long temp = (signed long)start;
	 this->sureFarki = curr-temp;
	 }else
	 this->sureFarki = (int16_t)(curr-start);
	 }
	 */
	int16_t getSureFarki() {
		return this->sureFarki;
	}
	void setSureFarki(int16_t fark) {
		this->sureFarki = fark;
	}
	void resetSureFarki() {
		this->sureFarki = -1;
	}
    void getSystemInfo(char *str,int &str_size){
		str_size = sprintf(str,"%d %d %d",this->place,this->isClosed(),this->getSureFarki());
		str[str_size]=NULL;
	}

};

#endif /* LIMITSWITCH_H_ */
