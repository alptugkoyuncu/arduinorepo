/*
 * SwitchSystem.cpp
 *
 *  Created on: 23 Eki 2015
 *      Author: WIN7
 */

#include "SwitchSystem.h"

SwitchSystem::SwitchSystem() {
	// TODO Auto-generated constructor stub
	this->groupSize = 0;
#if(KAPAMA_KONTROL_LIMIT_SWITCHES)
	this->groupSize++;
#endif
#if(ACMA_KONTROL_LIMIT_SWITCHES)
	this->groupSize++;
#endif
	this->groups = new SwitchGroup[2];
	int size = 0;
#if(KAPAMA_KONTROL_LIMIT_SWITCHES)
	KapamaKontrolSwitch ksw[NO_OF_KAPAMA_SWITCHES];
	ksw[0] = KapamaKontrolSwitch(LW_K1);
	ksw[1] = KapamaKontrolSwitch(LW_K2);
	ksw[2] = KapamaKontrolSwitch(LW_K3);
	ksw[3] = KapamaKontrolSwitch(LW_K4);
	ksw[4] = KapamaKontrolSwitch(LW_K5);
	ksw[5] = KapamaKontrolSwitch(LW_K6);
	ksw[6] = KapamaKontrolSwitch(LW_K7);
	ksw[7] = KapamaKontrolSwitch(LW_K8);
	ksw[8] = KapamaKontrolSwitch(LW_K9);
	ksw[9] = KapamaKontrolSwitch(LW_K10);
	this->groups[size++] = new KapamaSwitchGroup(
	DEFAULT_KAPAMA_SWITCH_ACIKLIK_MIKTARI, ksw, NO_OF_KAPAMA_SWITCHES);
#endif
#if(ACMA_KONTROL_LIMIT_SWITCHES)
	LimitSwitch asw[NO_OF_ACMA_SWITCHES];
	asw[0] = LimitSwitch(LW_A1);
	asw[1] = LimitSwitch(LW_A2);
	this->groups[size++] = new SwitchGroup(SWT_ACMA,
	DEFAULT_ACMA_SWITCH_ACIKLIK_MIKTARI, asw, NO_OF_ACMA_SWITCHES);
#endif
}

SwitchSystem::~SwitchSystem() {
	// TODO Auto-generated destructor stub
	delete[] groups;
}

void SwitchSystem::resetKapanmaSwitches() {
	for (int i = 0; i < this->groupSize; i++) {
		if (this->groups[i].getType() == SWT_KAPAMA) {
			(KapamaSwitchGroup(this->groups[i])).resetSwitchValues();
			return;
		}
	}
}

int16_t SwitchSystem::getAktifSwitchAciklikMiktari() {
	if (this->checkFatalError() != 0)
		return -1;
	for (int i = 0; i < this->groupSize; i++)
		if (this->groups[i].checkToStop())
			return this->groups[i].getAciklikMiktari();
	return -1;
}
bool SwitchSystem::checkToStop(int16_t istenilenMiktar) {
	bool flag;
	SwitchGroup * gr = this->getSwitchGroupByAciklikMiktari(istenilenMiktar);
	if (gr == NULL)
		return false;
	flag = gr->checkToStop();
	delete gr;
	return flag;
}

int16_t SwitchSystem::getSwitchAciklikMiktari(SwitchType type) {
	return this->getSwitchGroupByType(type)->getAciklikMiktari();
}

void SwitchSystem::getSystemInfo(char *str, int&size) {
	size = 0;
	if (this->groupSize == 0)
		return;
	for (int i = 0; i < this->groupSize; i++) {
		SwitchGroup *grp = this->getSwitchGroupByIndex(i);
		SwitchType type = grp->getType();
		int size = grp->switchSize;
		int aciklikMiktari = grp->getAciklikMiktari();
		if (i == 0) {
			size = sprintf(str, "{%d %d %d}", type,aciklikMiktari,size);
		}else
			size+=sprintf(str+size," {%d %d %d}", type,aciklikMiktari,size);
	}
	str[size]=NULL;
}

void SwitchSystem::setSwitchAciklikMiktari(SwitchType type, int16_t miktar) {
	this->getSwitchGroupByType(type)->setSwitchAciklikMiktari(miktar);
}

SwitchGroup* SwitchSystem::getSwitchGroupByAciklikMiktari(
		uint16_t aciklikMiktari) {
	for (int i = 0; i < this->groupSize; i++) {
		if (this->groups[i].getAciklikMiktari() == aciklikMiktari) {

			return &this->groups[i];
		}
	}
	return NULL;
}

SwitchGroup *SwitchSystem::getSwitchGroupByType(SwitchType enumSwitchType) {
	for (int i = 0; i < this->groupSize; i++) {
		if (this->groups[i].getType() == enumSwitchType) {
			return &this->groups[i];
		}
	}
	return NULL;
}

bool SwitchSystem::hasLimitSwitchAt(int16_t aciklikMiktari) {
	return this->getSwitchGroupByAciklikMiktari(aciklikMiktari) != NULL;

}

SwitchSystemErrors SwitchSystem::checkFatalError() {
	int errFlagIndex = 0;
	int targetIndex = -1;
	for (int i = 0; i < this->groupSize; i++) {
		if (this->groups[i].checkToStop()) {
			targetIndex = i;
			errFlagIndex++;
		}
	}
	if (errFlagIndex > 1)
		this->err = SSE_MULTIPLE_AKTIF_SG;
	else
		this->err = SSE_NO_ERROR;
	return this->err;
}

void SwitchSystem::getGroupSystemInfo(char* str, int& str_size, SwitchType type) {
	str_size=0;
	this->getSwitchGroupByType(type)->getSystemInfo(str,str_size);
}

SwitchGroup *SwitchSystem::getSwitchGroupByIndex(int index) {
	if (index < 0 || index > this->groupSize)
		return NULL;
	else
		return &this->groups[index];
}
