/*
 * TimerList.h
 *
 *  Created on: 27 Tem 2015
 *      Author: WIN7
 */

#ifndef TIMERLIST_H_
#define TIMERLIST_H_

#define LIST_SIZE 30
#define NULL 0
#include"BasicTime.h"
#include"My_Debug.h"

struct ListItem {
	BasicTime baslama;
	uint16_t aciklikMiktari;
	uint8_t isActive;
//	bool isDoneToday;
};

struct TimerList {
	ListItem list[LIST_SIZE];
	uint8_t size;
	uint8_t resetDay; // bugunun degerini alir eger yeni gun gelmisse ListItemin isDoneTodayleri false olur
	bool allReset;
	TimerList() {
		size = 0;
		resetDay = 0;
		allReset = true;
	}

	int8_t update(int8_t pos, ListItem newItem) {
		char buff[100];
		sprintf(buff,"Guncelle : %d.%d %d %d %d %d ",pos,newItem.isActive,newItem.baslama.saat,newItem.baslama.dk,newItem.baslama.sn,newItem.aciklikMiktari);
		Serial.println(buff);
		int8_t newPos;
		if (pos < 0 || pos > size) {
			return -1;
		}
		if (this->list[pos].baslama.compareTo(newItem.baslama) == 0) {
			this->list[pos].aciklikMiktari = newItem.aciklikMiktari;
			this->list[pos].isActive = newItem.isActive;
			newPos = pos;
		} else {
			this->remove(pos);
			newPos = this->add(newItem);
		}
		return newPos;
	}
	int8_t hasItemAtTime(BasicTime bt) {
		for (int8_t i = 0; i < this->size; i++) {
			BasicTime t2 = this->list[i].baslama;
			if ((t2.compareTo(bt)) == 0) {
				return i;
			}
		}
		return -1;
	}

	int8_t update(int8_t pos, uint8_t isActive, uint8_t h, uint8_t m, uint8_t s,
			uint16_t miktar/*, bool isDoneToday*/) {

		int8_t newPos;
		if (pos < 0 || pos > size) {
			return -1;
		}
		BasicTime t(h, m, s);
		ListItem tmp;
		tmp.baslama = t;
		tmp.aciklikMiktari = miktar;
		tmp.isActive = isActive == 0 ? 0 : 1;
		return this->update(pos, tmp);
	}

	int8_t add(uint8_t isActive, uint8_t h, uint8_t m, uint8_t s,
			uint16_t miktar/*, bool isDoneToday*/) {
#ifdef DEBUG
		DEBUG_S("In ADD");
#endif
		BasicTime t(h, m, s);
		if (this->hasItemAtTime(t) >= 0)
			return -1;
		ListItem l;
		if (isActive == false)
			l.isActive = false;
		else
			l.isActive = true;
		l.baslama = t;
		l.aciklikMiktari = miktar;
//		l.isDoneToday = isDoneToday;
		return add(l);
	}
	int8_t add(const ListItem l) {
		int8_t pos;
		if (this->hasItemAtTime(l.baslama) >= 0)
			return -1;
		if (size == LIST_SIZE)
			return -1;
		if (size < 0) {
			return -1;
		}else if(size==0){
			memcpy(&list[size++],&l,sizeof(ListItem));
			return 0;
		} else {
			BasicTime tmp = l.baslama;
			pos = size;
			for (uint8_t i = 0; i < size; i++) {
				if (tmp.compareTo(this->list[i].baslama) < 0) {
					pos = i;
					break;
				}
			}
			for (uint8_t i = size; i > pos; i--) {
				memcpy(&list[i],&list[i-1],sizeof(ListItem));
			}
			memcpy(&list[pos],&l,sizeof(ListItem));
			size++;
		}
#ifdef DEBUG
		DEBUG_S("Add Complete");
#endif
		return pos;
	}
	int8_t remove(uint8_t index, ListItem &removedItem) {
//		ListItem itm;
		if (index < 0 && index >= size) {
			return -1;
		}
		memcpy(&removedItem,&(list[index]),sizeof(ListItem));

		for (uint8_t i = index; i < size - 1; i++) {
			memcpy(&list[i],&list[i+1],sizeof(ListItem));
		}
//		free(removedItem->baslama);
//		free(removedItem);
		size--;
		return 0;
//		return &removedItem;
	}
	int8_t remove(uint8_t index) {
	//		ListItem itm;
			if (index < 0 && index >= size) {
				return -1;
			}

			for (uint8_t i = index; i < size - 1; i++) {
				list[i] = list[i + 1];
			}
	//		free(removedItem->baslama);
	//		free(removedItem);
			size--;
			return 0;
	//		return &removedItem;
		}
};

#endif /* TIMERLIST_H_ */
