/*
 * ESPSera.h
 *
 *  Created on: 6 Mar 2020
 *      Author: alptu
 */

#ifndef ESPSERA_H_
#define ESPSERA_H_



#include<ActionCode.h>
#include<MyDebug.h>
#include<DataPackage.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include<WifiClient.h>


void sentEspPosition(uint16_t actionCode);
void resetBoard();
uint8_t connectServer();
uint8_t connectWifi();
uint8_t sentHeader();
char* nexToken(char *str, uint8_t *lastIndex, uint8_t *length);
char* getParameter(uint16_t actionCode);
void fromShort(int16_t val, uint8_t *newVal, boolean isLittleEndian);
int16_t toShort(uint8_t *val, boolean isLittleEndian);

#endif /* ESPSERA_H_ */
