/**TODO:
 1-PING alma gonderme islemi iptal edildi.Bu islem' dolayli yoldan  arduino uzerinden yaapilacak
 2-board reset sonsuz while dongusu ile gerceklesiyor yapilabilirse daha duzenli bi reset islemi gerekli
 3-server ya da arduino ya gonderilen datalar gidip gitmedigi kontrol edilmiyor ileride daha bi duzenli data alisverisi kontrolu yapilmali
 4- room/client numbar islemi iptal edildi yine bu islem dolayli yoldan arduino uzerinden gerceklesecek.
 1. ve 4. maddelerdeki degisikleriden oturu artik esp guncellemesine gerek kalmayacak yukle esplerin hepsi herbir arduno uzerinden kontrol edilebilecek
 5-room client işlemi tekrar yerleÅŸtirildi.Arduino iÃ§inde loopta sÃ¼rekli kontrol etmek  manasÄ±z
 */

#include"ESPSera.h"

//INIT
char *ssid = NULL;
char *password = NULL;
IPAddress *ip;
uint16_t port = 0;
int16_t arduino_id = 0;

const int16_t REQUEST_ID_OK = 51;
//varÄ±able defÄ±nes

#define RS_NO_PROBLEM 0
#define RS_WIFI_CONN_OK 11
#define RS_TCP_CONN_OK 12
#define RS_WIFI_PROBLEM 21
#define RS_TCP_CONN_PROBLEM 22
#define RS_HEADER_PROBLEM 23
#define RS_DATA_NOT_COMPLETE 5
#define RS_SENT_PROBLEM 6

// servera oda numarasÄ±nÄ± gonderÄ±r basarÄ±lÄ± olursa loopa gÄ±rer
// basarÄ±sÄ±z durumda reset atar
#define MAX_FAIL 3
#define NO_TCP_TRY 10
#define NO_WIFI_TRY 30
unsigned long lastReceiveArduino, lastReceiveServer;
const unsigned long MAX_WAIT_MS = 300000; //5 dakika icinde data gelmezse resetler

//#define MAC_SERVER
//#define UNIX_SERVER

WiFiClient client;
uint8_t ERR_CODE;

DataPackage pck_recv, pck_sent;

void emptySerialData() {
	while (Serial.available() > 0)
		Serial.read();
}
void emptyClientData() {
	while (client.available() > 0)
		client.read();
}
////////////////////////// SETUP //////////////////////////////////////////////////////

void setup() {
	pck_recv.init();
	pck_sent.init();
	Serial.begin(38400);  //Start Serial
	Serial.flush();
	d_println("<ESP Started...>");
	sentEspPosition(AC_ESP_STARTED);
	yield();
	delay(100);

	uint8_t fails = 0;
	const unsigned long PARAMETER_WAIT_MS = 180000;
	bool wifiConnectionEstablished = false;
	bool serverConnectionEstablished = false;
	bool connectionEstablished = false;

	unsigned long start = millis();
	while (millis() - start < PARAMETER_WAIT_MS) {
		if (ssid == NULL) {
			ssid = getParameter(AC_SEND_WIFI_SSID);
			d_print("<WIFI : ");
			d_print(ssid);
			d_println(">");
			continue;
		}
		if (password == NULL) {
			password = getParameter(AC_SEND_PASS);
			d_print("<Pass :");
			d_print(password);
			d_println(">")
			continue;
		}
		// Start WiFi
		if (ssid != NULL && password != NULL && !wifiConnectionEstablished) {
			fails = 0;
			do {
				connectWifi();
				delay(50);
				yield();
				fails++;
			} while (fails < MAX_FAIL && ERR_CODE != RS_NO_PROBLEM);
			if (fails == MAX_FAIL) {
				sentEspPosition(AC_WIFI_CONNECTION_FAILED);
				wifiConnectionEstablished = false;
				serverConnectionEstablished = false;

				free(ssid);
				ssid = NULL;

				free(password);
				password = NULL;

				continue;
			} else {
				sentEspPosition(AC_WIFI_CONNECTED);
				wifiConnectionEstablished = true;
				start = millis();
			}
		}
		if (ip == NULL) {
			char *strIp = getParameter(AC_SEND_IP);
			if (strIp == NULL) {
				d_println("<StrIp NULL>");
				if (ip != NULL)
					delete ip;
			} else {
				d_print("<strIp :");
				d_print(strIp);
				d_println(">")
				ip = new IPAddress();
				ip->fromString(strIp);
				d_print("<IP :");
				d_print(*ip);
				d_println(">")
				free(strIp);
				strIp = NULL;
			}
			continue;
		}
		if (port == 0) {
			char *strPort = getParameter(AC_SEND_PORT);
			if (strPort == NULL) {
				d_println("<strPort NULL>");
				port = 0;
			} else {
				d_print("<strPort ");
				d_print(*strPort);
				d_println(">")
				port = atoi(strPort);
				free(strPort);
				strPort = NULL;
				d_print("<Port :");
				d_print(port);
				d_println(">");
			}
			continue;
		}

		//connect TCP SERVER
		if (wifiConnectionEstablished && ip != NULL && port != 0
				&& !serverConnectionEstablished) {
			fails = 0;
			do {
				connectServer();
				delay(50);
				yield();
				fails++;
			} while (fails < MAX_FAIL && ERR_CODE != RS_NO_PROBLEM);

			if (fails == MAX_FAIL) {
				sentEspPosition(AC_SERVER_CONNECTION_FAILED);
				serverConnectionEstablished = false;
				delete ip;
				ip = NULL;
				port = 0;

			} else {
				serverConnectionEstablished = true;
				sentEspPosition(AC_SERVER_CONNECTED);
				start = millis();
			}
			continue;
		}
		//send id
		if (serverConnectionEstablished) {
			if (arduino_id == 0) {
				char *strId = getParameter(AC_SEND_ID);
				if (strId == NULL) {
					d_println("<strId NULL>");
					arduino_id = 0;
				} else {
					d_print("<strId ");
					d_print(*strId);
					d_println(">")
					arduino_id = atoi(strId);
					d_print("<Arduino id :")
					d_print(arduino_id);
					d_println(">")
				}
				continue;
			} else {
				fails = 0;
				do {
					sentHeader();
					delay(50);
					yield();
					fails++;
				} while (fails < MAX_FAIL && ERR_CODE != RS_NO_PROBLEM);
				if (fails >= MAX_FAIL) {
					sentEspPosition(AC_ID_REFUSED);
					arduino_id = 0;
					continue;
				} else {
					sentEspPosition(AC_ID_ACCEPTED);
					fails = 0;
					connectionEstablished = true;
					break;
				}
			}
		}
	}
	if (!connectionEstablished) {
		resetBoard();
	}

	emptyClientData();
	emptySerialData();

	pck_recv.init();
	pck_sent.init();
	lastReceiveArduino = millis();
	lastReceiveServer = millis();

	d_println("<Setup Done!>");
}

////////////////////////// LOOP //////////////////////////////////////////////////////
byte loopData[100] = { 0 };
uint8_t loopDataSize = 0;
void loop() {
	loopDataSize = 0;
	while (Serial.available())
		loopData[loopDataSize++] = Serial.read();
	if (loopDataSize != 0) {
		client.write(loopData, loopDataSize);
		lastReceiveArduino = millis();
		loopDataSize = 0;
	}
	while (client.available()) {
		loopData[loopDataSize++] = client.read();
	}
	if (loopDataSize != 0) {
		Serial.write(loopData, loopDataSize);
		lastReceiveServer = millis();
		loopDataSize = 0;
	}
	if (client.available() > 0 || Serial.available() > 0)
		return;
	delay(5);
	yield();
	if ((millis() - lastReceiveArduino) > MAX_WAIT_MS
			|| (millis() - lastReceiveServer) > MAX_WAIT_MS) {
		d_println("<Timeout!>");
		resetBoard();
	}
	/*unsigned long serialWait = 0;

	 while (Serial.available() > 0) {
	 lastReceiveArduino = millis();
	 byte dt = Serial.read();
	 uint8_t flag = pck_sent.setData(dt);
	 if (flag == BITIS_AYIRACI) {
	 pck_sent.addServerEndOfLine();
	 size_t t = client.write((uint8_t*) pck_sent.data, pck_sent.size);
	 pck_sent.init();
	 break;
	 }
	 }
	 while (client.available() > 0) {
	 lastReceiveServer = millis();
	 byte dt = client.read();
	 uint8_t flag = pck_recv.setData(dt);

	 if (flag == BITIS_AYIRACI) {
	 size_t t = Serial.write((uint8_t*) pck_recv.data, pck_recv.size);
	 pck_recv.init();
	 break;
	 }
	 }*/

}
void fromShort(int16_t val, uint8_t *newVal, boolean isLittleEndian) {
// Little Endian
//        byte ret[2];
	if (isLittleEndian) {
		newVal[0] = (int8_t) val;
		newVal[1] = (int8_t) (val >> 8);
	} else {
		// Big Endian generally network conversion
		newVal[0] = (int8_t) (val >> 8);
		newVal[1] = (int8_t) val;
	}
//        return ret;
}
int16_t toShort(uint8_t *val, boolean isLittleEndian) {
	if (isLittleEndian) {
		return val[0] | val[1] << 8;
	} else
		return val[1] | val[0] << 8;
}

//siradaki tokeni alir fonk geriye dondurur. uzunluk degerini len e verir. tokenin str icindeki bitisini lastIndex'e kaydeder
//NOT: nexToken icin FONKSIYON DA memory de yer ayirttilir ve oraya kaydedilir. Fonksiyon diisndaki degiskenin isi bittiginde memory serbest birakilmalidir. buffer array yerine char*buffer yani char pointer tavsiye edilir
char* nexToken(char *str, uint8_t *lastIndex, uint8_t *len) {
	uint8_t bas = *lastIndex;
//eliminate empty character and start index
	d_print("<nex token baslangic :");
	d_print(str + bas);
	d_println(">")
	for (; str[bas] != NULL; bas++) {
		if (str[bas] == ' ' || str[bas] == '\t' || str[bas] == '\r'
				|| str[bas] == '\n' || str[bas] == '<')
			continue;
		else
			break;
	}
	d_print("<Baslangic index : ");
	d_print(bas);
	d_println(">")
//bosluk veya satir sonuna gelinceye kadar tara
	uint8_t sonIndex = bas;
	for (; str[sonIndex] != NULL; sonIndex++) {
		if (str[sonIndex] == ' ' || str[sonIndex] == '\t'
				|| str[sonIndex] == '\r' || str[sonIndex] == '\n'
				|| str[sonIndex] == '>')
			break;
	}
	d_print("<Son index : ");
	d_print(sonIndex);
	d_println(">")
	*lastIndex = sonIndex;
	if (sonIndex == bas) {
		*len = 0;
		return NULL;
	}
	*len = sonIndex - bas;
	char *tmp;
	tmp = (char*) malloc(*len + 1);
	memcpy(tmp, str + bas, *len);
	tmp[*len] = NULL;
	d_print("<Token :");
	d_print(tmp);
	d_println(">")
	d_println("<nex token bitis>");
	return tmp;
}
uint8_t connectServer() {
	uint8_t flag;
	int count = 0;
	d_print("<Tcp Connection:");
	client.stopAll();
	while (count < NO_TCP_TRY) {
		d_print(".");
		if (client.connect(*ip, port) > 0) {
			d_println("Established>");
			return ERR_CODE = RS_NO_PROBLEM;
		}
		yield();
		delay(10);
		count++;
	}
	d_println("Failed>");
	return ERR_CODE = RS_TCP_CONN_PROBLEM;
}

// Fucntion to connect WiFi
uint8_t connectWifi() {
	int WiFiCounter = 0;
// We start by connecting to a WiFi network
	d_print("<Connecting to ");
	d_print(ssid);
	WiFi.disconnect();
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED) {
		yield();
		delay(500);
		WiFiCounter++;
		d_print(".");
		if (WiFiCounter >= NO_WIFI_TRY) {
			d_println("Can not connect to the wifi>");
			return ERR_CODE = RS_WIFI_PROBLEM;
		}
	}
	d_println("WiFi connected>");
	d_print("<IP address: ");
	d_print(WiFi.localIP());
	d_println(">");
	return ERR_CODE = RS_NO_PROBLEM;
}
uint8_t sentHeader() {
	int8_t errTry = 10;
	char buffer[20];
	sprintf(buffer, "<%d %d>", AC_SEND_ID, arduino_id);
	pck_recv.init();
	unsigned long start = millis();
	const unsigned long MAX_WAIT_MS = 60000; //1 dakika sure tani
	bool idAccepted=false;
	while (!idAccepted &&(millis() - start < MAX_WAIT_MS)) {
		while (client.available()) {
			pck_recv.setData(client.read());
			if (pck_recv.isFinished) {
				uint8_t lastIndex = 0, len = 0;
				char *actCode = nexToken((char*) pck_recv.data, &lastIndex,
						&len);
				if(actCode!=NULL){
					uint16_t code = atoi(actCode);
					free(actCode);
					actCode=NULL;
					if(code==AC_ID_REQUESTED){
						d_println("Id requested");
						client.println(buffer);
						client.flush();
					}else if(code==AC_ID_ACCEPTED){
						d_println("id accepted");
						idAccepted=true;
					}else{
						d_println("geçersiz id");
					}
				}
				pck_recv.init();
			}
		}
		delay(10);

	}
	return ERR_CODE=idAccepted ? RS_NO_PROBLEM:RS_HEADER_PROBLEM;
}
uint8_t sentHeader2() {

	client.flush();
	int8_t errTry = 10;
	char buffer[20];
	sprintf(buffer, "<%d %d>", AC_SEND_ID, arduino_id);
	pck_recv.init();
	unsigned long start = millis();
	const unsigned long MAX_WAIT_MS = 3000;
	while (errTry >= 0) {
		client.println(buffer);
		client.flush();
		delay(10);
		while (client.available() && !pck_recv.isFinished) {
			pck_recv.setData(client.read());
			start = millis();
		}
		if (pck_recv.isFinished) {
			uint8_t lastIndex = 0, len = 0;
			char *actCode = nexToken((char*) pck_recv.data, &lastIndex, &len);
			if (actCode != NULL) {
				char *val = nexToken((char*) pck_recv.data, &lastIndex, &len);
				if (val != NULL) {
					uint16_t ac, ai;
					ac = atoi(actCode);
					free(actCode);
					actCode = NULL;
					ai = atoi(val);
					free(val);
					val = NULL;
					if (AC_SEND_ID == ac && arduino_id == ai) {
						return ERR_CODE = RS_NO_PROBLEM;
					}
				} else {
					free(actCode);
					actCode = NULL;
				}
			}
			pck_recv.init();
		}

		if (millis() - start > MAX_WAIT_MS) {
			errTry--;
			start = millis();
		}
	}
	return ERR_CODE = RS_HEADER_PROBLEM;

}

// Inifinite loop - Causes to reset self
void resetBoard() {
	client.print("<");
	client.print(AC_RESET_ESP);
	client.print(">");
	client.println(SERVER_END_OF_LINE);
	delay(10);
	sentEspPosition(AC_RESET_ESP);
	delay(10);
	yield();
	client.flush();
	Serial.flush();
	client.stopAll();
	WiFi.disconnect();
	d_println("<Reseting>");
//  ESP.restart();
	delay(100);
	do {
	} while (1);
}

void sentEspPosition(uint16_t actionCode) {
	char buffer[10];
	sprintf(buffer, "<%d>", actionCode);
	Serial.println(buffer);
	Serial.flush();

}
char* getParameter(uint16_t actionCode) {

	unsigned long waitms = 500;
	int8_t no_try = 10;
	char buffer[40];
	bool error = false;
	uint8_t i = 0;
	bool started = false, finished = false;
	sprintf(buffer, "<%d>", actionCode);
	Serial.println(buffer);
	unsigned long cur = millis();
	while (no_try >= 0) {
		if ((millis() - cur > waitms) || error) {
			no_try--;
			sprintf(buffer, "<%d>", actionCode);
			started = false;
			finished = false;
			i = 0;
			Serial.println(buffer);
			cur = millis();
			error = false;
		}

		while (Serial.available()) {
			char c = (char) Serial.read();
			if (c == '<') {
				started = true;
				i = 0;
			} else if (c == '>') {
				if (started) {
					finished = true;
					buffer[i] = NULL;
				} else {
					error = true;
					break;
				}
			} else
				buffer[i++] = c;
			if (finished) {
				uint8_t len;
				uint8_t nextIndex;
				nextIndex = 0;
				d_print("<before nexToken : ");
				d_print(buffer);
				d_println(" >")
				char *code = nexToken(buffer, &nextIndex, &len);
				int c = atoi(code);
				free(code);
				code = NULL;
				if (c != actionCode) {
					error = true;
					break;
				}
				d_print("<before 2.nexToken : >");
				char *str = nexToken(buffer, &nextIndex, &len);
				if (str == NULL) {
					error = true;
					break;
				}
				return str;
			}
		}
		delay(5);
	}
	return NULL;
}

