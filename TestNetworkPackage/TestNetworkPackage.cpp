#include <Arduino.h>

//#include "NetworkPackage/NetworkPackage.h"
#include"NetworkPackage.h"
#include <string.h>
NetworkPackage pck;
void toString(char *c,int d){
	sprintf(c,"%d",d);
}
void setup() {
	Serial.begin(38400);
	Serial.println("Network package deneme");
	Serial.println("setup ends\nReady to read from serial ");
	uint16_t actionCode = 12;
	uint16_t id = 999;

	char tmp1[10];
	char tmp2[10];
	char tmp3[10];
	toString(tmp1, 10);
	toString(tmp2, 20);
	toString(tmp3, 30);
	char *ptr[]{tmp1,tmp2,tmp3};


	pck.changeMessage(id, actionCode, ptr, 3);
	Serial.print("Server Package : ");
	Serial.println(pck.getMessage());
}
void loop() {
	while (Serial.available() && !pck.isDataCompleted()) {
		pck.appendMessage((char) Serial.read());
	}
	if (pck.isDataCompleted()) {
		Serial.print("data completed ");
		Serial.print("Size : ");
		Serial.println(pck.getMessageLength());
		Serial.print("Message : ");
		Serial.println(pck.getMessage());
		Serial.print("Id:");
		Serial.println(pck.getId());
		Serial.print("Ac: ");
		Serial.println(pck.getActionCode());
		Serial.print("Data miktari :");
		Serial.println(pck.getDataMiktari());
		Serial.print("Data line :");
		char val[100];
		int l = pck.getDataLine(val);
		Serial.println(val);
		memcpy(val, 0x0, 100);
		for (int i = 0; i < pck.getDataMiktari(); i++) {
			Serial.print(i + 1);
			Serial.print(". data : ");
			l = pck.getDataOf(i + 1, val);
			Serial.println(val == 0x0 ? "null" : val);
		}
		pck.reset();
	}
}
