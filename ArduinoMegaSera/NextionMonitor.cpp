/*
 * NextionMonitor.cpp
 *
 *  Created on: 23 Mar 2020
 *      Author: alptu
 */

#include "NextionMonitor.h"
bool setTemp(uint8_t pageId, uint8_t cmpId, const char *cmpName, double val);
void parseDouble(const double val, int precision, int *intValue,
		int *floatValue);
NextionMonitor::NextionMonitor() {
	// TODO Auto-generated constructor stub
	nexInit();
	sendCommand("rest");
	kaydetButton = new NexButton(2,16,"b0");
	nex_listen_list = new NexTouch*[2];
		nex_listen_list[0] = kaydetButton;
		nex_listen_list[1] = NULL;
		Serial.println("end of cons");
		delay(1000);
}

NextionMonitor::~NextionMonitor() {
	// TODO Auto-generated destructor stub
}

bool NextionMonitor::setTemp2(double val) {
	return setTemp(1, 9, "pageMain.t7", val);
}

bool NextionMonitor::setTemp1(double val) {
	return setTemp(1, 8, "pageMain.t6", val);
}

bool NextionMonitor::setBoardTemp(double val) {
	return false;
}

bool NextionMonitor::setTemp3(double val) {
	return setTemp(1, 10, "pageMain.t8", val);
}

void NextionMonitor::begin() {
	/* Set the baudrate which is for debug and communicate with Nextion screen. */

//	   b0PopCallback();
	/* Register the pop event callback function of the current button component. */


	kaydetButton->attachPop(b0PopCallback, this);

}



bool setTemp(uint8_t pageId, uint8_t cmpId, const char *cmpName, double val) {
	NexText txt(pageId, cmpId, cmpName);
	char buffer[10];
	int iVal;
	int fVal;

	parseDouble(val, 2, &iVal, &fVal);

	sprintf(buffer, "%d.%02d", iVal, fVal);
	return txt.setText(buffer);
}
void parseDouble(const double val, int precision, int *intValue,
		int *floatValue) {

	int pre = 1;
	for (int i = 0; i < precision; i++) {
		pre = pre * 10;
	}

	*intValue = (int) val;
	*floatValue = (int) ((val - *intValue) * pre);
}
void NextionMonitor::b0PopCallback(void *ptr) {
	d_println("BUTTON CLICKED");
	ConnectionRecord rec;
	const char *pageName = "pageAyarlar0";
	uint8_t pid=2;
	char buffer[30],buffer2[32];

	sprintf(buffer, "%s.tSsid", pageName);
	NexText t = NexText(pid, 11, buffer);
	int size=t.getText(buffer2,32);
	buffer2[size]=NULL;
	rec.setSSID(buffer2);
	d_print(buffer);
	d_print(" : ");
	d_println(buffer2);

	sprintf(buffer, "%s.tPass", pageName);
	t = NexText(pid, 12, buffer);
	size=t.getText(buffer2,16);
	buffer2[size]=NULL;
	rec.setPassword(buffer2);
	d_print(buffer);
	d_print(" : ");
	d_println(buffer2);

	sprintf(buffer, "%s.tIp", pageName);
	t = NexText(pid, 13, buffer);
	size=t.getText(buffer2,16);
	buffer2[size]=NULL;
	rec.setIp(buffer2);
	d_print(buffer);
	d_print(" : ");
	d_println(buffer2);

	sprintf(buffer, "%s.tPort", pageName);
	t = NexText(pid, 14, buffer);
	size=t.getText(buffer2,16);
	buffer2[size]=NULL;
	rec.setPort(atoi(buffer2));
	d_print(buffer);
	d_print(" : ");
	d_println(buffer2);

	sprintf(buffer, "%s.tId", pageName);
	t = NexText(pid, 15, buffer);
	size=t.getText(buffer2,16);
	buffer2[size]=NULL;
	rec.setId(atoi(buffer2));
	d_print(buffer);
	d_print(" : ");
	d_println(buffer2);
	NextionMonitor *m = ptr;
	rec.saveConnectionRecord();
	ConnectionRecord rec2;
	rec2.loadConnectionRecord();
	m->gotoPage(rec.equals(rec2)?MonitorPages::SUCCESS_PAGE:MonitorPages::FAILURE_PAGE);


}

void NextionMonitor::listenKeyboard() {
	nexLoop(nex_listen_list);
}

bool NextionMonitor::setConectionSettings(ConnectionRecord rec) {

	if (rec.hasAvailableRecord()) {
		uint8_t pid = 2;
		const char *pageName = "pageAyarlar0";
		char buffer[30];
		sprintf(buffer, "%s.tSsid", pageName);
		NexText t = NexText(pid, 11, buffer);
		t.setText(rec.getSSID());
		d_print(buffer);
		d_print(" : ");
		d_println(rec.getSSID());

		sprintf(buffer, "%s.tPass", pageName);
		t = NexText(pid, 12, buffer);
		t.setText(rec.getPassword());

		sprintf(buffer, "%s.tIp", pageName);
		t = NexText(pid, 13, buffer);
		t.setText(rec.getIp());

		sprintf(buffer, "%s.tPort", pageName);
		t = NexText(pid, 14, buffer);
		char buffer2[10];
		sprintf(buffer2, "%d", rec.getPort());
		t.setText(buffer2);

		sprintf(buffer, "%s.tId", pageName);
		t = NexText(pid, 15, buffer);
		sprintf(buffer2, "%d", rec.getId());
		t.setText(buffer2);
	}
	d_print("HAS RECORD :");
	d_println(rec.hasAvailableRecord());
	d_print("SSID :");
	d_println(rec.getSSID());
	d_print("Pass :");
	d_println(rec.getPassword());
	d_print("Ip:");
	d_println(rec.getIp());
	d_print("Port :");
	d_println(rec.getPort());
	d_print("Id :");
	d_println(rec.getId());
	return true;
}
void NextionMonitor::openOpeningPage() {
	NexPage page(0, 0, "pageOpening");
	page.show();
}

void NextionMonitor::openAdminPage() {
	NexPage page(2, 0, "pageAyarlar0");
	page.show();
}

void NextionMonitor::openMainPage() {
	NexPage page(1, 0, "pageMain");
	page.show();
}
void NextionMonitor::openSavingPage() {
	NexPage page(4,0,"pageSaving");
	page.show();
}

void NextionMonitor::openSuccessPage() {
	NexPage page(6,0,"pageSuccess");
	page.show();
}

void NextionMonitor::openFailurePage() {
	NexPage page(5,0,"pageError");
	page.show();
}

void NextionMonitor::openKeypadPage() {
	NexPage page(3,0,"pageKeypad");
	page.show();
}
