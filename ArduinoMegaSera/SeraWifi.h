/*
 * SeraWifi.h
 *
 *  Created on: 20 Tem 2017
 *      Author: alptug
 */

#ifndef SERAWIFI_H_
#define SERAWIFI_H_

#include"Arduino.h"
#include"DataPackage.h"
#define PACKAGE_DEBUG 0
#define MAX_ALLOACTE_TIME 50 //50 ms


//ACTION CODES
#define WIFI_PING 0
#define WIFI_IS_ALIVE 1






typedef enum{OK=0}SWResult;

class SeraWifi {
public:
	SeraWifi(){
		this->errCode = SWResult::OK;
	};
	virtual void begin()=0;
	virtual void connect()=0;
	virtual void disconnect()=0;
	virtual void write(DataPackage)=0;
	virtual void println(char *ch)=0;
	virtual void sendPig();
	virtual byte readByte(DataPackage *pck=0x0)=0;
	virtual size_t readAll(DataPackage *pck)=0;
	virtual bool isAvaible()=0;
	SWResult getErrorCode(){
		return errCode;
	}
protected:
	SWResult errCode;
};

#endif /* SERAWIFI_H_ */
