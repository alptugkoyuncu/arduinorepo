/*
 * Monitor.h
 *
 *  Created on: 17 Tem 2017
 *      Author: alptug
 */

#ifndef MONITOR_H_
#define MONITOR_H_

#include<RecordData.h>

typedef enum {OPENING_SCREEN,MAIN_SCREEN,ADMIN_PAGE,KEYPAD_PAGE,SAVING_PAGE,SUCCESS_PAGE,FAILURE_PAGE} MonitorPages;
class Monitor {
public:
	Monitor(){};
	virtual void begin()=0;
	virtual bool setTemp1(double val)=0;
	virtual bool setTemp2(double val)=0;
	virtual bool setTemp3(double val)=0;
	virtual bool setBoardTemp(double val)=0;
	virtual void listenKeyboard()=0;
	virtual bool setConectionSettings(ConnectionRecord rec)=0;
	void gotoPage(MonitorPages p){
		switch(p){
		case MonitorPages::OPENING_SCREEN:this->openOpeningPage();break;
		case MonitorPages::MAIN_SCREEN:this->openMainPage();break;
		case MonitorPages::ADMIN_PAGE:this->openAdminPage();break;
		case MonitorPages::KEYPAD_PAGE:this->openKeypadPage();break;
		case MonitorPages::SAVING_PAGE:this->openSavingPage();break;
		case MonitorPages::SUCCESS_PAGE:this->openSuccessPage();break;
		case MonitorPages::FAILURE_PAGE:this->openFailurePage();break;
		default:this->openMainPage();break;
		}
		this->currentPage=p;
	}
protected:
	MonitorPages currentPage;
	virtual void openMainPage()=0;
	virtual void openOpeningPage()=0;
	virtual void openAdminPage()=0;
	virtual void openKeypadPage()=0;
	virtual void openSavingPage()=0;
	virtual void openSuccessPage()=0;
	virtual void openFailurePage()=0;
};

#endif /* MONITOR_H_ */
