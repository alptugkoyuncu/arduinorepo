/*
 * ArduinoMegaSera.h
 *
 *  Created on: 18 Mar 2020
 *      Author: alptu
 */

#ifndef ARDUINOMEGASERA_H_
#define ARDUINOMEGASERA_H_


#include <Arduino.h>
#include"MyPt1000.h"
//#include"NokiaMonitor.h"
#include"NextionMonitor.h"
#include"ESPWifi.h"
#include"ActionCode.h"
#include"MyDebug.h"
//add your function definitions for the project SeraProjectWithProMini3_3 here


char* nexToken(char *str, uint8_t *lastIndex, uint8_t *len);
void doActions(char *buffer);
size_t dToStr(char *ch, double val);
//Do not add code below this line



#endif /* ARDUINOMEGASERA_H_ */
