#include"EspModuleSera.h"
WiFiClient client;
ConnectionRecord rec;
DataPackage pck;

void doActions();
void debugPrint(const char *str);
void debugPrint(const char *str, uint64_t timePassed);

uint32_t timeForClient, timeforWifi;
#define MAX_Wait_FOR_CLIENT 20
#define MAX_WAIT_FOR_WIFI_CONNECTION 60000 //20 saniye

typedef enum {
	WIFI_CONNECTED,
	WIFI_CONNECTION_PENDING,
	WIFI_DISCONNECTED,
	WIFI_CONNECTION_FAILED,
	SERVER_CONNECTED,
	SERVER_CONNECTION_PENDING,
	SERVER_DISCONNECTED,
	SERVER_CONNECTION_FAILED,
	ID_ACCEPTED,
	ID_REFUSED
} ConnectionState;
ConnectionState conState;
char *getConnectionState(ConnectionState cs){
	switch(cs){
	case WIFI_CONNECTED:
		return "WIFI CONNECTED";
	case WIFI_CONNECTION_PENDING:
		return "wifi connection pending";
	case WIFI_DISCONNECTED:
		return "wifi disconnected";
	case WIFI_CONNECTION_FAILED:
		return "wifi connection failed";
	case SERVER_CONNECTED:
		return "server connected";
	case SERVER_CONNECTION_PENDING:
		return "Server Connection pending";
	case SERVER_DISCONNECTED:
		return  "Server Disconnected";
	case SERVER_CONNECTION_FAILED:
		return "Server connection failed";
	case ID_ACCEPTED:
		return "id accepted";
	case ID_REFUSED:
		return "id refused";
	default:
		return "unknown cs";

	}
}
void setConnectionState(ConnectionState cs);
void setup() {
#ifdef AK_DEBUG
	Serial.println(9600);
#endif
	pck.init();
	rec.loadConnectionRecord();
	delay(10);
	WiFi.mode(WIFI_STA);
	WiFi.begin(rec.getSSID(), rec.getPassword());
	timeforWifi = millis();
	setConnectionState(WIFI_CONNECTION_PENDING);
	if (WiFi.status() == WL_CONNECTED) {
		client.connect(rec.getIp(), rec.getPort());
		setConnectionState(SERVER_CONNECTION_PENDING);
	}
}

void loop() {
	if (client.connected()) {
		if(conState!=SERVER_CONNECTED){
			setConnectionState(SERVER_CONNECTED);
		}
		timeForClient = millis();
		while ((client.available() > 0) && (timeForClient < millis() + MAX_Wait_FOR_CLIENT)) {
			pck.setData(client.read());
			if (pck.isFinished) {
				doActions();
				pck.init();
			}
		}
	} else {
		if (WiFi.status() != WL_CONNECTED) {
			if (millis() - timeforWifi > MAX_WAIT_FOR_WIFI_CONNECTION) {
				switch (conState) {
				case WIFI_CONNECTION_PENDING: {
					WiFi.disconnect();
					setConnectionState(WIFI_CONNECTION_FAILED);
					break;
				}
				case WIFI_CONNECTION_FAILED:
				case WIFI_DISCONNECTED: {
					WiFi.begin(rec.getIp(), rec.getPassword());
					setConnectionState(WIFI_CONNECTION_PENDING);
					break;
				}
				}
			}
		}else{
			if(conState!=WIFI_CONNECTED){
				setConnectionState(WIFI_CONNECTED);
				client.connect(rec.getIp(),rec.getPort());
				setConnectionState(SERVER_CONNECTION_PENDING);
			}
		}
	}

}
void debugPrint(const char *str) {
	d_print(str);
	d_print(F(" Free Mem:"))
	d_println(freeMemory());
}
void debugPrint(const char *str, uint64_t timePassed) {
	debugPrint(str);
	d_print(F("time passed :"));
	uint32_t mls = timePassed / 1000;
	uint32_t mics = timePassed - mls;
	d_print(mls);
	d_print(F(" milisec "));
	d_print(mics);
	d_println(F(" microsec"));
}
void doActions() {
	debugPrint("start doActions ");
	uint32_t timePassed = micros();
	timePassed -= micros();
	debugPrint("end doActions", timePassed);
}
void setConnectionState(ConnectionState cs){
	Serial.print("Connection State :");
	Serial.print(getConnectionState(conState));
	Serial.print("->");
	Serial.print(getConnectionState(cs));
	Serial.println();
	conState=cs;
}
