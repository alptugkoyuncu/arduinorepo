// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _WifiWheatherStation_H_
#define _WifiWheatherStation_H_

#include "Arduino.h"

//add your includes for the project WifiWheatherStation here

#include"WheatherSensors.h"
#include<SPI.h>
#include"DataPackage.h"
#include"MemoryFree.h"
#include"Errors.h"

//end of add your includes here

// WiFi network (change with your settings !)
#define WLAN_SSID       "KOYUNCU_CATI"
#define WLAN_PASS       "5458798185"
#define WLAN_SECURITY   WLAN_SEC_WPA2
#define SERVER_PORT  4444
//#define SERVER_IP  127,0,0,1
#define SERVER_IP 192,168,1,11
#define USER_NAME "Wheather_stn"
#ifdef __cplusplus
extern "C" {
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif

//add your function definitions for the project WifiWheatherStation here
void doActionCommands() ;
void communicateWithSerial() ;
void SerialPrintSensors();
void action_reset_esp(long t=5000);
int8_t action_receive_ping_server();
void checkWireless();
int8_t action_send_ping_server();
int8_t action_gecersiz_komut();
uint16_t action_respond_alive();

bool sendToWireless(int deneme, char *buff, int size);
uint8_t action_start_esp() ;
int8_t action_sent_memory_info();
int8_t action_sent_wifi_sensors_info();
//Do not add code below this line


#endif /* _WifiWheatherStation_H_ */
