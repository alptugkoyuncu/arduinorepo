// Do not remove the include below
#include "WifiWheatherStation.h"
#include "GeneralSystemDefines.h"
//def�ne act�ons

#define CONNECTION_ID WHEATHER_STATION

//sistem genele bilgileri
#define ACTION_GECERSIZ_KOMUT 0
#define ACTION_IS_ALIVE 1
#define ACTION_RESPOND_ALIVE 2
#define ACTION_SENT_CONNECTION_ID 3
#define ACTION_PRINT_ESP_DATA 4
#define ACTION_SENT_MEMORY_INFO 33

//esp b�lg�ler�
#define ACTION_SENT_WIFI_SENSORS_INFO 81

#define ACTION_RESET_ESP 91
#define ACTION_ESP_STARTED 92
#define ACTION_PING_TO_SERVER 98
#define ACTION_PING_FROM_SERVER 99

//// DHT11 sensor pins
//#define DHTPIN 7
//#define DHTTYPE DHT21

// Define chip pins
#define ESP_RESET_PIN 9

#define WIFI_SENT_TRY 1
bool isPingSent = false;
unsigned long pingSentTime;
#define PING_SENT_INTERVAL 600000
#define PING_REPLY_INTERVAL 20000

bool isSerialAvaible = true;
bool isWirelessAvaible = false;

DataPackage pck(RECEIVED);

#define wlConn Serial2
#define SERIAL_UART 115200
uint8_t w_fails = 0;
int maxMemory = 0;
int minMemory = 32000;
//
//#define pinLED     13   // LED connected to digital pin 13
//#define pinAnem    18  // Anenometer connected to pin 18 - Int 5 - Mega   / Uno pin 2
//#define pinRain    2  // Anenometer connected to pin 2 - Int 0 - Mega   / Uno Pin 3
//#define intAnem    5  // int 0 (check for Uno)
//#define intRain   0  // int 1
//#define DS18S20_Pin 8
////global variables
////dht21 sensor
//DHT dht(DHTPIN, DHTTYPE);

//// initialize SDL_Weather_80422 library
//SDL_Weather_80422 weatherStation(pinAnem, pinRain, intAnem, intRain, A0,
//		SDL_MODE_INTERNAL_AD);
//
////DS18B20
//OneWire ds(DS18S20_Pin);  // on digital pin 2
//
//end of global variables

//The setup function is called once at startup of the sketch

WheatherSensors *ws;
unsigned long ct;
void setup() {
// Add your initialization code here
	pck.init();
	if (isSerialAvaible)
		Serial.begin(115200);

//	Serial.print("--------------------------------------------------------");
//	Serial.print("Program basladi");
//	Serial.println("--------------------------------------------------------");
	pinMode(ESP_RESET_PIN, OUTPUT);
	action_reset_esp();

	ws = new WheatherSensors();
	action_sent_memory_info();
	ct = millis();
}

// The loop function is called in an endless loop
void loop() {
	checkWireless();
	if (isSerialAvaible && Serial.available()) {
		//		Serial.println("serial avaible");
		communicateWithSerial();
	}
	while (wlConn.available()) {

		byte dt = wlConn.read();
		uint8_t flag = pck.setData(dt);
		if (flag == BITIS_AYIRACI) {
			if (pck.size > 0)
				doActionCommands();
			pck.init();
		}
		if (flag == OVERLAPPED) {
			//DO NOTHING
			//Overlapped oldugunda verileri yedek bi yere kaydedecegiz bunun icin memory de kalan yeri belirleyecegiz loop icinde
			//kullanilan ve minimum memory limitini asmayacak sekilde sirayla yer acacagiz . Mesela bi kuyruk tasarlanip son gelen eleman
			// en sona eklenip bastaki eleman baska bi yerde yedeklenebilir
		}
		delay(1);
	}
	if (ws->update()) {
		action_sent_wifi_sensors_info();
	}
	if (millis() > ct + 360000L) {
		ct=millis();
		action_sent_memory_info();
	}
	delay(50);
//Add your repeated code here
}
int8_t action_sent_wifi_sensors_info() {
	int8_t flag = NO_ERROR;
	char buffer[200];
	int size;
	int act = ACTION_SENT_WIFI_SENSORS_INFO;

	char temp[100];
	ws->toString().toCharArray(temp, 100, 0);
	size = sprintf(buffer, "%d %d:%s", act, flag, temp);

	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
void SerialPrintSensors() {
	Serial.print("T1:");
	Serial.print(ws->getDs_temp());
	Serial.print(" T2:");
	Serial.print(ws->getDht1_temp());
	Serial.print(" H:");
	Serial.print(ws->getDht1_hum());
	Serial.print(" RT:");
	Serial.print(ws->getRain_total());
	Serial.print(" WS:");
	Serial.print(ws->getWindSpeed());
	Serial.print(" WD:");
	Serial.print(ws->getWind_direction());
	Serial.print(" WG:");
	Serial.println(ws->getWind_gust());

}
int8_t action_sent_connection_id(){
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size=0;
	int act = ACTION_SENT_CONNECTION_ID;

	size = sprintf(buffer, "%d %d:%d", act, flag,CONNECTION_ID);

	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_sent_memory_info() {
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_SENT_MEMORY_INFO;

	int mem = freeMemory();
	if (mem > maxMemory)
		maxMemory = mem;
	if (mem < minMemory) {
		minMemory = mem;
	}
	size = sprintf(buffer, "%d %d:%d %d %d", act, flag, mem, minMemory,
			maxMemory);

	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
uint16_t action_respond_alive() {
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_RESPOND_ALIVE;

	size = sprintf(buffer, "%d %d", act, flag);
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}

void doActionCommands() {
//	Serial.print("Received Data : ");
	//Serial.println((char *)pck.data);
	//parse the package
	const char headerParser = ':';
	const char araParser = ' ';
	uint8_t headerParserCount = 0;
	uint8_t araParserCount = 0;
	//header0 verinin geldigi baglantiyi veriri
	//header1 komutu ve komut sonucunu verir basarili veya hata sonucunu
	//header2 valuelari veriri

	char sendertype[4];
	uint8_t senderTypeCount = 0;
	char actionCommand[4];
	uint8_t actionCommanSize = 0;
	char hataKodu[4];
	uint8_t hataKoduSize = 0;
	char values[50];
	uint8_t valueCount = 0;
	for (int i = 0; i < pck.size; i++) {
		char next = pck.data[i];
		if (next == headerParser) {
			headerParserCount++;
			araParserCount = 0;
		} else if (next == araParser)
			araParserCount++;
		else {
			if (headerParserCount == 0)
				sendertype[senderTypeCount++] = next;
			else if (headerParserCount == 1) {
				if (araParserCount == 0)
					actionCommand[actionCommanSize++] = next;
				else if (araParserCount == 1)
					hataKodu[hataKoduSize++] = next;
				else {
#ifdef LOG
					LOG_S("HATA ARA PARSELDE");
#endif
				}
			} else if (headerParserCount == 2) {
				for (int j = i; j < pck.size; j++)
					values[valueCount++] = pck.data[j];
				break;
			} else {
#ifdef LOG
				LOG_S("HATA HEADER PARSER DE");
#endif
			}
		}

	}
	/*
	 if (senderTypeCount < 0) {
	 Serial.println("senderype header de sorun");
	 }
	 if (actionCommanSize < 0) {
	 Serial.println("action command size da sorun");
	 }
	 if (hataKoduSize < 0)
	 Serial.println("hata kodu size da sorun");
	 if (valueCount < 0)
	 Serial.println("value count da  sorun");
	 */

	if (senderTypeCount < 0 || actionCommanSize < 0 || hataKoduSize < 0
			|| valueCount < 0)
		return;

	sendertype[senderTypeCount] = NULL;
	actionCommand[actionCommanSize] = NULL;
	hataKodu[hataKoduSize] = NULL;
	if (valueCount > 0)
		values[valueCount] = NULL;
#ifdef DEBUG
	DEBUG_S("buraya kadar geldik");
#endif
	uint8_t sender = senderTypeCount > 0 ? atoi(sendertype) : 0;
	uint8_t actCmd = actionCommanSize > 0 ? atoi(actionCommand) : 0;
	uint8_t hata_kodu = hataKoduSize > 0 ? atoi(hataKodu) : 0;

//	char ch[50];
//	int ch_size;
//	if (valueCount > 0)
//		//	ch_size=
//		sprintf(ch, "sender : %d\tact: %d hata: %d value: %s", sender, actCmd,
//				hata_kodu, values);
//	else
//		//	ch_size =
//		sprintf(ch, "sender : %d\tact: %d hata: %d value:", sender, actCmd,
//				hata_kodu);
//	Serial.println(ch);

	switch (actCmd) {
	case ACTION_IS_ALIVE: {
		action_respond_alive();
		break;
	}

	case ACTION_SENT_CONNECTION_ID:{
		action_sent_connection_id();
		break;
	}
	case ACTION_PRINT_ESP_DATA:{
		Serial.print("ESP Data( ");
		Serial.print(valueCount);
		Serial.print(" ):");
		Serial.println(values);
		break;
	}
	case ACTION_SENT_MEMORY_INFO:
		action_sent_memory_info();
		break;

	case ACTION_RESET_ESP: {
		action_reset_esp();
		break;
	}
	case ACTION_ESP_STARTED: {
		action_start_esp();
		break;
	}
	case ACTION_PING_TO_SERVER: {
		action_send_ping_server();
		break;
	}
	case ACTION_PING_FROM_SERVER: {
		action_receive_ping_server();
		break;
	}
	case ACTION_GECERSIZ_KOMUT:
	default:
		action_gecersiz_komut();
		break;
	}

}
void communicateWithSerial() {

	long action = Serial.parseInt();
#ifdef DEBUG
//	char ch[50];
//	int ch_size;
//	size_my_dbg_buf =
	sprintf(my_dbg_buf, "Gelen Data : %d", action);
//	ch[ch_size] = NULL;
	DEBUG_S(my_dbg_buf);
#endif
	switch (action) {
	case ACTION_IS_ALIVE: {
		action_respond_alive();
		break;
	}

	case ACTION_SENT_MEMORY_INFO:
		action_sent_memory_info();
		break;
	case ACTION_RESET_ESP: {
		action_reset_esp();
		break;
	}
	case ACTION_ESP_STARTED: {
		action_start_esp();
		break;
	}
	case ACTION_PING_TO_SERVER: {
		action_send_ping_server();
		break;
	}
	case ACTION_PING_FROM_SERVER: {
		action_receive_ping_server();
		break;
	}
	default:
		action_gecersiz_komut();
		break;
	}
}

void action_reset_esp(long t) {
	if (t < 550)
		t = 550;
	if (t > 3000)
		t = 3000;
	unsigned long dly = t < 3000 ? t - 500 : 3000;
	wlConn.end();
	digitalWrite(ESP_RESET_PIN, LOW);
	delay(dly);
	digitalWrite(ESP_RESET_PIN, HIGH);
	delay(50);
	wlConn.begin(SERIAL_UART);
	delay(50);
	while (wlConn.available())
		wlConn.read();

	isWirelessAvaible = false;
	//Serial.println("RESET ESP");
}

void checkWireless() {
	if (isPingSent) {
		if (millis() - pingSentTime > PING_REPLY_INTERVAL) {
			action_reset_esp();
			isPingSent = false;
			delay(1);
			pingSentTime = millis();
			return;
		}
	} else {
		if (millis() - pingSentTime > PING_SENT_INTERVAL) {
			action_send_ping_server();
			delay(1);
			pingSentTime = millis();
			isPingSent = true;
		}
	}

}
int8_t action_send_ping_server() {
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_PING_TO_SERVER;

	size = sprintf(buffer, "%d %d", act, flag);
	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
int8_t action_receive_ping_server() {
	isPingSent = false;
	return true;
}
int8_t action_gecersiz_komut() {
#ifndef HAS_GECERSIZ_KOMUT
	return 0;
#endif;
	int8_t flag = NO_ERROR;
	char buffer[50];
	int size;
	int act = ACTION_GECERSIZ_KOMUT;
	size = sprintf(buffer, "%d %d", act, flag);

	buffer[size] = NULL;
	if (isSerialAvaible)
		Serial.println(buffer);
	if (isWirelessAvaible)
		sendToWireless(WIFI_SENT_TRY, buffer, size);
	return flag;
}
bool sendToWireless(int deneme, char *buff, int size) {
	int notry = 0;
	if (deneme <= 0)
		deneme = 1;
	while (1) {
		if (notry >= deneme)
			break;
		notry++;
		Serial.print("try to send on wireless : ");
		buff[size] = NULL;
		char buff2[size + 3];
		int size2 = sprintf(buff2, "<%s>", buff);
		buff2[size2] = NULL;
	Serial.println(buff2);
//		bool flag = conn->sendLine(buff2, size2);

		int s = wlConn.print(buff2);
//		bool flag = conn->sendLine(buff, size);
		delay(100);
		wlConn.flush();
		if (s == size2) {
			w_fails = 0;
			return true;
		}
		delay(100);
	}
	//Serial.println("sent failed");
	w_fails++;
	if (w_fails >= 3) {
		action_reset_esp();
		w_fails = 0;
	}
	return false;
}
uint8_t action_start_esp() {
	//Serial.println("Wireless avaible");
	isWirelessAvaible = true;
	isPingSent = false;
	pingSentTime = millis();
	return NO_ERROR;
}
