/*
 * Wheather.h
 *
 *  Created on: 17 Tem 2015
 *      Author: WIN7
 */

#ifndef _WHEATHER_STATION_H_
#define _WHEATHER_STATION_H_

#include <SDL_Weather_80422.h>
#include <OneWire.h>
#include<DHT.h>

// DHT21 sensor pins
#define DHT_1_PIN 7
#define DHT_1_TYPE DHT21
#define DHT_2_PIN 10
#define DHT_2_TYPE DHT21
#define DS18S20_Pin 8

#define pinLED     13   // LED connected to digital pin 13
#define pinAnem    18  // Anenometer connected to pin 18 - Int 5 - Mega   / Uno pin 2
#define pinRain    2  // Anenometer connected to pin 2 - Int 0 - Mega   / Uno Pin 3
#define intAnem    5  // int 0 (check for Uno)
#define intRain   0  // int 1

#define WAIT_TO_LONGEST_INTERVAL false
#define UPDATE_INTERVAL_TIME 30000 //ms 30sn

#define RAIN_INTERVAL_TIME 60000//ms 60sn
#define DHT_INTERVAL_TIME 2000//ms 2sn

#define KUZEY 0
#define KUZEY_DOGU 1
#define DOGU 2
#define GUNEY_DOGU 3
#define GUNEY 4
#define GUNEY_BATI 5
#define BATI 6
#define KUZEY_BATI 7

class WheatherSensors{

private:
	DHT *_dht21_1,*_dht21_2;
	OneWire *_ds;
	SDL_Weather_80422 *_ws;
	float ds_temp;
	float dht1_temp;
	float dht1_hum;
	float dht2_temp;
	float dht2_hum;
	float rain_total;//mm per dakika
	float wind_speed;
	float wind_direction;
	float wind_gust;
	void setDs_temp();
	void set_Ws_Variables();
	void setDHT_Variables();

public:
	WheatherSensors();
	void begin();
	bool update();
	float getDs_temp();
	float getDht1_temp();
	float getDht1_hum();
	float getDht2_temp();
	float getDht2_hum();
	float getRain_total();
	float getWind_gust();
	float getWind_direction(void);
	float getWindSpeed();
	uint8_t getWind_direction2();
	String toString();
	void toString(char *str,int &size);
};



#endif /* _WHEATHER_STATION_H_ */
