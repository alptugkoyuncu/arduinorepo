/*
 * Wheather.cpp
 *
 *  Created on: 17 Tem 2015
 *      Author: WIN7
 */

#include"WheatherSensors.h"
#include<stdlib.h>
WheatherSensors::WheatherSensors() {
//	Serial.println("Wheather sensor creation basladi");
	this->_dht21_1 = new DHT(DHT_1_PIN, DHT_1_TYPE);
	this->_dht21_2 = new DHT(DHT_2_PIN, DHT_2_TYPE);
	this->_ds = new OneWire(DS18S20_Pin);
	this->_ws = new SDL_Weather_80422(pinAnem, pinRain, intAnem, intRain, A0,
	SDL_MODE_INTERNAL_AD);
	;
	this->ds_temp = 0;
	this->dht1_temp = 0;
	this->dht1_hum = 0;
	this->dht2_hum = 0;
	this->dht2_temp = 0;
	this->rain_total = 0;
	this->wind_speed = 0;
	this->wind_direction = 0;
	this->wind_gust = 0;
	this->begin();
}

void WheatherSensors::begin() {
	_dht21_1->begin();
	this->_dht21_2->begin();
//	_ws->setWindMode(SDL_MODE_SAMPLE, 5.0);
	_ws->setWindMode(SDL_MODE_DELAY, 5.0);
	unsigned long waitMinute = 0;
	if (WAIT_TO_LONGEST_INTERVAL) {
		if (UPDATE_INTERVAL_TIME > RAIN_INTERVAL_TIME) {
			if (UPDATE_INTERVAL_TIME > DHT_INTERVAL_TIME)
				waitMinute = UPDATE_INTERVAL_TIME;
			else
				waitMinute = DHT_INTERVAL_TIME;

		} else {
			if (RAIN_INTERVAL_TIME > DHT_INTERVAL_TIME)
				waitMinute = RAIN_INTERVAL_TIME;
			else
				waitMinute = DHT_INTERVAL_TIME;
		}
		delay(waitMinute);
	}
}

void WheatherSensors::setDs_temp() {
	//returns the temperature from one DS18S20 in DEG Celsius

	byte data[12];
	byte addr[8];

	if (!_ds->search(addr)) {
		//no more sensors on chain, reset search
		this->_ds->reset_search();
		this->ds_temp = -1000;
		return;
	}

	if (OneWire::crc8(addr, 7) != addr[7]) {
		Serial.println("CRC is not valid!");
		this->ds_temp = -1000;
		return;
	}

	if (addr[0] != 0x10 && addr[0] != 0x28) {
		Serial.print("Device is not recognized");
		this->ds_temp = -1000;
		return;
	}

	_ds->reset();
	_ds->select(addr);
	_ds->write(0x44, 1); // start conversion, with parasite power on at the end

	byte present = _ds->reset();
	_ds->select(addr);
	_ds->write(0xBE); // Read Scratchpad

	for (int i = 0; i < 9; i++) { // we need 9 bytes
		data[i] = _ds->read();
	}

	_ds->reset_search();

	byte MSB = data[1];
	byte LSB = data[0];

	float tempRead = ((MSB << 8) | LSB); //using two's compliment
	float TemperatureSum = tempRead / 16;

	this->ds_temp = TemperatureSum;

}

unsigned long ws_bas = millis();
bool baslangic = true;
void WheatherSensors::set_Ws_Variables() {
	long now = millis();
	if (now - ws_bas >= RAIN_INTERVAL_TIME) {
		this->rain_total = _ws->get_current_rain_total();
		ws_bas = now;
		baslangic = false;
	} else {
		if (baslangic)
			this->rain_total += _ws->get_current_rain_total();
	}
	this->wind_direction = _ws->current_wind_direction();
	this->wind_speed = _ws->current_wind_speed();
	this->wind_gust = _ws->get_wind_gust();
}

unsigned long dht_bas = millis();

void WheatherSensors::setDHT_Variables() {
	unsigned long now = millis();
	if (now - dht_bas >= DHT_INTERVAL_TIME) {
		this->dht1_temp = this->_dht21_1->readTemperature(false);
		this->dht1_hum = this->_dht21_1->readHumidity();
		this->dht2_temp = this->_dht21_2->readTemperature(false);
		this->dht2_hum = this->_dht21_2->readHumidity();
	}
}

unsigned long update_now = millis();

bool WheatherSensors::update() {
	unsigned long now = millis();
	if (now - update_now < UPDATE_INTERVAL_TIME)
		return false;
	update_now = now;

	// dht21 elemanlarini (dht1_temp ile  dht1_hum verilerini guncelle)
	this->setDHT_Variables();

	// ds_temp verisini guncelle
	this->setDs_temp();

	//rain total wind speed wind direction ve wind gusti guncelle
	this->set_Ws_Variables();
	return true;
}

float WheatherSensors::getDs_temp() {
	return this->ds_temp;
}

float WheatherSensors::getDht1_temp() {
	return this->dht1_temp;
}

float WheatherSensors::getDht1_hum() {
	return this->dht1_hum;
}

float WheatherSensors::getRain_total() {
	return this->rain_total;
}

float WheatherSensors::getWind_gust() {
	return this->wind_gust;
}

float WheatherSensors::getWind_direction() {
	return this->wind_direction;
}

float WheatherSensors::getWindSpeed() {
	return this->wind_speed;
}
uint8_t WheatherSensors::getWind_direction2() {
	return KUZEY;
}
String WheatherSensors::toString() {
	String tmp = "";
	char *charVal = new char[10];

	dtostrf(this->getDs_temp(), 4, 2, charVal);
	tmp += String(charVal) + " ";
	free(charVal);

	charVal = new char[10];
	dtostrf(this->getDht1_temp(), 4, 2, charVal);
	tmp += String(charVal) + " ";
	free(charVal);

	charVal = new char[10];
	dtostrf(this->getDht2_temp(), 4, 2, charVal);
	tmp += String(charVal) + " ";
	free(charVal);

	charVal = new char[10];
	dtostrf(this->getDht1_hum(), 4, 2, charVal);
	tmp += String(charVal) + " ";
	free(charVal);

	charVal = new char[10];
	dtostrf(this->getDht2_hum(), 4, 2, charVal);
	tmp += String(charVal) + " ";
	free(charVal);

	charVal = new char[10];
	dtostrf(this->getRain_total(), 5, 2, charVal);
	tmp += String(charVal) + " ";
	free(charVal);

	charVal = new char[10];
	dtostrf(this->getWindSpeed(), 5, 2, charVal);
	tmp += String(charVal) + " ";
	free(charVal);

	charVal = new char[10];
	dtostrf(this->getWind_direction(), 5, 2, charVal);
	tmp += String(charVal) + " ";
	free(charVal);

	charVal = new char[10];
	dtostrf(this->getWind_gust(), 5, 2, charVal);
	tmp += String(charVal);
	free(charVal);
	charVal = NULL;
	return tmp;
}

float WheatherSensors::getDht2_temp() {
	return this->dht2_temp;
}

float WheatherSensors::getDht2_hum() {
	return this->dht2_hum;
}

void WheatherSensors::toString(char* str, int& size) {
	size = sprintf(str, "%.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f",
			this->getDs_temp(), this->getDht1_temp(), this->getDht2_temp(),
			this->getDht1_hum(), this->getDht2_hum(), this->getRain_total(),
			this->getWindSpeed(), this->getWind_direction(),
			this->getWind_gust());
}
