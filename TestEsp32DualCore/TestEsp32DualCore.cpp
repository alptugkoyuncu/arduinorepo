/*********
 Rui Santos
 Complete project details at http://randomnerdtutorials.com


 Note: in the code we create two tasks and assign one task to core 0
 and another to core 1. Arduino sketches run on core 1 by default.
 So, you could write the code for Task2 in the loop()
 (there was no need to create another task).
 In this case we create two different tasks for learning purposes.

 However, depending on your project requirements,
 it may be more practical to organize your code in tasks as
 demonstrated in this example.

 *********/
#include <Arduino.h>
TaskHandle_t Task1;
TaskHandle_t Task2;



/**
 * The for(;;) creates an infinite loop.
 * So, this function runs similarly to the loop() function.
 *  You can use it as a second loop in your code, for example.

If during your code execution you want to delete the created task,
you can use the vTaskDelete()function,
that accepts the task handle (Task1) as argument:
vTaskDelete(Task1);
 */
bool lock=false;
bool valueUpdated=false;
int value=0;
void Task1code(void *pvParameters) {

	Serial.print("Task1 running on core ");
		Serial.println(xPortGetCoreID());
	for (;;) {
		while(lock){
				delay(1);
			}
			lock=true;
			value++;
			lock=false;
			valueUpdated=true;
		delay(100);
	}
}

//Task2code: blinks an LED every 700 ms
void Task2code(void *pvParameters) {

	Serial.print("Task2 running on core ");
		Serial.println(xPortGetCoreID());
	for (;;) {
		while(lock){
			delay(1);
		}
		lock=true;
		if(valueUpdated){
			Serial.println("Value Updated value:");
			Serial.println(value);
			valueUpdated=false;
		}
		lock=false;
		delay(7);
	}
}
void setup() {
	Serial.begin(115200);

	//create a task that will be executed in the Task1code() function, with priority 1 and executed on core 0
	xTaskCreatePinnedToCore(Task1code, /* Task function. */
	"Task1", /* name of task. */
	10000, /* Stack size of task */
	NULL, /* parameter of the task */
	1, /* priority of the task */
	&Task1, /* Task handle to keep track of created task */
	0); /* pin task to core 0 */
	delay(500);

	//create a task that will be executed in the Task2code() function, with priority 1 and executed on core 1
	xTaskCreatePinnedToCore(Task2code, /* Task function. */
	"Task2", /* name of task. */
	10000, /* Stack size of task */
	NULL, /* parameter of the task */
	1, /* priority of the task */
	&Task2, /* Task handle to keep track of created task */
	1); /* pin task to core 1 */
	delay(500);
}

//Task1code: blinks an LED every 1000 ms

void loop() {

}
