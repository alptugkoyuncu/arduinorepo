#include <Arduino.h>
#include <SPI.h>

//SPI DATA TRANSEFER BETWEEN TWO MICROCONTROLER READ AND WRITE
// UNO AS MASTER
#define ACTION_SENT '1'
#define ACTION_RECEIVE '2'
byte requestData = 1;
byte sentCode = 2;
byte finishData = '\r';
#define INT_PIN 2
#define SLAVE_RESET 5
void setup() {
	Serial.begin(115200);
	Serial.println("Begin");
	digitalWrite(SS, HIGH); // disable Slave Select
	pinMode(SLAVE_RESET, OUTPUT);
	pinMode(INT_PIN, INPUT_PULLUP);
	digitalWrite(SLAVE_RESET, LOW);
	delay(500);
	digitalWrite(SLAVE_RESET, HIGH);
	delay(500);
	SPI.begin();
	SPI.setClockDivider(1000000); //divide the clock by 16
//	delay(5000);
}
char r;
void loop() {
	Serial.println("ne is yapacan \n\t1-sent\n\t2-receive");
	while (!Serial.available())
		delay(5);
	if (Serial.available()) {
		byte b = Serial.read();

		if (b == ACTION_RECEIVE) {
			Serial.print("Received Data ");
			char rec[100];
			int rind = 0;
			digitalWrite(SS, LOW);
			delay(1);
			char c;
//			while(digitalRead(INT_PIN));
			while (true) {
				c = SPI.transfer(requestData);
				delayMicroseconds(10);
				rec[rind++] = c;
				if (c == finishData) {
					rec[rind] = 0x0;
					break;
				} else
					rec[rind++] = c;
			}


			digitalWrite(SS, HIGH); // disable Slave Select
			delay(1);
			Serial.print(" len=");
			Serial.print(rind);
			Serial.print(" Data :");
			Serial.print(rec);
			Serial.println();
			delay(10);
		} else if (b == ACTION_SENT) {
			char read[100];
			int index = 0;
			while (Serial.available()) {
				read[index++] = (char) Serial.read();
			}
			read[index] = NULL;
			if (index > 0) {
				Serial.print("Ginderilecek data : ");
				Serial.println(read);
				delay(10);
				digitalWrite(SS, LOW); // enable Slave Select
				delay(1);
				for (int i = 0; i < index; i++) {
					SPI.transfer(read[i]);
					delayMicroseconds(10);
				}
				SPI.transfer(finishData);
				delayMicroseconds(10);
				digitalWrite(SS, HIGH);
				delay(1);
			}
		} else {
			Serial.println("Yanlis secenek");
		}

//   for (const char * p = "Hello Mega I am Uno\r" ; c = *p; p++)
//   {
//      SPI.transfer(c);
//   }
	}
	delay(50);
}
