/*
 * ArduinoPackage.h
 *
 *  Created on: 14 Ara 2020
 *      Author: ISIK
 */

//network package esp8266 da resetlenmeye neden oldugu icin daha basiti yapiliyor
#ifndef ARDUINOPACKAGE_H_
#define ARDUINOPACKAGE_H_

#include"NetworkPackage/NetworkPackage.h"
typedef struct ArduinoPackage{
	uint16_t targetId;
	uint16_t actionCode;
	char data[32];
	uint8_t dataSize;
	void parseMessage(char *message,uint8_t messageLength){
		const uint8_t posTargetId=0;
		const uint8_t posActionCode=1;
		const uint8_t posData=2;
		uint16_t nextPosition;
		for(uint8_t i=0;i<messageLength;i++){
			yield();
			char c = message[i];
			if(!isprint((byte)c))
				continue;
			if(c==BASLANGIC_AYIRACI){
				nextPosition=posTargetId;
				continue;
			}
			if(nextPosition==posTargetId){
				if(c==ANA_DATA_AYIRACI){
					data[dataSize]=0x0;
					targetId=atoi(data);
					dataSize=0;
					nextPosition=posActionCode;
					continue;
				}else{
					data[dataSize++]=c;
				}
			}
			if(nextPosition==posActionCode){
				if(c==ANA_DATA_AYIRACI){
					data[dataSize]=0x0;
					actionCode=atoi(data);
					dataSize=0;
					nextPosition=posData;
					continue;
				}else{
					data[dataSize++]=c;
				}
			}
			if(nextPosition==posData){
				if(c==BITIS_AYIRACI){
					data[dataSize]=0x0;
					break;
				}else{
					data[dataSize++]=c;
				}
			}
		}
	}
};


#endif /* ARDUINOPACKAGE_H_ */
