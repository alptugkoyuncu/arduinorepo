/**TODO:
 * esp basitlestirildi data alma ve gondermelerde paket kullanilmayacak okudugu bayti direk gonderecek
 * wifi password port ve ip bilgilerini setup kisminda arduino dan alacak burada paket kullanilacak
 *arduino id yani client id artik arduino uzerinden gonderilecek
 * loop da sadece data alma ve gonderme olacak onda da paket kullanilmayacak
 * resetleme islemi hem arduino hem de esp tarafindan kontrol edilecek
 */

#include"ESP8266_Sera2020.h"

//INIT
char *ssid = NULL;
char *password = NULL;
IPAddress *ip;
uint16_t port = 0;

#define RS_NO_PROBLEM 0
#define RS_WIFI_CONN_OK 11
#define RS_TCP_CONN_OK 12
#define RS_WIFI_PROBLEM 21
#define RS_TCP_CONN_PROBLEM 22
#define RS_HEADER_PROBLEM 23
#define RS_DATA_NOT_COMPLETE 5
#define RS_SENT_PROBLEM 6

// servera oda numarasını gonderır basarılı olursa loopa gırer
// basarısız durumda reset atar
#define MAX_FAIL 3
#define NO_TCP_TRY 10
#define NO_WIFI_TRY 300
unsigned long lastReceiveArduino, lastReceiveServer;
const unsigned long MAX_WAIT_MS = 200000; //200 sn icinde data gelmezse resetler

//#define MAC_SERVER
//#define UNIX_SERVER

WiFiClient client;
uint8_t ERR_CODE;
void emptySerialData() {
	while (Serial.available() > 0)
		Serial.read();
}
void emptyClientData() {
	while (client.available() > 0)
		client.read();
}
////////////////////////// SETUP //////////////////////////////////////////////////////

void setup() {
	Serial.begin(38400);  //Start Serial
	Serial.flush();
	sentEspPosition(AC_ESP_STARTED);
	yield();
	delay(100);

	uint8_t fails = 0;
	const unsigned long PARAMETER_WAIT_MS = 180000;
	bool wifiConnectionEstablished = false;
	bool serverConnectionEstablished = false;

	unsigned long start = millis();
	while ((millis() - start < PARAMETER_WAIT_MS)
			&& !serverConnectionEstablished) {
		delay(5);
		yield();
		if (ssid == NULL) {
			ssid = getParameter(AC_SEND_WIFI_SSID);
			d_2println("WIFI = ", ssid);
			continue;
		}
		if (password == NULL) {
			password = getParameter(AC_SEND_PASS);
			d_2println("Pass =", password);
			continue;
		}
		// Start WiFi
		if (!wifiConnectionEstablished) {
			fails = 0;
			connectWifi();
			delay(50);
			yield();
			if (ERR_CODE != RS_NO_PROBLEM) {
				sentEspPosition(AC_WIFI_CONNECTION_FAILED);
				wifiConnectionEstablished = false;
				serverConnectionEstablished = false;
				if (ssid != NULL) {
					free(ssid);
					ssid = NULL;
				}
				if (password != NULL) {
					free(password);
					password = NULL;
				}

			} else {
				sentEspPosition(AC_WIFI_CONNECTED);
				wifiConnectionEstablished = true;
				start = millis();
			}
			continue;
		}
		if (ip == NULL) {
			char *strIp = getParameter(AC_SEND_IP);
			if (strIp == NULL) {
				d_println("StrIp NULL");
				if (ip != NULL)
					delete ip;
			} else {
				d_2println("strIp =", strIp);

				ip = new IPAddress();
				ip->fromString(strIp);
				d_2println("IP =", *ip);
				free(strIp);
				strIp = NULL;
			}
			continue;
		}
		if (port == 0) {
			char *strPort = getParameter(AC_SEND_PORT);
			if (strPort == NULL) {
				d_println("strPort NULL");
				port = 0;
			} else {
				d_2println("strPort ", strPort);
				port = atoi(strPort);
				free(strPort);
				strPort = NULL;
				d_2println("Port =", port);
			}
			continue;
		}

		//connect TCP SERVER
		if (!serverConnectionEstablished) {
			fails = 0;

			connectServer();
			delay(50);
			yield();

			if (ERR_CODE != RS_NO_PROBLEM) {
				sentEspPosition(AC_SERVER_CONNECTION_FAILED);
				serverConnectionEstablished = false;
				if (ip != NULL) {
					delete ip;
					ip = NULL;
				}
				port = 0;

			} else {
				serverConnectionEstablished = true;
				sentEspPosition(AC_SERVER_CONNECTED);
			}
		}
	}
	delay(5);
	yield();
	if (!serverConnectionEstablished) {
		resetBoard();
	}

	emptyClientData();
	emptySerialData();

	lastReceiveArduino = millis();
	lastReceiveServer = lastReceiveArduino;

	d_println("Setup Done!");
}

////////////////////////// LOOP //////////////////////////////////////////////////////
//byte loopData[100] = { 0 };
//uint8_t loopDataSize = 0;
//bool fastLoop=true;
void loop() {
	//fastloop

	unsigned long now = millis();
	if (Serial.available() > 0) {
		client.write(Serial.read());
		lastReceiveArduino = now;
	}
	delay(5);
	yield();
	if (client.available() > 0) {
		Serial.write(client.read());
		lastReceiveServer = now;
	}
	delay(5);
	yield();
	now = millis();
	if ((now - lastReceiveArduino) > MAX_WAIT_MS
			|| (now - lastReceiveServer) > MAX_WAIT_MS) {
		d_println("Timeout!");
		resetBoard();
	}

	/*
	 loopDataSize = 0;
	 while (Serial.available() && loopDataSize < 100)
	 loopData[loopDataSize++] = Serial.read();
	 if (loopDataSize != 0) {
	 client.write(loopData, loopDataSize);
	 client.flush();
	 lastReceiveArduino = millis();
	 loopDataSize = 0;
	 }
	 yield();
	 delay(5);
	 while (client.available() && loopDataSize < 100) {
	 loopData[loopDataSize++] = client.read();
	 }
	 if (loopDataSize != 0) {
	 Serial.write(loopData, loopDataSize);
	 Serial.flush();
	 lastReceiveServer = millis();
	 loopDataSize = 0;
	 }
	 yield();
	 delay(5);


	 */
}

uint8_t connectServer() {
	uint8_t flag;
	int count = 0;

	client.stopAll();
	while (count < NO_TCP_TRY) {
		d_println("Tcp Connection try to connect...");
		if (client.connect(*ip, port) > 0) {
			d_println("Tcp connection Established");
			return ERR_CODE = RS_NO_PROBLEM;
		}
		yield();
		delay(100);
		count++;
	}
	d_println("Tcp connection Failed");
	return ERR_CODE = RS_TCP_CONN_PROBLEM;
}

// Fucntion to connect WiFi
uint8_t connectWifi() {
	int WiFiCounter = 0;
// We start by connecting to a WiFi network
	d_2println("Wifi connection try to connect ", ssid);
	WiFi.disconnect();
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	do {
		yield();
		delay(100);
		WiFiCounter++;
		if (WiFiCounter >= NO_WIFI_TRY) {
			d_println("Can not connect to the wifi");
			return ERR_CODE = RS_WIFI_PROBLEM;
		}
	} while (WiFi.status() != WL_CONNECTED);
	d_2println(" Wifi Connected IP address  ", WiFi.localIP());
	return ERR_CODE = RS_NO_PROBLEM;
}
// Inifinite loop - Causes to reset self
void resetBoard() {

	char buffer[20];
	sprintf(buffer, "%c%d%c%d%c%c", BASLANGIC_AYIRACI, SERVER_ID,
	ANA_DATA_AYIRACI,
	AC_CLIENT_DISCONNECTED, ANA_DATA_AYIRACI, BITIS_AYIRACI);
	client.println(buffer);
	client.flush();
	delay(100);
	sentEspPosition(AC_ESP_RESET);
	delay(100);
	yield();
	client.stopAll();
	WiFi.disconnect();
	d_println("Reseting");
//  ESP.restart();
	delay(100);
	do {
	} while (1);
}

void sentEspPosition(uint16_t actionCode) {
	char buffer[20];
	sprintf(buffer, "%c%d%c%d%c%c", BASLANGIC_AYIRACI, ESP_ID, ANA_DATA_AYIRACI,
			actionCode, ANA_DATA_AYIRACI, BITIS_AYIRACI);

	Serial.print(buffer);
	Serial.flush();

}

char* getParameter(uint16_t actionCode) {
	char sendBuffer[15];
	sprintf(sendBuffer, "%c%d%c%d%c%c", BASLANGIC_AYIRACI, ESP_ID,
	ANA_DATA_AYIRACI, actionCode, ANA_DATA_AYIRACI, BITIS_AYIRACI);

	ArduinoPackage pck;

	unsigned long waitms = 3000;
	unsigned long cur = millis();
	int8_t no_try = 10;
	bool error = false;
	uint8_t anaDataAyiraci = 0;
	char buffer[100];
	uint8_t bufferIndex = 0;
	bool messageOk = false;
	while (no_try > 0 && !messageOk) {
		if ((millis() - cur > waitms) || error) {
			no_try--;
			Serial.print(sendBuffer);
			Serial.flush();
			cur = millis();
			error = false;
			bufferIndex = 0;
		}
		while (Serial.available() > 0) {
			char c = (char) Serial.read();
			if (c == BASLANGIC_AYIRACI) {
				buffer[bufferIndex++] = c;
				continue;
			}
			//data baslamis demektir
			if (bufferIndex != 0) {
				buffer[bufferIndex++] = c;
			}
			// data bitmis demektir
			if (c == BITIS_AYIRACI) {
				yield();
				while (Serial.available() > 0)
					Serial.read();
				pck.parseMessage(buffer, bufferIndex);
				if (pck.actionCode != actionCode || pck.dataSize == 0) {
					error = true;
				} else {
					messageOk = true;
					break;
				}
			}

		}
		yield();
		delay(5);
	}
	if (!messageOk)
		return 0x0;
	char *msg = (char*) malloc(pck.dataSize + 1);
	strcpy(msg, pck.data);
	return msg;
}

/*
 char* getParameter(uint16_t actionCode) {

 char buffer[32];
 uint8_t bufferSize = 0;


 unsigned long waitms = 1000;

 pck.changeMessage(ESP_ID, actionCode, 0x0, 0);

 //	char *requestMessage;
 //	requestMessage = (char*) malloc(sizeof(pck.getMessageLength()) + 1);
 //	memcpy(requestMessage, pck.getMessage(), pck.getMessageLength());
 //	requestMessage[pck.getMessageLength()] = 0x0;
 //	d_println(requestMessage);
 //	Serial.println(requestMessage);
 //	d_println(pck.getMessage());
 Serial.println(pck.getMessage());


 //	d_print("Free memory : ")
 //	d_println(ESP.getFreeHeap());
 delay(2000);
 unsigned long cur = millis();
 int8_t no_try = 10;
 bool error = false;
 pck.reset();
 while (no_try >= 0) {
 if ((millis() - cur > waitms) || error) {
 no_try--;
 pck.changeMessage(ESP_ID, actionCode, 0x0, 0);
 Serial.println(pck.getMessage());
 cur = millis();
 error = false;
 pck.reset();
 }
 if (Serial.available() > 0) {
 char c = (char) Serial.read();
 pck.appendMessage(c);
 }
 if (pck.isDataCompleted()) {
 d_println("Package completed Pck: ");
 d_println(pck.getMessage());
 if (actionCode != pck.getActionCode()) {
 error = true;
 } else if (pck.getDataMiktari() == 0) {
 error = true;
 } else {
 bufferSize = pck.getDataLine(buffer);
 if (bufferSize == 0 || buffer == 0x0)
 error = true;
 else{
 pck.reset();
 break;
 }
 }
 pck.reset();
 }
 delay(5);
 }
 //	free(requestMessage);
 if (bufferSize == 0 || buffer == 0x0)
 return 0x0;
 char *data;
 data = (char*) malloc(sizeof(bufferSize + 1));
 memcpy(data, buffer, bufferSize);
 *(data+bufferSize) = 0x0;
 return data;
 }
 */
