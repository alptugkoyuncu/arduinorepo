/*
 * ESP8266_Sera2020.h
 *
 *  Created on: 13 Ara 2020
 *      Author: ISIK
 */

#ifndef ESP8266_SERA2020_H_
#define ESP8266_SERA2020_H_


#include<ActionCode.h>
#include<MyDebug.h>
#include"ArduinoPackage.h"
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include<WifiClient.h>
#include"GeneralSystemConstants.h"

void sentEspPosition(uint16_t actionCode);
void resetBoard();
uint8_t connectServer();
uint8_t connectWifi();
uint8_t sentHeader();
char * getParameter(uint16_t actionCode);



#endif /* ESP8266_SERA2020_H_ */
