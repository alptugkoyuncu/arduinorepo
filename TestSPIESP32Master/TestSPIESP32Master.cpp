#include <Arduino.h>
#include<SPI.h>
char buff[] = "I am ESP32 Hello Slave\r";

SPIClass vspi(VSPI);
void setup() {
	pinMode(SS, OUTPUT);
	vspi.begin();
	vspi.setClockDivider(1000000);
	digitalWrite(SS, HIGH);
}

void loop() {
	digitalWrite(SS, LOW);
	for (int i = 0; i < sizeof buff; i++) {
		vspi.transfer(buff[i]);
	}
	digitalWrite(SS, HIGH);
	delay(2000);
}
